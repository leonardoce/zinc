///////////////////////////////////////////////////////////////////////////////
// file-buffer
///////////////////////////////////////////////////////////////////////////////

import "base/types"
import "base/memory-allocator"
import "libc/io"

public enum fb error

	fb ok
	fb file not found
	fb seek error
	fb not enough memory

end

public struct file buffer

	base: [] byte
	size: size

end

//-----------------------------------------------------------------------------
// Initialize
//-----------------------------------------------------------------------------
public func initialize (buffer: file buffer, filename: string, is text: bool)

	base (buffer) = nil

	// Get file size
	def file size = file_size (filename)
	if file size == 0xFFFFFFFF
		return fb seek error
	end
	def buffer size = file size
	if is text
		++buffer size
	end
	
	// open the file
	def f = fopen (filename, "rb")
	if is nil (f)
		return fb file not found
	end

	// allocate memory
	def base = allocate memory (buffer size):[] byte
	if is nil (base) 
		return fb not enough memory
	end
		
	// read the file
	def res = fread (base, 1, file size, f)

	// close the file
	fclose (f)

	// append a zero if text
	if is text
		base[file size] = 0:byte
	end
	
	// setup object status and return
	base (buffer) = base
	size (buffer) = file size
	return fb ok

end

//-----------------------------------------------------------------------------
// Destructor
//-----------------------------------------------------------------------------
public func release (buffer: file buffer)

	if not nil (base (buffer))
		free memory (base (buffer))
	end

end
