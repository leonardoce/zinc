// For portability issues, these functions are still written in C

#include <stdio.h>
#include <errno.h>
#include <sys/stat.h>

/*
 * Tests if a file exists
 */
int file_exists(char* filename)
{
	struct stat st;
	int err = stat(filename, &st);
	return err==0 || errno != ENOENT;
}

/*
 * Returns the file size
 */
size_t file_size(char* filename)
{
	struct stat st;
	int err = stat(filename, &st);
	if( err!=0 )
		return 0xFFFFFFFF;
	else
		return st.st_size;
}

/*
 * Put a string to stderr
 */
void fputs_stderr(char* s)
{
	fputs(s, stderr);
}

/*
 * Returns the path separator
 */
char path_separator ()
{
#ifdef _WINDOWS
  return '\\';
#else
  return '/';
#endif
}

/*
 * Convert a filename with '/' to a platform compatible filename
 */
void convert_full_name (char* filename)
{
#ifdef _WINDOWS
  char* p = filename;
  char c;
  while (1)
  {
    c = *p;
    if (c==0) break;
    if (c=='/') *p = '\\';
    ++p;
  }
#endif
}
