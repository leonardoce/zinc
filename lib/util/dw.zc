import "base/types"

/// Dynamic Windows Interface
/// -------------------------

/// This is an interface for the Dynamic Windows Library (see
/// http://dwindows.netlabs.org/) that is a lightweight cross-platform
/// gui toolkit. This toolkit can be deployed, in windows, as a single
/// DLL file (dw.dll)

public equ dw_signal_configure	       =        "configure_event"
public equ dw_signal_key_press	       =	"key_press_event"
public equ dw_signal_button_press      =	"button_press_event"
public equ dw_signal_button_release    =	"button_release_event"
public equ dw_signal_motion_notify     =	"motion_notify_event"
public equ dw_signal_delete	       =	"delete_event"
public equ dw_signal_expose	       =	"expose_event"
public equ dw_signal_clicked	       =	"clicked"
public equ dw_signal_item_enter	       =	"container-select"
public equ dw_signal_item_context      =	"container-context"
public equ dw_signal_item_select       =	"tree-select"
public equ dw_signal_list_select       =	"item-select"
public equ dw_signal_set_focus	       =	"set-focus"
public equ dw_signal_value_changed     =	"value_changed"
public equ dw_signal_switch_page       =	"switch-page"
public equ dw_signal_column_click      =	"click-column"
public equ dw_signal_tree_expand       =	"tree-expand"
	
public typedef HWND = mem	
	
	// (replace-regexp "int .*?," "int,")
// (replace-string "unsigned int" "dword")

public [name="c"]
	import func dw_box_pack_start(HWND, HWND, int, int, int, int, int): void  
	import func dw_box_pack_end(HWND, HWND, int, int, int, int, int): void  
	import func dw_box_pack_at_index(HWND, HWND, int, int, int, int, int, int): void  
	import func dw_init(int, int,  [] string): int  
	import func dw_main: void  
	import func dw_main_sleep(int): void  
	import func dw_main_iteration(void): void  
	import func dw_free(mem): void  
	import func dw_window_show(HWND): int  
	import func dw_window_hide(HWND): int  
	import func dw_window_minimize(HWND): int  
	import func dw_window_raise(HWND): int  
	import func dw_window_lower(HWND): int  
	import func dw_window_destroy(HWND): int  
	import func dw_window_redraw(HWND): void  
	import func dw_window_set_font(HWND,  string fontname): int  
	import func dw_window_get_font(HWND):  string   
	import func dw_window_set_color(HWND, dword, dword): int  
	import func dw_window_new(HWND hwndOwner,  string title, dword): HWND  
	import func dw_box_new(int, int): HWND  
	import func dw_scrollbox_new(int, int): HWND  
	import func dw_scrollbox_get_pos( HWND, int orient ): int  
	import func dw_scrollbox_get_range( HWND, int orient ): int  
	import func dw_groupbox_new(int, int,  string title): HWND  
	import func dw_mdi_new(dword): HWND  
	import func dw_bitmap_new(dword): HWND  
	import func dw_bitmapbutton_new( string text, dword): HWND  
	import func dw_bitmapbutton_new_from_file( string text, dword,  string filename): HWND  
	import func dw_bitmapbutton_new_from_data( string text, dword,  string str, int len): HWND  
	import func dw_container_new(dword, int multi): HWND  
	import func dw_tree_new(dword): HWND  
	import func dw_text_new( string text, dword): HWND  
	import func dw_status_text_new( string text, dword): HWND  
	import func dw_mle_new(dword): HWND  
	import func dw_entryfield_new( string text, dword): HWND  
	import func dw_entryfield_password_new( string text, ULONG id): HWND  
	import func dw_combobox_new( string text, dword): HWND  
	import func dw_button_new( string text, dword): HWND  
	import func dw_spinbutton_new( string text, dword): HWND  
	import func dw_radiobutton_new( string text, ULONG id): HWND  
	import func dw_percent_new(dword): HWND  
	import func dw_slider_new(int, int, ULONG id): HWND  
	import func dw_scrollbar_new(int, ULONG id): HWND  
	import func dw_checkbox_new( string text, dword): HWND  
	import func dw_listbox_new(dword, int multi): HWND  
	import func dw_listbox_append(HWND,  string text): void  
	import func dw_listbox_insert(HWND,  string text, int pos): void  
	import func dw_listbox_list_append(HWND,  [] string, int count): void  
	import func dw_listbox_clear(HWND): void  
	import func dw_listbox_count(HWND): int  
	import func dw_listbox_set_top(HWND, int top): void  
	import func dw_listbox_select(HWND, int, int state): void  
	import func dw_listbox_delete(HWND, int): void  
	import func dw_listbox_get_text(HWND, dword,  string buffer, dword length): void  
	import func dw_listbox_set_text(HWND, dword,  string buffer): void  
	import func dw_listbox_selected(HWND): int  
	import func dw_listbox_selected_multi(HWND, int where): int  
	import func dw_percent_set_pos(HWND, dword position): void  
	import func int  dw_slider_get_pos(HWND): unsigned 
	import func dw_slider_set_pos(HWND, dword position): void  
	import func int  dw_scrollbar_get_pos(HWND): unsigned 
	import func dw_scrollbar_set_pos(HWND, dword position): void  
	import func dw_scrollbar_set_range(HWND, dword, dword visible): void  
	import func dw_window_set_pos(HWND, long x, long y): void  
	import func dw_window_set_size(HWND, dword, dword): void  
	import func dw_window_set_pos_size(HWND, long x, long y, dword, dword): void  
	import func dw_window_get_pos_size(HWND, -> int, -> int, dword, dword): void  
	import func dw_window_set_style(HWND, dword, dword): void  
	import func dw_window_set_icon(HWND, HICN icon): void  
	import func dw_window_set_bitmap(HWND, dword,  string filename): void  
	import func dw_window_set_bitmap_from_data(HWND, dword,  string data, int len): void  
	import func dw_window_get_text(HWND):  string   
	import func dw_window_set_text(HWND,  string text): void  
	import func dw_window_set_border(HWND, int border): int  
	import func dw_window_disable(HWND): void  
	import func dw_window_enable(HWND): void  
	import func dw_window_capture(HWND): void  
	import func dw_window_release(void): void  
	import func dw_window_reparent(HWND, HWND newparent): void  
	import func dw_window_set_pointer(HWND, int pointertype): void  
	import func dw_window_default(HWND, HWND defaultitem): void  
	import func dw_window_click_default(HWND, HWND next): void  
	import func dw_mle_import(HWND, string, int):dword
	import func dw_mle_export(HWND,  string buffer, int, int length): void  
	import func dw_mle_get_size(HWND, dword, dword): void  
	import func dw_mle_delete(HWND, int, int length): void  
	import func dw_mle_clear(HWND): void  
	import func dw_mle_freeze(HWND): void  
	import func dw_mle_thaw(HWND): void  
	import func dw_mle_set_cursor(HWND, int point): void  
	import func dw_mle_set_visible(HWND, int line): void  
	import func dw_mle_set_editable(HWND, int state): void  
	import func dw_mle_set_word_wrap(HWND, int state): void  
	import func dw_mle_search(HWND,  string text, int, dword): int  
	import func dw_spinbutton_set_pos(HWND, long position): void  
	import func dw_spinbutton_set_limits(HWND, long upper, long lower): void  
	import func dw_entryfield_set_limit(HWND, ULONG limit): void  
	import func dw_spinbutton_get_pos(HWND): long  
	import func dw_checkbox_get(HWND): int  
	import func dw_checkbox_set(HWND, int value): void  
	import func dw_tree_insert(HWND,  string title, HICN icon, HTREEITEM parent, memitemdata): HTREEITEM  
	import func dw_tree_insert_after(HWND, HTREEITEM item,  string title, HICN icon, HTREEITEM parent, memitemdata): HTREEITEM  
	import func dw_tree_clear(HWND): void  
	import func dw_tree_item_delete(HWND, HTREEITEM item): void  
	import func dw_tree_item_change(HWND, HTREEITEM item,  string title, HICN icon): void  
	import func dw_tree_item_expand(HWND, HTREEITEM item): void  
	import func dw_tree_item_collapse(HWND, HTREEITEM item): void  
	import func dw_tree_item_select(HWND, HTREEITEM item): void  
	import func dw_tree_item_set_data(HWND, HTREEITEM item, memitemdata): void  
	import func dw_tree_item_get_data(HWND, HTREEITEM item): mem  
	import func dw_tree_get_title(HWND, HTREEITEM item):  string   
	import func dw_tree_get_parent(HWND, HTREEITEM item): HTREEITEM  
	import func dw_container_setup(HWND, dword, [] string, int, int): int  
	import func dw_icon_load(dword, dword): HICN  
	import func dw_icon_load_from_file( string filename): HICN  
	import func dw_icon_load_from_data( string data, int len): HICN  
	import func dw_icon_free(HICN handle): void  
	import func dw_container_alloc(HWND, int rowcount): mem  
	import func dw_container_set_item(HWND, mempointer, int, int, memdata): void  
	import func dw_container_change_item(HWND, int, int, memdata): void  
	import func dw_container_set_column_width(HWND, int, int): void  
	import func dw_container_set_row_title(mempointer, int,  string title): void  
	import func dw_container_change_row_title(HWND, int,  string title): void  
	import func dw_container_insert(HWND, mempointer, int rowcount): void  
	import func dw_container_clear(HWND, int redraw): void  
	import func dw_container_delete(HWND, int rowcount): void  
	import func dw_container_query_start(HWND, dword):  string   
	import func dw_container_query_next(HWND, dword):  string   
	import func dw_container_scroll(HWND, int, long rows): void  
	import func dw_container_cursor(HWND,  string text): void  
	import func dw_container_delete_row(HWND,  string text): void  
	import func dw_container_optimize(HWND): void  
	import func dw_container_set_stripe(HWND, dword, dword): void  
	import func dw_filesystem_setup(HWND, dword,  [] string, int ): int  
	import func dw_filesystem_set_item(HWND, mempointer, int, int, memdata): void  
	import func dw_filesystem_set_file(HWND, mempointer, int,  string filename, HICN icon): void  
	import func dw_filesystem_change_item(HWND, int, int, memdata): void  
	import func dw_filesystem_change_file(HWND, int,  string filename, HICN icon): void  
	import func dw_container_get_column_type(HWND, int column): int  
	import func dw_filesystem_get_column_type(HWND, int column): int  
	import func dw_taskbar_insert(HWND, HICN icon,  string bubbletext): void  
	import func dw_taskbar_delete(HWND, HICN icon): void  
	import func dw_screen_width(void): int  
	import func dw_screen_height(void): int  
	import func long  dw_color_depth_get(void): unsigned 
	import func dw_notebook_new(dword, int top): HWND  
	import func long  dw_notebook_page_new(HWND, dword, int front): unsigned 
	import func dw_notebook_page_destroy(HWND, dword pageid): void  
	import func dw_notebook_page_set_text(HWND, dword,  string text): void  
	import func dw_notebook_page_set_status_text(HWND, dword,  string text): void  
	import func dw_notebook_page_set(HWND, dword pageid): void  
	import func long  dw_notebook_page_get(HWND): unsigned 
	import func dw_notebook_pack(HWND, dword, HWND page): void  
	import func dw_splitbar_new(int, HWND topleft, HWND bottomright, dword): HWND  
	import func dw_splitbar_set(HWND, float percent): void  
	import func dw_splitbar_get(HWND): float  
	import func dw_menu_new(dword): HMENUI  
	import func dw_menubar_new(HWND location): HMENUI  
	import func dw_menu_append_item(HMENUI menu,  string title, dword, dword, int, int, HMENUI submenu): HWND  
	import func #ifdef INCOMPLETE
	import func void  dw_menu_delete_item(HMENUI menu, dword);
	import func #endif
	import func dw_menu_item_set_check(HMENUI menu, dword, int check): void  
	import func dw_menu_item_set_state( HMENUI menux, dword, dword): void  
	import func dw_menu_popup(HMENUI *menu, HWND parent, int, int y): void  
	import func dw_menu_destroy(HMENUI *menu): void  
	import func dw_pointer_query_pos(long *x, long *y): void  
	import func dw_pointer_set_pos(long x, long y): void  
	import func dw_window_function(HWND, memfunction, memdata): void  
	import func dw_window_from_id(HWND, int id): HWND  
	import func dw_mutex_new(void): HMTX  
	import func dw_mutex_close(HMTX mutex): void  
	import func dw_mutex_lock(HMTX mutex): void  
	import func dw_mutex_trylock(HMTX mutex): int  
	import func dw_mutex_unlock(HMTX mutex): void  
	import func dw_event_new(void): HEV  
	import func dw_event_reset(HEV eve): int  
	import func dw_event_post(HEV eve): int  
	import func dw_event_wait(HEV eve, dword): int  
	import func dw_event_close (HEV *eve): int  
	import func dw_thread_new(memfunc, memdata, int stack): DWTID  
	import func dw_thread_end(void): void  
	import func dw_thread_id(void): DWTID  
	import func dw_exit(int exitcode): void  
	import func dw_render_new(dword): HWND  
	import func dw_color_foreground_set(dword): void  
	import func dw_color_background_set(dword): void  
	import func long  dw_color_choose(dword): unsigned 
	import func dw_font_choose( string currfont):  string   
	import func dw_draw_point(HWND, HPIXMAP pixmap, int, int y): void  
	import func dw_draw_line(HWND, HPIXMAP pixmap, int, int, int, int y2): void  
	import func dw_draw_rect(HWND, HPIXMAP pixmap, int, int, int, int, int): void  
	import func dw_draw_polygon(HWND, HPIXMAP pixmap, int, int, int, int *y): void  
	import func dw_draw_arc(HWND, HPIXMAP pixmap, int, int, int, int, int, int, int y2): void  
	import func dw_draw_text(HWND, HPIXMAP pixmap, int, int,  string text): void  
	import func dw_font_text_extents_get(HWND, HPIXMAP pixmap,  string text, int, int *height): void  
	import func dw_font_set_default( string fontname): void  
	import func dw_flush(void): void  
	import func dw_pixmap_bitblt(HWND dest, HPIXMAP destp, int, int, int, int, HWND src, HPIXMAP srcp, int, int ysrc): void  
	import func dw_pixmap_stretch_bitblt(HWND dest, HPIXMAP destp, int, int, int, int, HWND src, HPIXMAP srcp, int, int, int, int srcheight): int  
	import func dw_pixmap_new(HWND, dword, dword, int depth): HPIXMAP  
	import func dw_pixmap_new_from_file(HWND,  string filename): HPIXMAP  
	import func dw_pixmap_new_from_data(HWND,  string data, int len): HPIXMAP  
	import func dw_pixmap_grab(HWND, ULONG id): HPIXMAP  
	import func dw_pixmap_set_transparent_color( HPIXMAP pixmap, ULONG color ): void  
	import func dw_pixmap_set_font(HPIXMAP pixmap,  string fontname): int  
	import func dw_pixmap_destroy(HPIXMAP pixmap): void  
	import func dw_beep(int, int dur): void  
	import func dw_debug( string format, ...): void  
	import func dw_messagebox( string title, int,  string format, ...): int  
	import func dw_environment_query(DWEnv *env): void  
	import func dw_exec( string program, int,  string *params): int  
	import func dw_browse( string url): int  
	import func dw_file_browse( string title,  string defpath,  string ext, int flags):  string   
	import func dw_user_dir(void):  string   
	import func dw_dialog_new(memdata): DWDialog *  
	import func dw_dialog_dismiss(DWDialog *dialog, memresult): int  
	import func dw_dialog_wait(DWDialog *dialog): mem  
	import func dw_window_set_data(HWND,  string dataname, memdata): void  
	import func dw_window_get_data(HWND,  string dataname): mem  
	import func dw_module_load( string name, HMOD *handle): int  
	import func dw_module_symbol(HMOD handle,  string name, void**func): int  
	import func dw_module_close(HMOD handle): int  
	import func dw_timer_connect(int, memsigfunc, memdata): int  
	import func dw_timer_disconnect(int id): void  
	import func dw_signal_connect(HWND,  string signame, memsigfunc, memdata): void  
	import func dw_signal_disconnect_by_window(HWND): void  
	import func dw_signal_disconnect_by_data(HWND, memdata): void  
	import func dw_signal_disconnect_by_name(HWND,  string signame): void  
	import func dw_named_event_new( string name): HEV  
	import func dw_named_event_get( string name): HEV  
	import func dw_named_event_reset(HEV eve): int  
	import func dw_named_event_post(HEV eve): int  
	import func dw_named_event_wait(HEV eve, dword): int  
	import func dw_named_event_close(HEV eve): int  
	import func dw_named_memory_new(mem*dest, int,  string name): HSHM  
	import func dw_named_memory_get(mem*dest, int,  string name): HSHM  
	import func dw_named_memory_free(HSHM handle, mem): int  
	import func dw_html_action(HWND hwnd, int action): void  
	import func dw_html_raw(HWND hwnd,  string string): int  
	import func dw_html_url(HWND hwnd,  string url): int  
	import func dw_html_new(dword): HWND  
	import func dw_clipboard_get_text(void):  string   
	import func dw_clipboard_set_text(  string str, int len ): void  
	import func dw_calendar_new(dword): HWND  
	import func dw_calendar_set_date( HWND, dword, dword, dword day ): void  
	import func dw_calendar_get_date( HWND, dword, dword, dword *day ): void  
	import func dw_print_new( string jobname, dword, dword, memdrawfunc, memdrawdata): HPRINT  
	import func dw_print_run(HPRINT print, dword): int  
	import func dw_print_cancel(HPRINT print): void  
end
