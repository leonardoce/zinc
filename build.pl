#!/usr/bin/perl
use warnings;
use strict;
use File::Spec;
use File::Copy;

# ---------------------------
# Setup the build environment
# ---------------------------

sub is_windows {
	my $os = $^O;

	if ($os eq "mingw" || $os eq "msys" || $os =~ /win.*/i || $os eq "MSWin32" || $os eq "MSWin64") {
		return "1";
	} else {
		return "";
	}
}

our $TemporaryDirectory = "obj";
our $CCompiler = "gcc";
our $Defines = "";

if (is_windows()) {
    $Defines = "-D _WINDOWS";
}

our $COptions = "-O2 -g $Defines";
our $ExeSuffix = "";

if (is_windows()) {
    $ExeSuffix = ".exe";
}

# --------------
# Compiling Zinc
# --------------

# Execute a command and log
sub executeAndLog {
    my $command = shift();
    print "$command \n";
    system ($command) and die("failed");
}

sub bootstrap {
    my $bootstrapDir = "bootstrap";
    my $bootstrapOut = File::Spec->catfile("bootstrap", "bin");

    print "Bootstrapping Zinc...\n";
    executeAndLog ("$CCompiler $COptions -c -o " . File::Spec->catfile($bootstrapOut, "zc.o") . " ". File::Spec->catfile($bootstrapDir,"zc.c"));
    executeAndLog ("$CCompiler $COptions -c -o " . File::Spec->catfile($bootstrapOut, "io.o") . " ". File::Spec->catfile($bootstrapDir,"io.c"));
    executeAndLog ("$CCompiler $COptions -o " . File::Spec->catfile($bootstrapOut, "zc$ExeSuffix") . " ". 
		 File::Spec->catfile($bootstrapOut,"io.o") . " " . File::Spec->catfile($bootstrapOut,"zc.o"));
}

sub recompile {
    printf "Recompiling Zinc from the bootstrapped one...\n";
    executeAndLog (File::Spec->catfile("bootstrap","bin","zc") . " -I lib -I src -I " . File::Spec->catfile("lib","platform","default") . 
		   " -I src -o " . File::Spec->catfile("bin","zc.c") . " " . File::Spec->catfile("src","main.zc") );
    executeAndLog ("$CCompiler $COptions -c -o " . File::Spec->catfile("bin", "zc.o") . " ". File::Spec->catfile("bin","zc.c"));
    executeAndLog ("$CCompiler $COptions -c -o " . File::Spec->catfile("bin", "io.o") . " ". File::Spec->catfile("lib","libc","io.c"));
    executeAndLog ("$CCompiler $COptions -o " . File::Spec->catfile("bin", "zc$ExeSuffix") . " ". 
		 File::Spec->catfile("bin","io.o") . " " . File::Spec->catfile("bin","zc.o") );
}

bootstrap();
recompile();
print "Zinc built.\n\n";

# Copy script files
print "Copying script files\n\n";
copy ("src/tools/zcx.pl", "bin/zcx.pl");
copy ("src/tools/zcx.bat", "bin/zcx.bat");

print "All ok\n\n";
