/* Define structures */

struct _memory_bundle;
struct _mem_head;
struct _bundle_head;
struct _keyword;
struct _operator;
struct _signature;
struct _type;
struct _named_type;
struct _element;
struct _node;
struct _builtin_type;
struct _array_type;
struct _pointer_type;
struct _dictionary_class;
struct _definition_head;
struct _definition_item;
struct _definition;
struct _list;
struct _list_item;
struct _dictionary;
struct _association;
struct _collection;
struct _collection_of_expressions;
struct _module;
struct _compiler_session;
struct _include_directory;
struct _priv_vector;
struct _vector;
struct _type_dictionary;
struct _definition_dictionary;
struct _scope;
struct _compiler;
struct _set_of_directives;
struct _function;
struct _argument;
struct _expression;
struct _iterator;
struct _collection_of_arguments;
struct _file_buffer;
struct _file;
struct _string_buffer;
struct _structure_type;
struct _attribute;
struct _function_type;
struct _token_stream;
struct _parser;
union _lexeme;
struct _structure_node;
struct _collection_of_directives;
union _arg;
struct _binary_node;
struct _string_node;
struct _unary_node;
struct _variable_node;
struct _ternary_node;
struct _equate_node;
struct _iterator_node;
struct _long_node;
struct _priorities;
struct _attribute_node;
struct _function_node;
struct _section_node;
struct _collection_of_program_elements;
struct _module_expression;
struct _directive;
struct _directive_node;
struct _redirect_type;
struct _redirect_node;
struct _program_element;
struct _enumeration_type;
struct _enumeration_node;
struct _variable;
struct _variable_expression;
struct _argument_node;
struct _pointer_node;
struct _array_node;
struct _returned_node;
struct _expression_expression;
struct _collection_of_rule_nodes;
struct _condition_node;
struct _rule_node;
struct _condition_item;
struct _condition_expression;
struct _loop_expression;
struct _loop_node;
struct _rept_expression;
struct _selectors_node;
struct _collection_of_selectors;
struct _collection_of_values;
struct _selector_node;
struct _long_expression;
struct _constant;
struct _aswitch_item;
struct _aswitch_expression;
struct _sequence_expression;
struct _definition_expression;
struct _equate;
struct _extension;
struct _string_expression;
struct _definition_node;
struct _collection_expression;
struct _function_expression;
struct _enumeration_item;
struct _condition_operator_expression;
struct _yield_expression;
struct _iterator_call_node;
struct _function_reference_node;
struct _size_node;
struct _initializer_node;
struct _cast_node;
struct _invoke_node;
struct _cond_node;
struct _function_type_node;
struct _collection_of_enumeration_item_nodes;
struct _enumeration_item_node;
struct _litteral_string_dictionary;
struct _c_dump;
struct _global_constant;
struct _label;
struct _string_label;
struct _pointer_label;
struct _array_label;
struct _function_type_label;
struct _identifier_label;
struct _function_label;
struct _memory_bundle
{
unsigned char *_chunk_base;
unsigned char *_chunk_free;
unsigned int _chunk_size;
unsigned int _total;
};
struct _mem_head
{
unsigned char *_next;
unsigned char *_prev;
unsigned int _size;
};
struct _bundle_head
{
unsigned char *_next;
unsigned int _size;
};
struct _keyword
{
char *_name;
int _token;
};
struct _operator
{
char *_op;
int _min_arg;
int _max_arg;
};
struct _signature
{
int _number_of_types;
};
struct _type
{
struct _memory_bundle *_bundle;
int _id;
unsigned int _flags;
unsigned int _default_size;
unsigned int _default_align;
int _hash_code;
struct _type *_ptr;
};
struct _named_type
{
struct _type __parent;
char *_name;
struct _node *_source;
void *_module;
int _line;
};
struct _element
{
struct _element *_next_sibling;
};
struct _node
{
struct _element __parent;
int _code;
int _line;
};
struct _builtin_type
{
struct _named_type __parent;
};
struct _array_type
{
struct _type __parent;
struct _type *_array_type;
int _array_size;
};
struct _pointer_type
{
struct _type __parent;
struct _type *_target_type;
};
struct _dictionary_class
{
int (*_hash)(void *);
int (*_is_equal)(void *,void *);
};
struct _definition_head
{
struct _definition_item *_first;
char *_name;
struct _signature *_signature;
};
struct _definition_item
{
struct _definition_item *_next;
struct _definition *_value;
};
struct _definition
{
struct _element __parent;
char *_name;
struct _signature *_signature;
struct _type *_type;
int _opcode;
int _resolve_status;
int _lvalue;
int _expose;
int _used;
void *_module;
int _line;
struct _node *_source;
};
struct _list
{
struct _memory_bundle *_bundle;
struct _list_item *_first;
struct _list_item *_last;
unsigned int _size;
};
struct _list_item
{
struct _element __parent;
void *_value;
};
struct _dictionary
{
struct _dictionary_class *_class;
struct _association *_associations;
unsigned int _tally;
unsigned int _allocated;
int _collisions;
};
struct _association
{
void *_key;
void *_value;
};
struct _collection
{
struct _element *_first;
struct _element *_last;
};
struct _collection_of_expressions
{
struct _collection __parent;
};
struct _module
{
char *_name;
int _loading;
struct _list _exposed_imports;
struct _list _exposed_definitions;
struct _list _exposed_types;
struct _collection_of_expressions _elements;
int _scanned;
};
struct _compiler_session
{
struct _memory_bundle *_bundle;
struct _list _modules;
int _unicode;
struct _dictionary *_defines;
};
struct _include_directory
{
struct _list_item __parent;
};
struct _priv_vector
{
unsigned int _allocated;
void **_array;
};
struct _vector
{
struct _priv_vector __parent;
unsigned int _size;
};
struct _type_dictionary
{
struct _dictionary __parent;
};
struct _definition_dictionary
{
struct _dictionary __parent;
};
struct _scope
{
struct _scope *_next;
struct _type_dictionary _types;
struct _definition_dictionary _definitions;
};
struct _compiler
{
struct _memory_bundle *_bundle;
struct _compiler_session *_session;
struct _include_directory *_path;
struct _module *_module;
struct _vector _imports;
struct _scope _global_scope;
int _number_of_errors;
struct _scope *_current_scope;
int _current_exposition;
struct _set_of_directives *_current_directives;
struct _function *_current_function;
struct _type *_current_found_type;
struct _iterator *_current_iterator;
int _current_loop_level;
struct _collection_of_expressions *_current_block;
int _current_inside_equate;
};
struct _set_of_directives
{
struct _memory_bundle *_bundle;
struct _set_of_directives *_parent;
struct _collection _directives;
};
struct _function
{
struct _definition __parent;
struct _memory_bundle *_bundle;
struct _set_of_directives *_directives;
struct _argument *_arguments;
struct _expression *_instructions;
int _defined;
struct _type *_ref_type;
char *_extern_name;
struct _list _variables;
};
struct _argument
{
struct _definition __parent;
int _is_return;
struct _expression *_substitute;
};
struct _expression
{
struct _element __parent;
struct _type *_type;
int _opcode;
int _lvalue;
void *_user_data;
};
struct _iterator
{
struct _definition __parent;
struct _argument *_arguments;
struct _expression *_instructions;
struct _signature *_yield_signature;
struct _expression *_block;
struct _collection_of_arguments *_parameters;
};
struct _collection_of_arguments
{
struct _collection __parent;
};
struct _file_buffer
{
unsigned char *_base;
unsigned int _size;
};
struct _file
{
int _dummy;
};
struct _string_buffer
{
char *_p;
unsigned int _used;
unsigned int _buffer_size;
};
struct _structure_type
{
struct _named_type __parent;
char *_short_name;
struct _attribute *_attributes;
int _defined;
struct _type *_parent_type;
struct _signature *_signature;
struct _set_of_directives *_directives;
char *_extern_name;
};
struct _attribute
{
struct _definition __parent;
struct _structure_type *_structure;
struct _type *_storage_type;
};
struct _function_type
{
struct _type __parent;
struct _type *_returned_type;
struct _signature *_signature;
};
struct _token_stream
{
struct _memory_bundle *_bundle;
int _last_error;
char *_beginning_of_line;
char *_p;
int _line;
int _ignore_line_feed;
int _unicode;
struct _dictionary *_defines;
};
struct _parser
{
struct _token_stream __parent;
struct _memory_bundle *_bundle;
char *_filename;
int _number_of_errors;
};
union _lexeme
{
char *_string_value;
int _long_value;
};
struct _structure_node
{
struct _node __parent;
int _is_union;
struct _collection_of_directives *_directives;
char *_name;
struct _node *_parent;
struct _collection *_attributes;
};
struct _collection_of_directives
{
struct _collection __parent;
};
union _arg
{
void *_object;
int _value;
};
struct _binary_node
{
struct _node __parent;
void *_value_1;
void *_value_2;
};
struct _string_node
{
struct _node __parent;
char *_value;
};
struct _unary_node
{
struct _node __parent;
void *_value_1;
};
struct _variable_node
{
struct _node __parent;
struct _collection_of_directives *_directives;
char *_name;
struct _node *_type;
struct _node *_initial_value;
struct _node *_reference;
int _is_import;
};
struct _ternary_node
{
struct _node __parent;
void *_value_1;
void *_value_2;
void *_value_3;
};
struct _equate_node
{
struct _node __parent;
char *_name;
struct _collection *_arguments;
struct _node *_value;
int _global;
};
struct _iterator_node
{
struct _node __parent;
char *_name;
struct _collection *_arguments;
struct _collection *_body;
int _global;
};
struct _long_node
{
struct _node __parent;
unsigned int _value;
};
struct _priorities
{
int _left;
int _right;
};
struct _attribute_node
{
struct _node __parent;
char *_name;
struct _node *_type;
int _mode;
};
struct _function_node
{
struct _node __parent;
struct _collection_of_directives *_directives;
char *_name;
struct _collection *_arguments;
struct _node *_returned_type;
struct _collection *_body;
};
struct _section_node
{
struct _node __parent;
struct _collection_of_directives *_directives;
int _mode;
struct _collection *_content;
};
struct _collection_of_program_elements
{
struct _collection _collection;
struct _memory_bundle *_bundle;
};
struct _module_expression
{
struct _expression __parent;
struct _module *_value;
};
struct _directive
{
struct _element __parent;
char *_name;
char *_value;
};
struct _directive_node
{
struct _binary_node __parent;
};
struct _redirect_type
{
struct _named_type __parent;
struct _type *_source_type;
struct _type *_real_type;
};
struct _redirect_node
{
struct _binary_node __parent;
};
struct _program_element
{
struct _element __parent;
struct _node *_node;
void *_value;
int _expose;
struct _set_of_directives *_directives;
};
struct _enumeration_type
{
struct _named_type __parent;
int _defined;
};
struct _enumeration_node
{
struct _binary_node __parent;
};
struct _variable
{
struct _definition __parent;
struct _set_of_directives *_directives;
struct _expression *_initial_value;
struct _expression *_proxy;
int _defined;
struct _type *_storage_type;
struct _expression *_reference;
struct _variable *_redirect;
char *_extern_name;
struct _function *_function;
};
struct _variable_expression
{
struct _expression __parent;
struct _variable *_value;
};
struct _argument_node
{
struct _ternary_node __parent;
};
struct _pointer_node
{
struct _unary_node __parent;
};
struct _array_node
{
struct _binary_node __parent;
};
struct _returned_node
{
struct _unary_node __parent;
};
struct _expression_expression
{
struct _expression __parent;
struct _expression *_value;
};
struct _collection_of_rule_nodes
{
struct _collection __parent;
};
struct _condition_node
{
struct _unary_node __parent;
};
struct _rule_node
{
struct _binary_node __parent;
};
struct _condition_item
{
struct _element __parent;
struct _expression *_condition;
struct _expression *_block;
};
struct _condition_expression
{
struct _expression __parent;
struct _condition_item *_value;
};
struct _loop_expression
{
struct _expression __parent;
struct _expression *_condition;
struct _expression *_block;
};
struct _loop_node
{
struct _unary_node __parent;
};
struct _rept_expression
{
struct _expression __parent;
struct _expression *_block;
};
struct _selectors_node
{
struct _binary_node __parent;
};
struct _collection_of_selectors
{
struct _collection __parent;
};
struct _collection_of_values
{
struct _collection __parent;
};
struct _selector_node
{
struct _binary_node __parent;
};
struct _long_expression
{
struct _expression __parent;
int _value;
};
struct _constant
{
struct _element __parent;
int _value;
};
struct _aswitch_item
{
struct _element __parent;
struct _constant *_constants;
struct _expression *_block;
};
struct _aswitch_expression
{
struct _expression __parent;
struct _expression *_expression;
struct _aswitch_item *_rules;
};
struct _sequence_expression
{
struct _expression __parent;
struct _expression *_sequence;
};
struct _definition_expression
{
struct _expression __parent;
struct _definition *_definition;
struct _expression *_arguments;
};
struct _equate
{
struct _definition __parent;
struct _argument *_arguments;
struct _expression *_value;
};
struct _extension
{
struct _definition __parent;
struct _definition *_definition;
struct _argument *_arguments;
};
struct _string_expression
{
struct _expression __parent;
char *_value;
};
struct _definition_node
{
struct _binary_node __parent;
};
struct _collection_expression
{
struct _expression __parent;
struct _expression *_value;
};
struct _function_expression
{
struct _expression __parent;
struct _function *_value;
};
struct _enumeration_item
{
struct _definition __parent;
int _value;
};
struct _condition_operator_expression
{
struct _expression __parent;
struct _expression *_condition;
struct _expression *_then_expression;
struct _expression *_otherwise_expression;
};
struct _yield_expression
{
struct _expression __parent;
struct _expression *_arguments;
struct _iterator *_iterator;
};
struct _iterator_call_node
{
struct _ternary_node __parent;
};
struct _function_reference_node
{
struct _binary_node __parent;
};
struct _size_node
{
struct _unary_node __parent;
};
struct _initializer_node
{
struct _binary_node __parent;
};
struct _cast_node
{
struct _binary_node __parent;
};
struct _invoke_node
{
struct _binary_node __parent;
};
struct _cond_node
{
struct _ternary_node __parent;
};
struct _function_type_node
{
struct _binary_node __parent;
};
struct _collection_of_enumeration_item_nodes
{
struct _collection __parent;
};
struct _enumeration_item_node
{
struct _binary_node __parent;
};
struct _litteral_string_dictionary
{
struct _dictionary __parent;
};
struct _c_dump
{
struct _memory_bundle *_bundle;
struct _function *_current_function;
int _index;
struct _string_buffer _tmp;
struct _module *_module;
int _count;
struct _litteral_string_dictionary _strings;
struct _litteral_string_dictionary _wstrings;
struct _list _initializers;
struct _list _structures;
struct _list _functions;
struct _list _variables;
};
struct _global_constant
{
int _expose;
char *_name;
};
struct _label
{
struct _string_buffer *(*_append)(struct _label *,struct _string_buffer *);
};
struct _string_label
{
struct _label __parent;
char *_id;
};
struct _pointer_label
{
struct _label __parent;
struct _label *_inner;
};
struct _array_label
{
struct _label __parent;
int _size;
struct _label *_inner;
};
struct _function_type_label
{
struct _label __parent;
struct _function_type *_function;
struct _label *_inner;
};
struct _identifier_label
{
struct _label __parent;
char *_id;
};
struct _function_label
{
struct _label __parent;
struct _function *_function;
};

/* Declare all functions */

static void _initialize_allocator();
extern void *malloc(unsigned int );
static unsigned char *_allocate_memory_dw(unsigned int _size);
static void _new_chunk_p_s__memory_bundle_dw(struct _memory_bundle *_bundle, unsigned int _size);
static void _bundle_dw_p_s__memory_bundle(unsigned int _size, struct _memory_bundle *_bundle);
static void _initialize_lexer();
static struct _operator *_add_p_a_s__operator_p_a_ch_in_in(struct _operator *_p, char *_op, int _min, int _max);
static void _initialize_operators();
static void _initialize_signature();
static unsigned int _max_dw_dw(unsigned int _x, unsigned int _y);
static unsigned char *_allocate_p_s__memory_bundle_dw(struct _memory_bundle *_bundle, unsigned int _s);
static struct _type *_new_type_p_s__memory_bundle_dw_e__type_id_in_dw_dw(struct _memory_bundle *_bundle, unsigned int _structure_size, int _id, int _flags, unsigned int _size, unsigned int _align);
static struct _builtin_type *_new_builtin_type_p_s__memory_bundle_e__type_id_in_dw_p_a_ch(struct _memory_bundle *_bundle, int _id, int _flags, unsigned int _size, char *_name);
static unsigned int _align_p_s__type(struct _type *_t);
static struct _array_type *_new_array_type_p_s__memory_bundle_in_p_s__type(struct _memory_bundle *_bundle, int _cells, struct _type *_cell_type);
static struct _pointer_type *_new_pointer_type_p_s__memory_bundle_p_s__type(struct _memory_bundle *_bundle, struct _type *_target);
static struct _type *_pointer_p_s__type(struct _type *_t);
static void _initialize_builtin_types_p_s__memory_bundle(struct _memory_bundle *_bundle);
static int _hash_p_a_ch(char *_s);
static int _compare_p_a_ch_p_a_ch(char *_s1, char *_s2);
static int _is_equal_p_a_ch_p_a_ch(char *_s1, char *_s2);
static void _initialize_type_dictionary();
static void _initialize_type_p_s__memory_bundle(struct _memory_bundle *_bundle);
static int _hash_p_s__definition_head(struct _definition_head *_head);
static int _match_p_s__definition_head_p_s__definition_head(struct _definition_head *_h1, struct _definition_head *_h2);
static void _initialize_definition_dictionary();
static void _initialize_definition();
static void _initialize_litteral_string_dictionary();
static void _initialize_c_dump();
static void _initialize_p_s__list_p_s__memory_bundle(struct _list *_list, struct _memory_bundle *_bundle);
static void _initialize_p_s__dictionary_p_s__dictionary_class(struct _dictionary *_d, struct _dictionary_class *_class);
static int _size_p_a_ch(char *_s);
static void _copy_p_vo_p_vo_dw(void *_dst, void *_src, unsigned int _size);
static char *_new_string_p_s__memory_bundle_p_a_ch(struct _memory_bundle *_bundle, char *_s);
extern void convert_full_name(char *);
static void _to_OS_name_p_a_ch(char *_filename);
static void _append_p_s__list_p_vo(struct _list *_list, void *_value);
static char *_first_occurrence_p_a_ch_ch(char *_s, char _c);
static char *_new_string_p_s__memory_bundle_p_a_ch_dw(struct _memory_bundle *_bundle, char *_s, unsigned int _len);
static int _max_in_in(int _x, int _y);
static void _free_memory_p_vo(void *_base);
static void _resize_p_s__dictionary_dw(struct _dictionary *_d, unsigned int _s);
static void _increase_tally_p_s__dictionary(struct _dictionary *_d);
static void *_add_p_s__dictionary_p_vo_p_vo(struct _dictionary *_d, void *_k, void *_v);
extern int puts(char *);
static void _print_usage();
static void _initialize_p_s__collection(struct _collection *_c);
static struct _module *_new_module_p_s__memory_bundle_p_a_ch(struct _memory_bundle *_bundle, char *_name);
static void _initialize_p_s__vector_dw(struct _vector *_v, unsigned int _initial_allocated);
static void _initialize_p_s__type_dictionary(struct _type_dictionary *_d);
static void _initialize_p_s__definition_dictionary(struct _definition_dictionary *_d);
static void _initialize_p_s__scope_p_s__scope(struct _scope *_s, struct _scope *_next);
static void _add_builtin_type_p_s__compiler_p_a_ch_p_s__named_type(struct _compiler *_cc, char *_name, struct _named_type *_type);
static void _initialize_builtin_types_p_s__compiler(struct _compiler *_cc);
static void _initialize_p_s__definition_p_a_ch_p_s__signature_p_s__type_e__definition_code_bo_bo_p_vo_in(struct _definition *_d, char *_name, struct _signature *_signature, struct _type *_type, int _opcode, int _lvalue, int _expose, void *_module, int _line);
static void _initialize_p_s__definition_item_p_s__definition(struct _definition_item *_item, struct _definition *_d);
static void _definition_head_p_s__definition_item_p_a_ch_p_s__signature_p_s__definition_head(struct _definition_item *_first, char *_name, struct _signature *_signature, struct _definition_head *_d);
static void *_value_p_s__dictionary_p_vo(struct _dictionary *_d, void *_k);
static void _add_definition_p_s__definition_dictionary_p_s__definition(struct _definition_dictionary *_d, struct _definition *_definition);
static void _add_builtin_definition_p_s__compiler_p_s__definition(struct _compiler *_cc, struct _definition *_definition);
static void _add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code_bo(struct _compiler *_cc, char *_name, struct _signature *_signature, struct _type *_type, int _opcode, int _lvalue);
static void _add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(struct _compiler *_cc, char *_name, struct _signature *_signature, struct _type *_type, int _opcode);
static struct _signature *_new_signature_p_s__memory_bundle_in(struct _memory_bundle *_bundle, int _s);
static void _set_type_p_s__signature_in_p_s__type(struct _signature *_signature, int _index, struct _type *_t);
static struct _signature *_new_signature_p_s__memory_bundle_p_s__type_p_s__type(struct _memory_bundle *_bundle, struct _type *_t1, struct _type *_t2);
static struct _signature *_new_signature_p_s__memory_bundle_p_s__type(struct _memory_bundle *_bundle, struct _type *_t1);
static void _add_comparators_p_s__compiler_p_s__signature(struct _compiler *_cc, struct _signature *_sig);
static void _add_numeric_definitions_p_s__compiler_p_s__type(struct _compiler *_cc, struct _type *_type);
static void _initialize_builtin_definitions_p_s__compiler_p_s__memory_bundle(struct _compiler *_cc, struct _memory_bundle *_bundle);
static void _initialize_p_s__compiler_p_s__compiler_session_p_s__memory_bundle_p_s__module(struct _compiler *_cc, struct _compiler_session *_session, struct _memory_bundle *_bundle, struct _module *_module);
extern unsigned int file_size(char *);
extern struct _file *fopen(char *, char *);
extern unsigned int fread(void *, unsigned int , unsigned int , struct _file *);
extern int fclose(struct _file *);
static int _initialize_p_s__file_buffer_p_a_ch_bo(struct _file_buffer *_buffer, char *_filename, int _is_text);
static void _initialize_p_s__string_buffer(struct _string_buffer *_s);
static void _reserve_p_s__string_buffer_in(struct _string_buffer *_s, int _l);
static struct _string_buffer *_append_p_s__string_buffer_p_a_ch(struct _string_buffer *_s, char *_str);
static struct _string_buffer *_append_p_s__string_buffer_ch(struct _string_buffer *_s, char _c);
static struct _string_buffer *_append_p_s__string_buffer_dw(struct _string_buffer *_s, unsigned int _v);
static struct _string_buffer *_append_p_s__string_buffer_in(struct _string_buffer *_s, int _v);
static void _append_p_s__string_buffer_p_s__module_in(struct _string_buffer *_buffer, struct _module *_module, int _line);
static struct _type *_type_p_s__signature_in(struct _signature *_signature, int _index);
static struct _string_buffer *_append_p_s__string_buffer_p_s__type(struct _string_buffer *_stream, struct _type *_t);
extern void fputs_stderr(char *);
static char *_as_string_p_s__string_buffer(struct _string_buffer *_s);
static void _release_p_s__string_buffer(struct _string_buffer *_s);
static void _error_p_s__compiler_p_s__module_in_p_a_ch_p_a_p_vo(struct _compiler *_cc, struct _module *_module, int _line, char *_fmt, void **_args);
static void _error_p_s__compiler_in_p_a_ch_p_vo(struct _compiler *_cc, int _line, char *_fmt, void *_arg1);
static void _initialize_p_s__token_stream_p_s__memory_bundle_p_a_ch_p_s__dictionary_bo(struct _token_stream *_stream, struct _memory_bundle *_bundle, char *_s, struct _dictionary *_defines, int _unicode);
static struct _collection *_new_collection_p_s__parser(struct _parser *_cc);
static char *_skip_blank_p_s__token_stream_p_a_ch(struct _token_stream *_stream, char *_s);
static unsigned int _strtoul_p_a_ch_p_a_p_a_ch(char *_s, char **_endptr);
static int _error_p_s__token_stream_p_a_ch_e__lexer_error(struct _token_stream *_stream, char *_p, int _err);
static int _read_string_p_s__token_stream_p_s__lexeme(struct _token_stream *_stream, union _lexeme *_lexeme);
static int _is_identifier_char_ch(char _c);
static int _compare_p_vo_p_vo_dw(void *_dst, void *_src, unsigned int _size);
static int _get_token_p_s__token_stream_p_s__lexeme(struct _token_stream *_stream, union _lexeme *_lexeme);
static void _initialize_p_s__token_stream_p_s__token_stream(struct _token_stream *_stream, struct _token_stream *_source);
static int _peek_token_p_s__token_stream(struct _token_stream *_stream);
static int _get_token_p_s__token_stream(struct _token_stream *_stream);
static void _append_p_s__string_buffer_p_s__parser(struct _string_buffer *_buffer, struct _parser *_cc);
static void _append_p_s__string_buffer_e__token_p_s__lexeme(struct _string_buffer *_stream, int _token, union _lexeme *_lexeme);
static void _error_p_s__parser_p_a_ch_p_a_s__arg(struct _parser *_cc, char *_fmt, union _arg *_args);
static void _error_p_s__parser_p_a_ch_p_vo_p_vo_p_vo_p_vo(struct _parser *_cc, char *_fmt, void *_arg1, void *_arg2, void *_arg3, void *_arg4);
static int _unexpected_p_s__parser_e__token_p_s__lexeme_e__token(struct _parser *_cc, int _token, union _lexeme *_lexeme, int _expected_token);
static char *_get_id_p_s__parser(struct _parser *_cc);
static int _expected_p_s__parser_e__token(struct _parser *_cc, int _expected_token);
static void _error_p_s__parser_p_a_ch_p_vo_p_vo(struct _parser *_cc, char *_fmt, void *_arg1, void *_arg2);
static void* _syntax_error_p_s__parser_e__token_p_s__lexeme(struct _parser *_cc, int _token, union _lexeme *_lexeme);
static struct _binary_node *_new_node_p_s__parser_p_vo_p_vo(struct _parser *_cc, void *_value_1, void *_value_2);
static void _append_p_s__collection_p_s__element(struct _collection *_c, struct _element *_e);
static struct _collection *_get_set_of_directives_p_s__parser(struct _parser *_cc);
static struct _node *_new_node_p_s__parser_e__node_code_dw(struct _parser *_cc, int _code, unsigned int _size);
static struct _string_node *_new_string_p_s__parser_e__node_code_p_a_ch(struct _parser *_cc, int _code, char *_value);
static struct _unary_node *_new_node_p_s__parser_e__node_code_p_vo(struct _parser *_cc, int _code, void *_value);
static struct _collection *_get_mini_block_p_s__parser(struct _parser *_cc);
static struct _binary_node *_new_node_p_s__parser_e__node_code_p_vo_p_vo(struct _parser *_cc, int _code, void *_value_1, void *_value_2);
static struct _collection *_get_sequence_of_instructions_p_s__parser(struct _parser *_cc);
static struct _collection *_get_block_p_s__parser(struct _parser *_cc);
static struct _variable_node *_new_variable_p_s__parser_in_p_s__collection_p_a_ch_p_s__node_p_s__node_p_s__node_bo(struct _parser *_cc, int _line, struct _collection *_directives, char *_name, struct _node *_type, struct _node *_initial_value, struct _node *_reference, int _is_import);
static struct _variable_node *_get_variable_definition_p_s__parser_bo(struct _parser *_cc, int _is_import);
static struct _ternary_node *_new_node_p_s__parser_in_e__node_code_p_vo_p_vo_p_vo(struct _parser *_cc, int _line, int _code, void *_value_1, void *_value_2, void *_value_3);
static struct _collection *_get_parameters_p_s__parser_bo(struct _parser *_cc, int _type_only);
static struct _equate_node *_new_equate_p_s__parser_in_bo_p_a_ch_p_s__collection_p_s__node(struct _parser *_cc, int _line, int _global, char *_name, struct _collection *_arguments, struct _node *_value);
static struct _iterator_node *_new_iterator_p_s__parser_in_bo_p_a_ch_p_s__collection_p_s__collection(struct _parser *_cc, int _line, int _global, char *_name, struct _collection *_arguments, struct _collection *_body);
static struct _node *_get_equate_definition_p_s__parser_bo(struct _parser *_cc, int _is_global);
static struct _binary_node *_new_node_p_s__parser_in_e__node_code_p_vo_p_vo(struct _parser *_cc, int _line, int _code, void *_value_1, void *_value_2);
static struct _binary_node *_get_typedef_definition_p_s__parser(struct _parser *_cc);
static struct _long_node *_new_long_p_s__parser_e__node_code_dw(struct _parser *_cc, int _code, unsigned int _value);
static struct _collection *_get_list_of_expressions_p_s__parser(struct _parser *_cc);
static struct _collection *_get_list_of_arguments_p_s__parser(struct _parser *_cc);
static struct _binary_node *_get_initializer_p_s__parser(struct _parser *_cc);
static char *_prefix_params_e__token_p_a_in(int _op, int *_p);
static char *_operator_params_e__token_p_s__priorities(int _op, struct _priorities *_p);
static char *_postfix_params_e__token_p_a_in(int _op, int *_p);
static void _add_first_p_s__collection_p_s__element(struct _collection *_c, struct _element *_e);
static struct _ternary_node *_new_node_p_s__parser_e__node_code_p_vo_p_vo_p_vo(struct _parser *_cc, int _code, void *_value_1, void *_value_2, void *_value_3);
static struct _node *_get_expression_p_s__parser_in(struct _parser *_cc, int _current_priority);
static void *_get_end_of_line_p_s__parser(struct _parser *_cc);
static struct _collection *_get_enum_content_p_s__parser(struct _parser *_cc);
static struct _node *_get_type_p_s__parser(struct _parser *_cc);
static struct _attribute_node *_new_attribute_p_s__parser_in_e__node_code_p_a_ch_p_s__node_e__expose_mode(struct _parser *_cc, int _line, int _code, char *_name, struct _node *_type, int _mode);
static struct _structure_node *_new_structure_p_s__parser_in_bo_p_s__collection_p_a_ch_p_s__node_p_s__collection(struct _parser *_cc, int _line, int _is_union, struct _collection *_directives, char *_name, struct _node *_parent, struct _collection *_attributes);
static struct _structure_node *_get_struct_definition_p_s__parser_bo_bo_bo(struct _parser *_cc, int _is_union, int _anonymous, int _is_import);
static struct _binary_node *_get_enum_definition_p_s__parser_bo(struct _parser *_cc, int _is_import);
static struct _function_node *_new_function_p_s__parser_in_p_s__collection_p_a_ch_p_s__collection_p_s__node_p_s__collection(struct _parser *_cc, int _line, struct _collection *_directives, char *_name, struct _collection *_arguments, struct _node *_returned_type, struct _collection *_body);
static struct _function_node *_get_function_definition_p_s__parser_bo(struct _parser *_cc, int _is_import);
static struct _section_node *_new_section_p_s__parser_e__expose_mode_p_s__collection_p_s__collection(struct _parser *_cc, int _mode, struct _collection *_content, struct _collection *_directives);
static struct _node *_get_section_p_s__parser_e__expose_mode(struct _parser *_cc, int _mode);
static struct _node *_get_import_p_s__parser(struct _parser *_cc);
static struct _node *_get_program_element_p_s__parser_bo(struct _parser *_cc, int _accepts_end);
static struct _collection *_run_p_s__memory_bundle_p_s__memory_bundle_p_a_ch_p_a_ch_p_s__dictionary_bo(struct _memory_bundle *_string_bundle, struct _memory_bundle *_tree_bundle, char *_filename, char *_source_code, struct _dictionary *_defines, int _unicode);
static void _release_p_s__file_buffer(struct _file_buffer *_buffer);
static char *_last_occurrence_p_a_ch_ch(char *_s, char _c);
extern char path_separator();
static char *_file_path_p_s__memory_bundle_p_a_ch(struct _memory_bundle *_bundle, char *_filename);
static void _initialize_p_s__collection_of_program_elements_p_s__memory_bundle(struct _collection_of_program_elements *_c, struct _memory_bundle *_bundle);
static struct _set_of_directives *_new_set_of_directives_p_s__memory_bundle_p_s__set_of_directives(struct _memory_bundle *_bundle, struct _set_of_directives *_parent);
static int _ends_with_p_a_ch_ch(char *_s, char _c);
extern int file_exists(char *);
static int _file_exists_p_a_ch(char *_filename);
static char *_get_fullname_p_s__compiler_p_a_ch(struct _compiler *_cc, char *_module_name);
static void _error_p_s__compiler_p_s__node_p_a_ch_p_vo(struct _compiler *_cc, struct _node *_source, char *_fmt, void *_arg1);
static void _error_p_s__compiler_p_s__node_p_a_ch(struct _compiler *_cc, struct _node *_source, char *_fmt);
static void _reserve_p_s__vector(struct _vector *_v);
static void _append_p_s__vector_p_vo(struct _vector *_v, void *_value);
static struct _definition_item *_first_definition_p_s__definition_dictionary_p_a_ch_p_s__signature(struct _definition_dictionary *_d, char *_name, struct _signature *_signature);
static int _equals_p_s__type_p_s__type(struct _type *_t1, struct _type *_t2);
static int _equals_p_s__signature_p_s__signature(struct _signature *_s1, struct _signature *_s2);
static struct _definition *_find_definition_p_s__definition_dictionary_p_a_ch_p_s__signature(struct _definition_dictionary *_d, char *_name, struct _signature *_signature);
static struct _definition *_find_definition_p_s__compiler_p_a_ch_p_s__signature(struct _compiler *_cc, char *_name, struct _signature *_signature);
static void _error_p_s__compiler_p_s__module_in_p_a_ch(struct _compiler *_cc, struct _module *_module, int _line, char *_fmt);
static void _error_already_defined_p_s__compiler_p_a_ch_in_p_s__module_in(struct _compiler *_cc, char *_name, int _line, struct _module *_first_module, int _first_line);
static void _error_already_defined_p_s__compiler_p_a_ch_in_p_s__definition(struct _compiler *_cc, char *_name, int _line, struct _definition *_definition);
static int _add_global_definition_p_s__compiler_p_s__definition_bo(struct _compiler *_cc, struct _definition *_definition, int _expose);
static struct _named_type *_find_type_p_s__compiler_p_a_ch(struct _compiler *_cc, char *_name);
static void _error_already_defined_p_s__compiler_p_a_ch_in_p_s__named_type(struct _compiler *_cc, char *_name, int _line, struct _named_type *_type);
static int _add_global_type_p_s__compiler_p_s__named_type_bo(struct _compiler *_cc, struct _named_type *_type, int _expose);
static void _add_import_p_s__compiler_p_s__module_bo(struct _compiler *_cc, struct _module *_module, int _expose);
static void _add_p_s__compiler_p_s__expression(struct _compiler *_cc, struct _expression *_e);
static struct _expression *_new_expression_p_s__memory_bundle_dw_p_s__type_e__expression_code(struct _memory_bundle *_bundle, unsigned int _size, struct _type *_type, int _opcode);
static struct _module_expression *_new_module_expression_p_s__memory_bundle_p_s__type_e__expression_code_p_s__module(struct _memory_bundle *_bundle, struct _type *_type, int _opcode, struct _module *_value);
static struct _directive *_new_directive_p_s__memory_bundle_p_a_ch_p_a_ch(struct _memory_bundle *_bundle, char *_name, char *_value);
static void _add_p_s__set_of_directives_p_a_ch_p_a_ch(struct _set_of_directives *_s, char *_name, char *_value);
static struct _set_of_directives *_get_directives_p_s__compiler_p_s__set_of_directives_p_s__collection_of_directives(struct _compiler *_cc, struct _set_of_directives *_directives, struct _collection_of_directives *_extension);
static void _set_expose_p_s__type_bo(struct _type *_t, int _expose);
static struct _redirect_type *_new_redirect_type_p_s__memory_bundle_bo_p_vo_p_s__redirect_node(struct _memory_bundle *_bundle, int _expose, void *_module, struct _redirect_node *_source);
static void _add_p_s__collection_of_program_elements_p_s__node_p_vo(struct _collection_of_program_elements *_c, struct _node *_node, void *_value);
static char *_new_string_p_s__memory_bundle_dw(struct _memory_bundle *_bundle, unsigned int _len);
static char *_append_p_a_ch_ch(char *_s, char _c);
static char *_append_p_a_ch_p_a_ch(char *_s, char *_str);
static struct _structure_type *_new_structure_type_p_s__memory_bundle_bo_p_s__set_of_directives_p_vo_p_s__structure_node(struct _memory_bundle *_bundle, int _expose, struct _set_of_directives *_directives, void *_module, struct _structure_node *_source);
static struct _redirect_type *_new_redirect_type_p_s__memory_bundle_bo_p_a_ch_p_s__type_p_vo_in(struct _memory_bundle *_bundle, int _expose, char *_name, struct _type *_type, void *_module, int _line);
static struct _enumeration_type *_new_enumeration_type_p_s__memory_bundle_bo_p_vo_p_s__enumeration_node(struct _memory_bundle *_bundle, int _expose, void *_module, struct _enumeration_node *_source);
static void _add_p_s__collection_of_program_elements_p_s__node_p_vo_bo_p_s__set_of_directives(struct _collection_of_program_elements *_c, struct _node *_node, void *_value, int _expose, struct _set_of_directives *_directives);
static void _declare_types_p_s__compiler_p_s__collection_of_program_elements_p_s__collection_bo_p_s__set_of_directives(struct _compiler *_cc, struct _collection_of_program_elements *_list, struct _collection *_tree, int _expose, struct _set_of_directives *_base_directives);
static void _declare_types_p_s__compiler_p_s__collection_of_program_elements_p_s__collection(struct _compiler *_cc, struct _collection_of_program_elements *_list, struct _collection *_tree);
static void _initialize_p_s__definition_p_a_ch_p_s__signature_p_s__type_e__definition_code_bo_bo_p_vo_p_s__node(struct _definition *_d, char *_name, struct _signature *_signature, struct _type *_type, int _opcode, int _lvalue, int _expose, void *_module, struct _node *_source);
static struct _variable *_new_variable_p_s__memory_bundle_p_a_ch_bo_p_s__set_of_directives_p_vo_p_s__node(struct _memory_bundle *_bundle, char *_name, int _expose, struct _set_of_directives *_directives, void *_module, struct _node *_source);
static int _add_local_definition_p_s__compiler_p_s__definition(struct _compiler *_cc, struct _definition *_definition);
static int _add_definition_p_s__compiler_p_s__definition_bo_bo(struct _compiler *_cc, struct _definition *_definition, int _expose, int _local);
static struct _variable *_declare_variable_p_s__compiler_p_s__variable_node_bo_p_s__set_of_directives_bo(struct _compiler *_cc, struct _variable_node *_source, int _expose, struct _set_of_directives *_base_directives, int _local);
static struct _variable_expression *_new_variable_expression_p_s__memory_bundle_p_s__type_e__expression_code_p_s__variable(struct _memory_bundle *_bundle, struct _type *_type, int _opcode, struct _variable *_value);
static struct _collection_of_arguments *_new_collection_of_arguments_p_s__compiler(struct _compiler *_cc);
static void _set_resolving_p_s__type(struct _type *_t);
static void _set_resolved_p_s__type(struct _type *_t);
static struct _type *_define_redirect_p_s__compiler_p_s__redirect_type(struct _compiler *_cc, struct _redirect_type *_type);
static unsigned int _size_p_s__type(struct _type *_t);
static int _match_p_s__signature_p_s__signature(struct _signature *_s1, struct _signature *_s2);
static int _match_p_s__type_p_s__type(struct _type *_t1, struct _type *_t2);
static void _error_p_s__compiler_p_s__node_p_a_ch_p_vo_p_vo(struct _compiler *_cc, struct _node *_source, char *_fmt, void *_arg1, void *_arg2);
static struct _signature *_common_p_s__signature_p_s__signature(struct _signature *_s1, struct _signature *_s2);
static int _hash_p_s__signature(struct _signature *_s);
static struct _function_type *_new_function_type_p_s__memory_bundle_p_s__signature_p_s__type(struct _memory_bundle *_bundle, struct _signature *_signature, struct _type *_returned_type);
static struct _type *_common_p_s__type_p_s__type(struct _type *_t1, struct _type *_t2);
static void _check_return_p_s__compiler_p_s__node_p_s__expression(struct _compiler *_cc, struct _node *_source, struct _expression *_value);
static struct _expression_expression *_new_expression_expression_p_s__memory_bundle_p_s__type_e__expression_code_p_s__expression(struct _memory_bundle *_bundle, struct _type *_type, int _opcode, struct _expression *_value);
static struct _expression *_get_condition_p_s__compiler_p_s__node(struct _compiler *_cc, struct _node *_source);
static struct _scope *_new_scope_p_s__scope(struct _scope *_n);
static void _open_scope_p_s__compiler_p_s__scope(struct _compiler *_cc, struct _scope *_parent_scope);
static struct _collection_of_expressions *_new_collection_of_expressions_p_s__compiler(struct _compiler *_cc);
static struct _collection_of_expressions *_get_list_of_expressions_p_s__compiler_p_s__collection(struct _compiler *_cc, struct _collection *_source);
static void _release_p_s__definition_head(struct _definition_head *_head);
static void _release_p_s__dictionary(struct _dictionary *_d);
static void _release_p_s__definition_dictionary(struct _definition_dictionary *_d);
static void _release_p_s__scope(struct _scope *_s);
static void _delete_p_s__scope(struct _scope *_s);
static void _close_scope_p_s__compiler(struct _compiler *_cc);
static struct _collection_of_expressions *_get_block_p_s__compiler_p_s__collection(struct _compiler *_cc, struct _collection *_source);
static struct _condition_item *_new_condition_p_s__memory_bundle_p_s__expression_p_s__collection(struct _memory_bundle *_bundle, struct _expression *_condition, struct _collection *_block);
static struct _condition_expression *_new_condition_expression_p_s__memory_bundle_p_s__condition_item(struct _memory_bundle *_bundle, struct _condition_item *_value);
static struct _loop_expression *_new_loop_expression_p_s__memory_bundle_p_s__expression_p_s__collection_of_expressions(struct _memory_bundle *_bundle, struct _expression *_condition, struct _collection_of_expressions *_block);
static struct _rept_expression *_new_repeat_expression_p_s__memory_bundle_p_s__collection_of_expressions(struct _memory_bundle *_bundle, struct _collection_of_expressions *_block);
static int _get_constant_integer_p_s__compiler_p_s__node_p_s__expression(struct _compiler *_cc, struct _node *_source, struct _expression *_e);
static char _get_constant_char_p_s__compiler_p_s__node_p_s__expression(struct _compiler *_cc, struct _node *_source, struct _expression *_e);
static short _get_constant_wchar_p_s__compiler_p_s__node_p_s__expression(struct _compiler *_cc, struct _node *_source, struct _expression *_e);
static struct _constant *_new_constant_p_s__memory_bundle_in(struct _memory_bundle *_bundle, int _value);
static struct _aswitch_item *_new_switch_item_p_s__memory_bundle_p_s__constant_p_s__collection(struct _memory_bundle *_bundle, struct _constant *_constants, struct _collection *_block);
static struct _aswitch_item *_new_switch_item_p_s__memory_bundle_p_s__collection_p_s__collection(struct _memory_bundle *_bundle, struct _collection *_constants, struct _collection *_block);
static struct _aswitch_item *_new_switch_item_p_s__memory_bundle_p_s__collection(struct _memory_bundle *_bundle, struct _collection *_block);
static struct _aswitch_expression *_new_switch_expression_p_s__memory_bundle_p_s__expression_p_s__collection(struct _memory_bundle *_bundle, struct _expression *_expression, struct _collection *_rules);
static int _is_reference_p_s__expression(struct _expression *_e);
static struct _type *_effective_type_p_s__type(struct _type *_t);
static int _check_size_p_s__compiler_p_s__node_p_s__type(struct _compiler *_cc, struct _node *_source, struct _type *_type);
static int _do_define_variable_p_s__compiler_p_s__variable(struct _compiler *_cc, struct _variable *_variable);
static int _define_variable_p_s__compiler_p_s__variable(struct _compiler *_cc, struct _variable *_variable);
static struct _operator *_find_operator_p_a_ch(char *_name);
static int _size_p_s__collection(struct _collection *_c);
static int _check_arguments_p_s__compiler_p_s__node_p_a_ch_p_s__collection_of_arguments(struct _compiler *_cc, struct _node *_source, char *_name, struct _collection_of_arguments *_arguments);
static struct _equate *_new_equate_p_s__memory_bundle_p_a_ch_p_s__signature_p_s__collection_of_arguments_bo_p_vo_p_s__equate_node(struct _memory_bundle *_bundle, char *_name, struct _signature *_signature, struct _collection_of_arguments *_arguments, int _expose, void *_module, struct _equate_node *_source);
static struct _signature *_new_signature_from_p_s__compiler_p_s__collection_of_arguments(struct _compiler *_cc, struct _collection_of_arguments *_arguments);
static struct _argument *_new_argument_p_s__memory_bundle_p_a_ch_p_s__type_bo_p_vo_p_s__node(struct _memory_bundle *_bundle, char *_name, struct _type *_type, int _is_return, void *_module, struct _node *_source);
static struct _extension *_new_extension_p_s__memory_bundle_p_s__signature_p_s__type_p_s__definition_p_s__collection_of_arguments(struct _memory_bundle *_bundle, struct _signature *_signature, struct _type *_returned_type, struct _definition *_definition, struct _collection_of_arguments *_arguments);
static void _declare_extension_p_s__compiler_p_s__definition_p_s__collection_of_arguments_bo(struct _compiler *_cc, struct _definition *_source_definition, struct _collection_of_arguments *_source_arguments, int _local);
static struct _equate *_declare_equate_p_s__compiler_p_s__equate_node_bo_bo(struct _compiler *_cc, struct _equate_node *_source, int _expose, int _local);
static struct _expression *_new_ignore_expression_p_s__memory_bundle(struct _memory_bundle *_bundle);
static struct _iterator *_new_iterator_p_s__memory_bundle_p_a_ch_p_s__signature_p_s__collection_of_arguments_bo_p_vo_p_s__iterator_node(struct _memory_bundle *_bundle, char *_name, struct _signature *_signature, struct _collection_of_arguments *_arguments, int _expose, void *_module, struct _iterator_node *_source);
static struct _iterator *_declare_iterator_p_s__compiler_p_s__iterator_node_bo_bo(struct _compiler *_cc, struct _iterator_node *_source, int _expose, int _local);
static int _add_local_type_p_s__compiler_p_s__named_type(struct _compiler *_cc, struct _named_type *_type);
static struct _long_expression *_new_litteral_long_expression_p_s__memory_bundle_p_s__type_dw(struct _memory_bundle *_bundle, struct _type *_type, unsigned int _value);
static struct _string_expression *_new_string_expression_p_s__memory_bundle_p_s__type_e__expression_code_p_a_ch(struct _memory_bundle *_bundle, struct _type *_type, int _opcode, char *_value);
static struct _string_expression *_new_litteral_string_expression_p_s__memory_bundle_p_s__type_p_a_ch(struct _memory_bundle *_bundle, struct _type *_type, char *_value);
static int _match_p_s__collection_of_expressions_p_s__definition(struct _collection_of_expressions *_arguments, struct _definition *_definition);
static int _match_p_s__definition_p_s__definition(struct _definition *_d1, struct _definition *_d2);
static struct _definition *_find_definition_p_s__compiler_p_s__node_p_a_ch_p_s__collection_of_expressions(struct _compiler *_cc, struct _node *_source, char *_name, struct _collection_of_expressions *_arguments);
static struct _definition_expression *_new_definition_expression_p_s__memory_bundle_p_s__definition_p_s__collection_of_expressions(struct _memory_bundle *_bundle, struct _definition *_definition, struct _collection_of_expressions *_arguments);
static struct _type *_check_array_p_s__compiler_p_s__node_p_s__type(struct _compiler *_cc, struct _node *_source, struct _type *_type);
static int _add_local_definitions_p_s__compiler_p_s__argument(struct _compiler *_cc, struct _argument *_arguments);
static struct _collection_of_expressions *_get_sequence_of_instructions_p_s__compiler_p_s__collection(struct _compiler *_cc, struct _collection *_source);
static int _do_define_iterator_p_s__compiler_p_s__iterator(struct _compiler *_cc, struct _iterator *_iterator);
static int _define_iterator_p_s__compiler_p_s__iterator(struct _compiler *_cc, struct _iterator *_iterator);
static struct _collection_of_arguments *_new_collection_of_arguments_p_s__memory_bundle(struct _memory_bundle *_bundle);
static void _set_values_of_arguments_p_s__argument_p_s__expression(struct _argument *_arguments, struct _expression *_values);
static void _collection_of_expressions_p_s__collection_of_expressions(struct _collection_of_expressions *_c);
static struct _function_expression *_new_function_expression_p_s__memory_bundle_p_s__type_e__expression_code_p_s__function(struct _memory_bundle *_bundle, struct _type *_type, int _opcode, struct _function *_value);
static struct _type *_reference_type_p_s__function(struct _function *_f);
static void _check_assign_p_s__compiler_p_s__node_p_s__expression_p_s__expression(struct _compiler *_cc, struct _node *_source, struct _expression *_dst, struct _expression *_src);
static void _check_lvalue_p_s__compiler_p_s__node_p_s__expression(struct _compiler *_cc, struct _node *_source, struct _expression *_e);
static struct _long_expression *_new_bool_expression_p_s__memory_bundle_bo(struct _memory_bundle *_bundle, int _value);
static struct _long_expression *_new_nil_expression_p_s__memory_bundle(struct _memory_bundle *_bundle);
static struct _long_expression *_new_true_expression_p_s__memory_bundle(struct _memory_bundle *_bundle);
static struct _long_expression *_new_false_expression_p_s__memory_bundle(struct _memory_bundle *_bundle);
static struct _definition_expression *_new_definition_expression_p_s__memory_bundle_p_s__definition_p_s__collection_of_expressions_p_s__type_bo(struct _memory_bundle *_bundle, struct _definition *_definition, struct _collection_of_expressions *_arguments, struct _type *_type, int _lvalue);
static struct _expression *_build_definition_expression_p_s__compiler_p_s__node_p_s__definition_p_s__collection_of_expressions_p_s__type_bo(struct _compiler *_cc, struct _node *_source, struct _definition *_definition, struct _collection_of_expressions *_arguments, struct _type *_type, int _lvalue);
static int _is_variable_p_s__expression(struct _expression *_e);
static int _is_constant_p_s__expression(struct _expression *_expression);
static struct _expression *_check_initializer_p_s__compiler_p_s__node_p_s__expression_p_s__type(struct _compiler *_cc, struct _node *_source, struct _expression *_expression, struct _type *_type);
static struct _collection_expression *_new_collection_expression_p_s__memory_bundle_p_s__type_e__expression_code_p_s__collection_of_expressions(struct _memory_bundle *_bundle, struct _type *_type, int _opcode, struct _collection_of_expressions *_value);
static struct _variable *_new_variable_p_s__memory_bundle_p_s__variable_bo(struct _memory_bundle *_bundle, struct _variable *_old, int _expose);
static struct _variable *_expand_variable_p_s__compiler_p_s__node_p_s__variable(struct _compiler *_cc, struct _node *_source, struct _variable *_old_variable);
static struct _condition_operator_expression *_new_condition_operator_expression_p_s__memory_bundle_p_s__type_p_s__expression_p_s__expression_p_s__expression(struct _memory_bundle *_bundle, struct _type *_type, struct _expression *_condition, struct _expression *_then_expression, struct _expression *_otherwise_expression);
static struct _yield_expression *_new_yield_expression_p_s__memory_bundle_p_s__collection_of_expressions_p_s__iterator(struct _memory_bundle *_bundle, struct _collection_of_expressions *_arguments, struct _iterator *_iterator);
static struct _sequence_expression *_new_sequence_expression_p_s__memory_bundle_p_s__collection_of_expressions(struct _memory_bundle *_bundle, struct _collection_of_expressions *_sequence);
static struct _expression *_expand_expression_p_s__compiler_p_s__node_p_s__expression(struct _compiler *_cc, struct _node *_source, struct _expression *_s);
static struct _collection_of_expressions *_expand_expressions_p_s__compiler_p_s__node_p_s__expression_p_s__collection_of_expressions(struct _compiler *_cc, struct _node *_source, struct _expression *_s, struct _collection_of_expressions *_block);
static struct _collection_expression *_build_iterator_call_p_s__compiler_p_s__node_p_a_ch_p_s__collection_of_expressions_p_s__iterator_p_s__collection_of_arguments_p_s__collection_of_expressions(struct _compiler *_cc, struct _node *_source, char *_name, struct _collection_of_expressions *_arguments, struct _iterator *_iterator, struct _collection_of_arguments *_parameters, struct _collection_of_expressions *_block);
static int _do_define_equate_p_s__compiler_p_s__equate(struct _compiler *_cc, struct _equate *_equate);
static int _define_equate_p_s__compiler_p_s__equate(struct _compiler *_cc, struct _equate *_equate);
static void _define_p_s__function_p_s__collection_of_expressions(struct _function *_f, struct _collection_of_expressions *_instructions);
static int _always_return_p_s__expression(struct _expression *_block);
static int _do_define_function_p_s__compiler_p_s__function(struct _compiler *_cc, struct _function *_function);
static int _define_function_p_s__compiler_p_s__function(struct _compiler *_cc, struct _function *_function);
static struct _variable *_new_variable_p_s__memory_bundle_p_a_ch_p_s__type_p_s__set_of_directives_p_vo_p_s__node(struct _memory_bundle *_bundle, char *_name, struct _type *_type, struct _set_of_directives *_directives, void *_module, struct _node *_source);
static struct _expression *_build_expression_call_p_s__compiler_p_s__node_p_s__definition_p_s__collection_of_expressions(struct _compiler *_cc, struct _node *_source, struct _definition *_definition, struct _collection_of_expressions *_arguments);
static struct _expression *_build_expression_call_p_s__compiler_p_s__node_p_a_ch_p_s__collection_of_expressions(struct _compiler *_cc, struct _node *_source, char *_name, struct _collection_of_expressions *_arguments);
static struct _signature *_new_signature_from_p_s__compiler_p_s__collection_of_expressions(struct _compiler *_cc, struct _collection_of_expressions *_arguments);
static struct _type *_check_initializer_type_p_s__compiler_p_s__node_p_s__type(struct _compiler *_cc, struct _node *_source, struct _type *_type);
static struct _collection_expression *_get_initializer_p_s__compiler_p_s__initializer_node(struct _compiler *_cc, struct _initializer_node *_source);
static void _append_p_s__element_p_s__element(struct _element *_e1, struct _element *_e2);
static struct _expression *_get_expression_p_s__compiler_p_s__node(struct _compiler *_cc, struct _node *_node);
static int _get_constant_integer_p_s__compiler_p_s__node(struct _compiler *_cc, struct _node *_source);
static void _collection_of_arguments_p_s__collection_of_arguments(struct _collection_of_arguments *_c);
static struct _type *_get_registerable_type_or_void_p_s__compiler_p_s__node(struct _compiler *_cc, struct _node *_source);
static struct _enumeration_item *_new_enumeration_item_p_s__memory_bundle_p_a_ch_p_s__type_in_bo_p_vo_p_s__node(struct _memory_bundle *_bundle, char *_name, struct _type *_type, int _value, int _expose, void *_module, struct _node *_source);
static void _define_enumeration_p_s__compiler_p_s__enumeration_type(struct _compiler *_cc, struct _enumeration_type *_enumeration);
static struct _structure_type *_get_structure_p_s__compiler_p_s__node_bo(struct _compiler *_cc, struct _node *_source, int _is_union);
static struct _attribute *_new_attribute_p_s__memory_bundle_p_a_ch_p_s__type_bo_p_s__structure_type_p_vo_p_s__attribute_node(struct _memory_bundle *_bundle, char *_name, struct _type *_type, int _expose, struct _structure_type *_structure, void *_module, struct _attribute_node *_source);
static void _define_p_s__structure_type_p_s__structure_type_p_s__collection(struct _structure_type *_t, struct _structure_type *_parent, struct _collection *_attributes);
static void _define_p_s__structure_type_p_s__structure_type(struct _structure_type *_t, struct _structure_type *_parent);
static void _define_structure_p_s__compiler_p_s__structure_type(struct _compiler *_cc, struct _structure_type *_structure);
static struct _type *_get_type_p_s__compiler_p_s__node(struct _compiler *_cc, struct _node *_node);
static struct _type *_get_registerable_type_p_s__compiler_p_s__node(struct _compiler *_cc, struct _node *_source);
static struct _collection_of_arguments *_get_arguments_p_s__compiler_p_s__collection_p_a_in(struct _compiler *_cc, struct _collection *_source, int *_returned_arguments);
static struct _function *_new_function_p_s__memory_bundle_p_s__signature_p_s__collection_of_arguments_p_s__type_bo_p_s__set_of_directives_p_vo_p_s__function_node(struct _memory_bundle *_bundle, struct _signature *_signature, struct _collection_of_arguments *_arguments, struct _type *_returned_type, int _expose, struct _set_of_directives *_directives, void *_module, struct _function_node *_source);
static struct _function *_declare_function_p_s__compiler_p_s__function_node_bo_p_s__set_of_directives(struct _compiler *_cc, struct _function_node *_source, int _expose, struct _set_of_directives *_base_directives);
static void _declare_definitions_p_s__compiler_p_s__collection_of_program_elements(struct _compiler *_cc, struct _collection_of_program_elements *_list);
static void _define_types_p_s__compiler_p_s__collection_of_program_elements(struct _compiler *_cc, struct _collection_of_program_elements *_list);
static void _define_definitions_p_s__compiler_p_s__collection_of_program_elements(struct _compiler *_cc, struct _collection_of_program_elements *_list);
static int _start_p_s__compiler_p_s__include_directory(struct _compiler *_cc, struct _include_directory *_path);
static void _release_p_s__vector(struct _vector *_v);
static void _release_p_s__compiler(struct _compiler *_cc);
static struct _module *_load_module_p_s__compiler_session_p_a_ch_p_s__include_directory(struct _compiler_session *_self, char *_filename, struct _include_directory *_path);
static struct _module *_start_p_s__compiler_session_p_s__memory_bundle_p_a_ch_p_s__list_p_s__dictionary_bo(struct _compiler_session *_self, struct _memory_bundle *_bundle, char *_filename, struct _list *_path, struct _dictionary *_defines, int _unicode);
static void _initialize_p_s__litteral_string_dictionary(struct _litteral_string_dictionary *_d);
static void _initialize_p_s__c_dump_p_s__memory_bundle(struct _c_dump *_d, struct _memory_bundle *_bundle);
static char *_value_p_s__set_of_directives_p_a_ch(struct _set_of_directives *_s, char *_name);
static int _naming_convention_p_s__set_of_directives(struct _set_of_directives *_s);
static void _append_identifier_p_s__string_buffer_p_a_ch(struct _string_buffer *_sb, char *_s);
static void _scan_type_p_s__c_dump_p_s__type(struct _c_dump *_d, struct _type *_type);
static void _flush_identifier_p_s__string_buffer_p_s__type(struct _string_buffer *_sb, struct _type *_type);
static void _scan_definition_p_s__c_dump_p_s__definition(struct _c_dump *_d, struct _definition *_definition);
static void _scan_sub_module_p_s__c_dump_p_s__module(struct _c_dump *_d, struct _module *_m);
static struct _global_constant *_new_global_constant_p_s__memory_bundle_bo_p_a_ch(struct _memory_bundle *_bundle, int _expose, char *_name);
static char *_get_name_p_s__c_dump(struct _c_dump *_d);
static void _scan_litteral_string_p_s__c_dump_p_s__string_expression(struct _c_dump *_d, struct _string_expression *_e);
static void _scan_initializer_p_s__c_dump_p_s__collection_expression(struct _c_dump *_d, struct _collection_expression *_initializer);
static void _scan_expression_p_s__c_dump_p_s__expression(struct _c_dump *_d, struct _expression *_e);
static void _scan_list_of_expressions_p_s__c_dump_p_s__expression(struct _c_dump *_d, struct _expression *_expressions);
static struct _string_buffer *_append_p_s__string_label_p_s__string_buffer(struct _string_label *_self, struct _string_buffer *_stream);
static void _string_label_p_a_ch_p_s__string_label(char *_s, struct _string_label *_self);
static struct _string_buffer *_append_p_s__pointer_label_p_s__string_buffer(struct _pointer_label *_self, struct _string_buffer *_stream);
static void _pointer_label_p_s__label_p_s__pointer_label(struct _label *_inner, struct _pointer_label *_self);
static struct _string_buffer *_append_p_s__array_label_p_s__string_buffer(struct _array_label *_self, struct _string_buffer *_stream);
static void _array_label_in_p_s__label_p_s__array_label(int _size, struct _label *_inner, struct _array_label *_self);
static struct _string_buffer *_append_p_s__function_type_label_p_s__string_buffer(struct _function_type_label *_self, struct _string_buffer *_stream);
static void _function_type_label_p_s__function_type_p_s__label_p_s__function_type_label(struct _function_type *_f, struct _label *_inner, struct _function_type_label *_self);
static void _flush_declaration_p_s__string_buffer_p_s__type_p_s__label(struct _string_buffer *_stream, struct _type *_type, struct _label *_label);
static void _flush_declaration_p_s__string_buffer_p_s__type_p_a_ch(struct _string_buffer *_stream, struct _type *_type, char *_name);
static struct _string_buffer *_append_p_s__identifier_label_p_s__string_buffer(struct _identifier_label *_self, struct _string_buffer *_stream);
static void _identifier_label_p_a_ch_p_s__identifier_label(char *_s, struct _identifier_label *_self);
static void _flush_raw_declaration_p_s__string_buffer_p_s__type_p_a_ch(struct _string_buffer *_stream, struct _type *_type, char *_name);
static struct _string_buffer *_append_p_s__function_label_p_s__string_buffer(struct _function_label *_self, struct _string_buffer *_sb);
static void _function_label_p_s__function_p_s__function_label(struct _function *_f, struct _function_label *_self);
static void _flush_function_declaration_p_s__string_buffer_p_s__function(struct _string_buffer *_stream, struct _function *_function);
static void _flush_char_p_s__string_buffer_ch(struct _string_buffer *_stream, char _c);
static void _flush_pointer_comparator_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(struct _c_dump *_d, struct _string_buffer *_stream, char *_op, struct _expression *_arguments);
static void _flush_binary_operator_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(struct _c_dump *_d, struct _string_buffer *_stream, char *_op, struct _expression *_arguments);
static void _flush_prefix_operator_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(struct _c_dump *_d, struct _string_buffer *_stream, char *_op, struct _expression *_e);
static void _flush_assignment_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(struct _c_dump *_d, struct _string_buffer *_stream, char *_op, struct _expression *_arguments);
static void _flush_unary_assignment_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(struct _c_dump *_d, struct _string_buffer *_stream, char *_op, struct _expression *_arguments);
static void _flush_postfix_operator_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(struct _c_dump *_d, struct _string_buffer *_stream, char *_op, struct _expression *_e);
static void _flush_definition_p_s__c_dump_p_s__string_buffer_p_s__definition_p_s__expression(struct _c_dump *_d, struct _string_buffer *_stream, struct _definition *_definition, struct _expression *_arguments);
static void _flush_definition_p_s__c_dump_p_s__string_buffer_p_s__expression(struct _c_dump *_d, struct _string_buffer *_stream, struct _expression *_e);
static void _flush_instruction_p_s__c_dump_p_s__string_buffer_p_s__expression(struct _c_dump *_d, struct _string_buffer *_stream, struct _expression *_e);
static void _flush_instructions_p_s__c_dump_p_s__string_buffer_p_s__expression(struct _c_dump *_d, struct _string_buffer *_stream, struct _expression *_block);
static void _flush_block_p_s__c_dump_p_s__string_buffer_p_s__expression(struct _c_dump *_d, struct _string_buffer *_stream, struct _expression *_block);
static int _flush_expression_p_s__c_dump_p_s__string_buffer_p_s__expression(struct _c_dump *_d, struct _string_buffer *_stream, struct _expression *_e);
static void _flush_expression_p_s__c_dump_p_s__string_buffer_p_s__expression_p_s__type(struct _c_dump *_d, struct _string_buffer *_stream, struct _expression *_e, struct _type *_type);
static struct _expression *_flush_initializer_p_s__c_dump_p_s__string_buffer_p_s__expression_p_s__type(struct _c_dump *_d, struct _string_buffer *_stream, struct _expression *_expression, struct _type *_type);
static void _flush_body_p_s__c_dump_p_s__string_buffer_p_s__function(struct _c_dump *_d, struct _string_buffer *_stream, struct _function *_function);
static void _flush_module_p_s__c_dump_p_s__string_buffer_p_s__module(struct _c_dump *_d, struct _string_buffer *_stream, struct _module *_module);
extern unsigned int fwrite(void *, unsigned int , unsigned int , struct _file *);
static int _write_content_p_a_ch_p_a_ch_dw(char *_filename, char *_base, unsigned int _len);
static void _release_p_s__c_dump(struct _c_dump *_d);
static void _release_p_s__memory_bundle(struct _memory_bundle *_bundle);
static int _start_in_p_a_p_a_ch(int _argc, char **_argv);
extern void free(void *);
static void _release_allocator();
extern int main(int _argc, char **_argv);

/* Declare all variables */

static int _debug1;
static unsigned char *_first_block2;
static unsigned int _total_allocated3;
static unsigned int _max_allocated4;
static unsigned int _current_allocated7;
static struct _keyword (_keywords10)[22];
static struct _operator (_operators33)[37];
static char *_op_add36;
static char *_op_sub38;
static char *_op_mul40;
static char *_op_div42;
static char *_op_mod44;
static char *_op_shl46;
static char *_op_shr48;
static char *_op_eq50;
static char *_op_ne52;
static char *_op_le54;
static char *_op_lt56;
static char *_op_ge58;
static char *_op_gt60;
static char *_op_and62;
static char *_op_xor64;
static char *_op_or66;
static char *_op_andand68;
static char *_op_oror70;
static char *_op_assign_add72;
static char *_op_assign_sub74;
static char *_op_assign_mul76;
static char *_op_assign_div78;
static char *_op_assign_mod80;
static char *_op_assign_shl82;
static char *_op_assign_shr84;
static char *_op_assign_and86;
static char *_op_assign_or88;
static char *_op_assign_xor90;
static char *_op_assign_neg92;
static char *_op_assign_not94;
static char *_op_not96;
static char *_op_pre_inc98;
static char *_op_pre_dec100;
static char *_op_post_inc102;
static char *_op_post_dec104;
static char *_op_at106;
static struct _signature _empty_sig107;
static struct _named_type *_nil_type108;
static char *_tn_nil113;
static struct _named_type *_void_type114;
static char *_tn_void116;
static struct _named_type *_char_type117;
static char *_tn_achar119;
static struct _named_type *_wchar_type120;
static char *_tn_wchar122;
static struct _named_type *_bool_type123;
static char *_tn_bool125;
static struct _named_type *_byte_type126;
static char *_tn_byte128;
static struct _named_type *_word_type129;
static char *_tn_word131;
static struct _named_type *_dword_type132;
static char *_tn_dword134;
static struct _named_type *_qword_type135;
static char *_tn_qword137;
static struct _named_type *_octet_type138;
static char *_tn_octet140;
static struct _named_type *_short_type141;
static char *_tn_short143;
static struct _named_type *_int_type144;
static char *_tn_int146;
static struct _named_type *_long_type147;
static char *_tn_long149;
static struct _type *_string_type152;
static struct _type *_wstring_type156;
static struct _dictionary_class _type_dictionary_class157;
static struct _dictionary_class _definition_dictionary_class163;
static struct _dictionary_class _litteral_string_dictionary_class164;
static char *_empty_string171;
static char *_tn_char225;
static char *_op_assign247;
static char *_error_cannot_open_file306;
static char *_error_syntax_error433;
static char *_error_token_unexpected440;
static char *_error_cant_open_file630;
static char *_error_circular_dependency632;
static char *_error_already_defined656;
static char *_error_first_definition658;
static char *_error_undefined_type718;
static char *_error_circular_typedefs720;
static char *_error_bad_return731;
static char *_error_return_value735;
static char *_error_bad_return_type748;
static char *_error_must_return_value750;
static char *_error_incompatible_ret771;
static char *_error_invalid_condition782;
static char *_error_case_type816;
static char *_error_not_integer819;
static char *_error_not_constant821;
static char *_error_not_char823;
static char *_error_not_wchar825;
static char *_error_case_not_constant827;
static char *_error_circular_dependency_variable835;
static char *_error_must_be_initializer843;
static char *_error_type_undefined845;
static char *_error_too_many_returns851;
static char *_error_too_few_args859;
static char *_error_too_many_args861;
static char *_error_unknown_operator863;
static char *_error_no_return_arg_in_iterator883;
static char *_error_ambigous907;
static char *_error_def_not_found910;
static char *_error_super_no_struct918;
static char *_error_super_no_parent921;
static char *_error_unknown_size926;
static char *_error_cant_diff935;
static char *_error_circular_dependency_equate938;
static char *_error_missing_block957;
static char *_error_invalid_break977;
static char *_error_invalid_continue979;
static char *_error_cant_assign983;
static char *_error_must_be_lvalue985;
static char *_error_signed_mismatch992;
static char *_error_divide_by_zero1014;
static char *_error_too_few_arguments1039;
static char *_error_incompatible_array_size1044;
static char *_error_incompatible_types1046;
static char *_error_circular_dependency_function1097;
static char *_error_missing_return1118;
static char *_error_not_iterator1142;
static char *_error_unexpected_block1145;
static char *_error_too_many_vars1152;
static char *_error_too_few_vars1154;
static char *_error_invalid_yield1161;
static char *_error_yield_signature1169;
static char *_error_no_return_arg_in_references1174;
static char *_error_function_not_found1179;
static char *_error_no_function1181;
static char *_error_invalid_initializer_type1188;
static char *_error_too_many_arguments1194;
static char *_error_cast_void1200;
static char *_error_1st_not_function1208;
static char *_error_bad_arg_number1213;
static char *_error_bad_arg_type1217;
static char *_error_circular_dependency_type1236;
static char *_error_not_an_union1256;
static char *_error_not_a_struct1258;
static char *_error_parent_undefined1261;
static char *_error_must_be_a_pointer_to_struct_or_array1282;
static char T1377[]="unsigned long long";
static char T1378[]="long long";
static char T862[]="unknown operator \"#s\"";
static char T83[]="@assign shr";
static char T127[]="byte";
static char T81[]="@assign shl";
static char T1445[]="((char)";
static char T53[]="@less or equal";
static char T148[]="long";
static char T820[]="the expression must be a constant";
static char T1495[]="switch(";
static char T21[]="private";
static char T1255[]="\'#t\' is not an union";
static char T1153[]="too few arguments after \'?\'";
static char T133[]="dword";
static char T219[]="Usage:\n  zc [-I include-dir]* [-debug] [-o output-file] <filename>\nor\n  zc -v\n";
static char T1465[]="(void*)";
static char T1464[]="!=(void*)0)";
static char T79[]="@assign mod";
static char T1468[]=" = -";
static char T93[]="@assign not";
static char T1496[]=")\n{\n";
static char T91[]="@assign neg";
static char T15[]="struct";
static char T1260[]="can\'t define \'#t\' if parent structure is undefined";
static char T818[]="the expression does not evaluate to an integer";
static char T1374[]="unsigned char";
static char T369[]="<an identifier>";
static char T1518[]="\n/* Define functions */\n\n";
static char T294[]="struct ";
static char T71[]="@assign add";
static char T1096[]="recursive call of \'#s\', the return type must be explicitely declared";
static char T103[]="@postdec";
static char T920[]="super: \'#t\' has no parent";
static char T1523[]="}\n\n";
static char T991[]="signed/unsigned mismatch (\'#t\', \'#t\')";
static char T75[]="@assign mul";
static char T77[]="@assign div";
static char T925[]="unknown size for: #t";
static char T85[]="@assign and";
static char T367[]="<invalid>";
static char T1141[]="unexpected block, \'#s\' is not an iterator";
static char T937[]="recursive call of the equate \'#s\' is not permitted";
static char T1446[]="((short)";
static char T1514[]="};\n";
static char T1462[]="((void*)";
static char T101[]="@postinc";
static char T26[]="while";
static char T984[]="operand must be a l-value";
static char T1528[]="Error while writing output file\n";
static char T1392[]="*** unknown type ***";
static char T16[]="union";
static char T569[]="<anonymous enum>";
static char T747[]="bad return type: a \'#t\' is not a \'#t\'";
static char T1467[]=" = ~";
static char T1401[]="extern ";
static char T65[]="@or";
static char T1447[]="((long long)";
static char T956[]="missing block";
static char T173[]="a.c";
static char T906[]="several definitions found for: #s#u";
static char T105[]="@at";
static char T1417[]="[]=\"";
static char T1479[]="if(";
static char T1257[]="\'#t\' is not a struct";
static char T781[]="the condition is not a boolean expression";
static char T246[]="@assign";
static char T730[]="return statement is valid only in functions";
static char T250[]="not nil";
static char T121[]="wchar";
static char T982[]="can\'t assign a \'#t\' to a \'#t\'";
static char T1317[]="__anonymous_";
static char T1430[]="[]=L\"";
static char T59[]="@greater than";
static char T1193[]="too many arguments";
static char T1187[]="invalid initializer type";
static char T95[]="@not";
static char T295[]="union ";
static char T1491[]="while(1)\n";
static char T1212[]="function call: The argument number does not match with the function reference";
static char T142[]="short";
static char T1379[]="void*";
static char T1311[]="entry point";
static char T43[]="@mod";
static char T834[]="recursive evaluation of the variable \'#s\'";
static char T1045[]="incompatible types: a \'#t\' is not a \'#t\'";
static char T39[]="@mul";
static char T145[]="int";
static char T41[]="@div";
static char T882[]="return argument not allowed in iterator";
static char T18[]="typedef";
static char T13[]="equ";
static char T770[]="incompatible return types";
static char T235[]="nil";
static char T61[]="@and";
static char T1173[]="return argument not allowed in function references";
static char T1500[]="default:\n";
static char T370[]="<a string>";
static char T1180[]="\'#s\' is not a function";
static char T1235[]="circular dependency with definition of \'#t\'";
static char T1489[]="while(";
static char T1542[]="Max allocated........: ";
static char T35[]="@add";
static char T30[]="end";
static char T1531[]="================================\n";
static char T87[]="@assign or";
static char T19[]="sizeof";
static char T170[]="";
static char T97[]="@preinc";
static char T216[]="Invalid define (-D): missing \'=\' sign";
static char T136[]="qword";
static char T372[]="<a char>";
static char T12[]="def";
static char T249[]="is nil";
static char T37[]="@sub";
static char T1364[]="/* Define structures */\n\n";
static char T1393[]="__parent";
static char T63[]="@xor";
static char T248[]="super";
static char T734[]="procedures don\'t return value";
static char T1013[]="divide by zero";
static char T45[]="@shl";
static char T1424[]="\\\\";
static char T1463[]="==(void*)0)";
static char T1144[]="unexpected block, \'#s\' does not include any yield";
static char T99[]="@predec";
static char T439[]="#k unexpected";
static char T536[]="yield";
static char T1482[]="else if(";
static char T842[]="the exression must have a returned argument or must be an initializer";
static char T1376[]="unsigned int";
static char T1486[]="}\n";
static char T1484[]="{\n";
static char T1371[]="\n{\n";
static char T112[]="nil type";
static char T293[]="-> ";
static char T47[]="@shr";
static char T1442[]="((unsigned short)";
static char T406[]="|=";
static char T1543[]="Left.................: ";
static char T1420[]="\\x";
static char T55[]="@less than";
static char T379[]="\\t";
static char T380[]="\\v";
static char T381[]="\\w";
static char T1332[]="OP";
static char T378[]="\\r";
static char T1168[]="yield: different signatures";
static char T377[]="\\n";
static char T1199[]="can\'t cast a void expression";
static char T130[]="word";
static char T1444[]="((unsigned long long)";
static char T749[]="the function must return a value";
static char T657[]="first definition";
static char T409[]="~!";
static char T376[]="\\f";
static char T1151[]="too many arguments after \'?\'";
static char T1425[]="\";\n";
static char T374[]="\\a";
static char T300[]="} ";
static char T375[]="\\b";
static char T858[]="too few arguments for \"#s\"";
static char T115[]="void";
static char T1343[]="ch";
static char T1178[]="\'#s\' unknown";
static char T31[]="return";
static char T1416[]="char ";
static char T1429[]="unsigned short ";
static char T1334[]="bo";
static char T24[]="if";
static char T51[]="@not equal";
static char T1342[]="ln";
static char T717[]="undefined type: \'#s\'";
static char T631[]="can\'t import module: circular dependency";
static char T1341[]="in";
static char T1335[]="by";
static char T824[]="the expression does not evaluate to a wchar";
static char T217[]="Zc 1.3";
static char T1337[]="dw";
static char T1339[]="oc";
static char T1347[]="s_";
static char T1348[]="p_";
static char T397[]="||";
static char T417[]="<end-of-line>";
static char T218[]="filename missing\n";
static char T271[]="rb";
static char T1483[]="else\n";
static char T407[]="^=";
static char T1336[]="wo";
static char T1345[]="vo";
static char T1400[]="static ";
static char T860[]="too many arguments for \"#s\"";
static char T1344[]="wc";
static char T1340[]="sh";
static char T1524[]="wb";
static char T1532[]="Allocated in bundle...: ";
static char T180[]="-debug";
static char T404[]=">>=";
static char T1338[]="qw";
static char T978[]="invalid continue";
static char T1346[]="e_";
static char T118[]="achar";
static char T1441[]="((unsigned char)";
static char T1501[]="break;\n}\n";
static char T1350[]="f_";
static char T1474[]="])";
static char T1349[]="a_";
static char T1498[]="case ";
static char T403[]="<<=";
static char T305[]="can\'t open input file: #s";
static char T1423[]="\\\'";
static char T296[]="] ";
static char T22[]="const";
static char T430[]="->";
static char T1422[]="\\\"";
static char T399[]="-=";
static char T431[]="<unknown>";
static char T382[]="(c)";
static char T629[]="can\'t open input file \'#s\'";
static char T1121[]="t";
static char T401[]="/=";
static char T1117[]="\'#s\' does not always return a value";
static char T426[]="{";
static char T32[]="repeat";
static char T393[]="~";
static char T391[]="|";
static char T427[]="}";
static char T400[]="*=";
static char T1315[]="c";
static char T396[]="&&";
static char T398[]="+=";
static char T909[]="no definition found for: #s#u";
static char T258[]="true";
static char T23[]="import";
static char T395[]="--";
static char T402[]="%=";
static char T405[]="&=";
static char T844[]="\'#t\' can not be used since it is not defined";
static char T20[]="public";
static char T69[]="@oror";
static char T394[]="++";
static char T1043[]="incompatible size of arrays";
static char T303[]=", ";
static char T410[]="-!";
static char T388[]="<<";
static char T413[]="<=";
static char T49[]="@equal";
static char T25[]="elsif";
static char T17[]="enum";
static char T1357[]="T";
static char T411[]="==";
static char T389[]=">>";
static char T29[]="else";
static char T428[]="[";
static char T1038[]="too few arguments";
static char T124[]="bool";
static char T28[]="case";
static char T392[]="^";
static char T429[]="]";
static char T412[]="<>";
static char T415[]=">=";
static char T290[]=": ";
static char T1367[]=";\n";
static char T1385[]=")(";
static char T237[]="continue";
static char T224[]="char";
static char T57[]="@greater or equal";
static char T259[]="false";
static char T1375[]="unsigned short";
static char T1440[]="((";
static char T1449[]="(&";
static char T1461[]="(*";
static char T822[]="the expression does not evaluate to a char";
static char T1466[]="!=";
static char T371[]="<a wstring>";
static char T655[]="\'#s\' already defined:";
static char T1475[]="*** unknown definition opcode ***";
static char T1473[]="0";
static char T1481[]=")\n";
static char T1499[]=":\n";
static char T1513[]="*** unknown expression opcode ***";
static char T181[]="-v";
static char T289[]=":";
static char T368[]="<end-of-file>";
static char T685[]="local ";
static char T182[]="-u";
static char T416[]=">";
static char T422[]="?";
static char T414[]="<";
static char T408[]="=";
static char T183[]="-o";
static char T421[]="#";
static char T1370[]=" ";
static char T420[]="!";
static char T390[]="&";
static char T815[]="case: invalid type \'#t\' (must be a \'#t\')";
static char T423[]="$";
static char T387[]="%";
static char T385[]="*";
static char T383[]="+";
static char T424[]="(";
static char T425[]=")";
static char T73[]="@assign sub";
static char T386[]="/";
static char T418[]=".";
static char T384[]="-";
static char T419[]=",";
static char T432[]="Syntax Error: Expecting #k but found #k";
static char T826[]="case: invalid constant";
static char T934[]="cannot substract \'#t\' to \'#t\'";
static char T89[]="@assign xor";
static char T1281[]="\'#s\' must be a pointer to a struct or an array";
static char T1216[]="function call: Bad argument type, a \'#t\' is not a \'#t\'";
static char T850[]="too many returns";
static char T139[]="octet";
static char T14[]="func";
static char T625[]=".zc";
static char T236[]="break";
static char T1403[]="call";
static char T917[]="super: \'#t\' is not a struct";
static char T1410[]="\n/* Declare all variables */\n\n";
static char T1443[]="((unsigned int)";
static char T1397[]="\n/* Declare all functions */\n\n";
static char T1515[]="\n/* Define variables */\n\n";
static char T719[]="circular dependency with typedef \'#t\' ";
static char T976[]="invalid break";
static char T1207[]="function call: The first argument must be a function reference";
static char T1160[]="yield: must be in iterator";
static char T192[]="-I";
static char T304[]="\n";
static char T1313[]="name";
static char T194[]="-D";
static char T1541[]="Total allocated......: ";
static char T67[]="@andand";
static char T27[]="switch";
static struct _dictionary_class T169={((int (*)(void *))_hash_p_a_ch),((int (*)(void *,void *))_is_equal_p_a_ch_p_a_ch)};
static struct _collection _empty_collection351={((struct _element *)0),((struct _element *)0)};

/* Define variables */

static int _debug1=0;
static unsigned char *_first_block2;
static unsigned int _total_allocated3;
static unsigned int _max_allocated4;
static unsigned int _current_allocated7;
static struct _keyword (_keywords10)[22];
static struct _operator (_operators33)[37];
static char *_op_add36=T35;
static char *_op_sub38=T37;
static char *_op_mul40=T39;
static char *_op_div42=T41;
static char *_op_mod44=T43;
static char *_op_shl46=T45;
static char *_op_shr48=T47;
static char *_op_eq50=T49;
static char *_op_ne52=T51;
static char *_op_le54=T53;
static char *_op_lt56=T55;
static char *_op_ge58=T57;
static char *_op_gt60=T59;
static char *_op_and62=T61;
static char *_op_xor64=T63;
static char *_op_or66=T65;
static char *_op_andand68=T67;
static char *_op_oror70=T69;
static char *_op_assign_add72=T71;
static char *_op_assign_sub74=T73;
static char *_op_assign_mul76=T75;
static char *_op_assign_div78=T77;
static char *_op_assign_mod80=T79;
static char *_op_assign_shl82=T81;
static char *_op_assign_shr84=T83;
static char *_op_assign_and86=T85;
static char *_op_assign_or88=T87;
static char *_op_assign_xor90=T89;
static char *_op_assign_neg92=T91;
static char *_op_assign_not94=T93;
static char *_op_not96=T95;
static char *_op_pre_inc98=T97;
static char *_op_pre_dec100=T99;
static char *_op_post_inc102=T101;
static char *_op_post_dec104=T103;
static char *_op_at106=T105;
static struct _signature _empty_sig107;
static struct _named_type *_nil_type108;
static char *_tn_nil113=T112;
static struct _named_type *_void_type114;
static char *_tn_void116=T115;
static struct _named_type *_char_type117;
static char *_tn_achar119=T118;
static struct _named_type *_wchar_type120;
static char *_tn_wchar122=T121;
static struct _named_type *_bool_type123;
static char *_tn_bool125=T124;
static struct _named_type *_byte_type126;
static char *_tn_byte128=T127;
static struct _named_type *_word_type129;
static char *_tn_word131=T130;
static struct _named_type *_dword_type132;
static char *_tn_dword134=T133;
static struct _named_type *_qword_type135;
static char *_tn_qword137=T136;
static struct _named_type *_octet_type138;
static char *_tn_octet140=T139;
static struct _named_type *_short_type141;
static char *_tn_short143=T142;
static struct _named_type *_int_type144;
static char *_tn_int146=T145;
static struct _named_type *_long_type147;
static char *_tn_long149=T148;
static struct _type *_string_type152;
static struct _type *_wstring_type156;
static struct _dictionary_class _type_dictionary_class157;
static struct _dictionary_class _definition_dictionary_class163;
static struct _dictionary_class _litteral_string_dictionary_class164;
static char *_empty_string171=T170;
static char *_tn_char225=T224;
static char *_op_assign247=T246;
static char *_error_cannot_open_file306=T305;
static char *_error_syntax_error433=T432;
static char *_error_token_unexpected440=T439;
static char *_error_cant_open_file630=T629;
static char *_error_circular_dependency632=T631;
static char *_error_already_defined656=T655;
static char *_error_first_definition658=T657;
static char *_error_undefined_type718=T717;
static char *_error_circular_typedefs720=T719;
static char *_error_bad_return731=T730;
static char *_error_return_value735=T734;
static char *_error_bad_return_type748=T747;
static char *_error_must_return_value750=T749;
static char *_error_incompatible_ret771=T770;
static char *_error_invalid_condition782=T781;
static char *_error_case_type816=T815;
static char *_error_not_integer819=T818;
static char *_error_not_constant821=T820;
static char *_error_not_char823=T822;
static char *_error_not_wchar825=T824;
static char *_error_case_not_constant827=T826;
static char *_error_circular_dependency_variable835=T834;
static char *_error_must_be_initializer843=T842;
static char *_error_type_undefined845=T844;
static char *_error_too_many_returns851=T850;
static char *_error_too_few_args859=T858;
static char *_error_too_many_args861=T860;
static char *_error_unknown_operator863=T862;
static char *_error_no_return_arg_in_iterator883=T882;
static char *_error_ambigous907=T906;
static char *_error_def_not_found910=T909;
static char *_error_super_no_struct918=T917;
static char *_error_super_no_parent921=T920;
static char *_error_unknown_size926=T925;
static char *_error_cant_diff935=T934;
static char *_error_circular_dependency_equate938=T937;
static char *_error_missing_block957=T956;
static char *_error_invalid_break977=T976;
static char *_error_invalid_continue979=T978;
static char *_error_cant_assign983=T982;
static char *_error_must_be_lvalue985=T984;
static char *_error_signed_mismatch992=T991;
static char *_error_divide_by_zero1014=T1013;
static char *_error_too_few_arguments1039=T1038;
static char *_error_incompatible_array_size1044=T1043;
static char *_error_incompatible_types1046=T1045;
static char *_error_circular_dependency_function1097=T1096;
static char *_error_missing_return1118=T1117;
static char *_error_not_iterator1142=T1141;
static char *_error_unexpected_block1145=T1144;
static char *_error_too_many_vars1152=T1151;
static char *_error_too_few_vars1154=T1153;
static char *_error_invalid_yield1161=T1160;
static char *_error_yield_signature1169=T1168;
static char *_error_no_return_arg_in_references1174=T1173;
static char *_error_function_not_found1179=T1178;
static char *_error_no_function1181=T1180;
static char *_error_invalid_initializer_type1188=T1187;
static char *_error_too_many_arguments1194=T1193;
static char *_error_cast_void1200=T1199;
static char *_error_1st_not_function1208=T1207;
static char *_error_bad_arg_number1213=T1212;
static char *_error_bad_arg_type1217=T1216;
static char *_error_circular_dependency_type1236=T1235;
static char *_error_not_an_union1256=T1255;
static char *_error_not_a_struct1258=T1257;
static char *_error_parent_undefined1261=T1260;
static char *_error_must_be_a_pointer_to_struct_or_array1282=T1281;

/* Define functions */

static void _initialize_allocator()
{
_first_block2=((unsigned char *)0);
_total_allocated3=0;
_max_allocated4=0;
}

static unsigned char *_allocate_memory_dw(unsigned int _size)
{
unsigned char *_next_block_1;
_next_block_1=_first_block2;
_first_block2=(((unsigned char *)malloc((_size+((unsigned int)12))))+((unsigned int)12));
(((struct _mem_head *)(_first_block2-((unsigned int)12)))->_next)=_next_block_1;
(((struct _mem_head *)(_first_block2-((unsigned int)12)))->_prev)=((unsigned char *)0);
(((struct _mem_head *)(_first_block2-((unsigned int)12)))->_size)=_size;
if(((void*)_next_block_1!=(void*)0))
{
(((struct _mem_head *)(_next_block_1-((unsigned int)12)))->_prev)=_first_block2;
}
_total_allocated3+=_size;
_current_allocated7+=_size;
if((_current_allocated7>_max_allocated4))
{
_max_allocated4=_current_allocated7;
}
return _first_block2;
}

static void _new_chunk_p_s__memory_bundle_dw(struct _memory_bundle *_bundle, unsigned int _size)
{
unsigned char *_old_base_1;
_old_base_1=(_bundle->_chunk_base);
(_bundle->_chunk_base)=(_allocate_memory_dw((_size+((unsigned int)8)))+((unsigned int)8));
(_bundle->_chunk_free)=((_bundle->_chunk_base)+_size);
(((struct _bundle_head *)((_bundle->_chunk_base)-((unsigned int)8)))->_next)=_old_base_1;
(((struct _bundle_head *)((_bundle->_chunk_base)-((unsigned int)8)))->_size)=_size;
}

static void _bundle_dw_p_s__memory_bundle(unsigned int _size, struct _memory_bundle *_bundle)
{
(_bundle->_chunk_base)=((unsigned char *)0);
(_bundle->_chunk_size)=_size;
_new_chunk_p_s__memory_bundle_dw(_bundle,_size);
(_bundle->_total)=0;
}

static void _initialize_lexer()
{
struct _keyword *_k_1;
_k_1=_keywords10;
((&(_k_1[0]))->_name)=T12;
((&(_k_1[0]))->_token)=49;
(++_k_1);
((&(_k_1[0]))->_name)=T13;
((&(_k_1[0]))->_token)=50;
(++_k_1);
((&(_k_1[0]))->_name)=T14;
((&(_k_1[0]))->_token)=54;
(++_k_1);
((&(_k_1[0]))->_name)=T15;
((&(_k_1[0]))->_token)=51;
(++_k_1);
((&(_k_1[0]))->_name)=T16;
((&(_k_1[0]))->_token)=52;
(++_k_1);
((&(_k_1[0]))->_name)=T17;
((&(_k_1[0]))->_token)=53;
(++_k_1);
((&(_k_1[0]))->_name)=T18;
((&(_k_1[0]))->_token)=56;
(++_k_1);
((&(_k_1[0]))->_name)=T19;
((&(_k_1[0]))->_token)=57;
(++_k_1);
((&(_k_1[0]))->_name)=T20;
((&(_k_1[0]))->_token)=60;
(++_k_1);
((&(_k_1[0]))->_name)=T21;
((&(_k_1[0]))->_token)=61;
(++_k_1);
((&(_k_1[0]))->_name)=T22;
((&(_k_1[0]))->_token)=62;
(++_k_1);
((&(_k_1[0]))->_name)=T23;
((&(_k_1[0]))->_token)=63;
(++_k_1);
((&(_k_1[0]))->_name)=T24;
((&(_k_1[0]))->_token)=44;
(++_k_1);
((&(_k_1[0]))->_name)=T25;
((&(_k_1[0]))->_token)=45;
(++_k_1);
((&(_k_1[0]))->_name)=T26;
((&(_k_1[0]))->_token)=47;
(++_k_1);
((&(_k_1[0]))->_name)=T27;
((&(_k_1[0]))->_token)=58;
(++_k_1);
((&(_k_1[0]))->_name)=T28;
((&(_k_1[0]))->_token)=59;
(++_k_1);
((&(_k_1[0]))->_name)=T29;
((&(_k_1[0]))->_token)=46;
(++_k_1);
((&(_k_1[0]))->_name)=T30;
((&(_k_1[0]))->_token)=48;
(++_k_1);
((&(_k_1[0]))->_name)=T31;
((&(_k_1[0]))->_token)=43;
(++_k_1);
((&(_k_1[0]))->_name)=T32;
((&(_k_1[0]))->_token)=55;
(++_k_1);
((&(_k_1[0]))->_name)=((char *)0);
((&(_k_1[0]))->_token)=0;
(++_k_1);
}

static struct _operator *_add_p_a_s__operator_p_a_ch_in_in(struct _operator *_p, char *_op, int _min, int _max)
{
((&(_p[0]))->_op)=_op;
((&(_p[0]))->_min_arg)=_min;
((&(_p[0]))->_max_arg)=_max;
return (_p+1);
}

static void _initialize_operators()
{
struct _operator *_p_1;
_p_1=_operators33;
_p_1=((struct _operator *)_add_p_a_s__operator_p_a_ch_in_in(((struct _operator *)_p_1),_op_add36,1,2));
_p_1=((struct _operator *)_add_p_a_s__operator_p_a_ch_in_in(((struct _operator *)_p_1),_op_sub38,1,2));
_p_1=((struct _operator *)_add_p_a_s__operator_p_a_ch_in_in(((struct _operator *)_p_1),_op_mul40,2,2));
_p_1=((struct _operator *)_add_p_a_s__operator_p_a_ch_in_in(((struct _operator *)_p_1),_op_div42,2,2));
_p_1=((struct _operator *)_add_p_a_s__operator_p_a_ch_in_in(((struct _operator *)_p_1),_op_mod44,2,2));
_p_1=((struct _operator *)_add_p_a_s__operator_p_a_ch_in_in(((struct _operator *)_p_1),_op_shl46,2,2));
_p_1=((struct _operator *)_add_p_a_s__operator_p_a_ch_in_in(((struct _operator *)_p_1),_op_shr48,2,2));
_p_1=((struct _operator *)_add_p_a_s__operator_p_a_ch_in_in(((struct _operator *)_p_1),_op_eq50,2,2));
_p_1=((struct _operator *)_add_p_a_s__operator_p_a_ch_in_in(((struct _operator *)_p_1),_op_ne52,2,2));
_p_1=((struct _operator *)_add_p_a_s__operator_p_a_ch_in_in(((struct _operator *)_p_1),_op_le54,2,2));
_p_1=((struct _operator *)_add_p_a_s__operator_p_a_ch_in_in(((struct _operator *)_p_1),_op_lt56,2,2));
_p_1=((struct _operator *)_add_p_a_s__operator_p_a_ch_in_in(((struct _operator *)_p_1),_op_ge58,2,2));
_p_1=((struct _operator *)_add_p_a_s__operator_p_a_ch_in_in(((struct _operator *)_p_1),_op_gt60,2,2));
_p_1=((struct _operator *)_add_p_a_s__operator_p_a_ch_in_in(((struct _operator *)_p_1),_op_and62,2,2));
_p_1=((struct _operator *)_add_p_a_s__operator_p_a_ch_in_in(((struct _operator *)_p_1),_op_xor64,2,2));
_p_1=((struct _operator *)_add_p_a_s__operator_p_a_ch_in_in(((struct _operator *)_p_1),_op_or66,2,2));
_p_1=((struct _operator *)_add_p_a_s__operator_p_a_ch_in_in(((struct _operator *)_p_1),_op_andand68,2,2));
_p_1=((struct _operator *)_add_p_a_s__operator_p_a_ch_in_in(((struct _operator *)_p_1),_op_oror70,2,2));
_p_1=((struct _operator *)_add_p_a_s__operator_p_a_ch_in_in(((struct _operator *)_p_1),_op_assign_add72,2,2));
_p_1=((struct _operator *)_add_p_a_s__operator_p_a_ch_in_in(((struct _operator *)_p_1),_op_assign_sub74,2,2));
_p_1=((struct _operator *)_add_p_a_s__operator_p_a_ch_in_in(((struct _operator *)_p_1),_op_assign_mul76,2,2));
_p_1=((struct _operator *)_add_p_a_s__operator_p_a_ch_in_in(((struct _operator *)_p_1),_op_assign_div78,2,2));
_p_1=((struct _operator *)_add_p_a_s__operator_p_a_ch_in_in(((struct _operator *)_p_1),_op_assign_mod80,2,2));
_p_1=((struct _operator *)_add_p_a_s__operator_p_a_ch_in_in(((struct _operator *)_p_1),_op_assign_shl82,2,2));
_p_1=((struct _operator *)_add_p_a_s__operator_p_a_ch_in_in(((struct _operator *)_p_1),_op_assign_shr84,2,2));
_p_1=((struct _operator *)_add_p_a_s__operator_p_a_ch_in_in(((struct _operator *)_p_1),_op_assign_and86,2,2));
_p_1=((struct _operator *)_add_p_a_s__operator_p_a_ch_in_in(((struct _operator *)_p_1),_op_assign_or88,2,2));
_p_1=((struct _operator *)_add_p_a_s__operator_p_a_ch_in_in(((struct _operator *)_p_1),_op_assign_xor90,2,2));
_p_1=((struct _operator *)_add_p_a_s__operator_p_a_ch_in_in(((struct _operator *)_p_1),_op_assign_neg92,1,1));
_p_1=((struct _operator *)_add_p_a_s__operator_p_a_ch_in_in(((struct _operator *)_p_1),_op_assign_not94,1,1));
_p_1=((struct _operator *)_add_p_a_s__operator_p_a_ch_in_in(((struct _operator *)_p_1),_op_not96,1,1));
_p_1=((struct _operator *)_add_p_a_s__operator_p_a_ch_in_in(((struct _operator *)_p_1),_op_pre_inc98,1,1));
_p_1=((struct _operator *)_add_p_a_s__operator_p_a_ch_in_in(((struct _operator *)_p_1),_op_pre_dec100,1,1));
_p_1=((struct _operator *)_add_p_a_s__operator_p_a_ch_in_in(((struct _operator *)_p_1),_op_post_inc102,1,1));
_p_1=((struct _operator *)_add_p_a_s__operator_p_a_ch_in_in(((struct _operator *)_p_1),_op_post_dec104,1,1));
_p_1=((struct _operator *)_add_p_a_s__operator_p_a_ch_in_in(((struct _operator *)_p_1),_op_at106,0,0));
_p_1=((struct _operator *)_add_p_a_s__operator_p_a_ch_in_in(((struct _operator *)_p_1),((char *)0),0,0));
}

static void _initialize_signature()
{
((&_empty_sig107)->_number_of_types)=0;
}

static unsigned int _max_dw_dw(unsigned int _x, unsigned int _y)
{
if((_x>_y))
{
return _x;
}
else
{
return _y;
}
}

static unsigned char *_allocate_p_s__memory_bundle_dw(struct _memory_bundle *_bundle, unsigned int _s)
{
unsigned int _size_1;
_size_1=_s;
_size_1+=((unsigned int)3);
_size_1&=((unsigned int)-4);
(_bundle->_total)+=_size_1;
(_bundle->_chunk_free)-=_size_1;
if(((void*)(_bundle->_chunk_free)>=(void*)(_bundle->_chunk_base)))
{
return (_bundle->_chunk_free);
}
_new_chunk_p_s__memory_bundle_dw(_bundle,_max_dw_dw(_size_1,((unsigned int )16384)));
(_bundle->_chunk_free)-=_size_1;
return (_bundle->_chunk_free);
}

static struct _type *_new_type_p_s__memory_bundle_dw_e__type_id_in_dw_dw(struct _memory_bundle *_bundle, unsigned int _structure_size, int _id, int _flags, unsigned int _size, unsigned int _align)
{
struct _type *_t_1;
_t_1=((struct _type *)_allocate_p_s__memory_bundle_dw(_bundle,_structure_size));
(_t_1->_bundle)=_bundle;
(_t_1->_id)=_id;
(_t_1->_flags)=_flags;
(_t_1->_default_size)=_size;
(_t_1->_default_align)=_align;
(_t_1->_hash_code)=((int )_t_1);
(_t_1->_ptr)=((struct _type *)0);
return _t_1;
}

static struct _builtin_type *_new_builtin_type_p_s__memory_bundle_e__type_id_in_dw_p_a_ch(struct _memory_bundle *_bundle, int _id, int _flags, unsigned int _size, char *_name)
{
struct _builtin_type *_t_1;
_t_1=((struct _builtin_type *)_new_type_p_s__memory_bundle_dw_e__type_id_in_dw_dw(_bundle,((unsigned int)44),_id,_flags,_size,_size));
(((struct _named_type *)_t_1)->_name)=_name;
return _t_1;
}

static unsigned int _align_p_s__type(struct _type *_t)
{
return (_t->_default_align);
}

static struct _array_type *_new_array_type_p_s__memory_bundle_in_p_s__type(struct _memory_bundle *_bundle, int _cells, struct _type *_cell_type)
{
struct _array_type *_t_1;
_t_1=((struct _array_type *)_new_type_p_s__memory_bundle_dw_e__type_id_in_dw_dw(_bundle,((unsigned int)36),18,4,((unsigned int )-1),_align_p_s__type(_cell_type)));
(((struct _type *)_t_1)->_hash_code)=((_cell_type->_hash_code)+2);
(_t_1->_array_size)=_cells;
(_t_1->_array_type)=_cell_type;
return _t_1;
}

static struct _pointer_type *_new_pointer_type_p_s__memory_bundle_p_s__type(struct _memory_bundle *_bundle, struct _type *_target)
{
struct _pointer_type *_t_1;
_t_1=((struct _pointer_type *)_new_type_p_s__memory_bundle_dw_e__type_id_in_dw_dw(_bundle,((unsigned int)32),15,32,((unsigned int)4),((unsigned int)4)));
(((struct _type *)_t_1)->_hash_code)=((_target->_hash_code)+1);
(_t_1->_target_type)=_target;
return _t_1;
}

static struct _type *_pointer_p_s__type(struct _type *_t)
{
struct _type *_pointer_1;
_pointer_1=(_t->_ptr);
if(((void*)_pointer_1!=(void*)0))
{
return _pointer_1;
}
_pointer_1=((struct _type *)_new_pointer_type_p_s__memory_bundle_p_s__type((_t->_bundle),_t));
(_t->_ptr)=_pointer_1;
return _pointer_1;
}

static void _initialize_builtin_types_p_s__memory_bundle(struct _memory_bundle *_bundle)
{
struct _array_type *_char_ary_type_1;
struct _array_type *_wchar_ary_type_2;
_nil_type108=((struct _named_type *)_new_builtin_type_p_s__memory_bundle_e__type_id_in_dw_p_a_ch(_bundle,2,32,((unsigned int)4),_tn_nil113));
_void_type114=((struct _named_type *)_new_builtin_type_p_s__memory_bundle_e__type_id_in_dw_p_a_ch(_bundle,1,32,((unsigned int )-1),_tn_void116));
_char_type117=((struct _named_type *)_new_builtin_type_p_s__memory_bundle_e__type_id_in_dw_p_a_ch(_bundle,4,32,((unsigned int)1),_tn_achar119));
_wchar_type120=((struct _named_type *)_new_builtin_type_p_s__memory_bundle_e__type_id_in_dw_p_a_ch(_bundle,5,32,((unsigned int)2),_tn_wchar122));
_bool_type123=((struct _named_type *)_new_builtin_type_p_s__memory_bundle_e__type_id_in_dw_p_a_ch(_bundle,3,32,((unsigned int)4),_tn_bool125));
_byte_type126=((struct _named_type *)_new_builtin_type_p_s__memory_bundle_e__type_id_in_dw_p_a_ch(_bundle,6,40,((unsigned int)1),_tn_byte128));
_word_type129=((struct _named_type *)_new_builtin_type_p_s__memory_bundle_e__type_id_in_dw_p_a_ch(_bundle,7,40,((unsigned int)2),_tn_word131));
_dword_type132=((struct _named_type *)_new_builtin_type_p_s__memory_bundle_e__type_id_in_dw_p_a_ch(_bundle,8,40,((unsigned int)4),_tn_dword134));
_qword_type135=((struct _named_type *)_new_builtin_type_p_s__memory_bundle_e__type_id_in_dw_p_a_ch(_bundle,9,40,((unsigned int)8),_tn_qword137));
_octet_type138=((struct _named_type *)_new_builtin_type_p_s__memory_bundle_e__type_id_in_dw_p_a_ch(_bundle,10,56,((unsigned int)1),_tn_octet140));
_short_type141=((struct _named_type *)_new_builtin_type_p_s__memory_bundle_e__type_id_in_dw_p_a_ch(_bundle,11,56,((unsigned int)2),_tn_short143));
_int_type144=((struct _named_type *)_new_builtin_type_p_s__memory_bundle_e__type_id_in_dw_p_a_ch(_bundle,12,56,((unsigned int)4),_tn_int146));
_long_type147=((struct _named_type *)_new_builtin_type_p_s__memory_bundle_e__type_id_in_dw_p_a_ch(_bundle,13,56,((unsigned int)8),_tn_long149));
_char_ary_type_1=_new_array_type_p_s__memory_bundle_in_p_s__type(_bundle,0,((struct _type *)_char_type117));
_string_type152=_pointer_p_s__type(((struct _type *)_char_ary_type_1));
_wchar_ary_type_2=_new_array_type_p_s__memory_bundle_in_p_s__type(_bundle,0,((struct _type *)_wchar_type120));
_wstring_type156=_pointer_p_s__type(((struct _type *)_wchar_ary_type_2));
}

static int _hash_p_a_ch(char *_s)
{
char *_q_1;
int _sum_2;
_q_1=_s;
_sum_2=12345;
while(((_q_1[0])!='\x00'))
{
_sum_2=((_sum_2<<1)^((int )((_q_1++)[0])));
}
return _sum_2;
}

static int _compare_p_a_ch_p_a_ch(char *_s1, char *_s2)
{
char *_p1_1;
char *_p2_2;
char _c_3;
_p1_1=_s1;
_p2_2=_s2;
while(1)
{
_c_3=(_p1_1[0]);
if((_c_3!=(_p2_2[0])))
{
break;
}
if((_c_3=='\x00'))
{
break;
}
(++_p1_1);
(++_p2_2);
}
return ((int )((char )(_c_3-(_p2_2[0]))));
}

static int _is_equal_p_a_ch_p_a_ch(char *_s1, char *_s2)
{
return (_compare_p_a_ch_p_a_ch(_s1,_s2)==0);
}

static void _initialize_type_dictionary()
{
((&_type_dictionary_class157)->_hash)=((int (*)(void *))_hash_p_a_ch);
((&_type_dictionary_class157)->_is_equal)=((int (*)(void *,void *))_is_equal_p_a_ch_p_a_ch);
}

static void _initialize_type_p_s__memory_bundle(struct _memory_bundle *_bundle)
{
_initialize_signature();
_initialize_builtin_types_p_s__memory_bundle(_bundle);
_initialize_type_dictionary();
}

static int _hash_p_s__definition_head(struct _definition_head *_head)
{
return ((_hash_p_a_ch((_head->_name))<<4)+((_head->_signature)->_number_of_types));
}

static int _match_p_s__definition_head_p_s__definition_head(struct _definition_head *_h1, struct _definition_head *_h2)
{
return (_is_equal_p_a_ch_p_a_ch((_h1->_name),(_h2->_name))&&(((_h1->_signature)->_number_of_types)==((_h2->_signature)->_number_of_types)));
}

static void _initialize_definition_dictionary()
{
((&_definition_dictionary_class163)->_hash)=((int (*)(void *))_hash_p_s__definition_head);
((&_definition_dictionary_class163)->_is_equal)=((int (*)(void *,void *))_match_p_s__definition_head_p_s__definition_head);
}

static void _initialize_definition()
{
_initialize_definition_dictionary();
}

static void _initialize_litteral_string_dictionary()
{
((&_litteral_string_dictionary_class164)->_hash)=((int (*)(void *))_hash_p_a_ch);
((&_litteral_string_dictionary_class164)->_is_equal)=((int (*)(void *,void *))_is_equal_p_a_ch_p_a_ch);
}

static void _initialize_c_dump()
{
_initialize_litteral_string_dictionary();
}

static void _initialize_p_s__list_p_s__memory_bundle(struct _list *_list, struct _memory_bundle *_bundle)
{
(_list->_first)=((struct _list_item *)0);
(_list->_last)=((struct _list_item *)0);
(_list->_size)=0;
(_list->_bundle)=_bundle;
}

static void _initialize_p_s__dictionary_p_s__dictionary_class(struct _dictionary *_d, struct _dictionary_class *_class)
{
unsigned int _i_1;
(_d->_class)=_class;
(_d->_collisions)=0;
(_d->_tally)=((unsigned int)0);
(_d->_allocated)=10;
(_d->_associations)=((struct _association *)_allocate_memory_dw(((unsigned int)80)));
_i_1=((unsigned int)0);
while((_i_1<10))
{
((&((_d->_associations)[_i_1]))->_key)=((void *)0);
(++_i_1);
}
}

static int _size_p_a_ch(char *_s)
{
char *_p_1;
_p_1=_s;
while(((_p_1[0])!='\x00'))
{
(++_p_1);
}
return (_p_1-_s);
}

static void _copy_p_vo_p_vo_dw(void *_dst, void *_src, unsigned int _size)
{
unsigned char *_p_1;
unsigned char *_q_2;
unsigned char *_limit_3;
unsigned int *_pd_4;
unsigned int *_qd_5;
_p_1=((unsigned char *)_src);
_q_2=((unsigned char *)_dst);
_limit_3=(_q_2+_size);
if((_size>64))
{
while(((((unsigned int )_q_2)&3)!=0))
{
((_q_2++)[0])=((_p_1++)[0]);
}
_limit_3-=3;
_pd_4=((unsigned int *)_p_1);
_qd_5=((unsigned int *)_q_2);
while(((void*)_qd_5<(void*)_limit_3))
{
((_qd_5++)[0])=((_pd_4++)[0]);
}
_limit_3+=3;
_p_1=((unsigned char *)_pd_4);
_q_2=((unsigned char *)_qd_5);
}
while(((void*)_q_2!=(void*)_limit_3))
{
((_q_2++)[0])=((_p_1++)[0]);
}
}

static char *_new_string_p_s__memory_bundle_p_a_ch(struct _memory_bundle *_bundle, char *_s)
{
int _len_1;
char *_buf_2;
_len_1=(_size_p_a_ch(_s)+1);
_buf_2=((char *)_allocate_p_s__memory_bundle_dw(_bundle,((unsigned int )_len_1)));
_copy_p_vo_p_vo_dw(((void *)_buf_2),((void *)_s),((unsigned int )_len_1));
return _buf_2;
}

static void _to_OS_name_p_a_ch(char *_filename)
{
convert_full_name(_filename);
}

static void _append_p_s__list_p_vo(struct _list *_list, void *_value)
{
struct _list_item *_e_1;
_e_1=((struct _list_item *)_allocate_p_s__memory_bundle_dw((_list->_bundle),((unsigned int)8)));
if(((void*)(_list->_last)!=(void*)0))
{
(((struct _element *)(_list->_last))->_next_sibling)=((struct _element *)_e_1);
}
(_list->_last)=_e_1;
if(((void*)(_list->_first)==(void*)0))
{
(_list->_first)=_e_1;
}
(((struct _element *)_e_1)->_next_sibling)=((struct _element *)0);
(_e_1->_value)=_value;
(++(_list->_size));
}

static char *_first_occurrence_p_a_ch_ch(char *_s, char _c)
{
char *_q_1;
char _d_2;
_q_1=_s;
_d_2=((_q_1++)[0]);
while((_d_2!='\x00'))
{
if((_d_2==_c))
{
return (_q_1-1);
}
_d_2=((_q_1++)[0]);
}
return ((char *)0);
}

static char *_new_string_p_s__memory_bundle_p_a_ch_dw(struct _memory_bundle *_bundle, char *_s, unsigned int _len)
{
char *_buf_1;
_buf_1=((char *)_allocate_p_s__memory_bundle_dw(_bundle,(_len+1)));
_copy_p_vo_p_vo_dw(((void *)_buf_1),((void *)_s),_len);
(_buf_1[_len])='\x00';
return _buf_1;
}

static int _max_in_in(int _x, int _y)
{
if((_x>_y))
{
return _x;
}
else
{
return _y;
}
}

static void _free_memory_p_vo(void *_base)
{
unsigned char *_p_1;
unsigned char *_n_2;
_p_1=(((struct _mem_head *)(((unsigned char *)_base)-((unsigned int)12)))->_prev);
_n_2=(((struct _mem_head *)(((unsigned char *)_base)-((unsigned int)12)))->_next);
if(((void*)_p_1!=(void*)0))
{
(((struct _mem_head *)(_p_1-((unsigned int)12)))->_next)=_n_2;
}
if(((void*)_n_2!=(void*)0))
{
(((struct _mem_head *)(_n_2-((unsigned int)12)))->_prev)=_p_1;
}
if(((void*)_first_block2==(void*)_base))
{
_first_block2=_n_2;
}
_current_allocated7-=(((struct _mem_head *)(((unsigned char *)_base)-((unsigned int)12)))->_size);
}

static void _resize_p_s__dictionary_dw(struct _dictionary *_d, unsigned int _s)
{
int _c_1;
struct _association *_a_2;
unsigned int _i_3;
unsigned int _i_4;
unsigned int _size_5;
struct _association *_a_6;
int _count_7;
unsigned int _location_8;
_c_1=0;
_a_2=((struct _association *)_allocate_memory_dw((_s*((unsigned int)8))));
_i_3=((unsigned int)0);
while((_i_3<_s))
{
((&(_a_2[_i_3]))->_key)=((void *)0);
(++_i_3);
}
_i_4=((unsigned int)0);
_size_5=(_d->_allocated);
while((_i_4<_size_5))
{
if(((void*)((&((_d->_associations)[_i_4]))->_key)!=(void*)0))
{
_a_6=(&((_d->_associations)[_i_4]));
_count_7=1;
_location_8=(((unsigned int )((_d->_class)->_hash)((_a_6->_key)))%_s);
while(((void*)((&(_a_2[_location_8]))->_key)!=(void*)0))
{
(++_location_8);
if((_location_8==_s))
{
_location_8=0;
}
(++_count_7);
}
((&(_a_2[_location_8]))->_key)=(_a_6->_key);
((&(_a_2[_location_8]))->_value)=(_a_6->_value);
_c_1=_max_in_in(_count_7,_c_1);
}
(++_i_4);
}
_free_memory_p_vo(((void *)(_d->_associations)));
(_d->_collisions)=_c_1;
(_d->_associations)=_a_2;
(_d->_allocated)=_s;
}

static void _increase_tally_p_s__dictionary(struct _dictionary *_d)
{
(++(_d->_tally));
if(((4*(_d->_tally))>(3*(_d->_allocated))))
{
_resize_p_s__dictionary_dw(_d,(((_d->_allocated)*3)/2));
}
}

static void *_add_p_s__dictionary_p_vo_p_vo(struct _dictionary *_d, void *_k, void *_v)
{
unsigned int _location_1;
int _count_2;
struct _association *_a_3;
void *_old_4;
_location_1=(((unsigned int )((_d->_class)->_hash)(_k))%(_d->_allocated));
_count_2=1;
while(1)
{
_a_3=(&((_d->_associations)[_location_1]));
if(((void*)(_a_3->_key)!=(void*)0))
{
if(((_d->_class)->_is_equal)(_k,(_a_3->_key)))
{
_old_4=(_a_3->_value);
(_a_3->_value)=_v;
return _old_4;
}
else
{
(++_location_1);
if((_location_1==(_d->_allocated)))
{
_location_1=0;
}
(++_count_2);
}
}
else
{
(_a_3->_key)=_k;
(_a_3->_value)=_v;
if((_count_2>(_d->_collisions)))
{
(_d->_collisions)=_count_2;
}
_increase_tally_p_s__dictionary(_d);
return ((void *)0);
}
}
return ((void *)0);
}

static void _print_usage()
{
puts(T219);
}

static void _initialize_p_s__collection(struct _collection *_c)
{
(_c->_first)=((struct _element *)0);
(_c->_last)=((struct _element *)0);
}

static struct _module *_new_module_p_s__memory_bundle_p_a_ch(struct _memory_bundle *_bundle, char *_name)
{
struct _module *_m_1;
_m_1=((struct _module *)_allocate_p_s__memory_bundle_dw(_bundle,((unsigned int)68)));
(_m_1->_name)=_name;
(_m_1->_loading)=0;
_initialize_p_s__list_p_s__memory_bundle((&(_m_1->_exposed_imports)),_bundle);
_initialize_p_s__list_p_s__memory_bundle((&(_m_1->_exposed_definitions)),_bundle);
_initialize_p_s__list_p_s__memory_bundle((&(_m_1->_exposed_types)),_bundle);
_initialize_p_s__collection(((struct _collection *)(&(_m_1->_elements))));
(_m_1->_scanned)=0;
return _m_1;
}

static void _initialize_p_s__vector_dw(struct _vector *_v, unsigned int _initial_allocated)
{
(_v->_size)=0;
(((struct _priv_vector *)_v)->_allocated)=_initial_allocated;
(((struct _priv_vector *)_v)->_array)=((void **)_allocate_memory_dw((_initial_allocated*((unsigned int)4))));
}

static void _initialize_p_s__type_dictionary(struct _type_dictionary *_d)
{
_initialize_p_s__dictionary_p_s__dictionary_class(((struct _dictionary *)_d),(&_type_dictionary_class157));
}

static void _initialize_p_s__definition_dictionary(struct _definition_dictionary *_d)
{
_initialize_p_s__dictionary_p_s__dictionary_class(((struct _dictionary *)_d),(&_definition_dictionary_class163));
}

static void _initialize_p_s__scope_p_s__scope(struct _scope *_s, struct _scope *_next)
{
(_s->_next)=_next;
_initialize_p_s__type_dictionary((&(_s->_types)));
_initialize_p_s__definition_dictionary((&(_s->_definitions)));
}

static void _add_builtin_type_p_s__compiler_p_a_ch_p_s__named_type(struct _compiler *_cc, char *_name, struct _named_type *_type)
{
_add_p_s__dictionary_p_vo_p_vo(((struct _dictionary *)(&((&(_cc->_global_scope))->_types))),((void *)_name),((void *)_type));
}

static void _initialize_builtin_types_p_s__compiler(struct _compiler *_cc)
{
_add_builtin_type_p_s__compiler_p_a_ch_p_s__named_type(_cc,_tn_void116,_void_type114);
_add_builtin_type_p_s__compiler_p_a_ch_p_s__named_type(_cc,_tn_byte128,_byte_type126);
_add_builtin_type_p_s__compiler_p_a_ch_p_s__named_type(_cc,_tn_word131,_word_type129);
_add_builtin_type_p_s__compiler_p_a_ch_p_s__named_type(_cc,_tn_dword134,_dword_type132);
_add_builtin_type_p_s__compiler_p_a_ch_p_s__named_type(_cc,_tn_qword137,_qword_type135);
_add_builtin_type_p_s__compiler_p_a_ch_p_s__named_type(_cc,_tn_octet140,_octet_type138);
_add_builtin_type_p_s__compiler_p_a_ch_p_s__named_type(_cc,_tn_short143,_short_type141);
_add_builtin_type_p_s__compiler_p_a_ch_p_s__named_type(_cc,_tn_int146,_int_type144);
_add_builtin_type_p_s__compiler_p_a_ch_p_s__named_type(_cc,_tn_long149,_long_type147);
_add_builtin_type_p_s__compiler_p_a_ch_p_s__named_type(_cc,_tn_bool125,_bool_type123);
_add_builtin_type_p_s__compiler_p_a_ch_p_s__named_type(_cc,_tn_achar119,_char_type117);
_add_builtin_type_p_s__compiler_p_a_ch_p_s__named_type(_cc,_tn_wchar122,_wchar_type120);
if(((_cc->_session)->_unicode))
{
_add_builtin_type_p_s__compiler_p_a_ch_p_s__named_type(_cc,_tn_char225,_wchar_type120);
}
else
{
_add_builtin_type_p_s__compiler_p_a_ch_p_s__named_type(_cc,_tn_char225,_char_type117);
}
}

static void _initialize_p_s__definition_p_a_ch_p_s__signature_p_s__type_e__definition_code_bo_bo_p_vo_in(struct _definition *_d, char *_name, struct _signature *_signature, struct _type *_type, int _opcode, int _lvalue, int _expose, void *_module, int _line)
{
(_d->_opcode)=_opcode;
(_d->_name)=_name;
(_d->_signature)=_signature;
(_d->_type)=_type;
(_d->_resolve_status)=0;
(_d->_lvalue)=_lvalue;
(_d->_expose)=_expose;
(_d->_used)=0;
(_d->_module)=_module;
(_d->_line)=_line;
(_d->_source)=((struct _node *)0);
}

static void _initialize_p_s__definition_item_p_s__definition(struct _definition_item *_item, struct _definition *_d)
{
(_item->_next)=((struct _definition_item *)0);
(_item->_value)=_d;
}

static void _definition_head_p_s__definition_item_p_a_ch_p_s__signature_p_s__definition_head(struct _definition_item *_first, char *_name, struct _signature *_signature, struct _definition_head *_d)
{
(_d->_first)=_first;
(_d->_name)=_name;
(_d->_signature)=_signature;
}

static void *_value_p_s__dictionary_p_vo(struct _dictionary *_d, void *_k)
{
unsigned int _location_1;
int _count_2;
struct _association *_a_3;
_location_1=(((unsigned int )((_d->_class)->_hash)(_k))%(_d->_allocated));
_count_2=(_d->_collisions);
while(((_count_2>0)&&((void*)((&((_d->_associations)[_location_1]))->_key)!=(void*)0)))
{
_a_3=(&((_d->_associations)[_location_1]));
if(((_d->_class)->_is_equal)((_a_3->_key),_k))
{
return (_a_3->_value);
}
(++_location_1);
if((_location_1==(_d->_allocated)))
{
_location_1=0;
}
(--_count_2);
}
return ((void *)0);
}

static void _add_definition_p_s__definition_dictionary_p_s__definition(struct _definition_dictionary *_d, struct _definition *_definition)
{
struct _definition_item *_item_1;
struct _definition_head _t_2;
struct _definition_head *_head_3;
struct _definition_item *_next_4;
_item_1=((struct _definition_item *)_allocate_memory_dw(((unsigned int)8)));
_initialize_p_s__definition_item_p_s__definition(_item_1,_definition);
_definition_head_p_s__definition_item_p_a_ch_p_s__signature_p_s__definition_head(((struct _definition_item *)0),(_definition->_name),(_definition->_signature),(&_t_2));
_head_3=((struct _definition_head *)_value_p_s__dictionary_p_vo(((struct _dictionary *)_d),((void *)(&_t_2))));
if(((void*)_head_3!=(void*)0))
{
_next_4=(_head_3->_first);
(_head_3->_first)=_item_1;
(_item_1->_next)=_next_4;
}
else
{
_head_3=((struct _definition_head *)_allocate_memory_dw(((unsigned int)12)));
_definition_head_p_s__definition_item_p_a_ch_p_s__signature_p_s__definition_head(_item_1,(_definition->_name),(_definition->_signature),_head_3);
(_item_1->_next)=((struct _definition_item *)0);
((struct _definition_head *)_add_p_s__dictionary_p_vo_p_vo(((struct _dictionary *)_d),((void *)_head_3),((void *)_head_3)));
}
}

static void _add_builtin_definition_p_s__compiler_p_s__definition(struct _compiler *_cc, struct _definition *_definition)
{
_add_definition_p_s__definition_dictionary_p_s__definition((&((&(_cc->_global_scope))->_definitions)),_definition);
}

static void _add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code_bo(struct _compiler *_cc, char *_name, struct _signature *_signature, struct _type *_type, int _opcode, int _lvalue)
{
struct _definition *_d_1;
_d_1=((struct _definition *)_allocate_p_s__memory_bundle_dw((_cc->_bundle),((unsigned int)48)));
_initialize_p_s__definition_p_a_ch_p_s__signature_p_s__type_e__definition_code_bo_bo_p_vo_in(_d_1,_name,_signature,_type,_opcode,_lvalue,(_cc->_current_exposition),((void *)0),0);
_add_builtin_definition_p_s__compiler_p_s__definition(_cc,_d_1);
}

static void _add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(struct _compiler *_cc, char *_name, struct _signature *_signature, struct _type *_type, int _opcode)
{
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code_bo(_cc,_name,_signature,_type,_opcode,0);
}

static struct _signature *_new_signature_p_s__memory_bundle_in(struct _memory_bundle *_bundle, int _s)
{
unsigned int _bytes_1;
struct _signature *_signature_2;
if((_s==0))
{
return (&_empty_sig107);
}
_bytes_1=((_s*((unsigned int)4))+((unsigned int)4));
_signature_2=((struct _signature *)_allocate_p_s__memory_bundle_dw(_bundle,_bytes_1));
(_signature_2->_number_of_types)=_s;
return _signature_2;
}

static void _set_type_p_s__signature_in_p_s__type(struct _signature *_signature, int _index, struct _type *_t)
{
struct _type **_p_1;
_p_1=((struct _type **)(((unsigned char *)_signature)+((unsigned int)4)));
(_p_1[_index])=_t;
}

static struct _signature *_new_signature_p_s__memory_bundle_p_s__type_p_s__type(struct _memory_bundle *_bundle, struct _type *_t1, struct _type *_t2)
{
struct _signature *_signature_1;
_signature_1=_new_signature_p_s__memory_bundle_in(_bundle,2);
_set_type_p_s__signature_in_p_s__type(_signature_1,0,_t1);
_set_type_p_s__signature_in_p_s__type(_signature_1,1,_t2);
return _signature_1;
}

static struct _signature *_new_signature_p_s__memory_bundle_p_s__type(struct _memory_bundle *_bundle, struct _type *_t1)
{
struct _signature *_signature_1;
_signature_1=_new_signature_p_s__memory_bundle_in(_bundle,1);
_set_type_p_s__signature_in_p_s__type(_signature_1,0,_t1);
return _signature_1;
}

static void _add_comparators_p_s__compiler_p_s__signature(struct _compiler *_cc, struct _signature *_sig)
{
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_eq50,_sig,((struct _type *)_bool_type123),16);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_ne52,_sig,((struct _type *)_bool_type123),17);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_le54,_sig,((struct _type *)_bool_type123),18);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_lt56,_sig,((struct _type *)_bool_type123),19);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_ge58,_sig,((struct _type *)_bool_type123),20);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_gt60,_sig,((struct _type *)_bool_type123),21);
}

static void _add_numeric_definitions_p_s__compiler_p_s__type(struct _compiler *_cc, struct _type *_type)
{
struct _signature *_sig1_1;
struct _signature *_sig2_2;
struct _signature *_sigs32_3;
struct _signature *_sigs16_4;
struct _signature *_sigs8_5;
_sig1_1=_new_signature_p_s__memory_bundle_p_s__type((_cc->_bundle),_type);
_sig2_2=_new_signature_p_s__memory_bundle_p_s__type_p_s__type((_cc->_bundle),_type,_type);
_add_comparators_p_s__compiler_p_s__signature(_cc,_sig2_2);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_add36,_sig2_2,_type,26);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_sub38,_sig2_2,_type,27);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_mul40,_sig2_2,_type,28);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_div42,_sig2_2,_type,29);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_mod44,_sig2_2,_type,30);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_add36,_sig1_1,_type,33);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_sub38,_sig1_1,_type,34);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_pre_inc98,_sig1_1,_type,62);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_pre_dec100,_sig1_1,_type,63);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_post_inc102,_sig1_1,_type,64);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_post_dec104,_sig1_1,_type,65);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_and62,_sig2_2,_type,22);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_or66,_sig2_2,_type,23);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_xor64,_sig2_2,_type,24);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_not96,_sig1_1,_type,25);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_assign247,_sig2_2,((struct _type *)_void_type114),49);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_assign_add72,_sig2_2,((struct _type *)_void_type114),50);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_assign_sub74,_sig2_2,((struct _type *)_void_type114),51);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_assign_mul76,_sig2_2,((struct _type *)_void_type114),52);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_assign_div78,_sig2_2,((struct _type *)_void_type114),53);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_assign_mod80,_sig2_2,((struct _type *)_void_type114),54);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_assign_shl82,_sig2_2,((struct _type *)_void_type114),55);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_assign_shr84,_sig2_2,((struct _type *)_void_type114),56);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_assign_and86,_sig2_2,((struct _type *)_void_type114),57);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_assign_or88,_sig2_2,((struct _type *)_void_type114),58);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_assign_xor90,_sig2_2,((struct _type *)_void_type114),59);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_assign_not94,_sig1_1,((struct _type *)_void_type114),61);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_assign_neg92,_sig1_1,((struct _type *)_void_type114),60);
_sigs32_3=_new_signature_p_s__memory_bundle_p_s__type_p_s__type((_cc->_bundle),_type,((struct _type *)_dword_type132));
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_shl46,_sigs32_3,_type,31);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_shr48,_sigs32_3,_type,32);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_assign_shl82,_sigs32_3,((struct _type *)_void_type114),55);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_assign_shr84,_sigs32_3,((struct _type *)_void_type114),56);
_sigs16_4=_new_signature_p_s__memory_bundle_p_s__type_p_s__type((_cc->_bundle),_type,((struct _type *)_word_type129));
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_shl46,_sigs16_4,_type,31);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_shr48,_sigs16_4,_type,32);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_assign_shl82,_sigs16_4,((struct _type *)_void_type114),55);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_assign_shr84,_sigs16_4,((struct _type *)_void_type114),56);
_sigs8_5=_new_signature_p_s__memory_bundle_p_s__type_p_s__type((_cc->_bundle),_type,((struct _type *)_byte_type126));
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_shl46,_sigs8_5,_type,31);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_shr48,_sigs8_5,_type,32);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_assign_shl82,_sigs8_5,((struct _type *)_void_type114),55);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_assign_shr84,_sigs8_5,((struct _type *)_void_type114),56);
}

static void _initialize_builtin_definitions_p_s__compiler_p_s__memory_bundle(struct _compiler *_cc, struct _memory_bundle *_bundle)
{
struct _type *_void_ptr_type_1;
struct _signature *_p_p_2;
struct _signature *_p_3;
struct _array_type *_void_ary_type_4;
struct _type *_ary_ptr_type_5;
struct _signature *_a_a_6;
struct _signature *_a_i_7;
struct _signature *_a_8;
struct _signature *_b_b_9;
struct _signature *_b_10;
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,T235,(&_empty_sig107),((struct _type *)_nil_type108),12);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,T236,(&_empty_sig107),((struct _type *)_void_type114),6);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,T237,(&_empty_sig107),((struct _type *)_void_type114),7);
_void_ptr_type_1=_pointer_p_s__type(((struct _type *)_void_type114));
_p_p_2=_new_signature_p_s__memory_bundle_p_s__type_p_s__type(_bundle,_void_ptr_type_1,_void_ptr_type_1);
_p_3=_new_signature_p_s__memory_bundle_p_s__type(_bundle,_void_ptr_type_1);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_at106,_p_3,((struct _type *)0),67);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_assign247,_p_p_2,((struct _type *)_void_type114),66);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_eq50,_p_p_2,((struct _type *)_bool_type123),43);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_ne52,_p_p_2,((struct _type *)_bool_type123),44);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,T248,_p_3,((struct _type *)0),10);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,T249,_p_3,((struct _type *)_bool_type123),8);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,T250,_p_3,((struct _type *)_bool_type123),9);
_void_ary_type_4=_new_array_type_p_s__memory_bundle_in_p_s__type(_bundle,0,((struct _type *)_void_type114));
_ary_ptr_type_5=_pointer_p_s__type(((struct _type *)_void_ary_type_4));
_a_a_6=_new_signature_p_s__memory_bundle_p_s__type_p_s__type(_bundle,_ary_ptr_type_5,_ary_ptr_type_5);
_a_i_7=_new_signature_p_s__memory_bundle_p_s__type_p_s__type(_bundle,_ary_ptr_type_5,((struct _type *)_dword_type132));
_a_8=_new_signature_p_s__memory_bundle_p_s__type(_bundle,_ary_ptr_type_5);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_le54,_a_a_6,((struct _type *)_bool_type123),45);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_lt56,_a_a_6,((struct _type *)_bool_type123),46);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_ge58,_a_a_6,((struct _type *)_bool_type123),47);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_gt60,_a_a_6,((struct _type *)_bool_type123),48);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_at106,_a_8,((struct _type *)0),68);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_at106,_a_i_7,((struct _type *)0),68);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_add36,_a_i_7,((struct _type *)0),69);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_sub38,_a_i_7,((struct _type *)0),70);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_sub38,_a_a_6,((struct _type *)_int_type144),71);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_pre_inc98,_a_8,((struct _type *)0),72);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_pre_dec100,_a_8,((struct _type *)0),73);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_post_inc102,_a_8,((struct _type *)0),74);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_post_dec104,_a_8,((struct _type *)0),75);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_assign_add72,_a_i_7,((struct _type *)_void_type114),76);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_assign_sub74,_a_i_7,((struct _type *)_void_type114),77);
_b_b_9=_new_signature_p_s__memory_bundle_p_s__type_p_s__type(_bundle,((struct _type *)_bool_type123),((struct _type *)_bool_type123));
_b_10=_new_signature_p_s__memory_bundle_p_s__type(_bundle,((struct _type *)_bool_type123));
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,T258,(&_empty_sig107),((struct _type *)_bool_type123),13);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,T259,(&_empty_sig107),((struct _type *)_bool_type123),14);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_and62,_b_b_9,((struct _type *)_bool_type123),35);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_or66,_b_b_9,((struct _type *)_bool_type123),36);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_xor64,_b_b_9,((struct _type *)_bool_type123),37);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_not96,_b_10,((struct _type *)_bool_type123),38);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_andand68,_b_b_9,((struct _type *)_bool_type123),39);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_oror70,_b_b_9,((struct _type *)_bool_type123),40);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_eq50,_b_b_9,((struct _type *)_bool_type123),41);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_ne52,_b_b_9,((struct _type *)_bool_type123),42);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_assign247,_b_b_9,((struct _type *)_void_type114),49);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_assign_and86,_b_b_9,((struct _type *)_void_type114),57);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_assign_or88,_b_b_9,((struct _type *)_void_type114),58);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_assign_xor90,_b_b_9,((struct _type *)_void_type114),59);
_add_definition_p_s__compiler_p_a_ch_p_s__signature_p_s__type_e__definition_code(_cc,_op_assign_not94,_b_10,((struct _type *)_void_type114),61);
_add_numeric_definitions_p_s__compiler_p_s__type(_cc,((struct _type *)_byte_type126));
_add_numeric_definitions_p_s__compiler_p_s__type(_cc,((struct _type *)_word_type129));
_add_numeric_definitions_p_s__compiler_p_s__type(_cc,((struct _type *)_dword_type132));
_add_numeric_definitions_p_s__compiler_p_s__type(_cc,((struct _type *)_qword_type135));
_add_numeric_definitions_p_s__compiler_p_s__type(_cc,((struct _type *)_octet_type138));
_add_numeric_definitions_p_s__compiler_p_s__type(_cc,((struct _type *)_short_type141));
_add_numeric_definitions_p_s__compiler_p_s__type(_cc,((struct _type *)_int_type144));
_add_numeric_definitions_p_s__compiler_p_s__type(_cc,((struct _type *)_long_type147));
_add_numeric_definitions_p_s__compiler_p_s__type(_cc,((struct _type *)_char_type117));
_add_numeric_definitions_p_s__compiler_p_s__type(_cc,((struct _type *)_wchar_type120));
}

static void _initialize_p_s__compiler_p_s__compiler_session_p_s__memory_bundle_p_s__module(struct _compiler *_cc, struct _compiler_session *_session, struct _memory_bundle *_bundle, struct _module *_module)
{
(_cc->_bundle)=_bundle;
(_cc->_session)=_session;
(_cc->_module)=_module;
(_cc->_number_of_errors)=0;
(_cc->_current_scope)=(&(_cc->_global_scope));
(_cc->_current_function)=((struct _function *)0);
(_cc->_current_iterator)=((struct _iterator *)0);
(_cc->_current_loop_level)=0;
(_cc->_current_exposition)=0;
(_cc->_current_directives)=((struct _set_of_directives *)0);
(_cc->_current_block)=((struct _collection_of_expressions *)0);
(_cc->_current_inside_equate)=0;
_initialize_p_s__vector_dw((&(_cc->_imports)),((unsigned int )256));
_initialize_p_s__scope_p_s__scope((&(_cc->_global_scope)),((struct _scope *)0));
_initialize_builtin_types_p_s__compiler(_cc);
_initialize_builtin_definitions_p_s__compiler_p_s__memory_bundle(_cc,_bundle);
}

static int _initialize_p_s__file_buffer_p_a_ch_bo(struct _file_buffer *_buffer, char *_filename, int _is_text)
{
unsigned int _file_size_1;
unsigned int _buffer_size_2;
struct _file *_f_3;
unsigned char *_base_4;
unsigned int _res_5;
(_buffer->_base)=((unsigned char *)0);
_file_size_1=file_size(_filename);
if((_file_size_1==-1))
{
return 2;
}
_buffer_size_2=_file_size_1;
if(_is_text)
{
(++_buffer_size_2);
}
_f_3=fopen(_filename,T271);
if(((void*)_f_3==(void*)0))
{
return 1;
}
_base_4=_allocate_memory_dw(_buffer_size_2);
if(((void*)_base_4==(void*)0))
{
return 3;
}
_res_5=fread(((void *)_base_4),((unsigned int )1),_file_size_1,_f_3);
fclose(_f_3);
if(_is_text)
{
(_base_4[_file_size_1])=((unsigned char)0);
}
(_buffer->_base)=_base_4;
(_buffer->_size)=_file_size_1;
return 0;
}

static void _initialize_p_s__string_buffer(struct _string_buffer *_s)
{
(_s->_p)=((char *)_allocate_memory_dw(((unsigned int )16384)));
(_s->_used)=0;
(_s->_buffer_size)=16384;
}

static void _reserve_p_s__string_buffer_in(struct _string_buffer *_s, int _l)
{
unsigned int _new_size_1;
char *_q_2;
if(((((_s->_used)+_l)+1)<=(_s->_buffer_size)))
{
return;
}
_new_size_1=(((((_s->_used)+_l)+1)*5)/4);
_q_2=((char *)_allocate_memory_dw(_new_size_1));
_copy_p_vo_p_vo_dw(((void *)_q_2),((void *)(_s->_p)),(_s->_used));
_free_memory_p_vo(((void *)(_s->_p)));
(_s->_buffer_size)=_new_size_1;
(_s->_p)=_q_2;
}

static struct _string_buffer *_append_p_s__string_buffer_p_a_ch(struct _string_buffer *_s, char *_str)
{
int _size_1;
_size_1=_size_p_a_ch(_str);
_reserve_p_s__string_buffer_in(_s,_size_1);
_copy_p_vo_p_vo_dw(((void *)((_s->_p)+(_s->_used))),((void *)_str),((unsigned int )_size_1));
(_s->_used)+=_size_1;
return _s;
}

static struct _string_buffer *_append_p_s__string_buffer_ch(struct _string_buffer *_s, char _c)
{
_reserve_p_s__string_buffer_in(_s,1);
((_s->_p)[(_s->_used)])=_c;
(++(_s->_used));
return _s;
}

static struct _string_buffer *_append_p_s__string_buffer_dw(struct _string_buffer *_s, unsigned int _v)
{
unsigned int _i_1;
char (_buf_2)[12];
char *_p_3;
unsigned int _r_4;
_i_1=_v;
_p_3=(_buf_2+11);
(_p_3[0])='\x00';
if((_i_1==0))
{
((--_p_3)[0])='0';
}
else
{
while((_i_1!=0))
{
_r_4=(_i_1%10);
((--_p_3)[0])=('0'+((char )_r_4));
_i_1/=10;
}
}
return _append_p_s__string_buffer_p_a_ch(_s,((char *)_p_3));
}

static struct _string_buffer *_append_p_s__string_buffer_in(struct _string_buffer *_s, int _v)
{
int _i_1;
_i_1=_v;
if((_i_1<0))
{
_append_p_s__string_buffer_ch(_s,'-');
_i_1 = -_i_1;
}
return _append_p_s__string_buffer_dw(_s,((unsigned int )_i_1));
}

static void _append_p_s__string_buffer_p_s__module_in(struct _string_buffer *_buffer, struct _module *_module, int _line)
{
if((((void*)_module!=(void*)0)&&(_line>0)))
{
_append_p_s__string_buffer_p_a_ch(_buffer,(_module->_name));
_append_p_s__string_buffer_p_a_ch(_append_p_s__string_buffer_in(_append_p_s__string_buffer_p_a_ch(_buffer,T289),_line),T290);
}
}

static struct _type *_type_p_s__signature_in(struct _signature *_signature, int _index)
{
struct _type **_p_1;
_p_1=((struct _type **)(((unsigned char *)_signature)+((unsigned int)4)));
return (_p_1[_index]);
}

static struct _string_buffer *_append_p_s__string_buffer_p_s__type(struct _string_buffer *_stream, struct _type *_t)
{
int _n_1;
int _i_2;
switch((_t->_id))
{
case 15:
{
_append_p_s__string_buffer_p_s__type(_append_p_s__string_buffer_p_a_ch(_stream,T293),(((struct _pointer_type *)_t)->_target_type));
break;
}
case 16:
{
_append_p_s__string_buffer_p_a_ch(_append_p_s__string_buffer_p_a_ch(_stream,T294),(((struct _structure_type *)_t)->_short_name));
break;
}
case 17:
{
_append_p_s__string_buffer_p_a_ch(_append_p_s__string_buffer_p_a_ch(_stream,T295),(((struct _structure_type *)_t)->_short_name));
break;
}
case 18:
{
_append_p_s__string_buffer_ch(_stream,'[');
_append_p_s__string_buffer_in(_stream,(((struct _array_type *)_t)->_array_size));
_append_p_s__string_buffer_p_a_ch(_stream,T296);
_append_p_s__string_buffer_p_s__type(_stream,(((struct _array_type *)_t)->_array_type));
break;
}
case 19:
{
_append_p_s__string_buffer_ch(_stream,'{');
_n_1=((((struct _function_type *)_t)->_signature)->_number_of_types);
_i_2=0;
while((_i_2<_n_1))
{
if((_i_2>0))
{
_append_p_s__string_buffer_ch(_stream,',');
}
_append_p_s__string_buffer_p_s__type(_stream,_type_p_s__signature_in((((struct _function_type *)_t)->_signature),_i_2));
(++_i_2);
}
_append_p_s__string_buffer_p_a_ch(_stream,T300);
_append_p_s__string_buffer_p_s__type(_stream,(((struct _function_type *)_t)->_returned_type));
break;
}
case 2:
case 1:
case 4:
case 5:
case 3:
case 6:
case 7:
case 8:
case 9:
case 10:
case 11:
case 12:
case 13:
case 14:
case 0:
case 20:
{
_append_p_s__string_buffer_p_a_ch(_stream,(((struct _named_type *)_t)->_name));
break;
}
}
return _stream;
}

static char *_as_string_p_s__string_buffer(struct _string_buffer *_s)
{
_reserve_p_s__string_buffer_in(_s,1);
((_s->_p)[(_s->_used)])='\x00';
return (_s->_p);
}

static void _release_p_s__string_buffer(struct _string_buffer *_s)
{
_free_memory_p_vo(((void *)(_s->_p)));
}

static void _error_p_s__compiler_p_s__module_in_p_a_ch_p_a_p_vo(struct _compiler *_cc, struct _module *_module, int _line, char *_fmt, void **_args)
{
void **_arg_1;
struct _string_buffer _t_2;
struct _string_buffer *_s_3;
char *_p_4;
char _c_5;
struct _collection_of_expressions *_arguments_6;
struct _element *_e_7;
_arg_1=_args;
(++(_cc->_number_of_errors));
_s_3=(_initialize_p_s__string_buffer((&_t_2)),(&_t_2));
_append_p_s__string_buffer_p_s__module_in(_s_3,_module,_line);
_p_4=_fmt;
while(1)
{
_c_5=((_p_4++)[0]);
if((_c_5=='\x00'))
{
break;
}
else if((_c_5=='#'))
{
_c_5=((_p_4++)[0]);
if((_c_5=='s'))
{
_append_p_s__string_buffer_p_a_ch(_s_3,((char *)((_arg_1++)[0])));
}
else if((_c_5=='t'))
{
_append_p_s__string_buffer_p_s__type(_s_3,((struct _type *)((_arg_1++)[0])));
}
else if((_c_5=='u'))
{
_arguments_6=((struct _collection_of_expressions *)((_arg_1++)[0]));
if(((void*)(((struct _collection *)_arguments_6)->_first)!=(void*)0))
{
_append_p_s__string_buffer_ch(_s_3,'(');
_e_7=(((struct _collection *)_arguments_6)->_first);
while(((void*)_e_7!=(void*)0))
{
if(((void*)((struct _expression *)_e_7)!=(void*)((struct _expression *)(((struct _collection *)_arguments_6)->_first))))
{
_append_p_s__string_buffer_p_a_ch(_s_3,T303);
}
_append_p_s__string_buffer_p_s__type(_s_3,(((struct _expression *)_e_7)->_type));
_e_7=(_e_7->_next_sibling);
}
_append_p_s__string_buffer_ch(_s_3,')');
}
}
else
{
_append_p_s__string_buffer_ch(_s_3,_c_5);
}
}
else
{
_append_p_s__string_buffer_ch(_s_3,_c_5);
}
}
fputs_stderr(_as_string_p_s__string_buffer(_s_3));
fputs_stderr(T304);
_release_p_s__string_buffer(_s_3);
}

static void _error_p_s__compiler_in_p_a_ch_p_vo(struct _compiler *_cc, int _line, char *_fmt, void *_arg1)
{
void *(_args_1)[1];
(_args_1[0])=_arg1;
_error_p_s__compiler_p_s__module_in_p_a_ch_p_a_p_vo(_cc,(_cc->_module),_line,_fmt,((void **)_args_1));
}

static void _initialize_p_s__token_stream_p_s__memory_bundle_p_a_ch_p_s__dictionary_bo(struct _token_stream *_stream, struct _memory_bundle *_bundle, char *_s, struct _dictionary *_defines, int _unicode)
{
(_stream->_bundle)=_bundle;
(_stream->_last_error)=0;
(_stream->_p)=_s;
(_stream->_beginning_of_line)=_s;
(_stream->_line)=1;
(_stream->_ignore_line_feed)=1;
(_stream->_defines)=_defines;
(_stream->_unicode)=_unicode;
}

static struct _collection *_new_collection_p_s__parser(struct _parser *_cc)
{
struct _collection *_c_1;
_c_1=((struct _collection *)_allocate_p_s__memory_bundle_dw((_cc->_bundle),((unsigned int)8)));
_initialize_p_s__collection(_c_1);
return _c_1;
}

static char *_skip_blank_p_s__token_stream_p_a_ch(struct _token_stream *_stream, char *_s)
{
char *_p_1;
char _c_2;
_p_1=_s;
_c_2=(_p_1[0]);
while(1)
{
if((!(_stream->_ignore_line_feed)))
{
while(((_c_2==' ')||(_c_2=='\t')))
{
(++_p_1);
_c_2=(_p_1[0]);
}
}
else
{
while(1)
{
if(((_c_2==' ')||(_c_2=='\t')))
{
_c_2=((++_p_1)[0]);
}
else if((_c_2=='\r'))
{
_c_2=((++_p_1)[0]);
if((_c_2=='\n'))
{
_c_2=((++_p_1)[0]);
}
(++(_stream->_line));
(_stream->_beginning_of_line)=_p_1;
}
else if((_c_2=='\n'))
{
_c_2=((++_p_1)[0]);
(++(_stream->_line));
(_stream->_beginning_of_line)=_p_1;
}
else
{
break;
}
}
}
if((_c_2=='\\'))
{
_c_2=((++_p_1)[0]);
if((_c_2=='\r'))
{
_c_2=((++_p_1)[0]);
if((_c_2=='\n'))
{
_c_2=((++_p_1)[0]);
}
(++(_stream->_line));
(_stream->_beginning_of_line)=_p_1;
}
else if((_c_2=='\n'))
{
_c_2=((++_p_1)[0]);
(++(_stream->_line));
(_stream->_beginning_of_line)=_p_1;
}
else
{
(--_p_1);
break;
}
}
else
{
break;
}
}
return _p_1;
}

static unsigned int _strtoul_p_a_ch_p_a_p_a_ch(char *_s, char **_endptr)
{
char *_p_1;
unsigned int _value_2;
int _base_3;
char _c_4;
_p_1=_s;
_value_2=((unsigned int)0);
if((((_p_1[0])=='0')&&(((_p_1[1])=='x')||((_p_1[1])=='X'))))
{
_p_1+=2;
_base_3=16;
}
else
{
_base_3=10;
}
while(1)
{
_c_4=((_p_1++)[0]);
if((_c_4=='\x00'))
{
break;
}
if((_c_4>='a'))
{
_c_4-='W';
}
else if((_c_4>='A'))
{
_c_4-='7';
}
else if((_c_4<='9'))
{
_c_4-='0';
}
else
{
_c_4='�';
}
if((((unsigned char )_c_4)>((unsigned char )_base_3)))
{
break;
}
_value_2*=_base_3;
_value_2+=((unsigned int )((unsigned char )_c_4));
}
(_endptr[0])=(_p_1-1);
return _value_2;
}

static int _error_p_s__token_stream_p_a_ch_e__lexer_error(struct _token_stream *_stream, char *_p, int _err)
{
(_stream->_p)=_p;
(_stream->_last_error)=_err;
return 0;
}

static int _read_string_p_s__token_stream_p_s__lexeme(struct _token_stream *_stream, union _lexeme *_lexeme)
{
char *_p_1;
char (_buf_2)[2049];
char *_q_3;
char *_limit_4;
char _c_5;
char (_id_6)[2049];
char *_s_7;
int _n_8;
char *_value_9;
char *_v_10;
int _len_11;
char *_persistent_buf_12;
_p_1=(_stream->_p);
_q_3=_buf_2;
_limit_4=(_buf_2+2048);
while(1)
{
_c_5=((_p_1++)[0]);
if((_c_5=='\"'))
{
break;
}
if((_c_5=='\x00'))
{
return _error_p_s__token_stream_p_a_ch_e__lexer_error(_stream,(_p_1-1),1);
}
if((_c_5=='\n'))
{
return _error_p_s__token_stream_p_a_ch_e__lexer_error(_stream,(_p_1-1),2);
}
if(((void*)_q_3==(void*)_limit_4))
{
return _error_p_s__token_stream_p_a_ch_e__lexer_error(_stream,(_p_1-1),3);
}
if((_c_5=='\\'))
{
_c_5=((_p_1++)[0]);
if((_c_5=='\x00'))
{
return _error_p_s__token_stream_p_a_ch_e__lexer_error(_stream,(_p_1-1),1);
}
if((_c_5=='('))
{
_s_7=_id_6;
_n_8=0;
while(1)
{
_c_5=((_p_1++)[0]);
if((_c_5=='\x00'))
{
return _error_p_s__token_stream_p_a_ch_e__lexer_error(_stream,(_p_1-1),1);
}
else if((_c_5=='\n'))
{
return _error_p_s__token_stream_p_a_ch_e__lexer_error(_stream,(_p_1-1),2);
}
else if((_c_5==')'))
{
break;
}
if((_n_8==2048))
{
return _error_p_s__token_stream_p_a_ch_e__lexer_error(_stream,(_p_1-1),3);
}
((_s_7++)[0])=_c_5;
}
((_s_7++)[0])='\x00';
_value_9=((char *)_value_p_s__dictionary_p_vo((_stream->_defines),((void *)_id_6)));
if(((void*)_value_9==(void*)0))
{
return _error_p_s__token_stream_p_a_ch_e__lexer_error(_stream,_p_1,9);
}
_v_10=_value_9;
while(1)
{
_c_5=((_v_10++)[0]);
if((_c_5=='\x00'))
{
break;
}
if(((void*)_q_3==(void*)_limit_4))
{
return _error_p_s__token_stream_p_a_ch_e__lexer_error(_stream,(_p_1-1),3);
}
((_q_3++)[0])=_c_5;
}
}
else
{
switch(_c_5)
{
case 97:
{
_c_5='\a';
break;
}
case 98:
{
_c_5='\b';
break;
}
case 102:
{
_c_5='\f';
break;
}
case 110:
{
_c_5='\n';
break;
}
case 114:
{
_c_5='\r';
break;
}
case 116:
{
_c_5='\t';
break;
}
case 118:
{
_c_5='\v';
break;
}
case 119:
{
_c_5=' ';
break;
}
}
((_q_3++)[0])=_c_5;
}
}
else
{
((_q_3++)[0])=_c_5;
}
}
((_q_3++)[0])='\x00';
if(((void*)_lexeme!=(void*)0))
{
_len_11=(_q_3-_buf_2);
_persistent_buf_12=((char *)_allocate_p_s__memory_bundle_dw((_stream->_bundle),((unsigned int )_len_11)));
_copy_p_vo_p_vo_dw(((void *)_persistent_buf_12),((void *)_buf_2),((unsigned int )_len_11));
(_lexeme->_string_value)=_persistent_buf_12;
}
(_stream->_p)=_p_1;
return 4;
}

static int _is_identifier_char_ch(char _c)
{
return ((((((unsigned char )((_c|' ')-'a'))<((unsigned char)26))||(((unsigned char )(_c-'0'))<((unsigned char)10)))||(_c=='_'))||(_c=='@'));
}

static int _compare_p_vo_p_vo_dw(void *_dst, void *_src, unsigned int _size)
{
unsigned char *_p_1;
unsigned char *_q_2;
unsigned char *_limit_3;
unsigned char _diff_4;
_p_1=((unsigned char *)_src);
_q_2=((unsigned char *)_dst);
_limit_3=(_q_2+_size);
while(((void*)_q_2<(void*)_limit_3))
{
_diff_4=((_q_2[0])-(_p_1[0]));
if((_diff_4!=((unsigned char)0)))
{
return ((int )((char )_diff_4));
}
(++_p_1);
(++_q_2);
}
return 0;
}

static int _get_token_p_s__token_stream_p_s__lexeme(struct _token_stream *_stream, union _lexeme *_lexeme)
{
char *_p_1;
int _unicode_2;
char _c_3;
int _token_4;
char *(_endc_5)[1];
int _value_6;
char *_s_7;
int _len_8;
int _i_9;
char *_endc_10;
char *_buf_11;
char *_q_12;
char *(_endc_13)[1];
unsigned int _l_14;
_p_1=(_stream->_p);
_unicode_2=0;
_p_1=_skip_blank_p_s__token_stream_p_a_ch(_stream,_p_1);
while(1)
{
if((((_p_1[0])=='/')&&((_p_1[1])=='/')))
{
_p_1+=2;
_c_3=(_p_1[0]);
while((((_c_3!='\x00')&&(_c_3!='\r'))&&(_c_3!='\n')))
{
_c_3=((++_p_1)[0]);
}
_p_1=_skip_blank_p_s__token_stream_p_a_ch(_stream,_p_1);
}
else if((((_p_1[0])=='/')&&((_p_1[1])=='*')))
{
_p_1+=2;
_c_3=(_p_1[0]);
while(1)
{
if(((_c_3=='*')&&((_p_1[1])=='/')))
{
_p_1+=2;
break;
}
else if((_c_3=='\x00'))
{
break;
}
else if((_c_3=='\r'))
{
_c_3=((++_p_1)[0]);
if((_c_3=='\n'))
{
_c_3=((++_p_1)[0]);
}
(++(_stream->_line));
(_stream->_beginning_of_line)=_p_1;
}
else if((_c_3=='\n'))
{
_c_3=((++_p_1)[0]);
(++(_stream->_line));
(_stream->_beginning_of_line)=_p_1;
}
else
{
_c_3=((++_p_1)[0]);
}
}
_p_1=_skip_blank_p_s__token_stream_p_a_ch(_stream,_p_1);
}
else
{
break;
}
}
(_stream->_ignore_line_feed)=0;
switch(((_p_1++)[0]))
{
case 0:
{
(--_p_1);
_token_4=1;
break;
}
case 13:
{
if(((_p_1[0])=='\n'))
{
(++_p_1);
}
(++(_stream->_line));
(_stream->_beginning_of_line)=_p_1;
_token_4=64;
(_stream->_ignore_line_feed)=1;
break;
}
case 10:
{
(++(_stream->_line));
(_stream->_beginning_of_line)=_p_1;
_token_4=64;
(_stream->_ignore_line_feed)=1;
break;
}
case 59:
{
_token_4=64;
(_stream->_ignore_line_feed)=1;
break;
}
case 48:
case 49:
case 50:
case 51:
case 52:
case 53:
case 54:
case 55:
case 56:
case 57:
{
_value_6=((int )_strtoul_p_a_ch_p_a_p_a_ch((_p_1-1),((char **)_endc_5)));
if(((void*)_endc_5!=(void*)(_p_1-1)))
{
_p_1=(_endc_5[0]);
}
_token_4=3;
if(((void*)_lexeme!=(void*)0))
{
(_lexeme->_long_value)=_value_6;
}
break;
}
case 64:
case 95:
case 97:
case 98:
case 99:
case 100:
case 101:
case 102:
case 103:
case 104:
case 105:
case 106:
case 107:
case 108:
case 109:
case 110:
case 111:
case 112:
case 113:
case 114:
case 115:
case 116:
case 117:
case 118:
case 119:
case 120:
case 121:
case 122:
case 65:
case 66:
case 67:
case 68:
case 69:
case 70:
case 71:
case 72:
case 73:
case 74:
case 75:
case 76:
case 77:
case 78:
case 79:
case 80:
case 81:
case 82:
case 83:
case 84:
case 85:
case 86:
case 87:
case 88:
case 89:
case 90:
{
(--_p_1);
_s_7=_p_1;
_c_3=(_s_7[0]);
if(((_c_3=='a')&&((_s_7[1])=='\"')))
{
(_stream->_p)=(_p_1+2);
_token_4=_read_string_p_s__token_stream_p_s__lexeme(_stream,_lexeme);
return ((_token_4==0)?_token_4:4);
}
else if(((_c_3=='w')&&((_s_7[1])=='\"')))
{
(_stream->_p)=(_p_1+2);
_token_4=_read_string_p_s__token_stream_p_s__lexeme(_stream,_lexeme);
return ((_token_4==0)?_token_4:5);
}
else
{
while(_is_identifier_char_ch(_c_3))
{
_c_3=((++_s_7)[0]);
}
_len_8=(_s_7-_p_1);
_i_9=0;
while((_i_9<21))
{
if(((_compare_p_vo_p_vo_dw(((void *)((&(_keywords10[_i_9]))->_name)),((void *)_p_1),((unsigned int )_len_8))==0)&&((((&(_keywords10[_i_9]))->_name)[_len_8])=='\x00')))
{
break;
}
(++_i_9);
}
if((_i_9<21))
{
_token_4=((&(_keywords10[_i_9]))->_token);
_p_1+=_len_8;
}
else
{
_token_4=2;
_endc_10=_s_7;
while(1)
{
_s_7=_skip_blank_p_s__token_stream_p_a_ch(_stream,_s_7);
if((!_is_identifier_char_ch((_s_7[0]))))
{
break;
}
while(_is_identifier_char_ch((_s_7[0])))
{
(++_s_7);
(++_len_8);
}
(++_len_8);
_endc_10=_s_7;
}
if(((void*)_lexeme!=(void*)0))
{
_buf_11=((char *)_allocate_p_s__memory_bundle_dw((_stream->_bundle),((unsigned int )(_len_8+1))));
_q_12=_buf_11;
while(((void*)_p_1<(void*)_endc_10))
{
_c_3=((_p_1++)[0]);
if(((_c_3==' ')||(_c_3=='\t')))
{
((_q_12++)[0])=' ';
while(((_c_3==' ')||(_c_3=='\t')))
{
_c_3=((_p_1++)[0]);
}
}
((_q_12++)[0])=_c_3;
}
((_q_12++)[0])='\x00';
(_lexeme->_string_value)=_buf_11;
}
_p_1=_endc_10;
}
}
break;
}
case 34:
{
(_stream->_p)=_p_1;
_token_4=_read_string_p_s__token_stream_p_s__lexeme(_stream,_lexeme);
return ((_token_4==0)?_token_4:((_stream->_unicode)?5:4));
break;
}
case 36:
{
_c_3=((_p_1++)[0]);
if((_c_3=='\x00'))
{
return _error_p_s__token_stream_p_a_ch_e__lexer_error(_stream,(_p_1-2),4);
}
if(((void*)_lexeme!=(void*)0))
{
(_lexeme->_long_value)=((int )((unsigned int )_c_3));
}
_token_4=((_stream->_unicode)?7:6);
break;
}
case 92:
{
_c_3=((_p_1++)[0]);
if((_c_3=='\x00'))
{
return _error_p_s__token_stream_p_a_ch_e__lexer_error(_stream,(_p_1-2),4);
}
switch(_c_3)
{
case 97:
{
_c_3='\a';
break;
}
case 98:
{
_c_3='\b';
break;
}
case 102:
{
_c_3='\f';
break;
}
case 110:
{
_c_3='\n';
break;
}
case 114:
{
_c_3='\r';
break;
}
case 116:
{
_c_3='\t';
break;
}
case 118:
{
_c_3='\v';
break;
}
case 119:
{
_c_3=' ';
break;
}
default:
{
if(((_c_3>='0')&&(_c_3<='9')))
{
_l_14=_strtoul_p_a_ch_p_a_p_a_ch((_p_1-1),((char **)_endc_13));
if(((void*)_endc_13!=(void*)(_p_1-1)))
{
_p_1=(_endc_13[0]);
}
_c_3=((char )_l_14);
}
else
{
return _error_p_s__token_stream_p_a_ch_e__lexer_error(_stream,(_p_1-1),5);
}
break;
}
}
if(((void*)_lexeme!=(void*)0))
{
(_lexeme->_long_value)=((int )_c_3);
}
_token_4=((_stream->_unicode)?7:6);
break;
}
case 44:
{
_token_4=66;
(_stream->_ignore_line_feed)=1;
break;
}
case 126:
{
if(((_p_1[0])=='!'))
{
_token_4=35;
(++_p_1);
}
else
{
_token_4=18;
(_stream->_ignore_line_feed)=1;
}
break;
}
case 38:
{
if(((_p_1[0])=='&'))
{
_token_4=21;
(++_p_1);
}
else if(((_p_1[0])=='='))
{
_token_4=30;
(++_p_1);
}
else
{
_token_4=15;
}
(_stream->_ignore_line_feed)=1;
break;
}
case 124:
{
if(((_p_1[0])=='|'))
{
_token_4=22;
(++_p_1);
}
else if(((_p_1[0])=='='))
{
_token_4=31;
(++_p_1);
}
else
{
_token_4=16;
}
(_stream->_ignore_line_feed)=1;
break;
}
case 43:
{
if(((_p_1[0])=='+'))
{
_token_4=19;
(++_p_1);
}
else if(((_p_1[0])=='='))
{
_token_4=23;
(++_p_1);
(_stream->_ignore_line_feed)=1;
}
else
{
_token_4=8;
(_stream->_ignore_line_feed)=1;
}
break;
}
case 45:
{
if(((_p_1[0])=='-'))
{
_token_4=20;
(++_p_1);
}
else if(((_p_1[0])=='!'))
{
_token_4=36;
(++_p_1);
}
else if(((_p_1[0])=='='))
{
_token_4=24;
(++_p_1);
(_stream->_ignore_line_feed)=1;
}
else if(((_p_1[0])=='>'))
{
_token_4=78;
(++_p_1);
(_stream->_ignore_line_feed)=1;
}
else
{
_token_4=9;
(_stream->_ignore_line_feed)=1;
}
break;
}
case 62:
{
if(((_p_1[0])=='='))
{
_token_4=41;
(++_p_1);
}
else if((((_p_1[0])=='>')&&((_p_1[1])=='=')))
{
_token_4=29;
_p_1+=2;
}
else if(((_p_1[0])=='>'))
{
_token_4=14;
(++_p_1);
}
else
{
_token_4=42;
}
(_stream->_ignore_line_feed)=1;
break;
}
case 60:
{
if(((_p_1[0])=='='))
{
_token_4=39;
(++_p_1);
}
else if(((_p_1[0])=='>'))
{
_token_4=38;
(++_p_1);
}
else if((((_p_1[0])=='<')&&((_p_1[1])=='=')))
{
_token_4=28;
_p_1+=2;
}
else if(((_p_1[0])=='<'))
{
_token_4=13;
(++_p_1);
}
else
{
_token_4=40;
}
(_stream->_ignore_line_feed)=1;
break;
}
case 61:
{
if(((_p_1[0])=='='))
{
_token_4=37;
(++_p_1);
}
else
{
_token_4=34;
}
(_stream->_ignore_line_feed)=1;
break;
}
case 94:
{
if(((_p_1[0])=='='))
{
_token_4=32;
(++_p_1);
}
else
{
_token_4=17;
}
(_stream->_ignore_line_feed)=1;
break;
}
case 42:
{
if(((_p_1[0])=='='))
{
_token_4=25;
(++_p_1);
}
else
{
_token_4=10;
}
(_stream->_ignore_line_feed)=1;
break;
}
case 47:
{
if(((_p_1[0])=='='))
{
_token_4=26;
(++_p_1);
}
else
{
_token_4=11;
}
(_stream->_ignore_line_feed)=1;
break;
}
case 37:
{
if(((_p_1[0])=='='))
{
_token_4=27;
(++_p_1);
}
else
{
_token_4=12;
}
(_stream->_ignore_line_feed)=1;
break;
}
case 58:
{
if(((_p_1[0])=='='))
{
_token_4=33;
(++_p_1);
}
else
{
_token_4=67;
}
(_stream->_ignore_line_feed)=1;
break;
}
case 33:
{
_token_4=68;
break;
}
case 35:
{
_token_4=69;
break;
}
case 40:
{
_token_4=72;
(_stream->_ignore_line_feed)=1;
break;
}
case 41:
{
_token_4=73;
break;
}
case 91:
{
_token_4=76;
(_stream->_ignore_line_feed)=1;
break;
}
case 93:
{
_token_4=77;
break;
}
case 123:
{
_token_4=74;
(_stream->_ignore_line_feed)=1;
break;
}
case 125:
{
_token_4=75;
break;
}
case 46:
{
_token_4=65;
break;
}
case 63:
{
_token_4=70;
break;
}
default:
{
return _error_p_s__token_stream_p_a_ch_e__lexer_error(_stream,(_p_1-1),6);
break;
}
}
(_stream->_p)=_p_1;
return _token_4;
}

static void _initialize_p_s__token_stream_p_s__token_stream(struct _token_stream *_stream, struct _token_stream *_source)
{
(_stream->_beginning_of_line)=(_source->_beginning_of_line);
(_stream->_p)=(_source->_p);
(_stream->_line)=(_source->_line);
(_stream->_ignore_line_feed)=(_source->_ignore_line_feed);
(_stream->_defines)=(_source->_defines);
(_stream->_unicode)=(_source->_unicode);
}

static int _peek_token_p_s__token_stream(struct _token_stream *_stream)
{
struct _token_stream _tmp_1;
_initialize_p_s__token_stream_p_s__token_stream((&_tmp_1),_stream);
return _get_token_p_s__token_stream_p_s__lexeme((&_tmp_1),((union _lexeme *)0));
}

static int _get_token_p_s__token_stream(struct _token_stream *_stream)
{
return _get_token_p_s__token_stream_p_s__lexeme(_stream,((union _lexeme *)0));
}

static void _append_p_s__string_buffer_p_s__parser(struct _string_buffer *_buffer, struct _parser *_cc)
{
char *_filename_1;
int _line_2;
_filename_1=(_cc->_filename);
_line_2=(((struct _token_stream *)_cc)->_line);
if(((void*)_filename_1!=(void*)0))
{
_append_p_s__string_buffer_p_a_ch(_buffer,_filename_1);
_append_p_s__string_buffer_p_a_ch(_append_p_s__string_buffer_in(_append_p_s__string_buffer_p_a_ch(_buffer,T289),_line_2),T290);
}
}

static void _append_p_s__string_buffer_e__token_p_s__lexeme(struct _string_buffer *_stream, int _token, union _lexeme *_lexeme)
{
char *_s_1;
char _c_2;
switch(_token)
{
case 0:
{
_s_1=T367;
break;
}
case 1:
{
_s_1=T368;
break;
}
case 2:
{
if(((void*)_lexeme!=(void*)0))
{
_s_1=(_lexeme->_string_value);
}
else
{
_s_1=T369;
}
break;
}
case 3:
{
_append_p_s__string_buffer_in(_stream,(_lexeme->_long_value));
return;
break;
}
case 4:
{
_s_1=T370;
break;
}
case 5:
{
_s_1=T371;
break;
}
case 6:
{
if(((void*)_lexeme==(void*)0))
{
_s_1=T372;
}
else
{
_c_2=((char )(_lexeme->_long_value));
if((_c_2>' '))
{
_append_p_s__string_buffer_ch(_stream,'$');
_append_p_s__string_buffer_ch(_stream,_c_2);
return;
}
else
{
switch(_c_2)
{
case 7:
{
_s_1=T374;
break;
}
case 8:
{
_s_1=T375;
break;
}
case 12:
{
_s_1=T376;
break;
}
case 10:
{
_s_1=T377;
break;
}
case 13:
{
_s_1=T378;
break;
}
case 9:
{
_s_1=T379;
break;
}
case 11:
{
_s_1=T380;
break;
}
case 32:
{
_s_1=T381;
break;
}
default:
{
_append_p_s__string_buffer_in(_stream,((int )((unsigned int )_c_2)));
_append_p_s__string_buffer_p_a_ch(_stream,T382);
return;
break;
}
}
}
}
break;
}
case 8:
{
_s_1=T383;
break;
}
case 9:
{
_s_1=T384;
break;
}
case 10:
{
_s_1=T385;
break;
}
case 11:
{
_s_1=T386;
break;
}
case 12:
{
_s_1=T387;
break;
}
case 13:
{
_s_1=T388;
break;
}
case 14:
{
_s_1=T389;
break;
}
case 15:
{
_s_1=T390;
break;
}
case 16:
{
_s_1=T391;
break;
}
case 17:
{
_s_1=T392;
break;
}
case 18:
{
_s_1=T393;
break;
}
case 19:
{
_s_1=T394;
break;
}
case 20:
{
_s_1=T395;
break;
}
case 21:
{
_s_1=T396;
break;
}
case 22:
{
_s_1=T397;
break;
}
case 23:
{
_s_1=T398;
break;
}
case 24:
{
_s_1=T399;
break;
}
case 25:
{
_s_1=T400;
break;
}
case 26:
{
_s_1=T401;
break;
}
case 27:
{
_s_1=T402;
break;
}
case 28:
{
_s_1=T403;
break;
}
case 29:
{
_s_1=T404;
break;
}
case 30:
{
_s_1=T405;
break;
}
case 31:
{
_s_1=T406;
break;
}
case 32:
{
_s_1=T407;
break;
}
case 34:
{
_s_1=T408;
break;
}
case 35:
{
_s_1=T409;
break;
}
case 36:
{
_s_1=T410;
break;
}
case 37:
{
_s_1=T411;
break;
}
case 38:
{
_s_1=T412;
break;
}
case 39:
{
_s_1=T413;
break;
}
case 40:
{
_s_1=T414;
break;
}
case 41:
{
_s_1=T415;
break;
}
case 42:
{
_s_1=T416;
break;
}
case 43:
{
_s_1=T31;
break;
}
case 44:
{
_s_1=T24;
break;
}
case 45:
{
_s_1=T25;
break;
}
case 46:
{
_s_1=T29;
break;
}
case 47:
{
_s_1=T26;
break;
}
case 58:
{
_s_1=T27;
break;
}
case 59:
{
_s_1=T28;
break;
}
case 48:
{
_s_1=T30;
break;
}
case 49:
{
_s_1=T12;
break;
}
case 50:
{
_s_1=T13;
break;
}
case 51:
{
_s_1=T15;
break;
}
case 52:
{
_s_1=T16;
break;
}
case 53:
{
_s_1=T17;
break;
}
case 54:
{
_s_1=T14;
break;
}
case 55:
{
_s_1=T32;
break;
}
case 56:
{
_s_1=T18;
break;
}
case 57:
{
_s_1=T19;
break;
}
case 60:
{
_s_1=T20;
break;
}
case 61:
{
_s_1=T21;
break;
}
case 62:
{
_s_1=T22;
break;
}
case 63:
{
_s_1=T23;
break;
}
case 64:
{
_s_1=T417;
break;
}
case 65:
{
_s_1=T418;
break;
}
case 66:
{
_s_1=T419;
break;
}
case 67:
{
_s_1=T289;
break;
}
case 68:
{
_s_1=T420;
break;
}
case 69:
{
_s_1=T421;
break;
}
case 70:
{
_s_1=T422;
break;
}
case 71:
{
_s_1=T423;
break;
}
case 72:
{
_s_1=T424;
break;
}
case 73:
{
_s_1=T425;
break;
}
case 74:
{
_s_1=T426;
break;
}
case 75:
{
_s_1=T427;
break;
}
case 76:
{
_s_1=T428;
break;
}
case 77:
{
_s_1=T429;
break;
}
case 78:
{
_s_1=T430;
break;
}
default:
{
_s_1=T431;
break;
}
}
_append_p_s__string_buffer_p_a_ch(_stream,_s_1);
}

static void _error_p_s__parser_p_a_ch_p_a_s__arg(struct _parser *_cc, char *_fmt, union _arg *_args)
{
union _arg *_arg_1;
struct _string_buffer _t_2;
struct _string_buffer *_s_3;
char *_p_4;
char _c_5;
int _token_6;
union _lexeme *_lexeme_7;
_arg_1=_args;
(++(_cc->_number_of_errors));
_s_3=(_initialize_p_s__string_buffer((&_t_2)),(&_t_2));
_append_p_s__string_buffer_p_s__parser(_s_3,_cc);
_p_4=_fmt;
while(1)
{
_c_5=((_p_4++)[0]);
if((_c_5=='\x00'))
{
break;
}
else if((_c_5=='#'))
{
_c_5=((_p_4++)[0]);
if((_c_5=='s'))
{
_append_p_s__string_buffer_p_a_ch(_s_3,((char *)(&((_arg_1++)[0]))));
}
else if((_c_5=='k'))
{
_token_6=((int )((&((_arg_1++)[0]))->_value));
_lexeme_7=((union _lexeme *)((&((_arg_1++)[0]))->_object));
_append_p_s__string_buffer_e__token_p_s__lexeme(_s_3,_token_6,_lexeme_7);
}
else
{
_append_p_s__string_buffer_ch(_s_3,_c_5);
}
}
else
{
_append_p_s__string_buffer_ch(_s_3,_c_5);
}
}
fputs_stderr(_as_string_p_s__string_buffer(_s_3));
fputs_stderr(T304);
_release_p_s__string_buffer(_s_3);
}

static void _error_p_s__parser_p_a_ch_p_vo_p_vo_p_vo_p_vo(struct _parser *_cc, char *_fmt, void *_arg1, void *_arg2, void *_arg3, void *_arg4)
{
union _arg (_args_1)[4];
((&(_args_1[0]))->_object)=_arg1;
((&(_args_1[1]))->_object)=_arg2;
((&(_args_1[2]))->_object)=_arg3;
((&(_args_1[3]))->_object)=_arg4;
_error_p_s__parser_p_a_ch_p_a_s__arg(_cc,_fmt,((union _arg *)_args_1));
}

static int _unexpected_p_s__parser_e__token_p_s__lexeme_e__token(struct _parser *_cc, int _token, union _lexeme *_lexeme, int _expected_token)
{
if((_token==_expected_token))
{
return 0;
}
_error_p_s__parser_p_a_ch_p_vo_p_vo_p_vo_p_vo(_cc,_error_syntax_error433,((void *)_expected_token),((void *)0),((void *)_token),((void *)_lexeme));
return 1;
}

static char *_get_id_p_s__parser(struct _parser *_cc)
{
union _lexeme _lexeme_1;
int _token_2;
_token_2=_get_token_p_s__token_stream_p_s__lexeme(((struct _token_stream *)_cc),(&_lexeme_1));
if(_unexpected_p_s__parser_e__token_p_s__lexeme_e__token(_cc,_token_2,(&_lexeme_1),2))
{
return ((char *)0);
}
return ((&_lexeme_1)->_string_value);
}

static int _expected_p_s__parser_e__token(struct _parser *_cc, int _expected_token)
{
union _lexeme _lexeme_1;
int _token_2;
_token_2=_get_token_p_s__token_stream_p_s__lexeme(((struct _token_stream *)_cc),(&_lexeme_1));
return (!_unexpected_p_s__parser_e__token_p_s__lexeme_e__token(_cc,_token_2,(&_lexeme_1),_expected_token));
}

static void _error_p_s__parser_p_a_ch_p_vo_p_vo(struct _parser *_cc, char *_fmt, void *_arg1, void *_arg2)
{
union _arg (_args_1)[2];
((&(_args_1[0]))->_object)=_arg1;
((&(_args_1[1]))->_object)=_arg2;
_error_p_s__parser_p_a_ch_p_a_s__arg(_cc,_fmt,((union _arg *)_args_1));
}

static void* _syntax_error_p_s__parser_e__token_p_s__lexeme(struct _parser *_cc, int _token, union _lexeme *_lexeme)
{
_error_p_s__parser_p_a_ch_p_vo_p_vo(_cc,_error_token_unexpected440,((void *)_token),((void *)_lexeme));
return 0;
}

static struct _binary_node *_new_node_p_s__parser_p_vo_p_vo(struct _parser *_cc, void *_value_1, void *_value_2)
{
struct _binary_node *_n_1;
_n_1=((struct _binary_node *)_allocate_p_s__memory_bundle_dw((_cc->_bundle),((unsigned int)20)));
(((struct _node *)_n_1)->_code)=0;
(((struct _node *)_n_1)->_line)=(((struct _token_stream *)_cc)->_line);
(_n_1->_value_1)=_value_1;
(_n_1->_value_2)=_value_2;
return _n_1;
}

static void _append_p_s__collection_p_s__element(struct _collection *_c, struct _element *_e)
{
if(((void*)(_c->_last)!=(void*)0))
{
((_c->_last)->_next_sibling)=_e;
}
(_c->_last)=_e;
if(((void*)(_c->_first)==(void*)0))
{
(_c->_first)=_e;
}
(_e->_next_sibling)=((struct _element *)0);
}

static struct _collection *_get_set_of_directives_p_s__parser(struct _parser *_cc)
{
int _token_1;
struct _collection *_directives_2;
int _ok_3;
char *_name_4;
union _lexeme _lexeme_5;
char *_value_6;
struct _binary_node *_directive_7;
_token_1=_peek_token_p_s__token_stream(((struct _token_stream *)_cc));
if((_token_1!=76))
{
return (&_empty_collection351);
}
else
{
_get_token_p_s__token_stream(((struct _token_stream *)_cc));
_directives_2=_new_collection_p_s__parser(_cc);
while(1)
{
_ok_3=1;
_name_4=_get_id_p_s__parser(_cc);
_ok_3&=((void*)_name_4!=(void*)0);
_ok_3&=_expected_p_s__parser_e__token(_cc,34);
_token_1=_get_token_p_s__token_stream_p_s__lexeme(((struct _token_stream *)_cc),(&_lexeme_5));
if(((_ok_3&&(_token_1!=4))&&(_token_1!=5)))
{
_syntax_error_p_s__parser_e__token_p_s__lexeme(_cc,_token_1,(&_lexeme_5));
_ok_3=0;
}
if(_ok_3)
{
_value_6=((&_lexeme_5)->_string_value);
_directive_7=_new_node_p_s__parser_p_vo_p_vo(_cc,((void *)_name_4),((void *)_value_6));
_append_p_s__collection_p_s__element(_directives_2,((struct _element *)_directive_7));
}
_token_1=_peek_token_p_s__token_stream(((struct _token_stream *)_cc));
if((_token_1!=66))
{
break;
}
_get_token_p_s__token_stream(((struct _token_stream *)_cc));
}
_expected_p_s__parser_e__token(_cc,77);
return _directives_2;
}
}

static struct _node *_new_node_p_s__parser_e__node_code_dw(struct _parser *_cc, int _code, unsigned int _size)
{
struct _node *_n_1;
_n_1=((struct _node *)_allocate_p_s__memory_bundle_dw((_cc->_bundle),_size));
(_n_1->_code)=_code;
(_n_1->_line)=(((struct _token_stream *)_cc)->_line);
return _n_1;
}

static struct _string_node *_new_string_p_s__parser_e__node_code_p_a_ch(struct _parser *_cc, int _code, char *_value)
{
struct _string_node *_n_1;
_n_1=((struct _string_node *)_new_node_p_s__parser_e__node_code_dw(_cc,_code,((unsigned int)16)));
(_n_1->_value)=_value;
return _n_1;
}

static struct _unary_node *_new_node_p_s__parser_e__node_code_p_vo(struct _parser *_cc, int _code, void *_value)
{
struct _unary_node *_n_1;
_n_1=((struct _unary_node *)_allocate_p_s__memory_bundle_dw((_cc->_bundle),((unsigned int)16)));
(((struct _node *)_n_1)->_code)=_code;
(((struct _node *)_n_1)->_line)=(((struct _token_stream *)_cc)->_line);
(_n_1->_value_1)=_value;
return _n_1;
}

static struct _collection *_get_mini_block_p_s__parser(struct _parser *_cc)
{
struct _collection *_block_1;
int _token_2;
struct _node *_instr_3;
if((!_expected_p_s__parser_e__token(_cc,64)))
{
return ((struct _collection *)0);
}
_block_1=_new_collection_p_s__parser(_cc);
while(1)
{
_token_2=_peek_token_p_s__token_stream(((struct _token_stream *)_cc));
if(((((_token_2==48)||(_token_2==46))||(_token_2==45))||(_token_2==59)))
{
break;
}
_instr_3=_get_expression_p_s__parser_in(_cc,0);
if(((void*)_instr_3==(void*)0))
{
return ((struct _collection *)0);
}
_append_p_s__collection_p_s__element(_block_1,((struct _element *)_instr_3));
if((!_expected_p_s__parser_e__token(_cc,64)))
{
break;
}
}
return _block_1;
}

static struct _binary_node *_new_node_p_s__parser_e__node_code_p_vo_p_vo(struct _parser *_cc, int _code, void *_value_1, void *_value_2)
{
struct _binary_node *_n_1;
_n_1=((struct _binary_node *)_allocate_p_s__memory_bundle_dw((_cc->_bundle),((unsigned int)20)));
(((struct _node *)_n_1)->_code)=_code;
(((struct _node *)_n_1)->_line)=(((struct _token_stream *)_cc)->_line);
(_n_1->_value_1)=_value_1;
(_n_1->_value_2)=_value_2;
return _n_1;
}

static struct _collection *_get_sequence_of_instructions_p_s__parser(struct _parser *_cc)
{
struct _collection *_block_1;
int _token_2;
struct _node *_instr_3;
_block_1=_new_collection_p_s__parser(_cc);
while(1)
{
_token_2=_peek_token_p_s__token_stream(((struct _token_stream *)_cc));
if((_token_2==48))
{
_get_token_p_s__token_stream(((struct _token_stream *)_cc));
break;
}
_instr_3=_get_expression_p_s__parser_in(_cc,0);
if(((void*)_instr_3==(void*)0))
{
return ((struct _collection *)0);
}
_append_p_s__collection_p_s__element(_block_1,((struct _element *)_instr_3));
if((!_expected_p_s__parser_e__token(_cc,64)))
{
break;
}
}
return _block_1;
}

static struct _collection *_get_block_p_s__parser(struct _parser *_cc)
{
if((!_expected_p_s__parser_e__token(_cc,64)))
{
return ((struct _collection *)0);
}
return _get_sequence_of_instructions_p_s__parser(_cc);
}

static struct _variable_node *_new_variable_p_s__parser_in_p_s__collection_p_a_ch_p_s__node_p_s__node_p_s__node_bo(struct _parser *_cc, int _line, struct _collection *_directives, char *_name, struct _node *_type, struct _node *_initial_value, struct _node *_reference, int _is_import)
{
struct _variable_node *_n_1;
_n_1=((struct _variable_node *)_new_node_p_s__parser_e__node_code_dw(_cc,5,((unsigned int)36)));
(((struct _node *)_n_1)->_line)=_line;
(_n_1->_directives)=((struct _collection_of_directives *)_directives);
(_n_1->_name)=_name;
(_n_1->_type)=_type;
(_n_1->_initial_value)=_initial_value;
(_n_1->_reference)=_reference;
(_n_1->_is_import)=_is_import;
return _n_1;
}

static struct _variable_node *_get_variable_definition_p_s__parser_bo(struct _parser *_cc, int _is_import)
{
struct _node *_initial_value_1;
struct _node *_reference_2;
struct _node *_type_3;
struct _collection *_directives_4;
char *_name_5;
int _line_6;
union _lexeme _lexeme_7;
int _token_8;
_initial_value_1=((struct _node *)0);
_reference_2=((struct _node *)0);
_type_3=((struct _node *)0);
_directives_4=_get_set_of_directives_p_s__parser(_cc);
if(((void*)_directives_4==(void*)0))
{
return ((struct _variable_node *)0);
}
_name_5=_get_id_p_s__parser(_cc);
if(((void*)_name_5==(void*)0))
{
return ((struct _variable_node *)0);
}
_line_6=(((struct _token_stream *)_cc)->_line);
_token_8=_get_token_p_s__token_stream_p_s__lexeme(((struct _token_stream *)_cc),(&_lexeme_7));
if((_token_8==67))
{
_type_3=_get_type_p_s__parser(_cc);
if(((void*)_type_3==(void*)0))
{
return ((struct _variable_node *)0);
}
}
else if(((_token_8==34)&&(!_is_import)))
{
_initial_value_1=_get_expression_p_s__parser_in(_cc,0);
if(((void*)_initial_value_1==(void*)0))
{
return ((struct _variable_node *)0);
}
}
else if(((_token_8==33)&&(!_is_import)))
{
_reference_2=_get_expression_p_s__parser_in(_cc,0);
if(((void*)_reference_2==(void*)0))
{
return ((struct _variable_node *)0);
}
}
else
{
return ((struct _variable_node *)_syntax_error_p_s__parser_e__token_p_s__lexeme(_cc,_token_8,(&_lexeme_7)));
}
return _new_variable_p_s__parser_in_p_s__collection_p_a_ch_p_s__node_p_s__node_p_s__node_bo(_cc,_line_6,_directives_4,_name_5,_type_3,_initial_value_1,_reference_2,_is_import);
}

static struct _ternary_node *_new_node_p_s__parser_in_e__node_code_p_vo_p_vo_p_vo(struct _parser *_cc, int _line, int _code, void *_value_1, void *_value_2, void *_value_3)
{
struct _ternary_node *_n_1;
_n_1=((struct _ternary_node *)_allocate_p_s__memory_bundle_dw((_cc->_bundle),((unsigned int)24)));
(((struct _node *)_n_1)->_code)=_code;
(((struct _node *)_n_1)->_line)=_line;
(_n_1->_value_1)=_value_1;
(_n_1->_value_2)=_value_2;
(_n_1->_value_3)=_value_3;
return _n_1;
}

static struct _collection *_get_parameters_p_s__parser_bo(struct _parser *_cc, int _type_only)
{
struct _collection *_arguments_1;
int _token_2;
void *_is_return_3;
char *_name_4;
int _line_5;
struct _node *_type_6;
struct _ternary_node *_argument_7;
union _lexeme _lexeme_8;
_arguments_1=_new_collection_p_s__parser(_cc);
_token_2=_peek_token_p_s__token_stream(((struct _token_stream *)_cc));
if((_token_2==72))
{
_get_token_p_s__token_stream(((struct _token_stream *)_cc));
while(1)
{
_is_return_3=((void *)0);
_token_2=_peek_token_p_s__token_stream(((struct _token_stream *)_cc));
if((_token_2==43))
{
_is_return_3=((void *)_arguments_1);
_get_token_p_s__token_stream(((struct _token_stream *)_cc));
}
if(_type_only)
{
_name_4=_empty_string171;
_line_5=0;
}
else
{
_name_4=_get_id_p_s__parser(_cc);
if(((void*)_name_4==(void*)0))
{
return ((struct _collection *)0);
}
_line_5=(((struct _token_stream *)_cc)->_line);
if((!_expected_p_s__parser_e__token(_cc,67)))
{
return ((struct _collection *)0);
}
}
_type_6=_get_type_p_s__parser(_cc);
if(((void*)_type_6==(void*)0))
{
return ((struct _collection *)0);
}
_argument_7=_new_node_p_s__parser_in_e__node_code_p_vo_p_vo_p_vo(_cc,_line_5,0,((void *)_name_4),((void *)_type_6),_is_return_3);
_append_p_s__collection_p_s__element(_arguments_1,((struct _element *)_argument_7));
_token_2=_get_token_p_s__token_stream_p_s__lexeme(((struct _token_stream *)_cc),(&_lexeme_8));
if((_token_2!=66))
{
if(_unexpected_p_s__parser_e__token_p_s__lexeme_e__token(_cc,_token_2,(&_lexeme_8),73))
{
return ((struct _collection *)0);
}
break;
}
}
}
return _arguments_1;
}

static struct _equate_node *_new_equate_p_s__parser_in_bo_p_a_ch_p_s__collection_p_s__node(struct _parser *_cc, int _line, int _global, char *_name, struct _collection *_arguments, struct _node *_value)
{
struct _equate_node *_n_1;
_n_1=((struct _equate_node *)_new_node_p_s__parser_e__node_code_dw(_cc,6,((unsigned int)28)));
(((struct _node *)_n_1)->_line)=_line;
(_n_1->_name)=_name;
(_n_1->_global)=_global;
(_n_1->_arguments)=_arguments;
(_n_1->_value)=_value;
return _n_1;
}

static struct _iterator_node *_new_iterator_p_s__parser_in_bo_p_a_ch_p_s__collection_p_s__collection(struct _parser *_cc, int _line, int _global, char *_name, struct _collection *_arguments, struct _collection *_body)
{
struct _iterator_node *_n_1;
_n_1=((struct _iterator_node *)_new_node_p_s__parser_e__node_code_dw(_cc,7,((unsigned int)28)));
(((struct _node *)_n_1)->_line)=_line;
(_n_1->_name)=_name;
(_n_1->_global)=_global;
(_n_1->_arguments)=_arguments;
(_n_1->_body)=_body;
return _n_1;
}

static struct _node *_get_equate_definition_p_s__parser_bo(struct _parser *_cc, int _is_global)
{
char *_name_1;
int _line_2;
struct _collection *_arguments_3;
union _lexeme _lexeme_4;
int _token_5;
struct _node *_value_6;
struct _collection *_body_7;
_name_1=_get_id_p_s__parser(_cc);
if(((void*)_name_1==(void*)0))
{
return ((struct _node *)0);
}
_line_2=(((struct _token_stream *)_cc)->_line);
_arguments_3=_get_parameters_p_s__parser_bo(_cc,0);
if(((void*)_arguments_3==(void*)0))
{
return ((struct _node *)0);
}
_token_5=_get_token_p_s__token_stream_p_s__lexeme(((struct _token_stream *)_cc),(&_lexeme_4));
if((_token_5==34))
{
_value_6=_get_expression_p_s__parser_in(_cc,0);
if(((void*)_value_6==(void*)0))
{
return ((struct _node *)0);
}
return ((struct _node *)_new_equate_p_s__parser_in_bo_p_a_ch_p_s__collection_p_s__node(_cc,_line_2,_is_global,_name_1,_arguments_3,_value_6));
}
else if((_token_5==64))
{
_body_7=_get_sequence_of_instructions_p_s__parser(_cc);
if(((void*)_body_7==(void*)0))
{
return ((struct _node *)0);
}
return ((struct _node *)_new_iterator_p_s__parser_in_bo_p_a_ch_p_s__collection_p_s__collection(_cc,_line_2,_is_global,_name_1,_arguments_3,_body_7));
}
else
{
return ((struct _node *)_syntax_error_p_s__parser_e__token_p_s__lexeme(_cc,_token_5,(&_lexeme_4)));
}
}

static struct _binary_node *_new_node_p_s__parser_in_e__node_code_p_vo_p_vo(struct _parser *_cc, int _line, int _code, void *_value_1, void *_value_2)
{
struct _binary_node *_n_1;
_n_1=((struct _binary_node *)_allocate_p_s__memory_bundle_dw((_cc->_bundle),((unsigned int)20)));
(((struct _node *)_n_1)->_code)=_code;
(((struct _node *)_n_1)->_line)=_line;
(_n_1->_value_1)=_value_1;
(_n_1->_value_2)=_value_2;
return _n_1;
}

static struct _binary_node *_get_typedef_definition_p_s__parser(struct _parser *_cc)
{
char *_name_1;
int _line_2;
struct _node *_type_3;
_name_1=_get_id_p_s__parser(_cc);
if(((void*)_name_1==(void*)0))
{
return ((struct _binary_node *)0);
}
_line_2=(((struct _token_stream *)_cc)->_line);
if((!_expected_p_s__parser_e__token(_cc,34)))
{
return ((struct _binary_node *)0);
}
_type_3=_get_type_p_s__parser(_cc);
if(((void*)_type_3==(void*)0))
{
return ((struct _binary_node *)0);
}
return _new_node_p_s__parser_in_e__node_code_p_vo_p_vo(_cc,_line_2,2,((void *)_name_1),((void *)_type_3));
}

static struct _long_node *_new_long_p_s__parser_e__node_code_dw(struct _parser *_cc, int _code, unsigned int _value)
{
struct _long_node *_n_1;
_n_1=((struct _long_node *)_new_node_p_s__parser_e__node_code_dw(_cc,_code,((unsigned int)16)));
(_n_1->_value)=_value;
return _n_1;
}

static struct _collection *_get_list_of_expressions_p_s__parser(struct _parser *_cc)
{
struct _collection *_expressions_1;
struct _node *_e_2;
int _token_3;
_expressions_1=_new_collection_p_s__parser(_cc);
while(1)
{
_e_2=_get_expression_p_s__parser_in(_cc,0);
if(((void*)_e_2==(void*)0))
{
return ((struct _collection *)0);
}
_append_p_s__collection_p_s__element(_expressions_1,((struct _element *)_e_2));
_token_3=_peek_token_p_s__token_stream(((struct _token_stream *)_cc));
if((_token_3!=66))
{
break;
}
_get_token_p_s__token_stream(((struct _token_stream *)_cc));
}
return _expressions_1;
}

static struct _collection *_get_list_of_arguments_p_s__parser(struct _parser *_cc)
{
int _token_1;
struct _collection *_arguments_2;
_token_1=_peek_token_p_s__token_stream(((struct _token_stream *)_cc));
if((_token_1==72))
{
_get_token_p_s__token_stream(((struct _token_stream *)_cc));
_arguments_2=_get_list_of_expressions_p_s__parser(_cc);
if(((void*)_arguments_2==(void*)0))
{
return ((struct _collection *)0);
}
if((!_expected_p_s__parser_e__token(_cc,73)))
{
return ((struct _collection *)0);
}
return _arguments_2;
}
else
{
return _new_collection_p_s__parser(_cc);
}
}

static struct _binary_node *_get_initializer_p_s__parser(struct _parser *_cc)
{
struct _node *_type_1;
struct _collection *_arguments_2;
_type_1=_get_type_p_s__parser(_cc);
if(((void*)_type_1==(void*)0))
{
return ((struct _binary_node *)0);
}
_arguments_2=_get_list_of_arguments_p_s__parser(_cc);
if(((void*)_arguments_2==(void*)0))
{
return ((struct _binary_node *)0);
}
return _new_node_p_s__parser_e__node_code_p_vo_p_vo(_cc,19,((void *)_type_1),((void *)_arguments_2));
}

static char *_prefix_params_e__token_p_a_in(int _op, int *_p)
{
switch(_op)
{
case 8:
{
(_p[0])=30;
return _op_add36;
break;
}
case 9:
{
(_p[0])=30;
return _op_sub38;
break;
}
case 18:
{
(_p[0])=30;
return _op_not96;
break;
}
case 19:
{
(_p[0])=30;
return _op_pre_inc98;
break;
}
case 20:
{
(_p[0])=30;
return _op_pre_dec100;
break;
}
}
return _empty_string171;
}

static char *_operator_params_e__token_p_s__priorities(int _op, struct _priorities *_p)
{
switch(_op)
{
case 10:
{
(_p->_left)=26;
(_p->_right)=26;
return _op_mul40;
break;
}
case 11:
{
(_p->_left)=26;
(_p->_right)=26;
return _op_div42;
break;
}
case 12:
{
(_p->_left)=26;
(_p->_right)=26;
return _op_mod44;
break;
}
case 8:
{
(_p->_left)=24;
(_p->_right)=24;
return _op_add36;
break;
}
case 9:
{
(_p->_left)=24;
(_p->_right)=24;
return _op_sub38;
break;
}
case 13:
{
(_p->_left)=22;
(_p->_right)=22;
return _op_shl46;
break;
}
case 14:
{
(_p->_left)=22;
(_p->_right)=22;
return _op_shr48;
break;
}
case 39:
{
(_p->_left)=20;
(_p->_right)=20;
return _op_le54;
break;
}
case 40:
{
(_p->_left)=20;
(_p->_right)=20;
return _op_lt56;
break;
}
case 41:
{
(_p->_left)=20;
(_p->_right)=20;
return _op_ge58;
break;
}
case 42:
{
(_p->_left)=20;
(_p->_right)=20;
return _op_gt60;
break;
}
case 37:
{
(_p->_left)=18;
(_p->_right)=18;
return _op_eq50;
break;
}
case 38:
{
(_p->_left)=18;
(_p->_right)=18;
return _op_ne52;
break;
}
case 15:
{
(_p->_left)=16;
(_p->_right)=16;
return _op_and62;
break;
}
case 17:
{
(_p->_left)=14;
(_p->_right)=14;
return _op_xor64;
break;
}
case 16:
{
(_p->_left)=12;
(_p->_right)=12;
return _op_or66;
break;
}
case 21:
{
(_p->_left)=16;
(_p->_right)=16;
return _op_andand68;
break;
}
case 22:
{
(_p->_left)=12;
(_p->_right)=12;
return _op_oror70;
break;
}
case 34:
{
(_p->_left)=2;
(_p->_right)=2;
return _op_assign247;
break;
}
case 23:
{
(_p->_left)=2;
(_p->_right)=2;
return _op_assign_add72;
break;
}
case 24:
{
(_p->_left)=2;
(_p->_right)=2;
return _op_assign_sub74;
break;
}
case 25:
{
(_p->_left)=2;
(_p->_right)=2;
return _op_assign_mul76;
break;
}
case 26:
{
(_p->_left)=2;
(_p->_right)=2;
return _op_assign_div78;
break;
}
case 27:
{
(_p->_left)=2;
(_p->_right)=2;
return _op_assign_mod80;
break;
}
case 28:
{
(_p->_left)=2;
(_p->_right)=2;
return _op_assign_shl82;
break;
}
case 29:
{
(_p->_left)=2;
(_p->_right)=2;
return _op_assign_shr84;
break;
}
case 30:
{
(_p->_left)=2;
(_p->_right)=2;
return _op_assign_and86;
break;
}
case 31:
{
(_p->_left)=2;
(_p->_right)=2;
return _op_assign_or88;
break;
}
case 32:
{
(_p->_left)=2;
(_p->_right)=2;
return _op_assign_xor90;
break;
}
}
return _empty_string171;
}

static char *_postfix_params_e__token_p_a_in(int _op, int *_p)
{
switch(_op)
{
case 19:
{
(_p[0])=32;
return _op_post_inc102;
break;
}
case 20:
{
(_p[0])=32;
return _op_post_dec104;
break;
}
case 76:
{
(_p[0])=32;
return _op_at106;
break;
}
case 35:
{
(_p[0])=2;
return _op_assign_not94;
break;
}
case 36:
{
(_p[0])=2;
return _op_assign_neg92;
break;
}
}
return _empty_string171;
}

static void _add_first_p_s__collection_p_s__element(struct _collection *_c, struct _element *_e)
{
(_e->_next_sibling)=(_c->_first);
(_c->_first)=_e;
if(((void*)(_c->_last)==(void*)0))
{
(_c->_last)=_e;
}
}

static struct _ternary_node *_new_node_p_s__parser_e__node_code_p_vo_p_vo_p_vo(struct _parser *_cc, int _code, void *_value_1, void *_value_2, void *_value_3)
{
struct _ternary_node *_n_1;
_n_1=((struct _ternary_node *)_allocate_p_s__memory_bundle_dw((_cc->_bundle),((unsigned int)24)));
(((struct _node *)_n_1)->_code)=_code;
(((struct _node *)_n_1)->_line)=(((struct _token_stream *)_cc)->_line);
(_n_1->_value_1)=_value_1;
(_n_1->_value_2)=_value_2;
(_n_1->_value_3)=_value_3;
return _n_1;
}

static struct _node *_get_expression_p_s__parser_in(struct _parser *_cc, int _current_priority)
{
struct _node *_e_1;
union _lexeme _lexeme_2;
int _token_3;
struct _node *_value_4;
struct _collection *_lc_5;
struct _node *_condition_6;
struct _collection *_block_7;
int _no_else_block_8;
struct _node *_condition_9;
struct _collection *_block_10;
struct _collection *_block_11;
struct _node *_value_12;
struct _collection *_selectors_13;
int _no_else_block_14;
struct _collection *_cl_15;
struct _node *_ce_16;
struct _collection *_block_17;
struct _collection *_block_18;
char *_string_19;
struct _string_buffer _t_20;
struct _string_buffer *_sb_21;
char *_string_22;
struct _string_buffer _t_23;
struct _string_buffer *_sb_24;
char *_name_25;
struct _collection *_arguments_26;
struct _node *_type_27;
char *_name_28;
struct _collection *_arguments_29;
int (_p_30)[1];
char *_name_31;
struct _node *_arg_32;
struct _collection *_arguments_33;
int _op_34;
struct _priorities _priorities_35;
char *_name_36;
struct _node *_e2_37;
struct _collection *_arguments_38;
int (_left_priority_39)[1];
char *_name_40;
struct _collection *_arguments_41;
struct _node *_type_42;
int (_left_priority_43)[1];
char *_name_44;
struct _collection *_arguments_45;
int _left_priority_46;
struct _collection *_arguments_47;
struct _node *_e2_48;
struct _node *_e3_49;
int _left_priority_50;
char *_name_51;
struct _collection *_arguments_52;
struct _collection *_parameters_53;
char *_arg_name_54;
struct _collection *_block_55;
_e_1=((struct _node *)0);
_token_3=_get_token_p_s__token_stream_p_s__lexeme(((struct _token_stream *)_cc),(&_lexeme_2));
switch(_token_3)
{
case 43:
{
_value_4=((struct _node *)0);
if((_peek_token_p_s__token_stream(((struct _token_stream *)_cc))!=64))
{
_value_4=_get_expression_p_s__parser_in(_cc,0);
if(((void*)_value_4==(void*)0))
{
return ((struct _node *)0);
}
}
_e_1=((struct _node *)_new_node_p_s__parser_e__node_code_p_vo(_cc,20,((void *)_value_4)));
break;
}
case 44:
{
_lc_5=_new_collection_p_s__parser(_cc);
_condition_6=_get_expression_p_s__parser_in(_cc,0);
if(((void*)_condition_6==(void*)0))
{
return ((struct _node *)0);
}
_block_7=_get_mini_block_p_s__parser(_cc);
if(((void*)_block_7==(void*)0))
{
return ((struct _node *)0);
}
_append_p_s__collection_p_s__element(_lc_5,((struct _element *)_new_node_p_s__parser_e__node_code_p_vo_p_vo(_cc,0,((void *)_condition_6),((void *)_block_7))));
_no_else_block_8=1;
while(1)
{
_token_3=_get_token_p_s__token_stream_p_s__lexeme(((struct _token_stream *)_cc),(&_lexeme_2));
if(((_token_3==45)&&_no_else_block_8))
{
_condition_6=_get_expression_p_s__parser_in(_cc,0);
if(((void*)_condition_6==(void*)0))
{
return ((struct _node *)0);
}
_block_7=_get_mini_block_p_s__parser(_cc);
if(((void*)_block_7==(void*)0))
{
return ((struct _node *)0);
}
_append_p_s__collection_p_s__element(_lc_5,((struct _element *)_new_node_p_s__parser_e__node_code_p_vo_p_vo(_cc,0,((void *)_condition_6),((void *)_block_7))));
}
else if((_token_3==46))
{
_block_7=_get_mini_block_p_s__parser(_cc);
if(((void*)_block_7==(void*)0))
{
return ((struct _node *)0);
}
_append_p_s__collection_p_s__element(_lc_5,((struct _element *)_new_node_p_s__parser_e__node_code_p_vo_p_vo(_cc,0,((void *)0),((void *)_block_7))));
_no_else_block_8=0;
}
else if((_token_3==48))
{
break;
}
else
{
return ((struct _node *)_syntax_error_p_s__parser_e__token_p_s__lexeme(_cc,_token_3,(&_lexeme_2)));
}
}
_e_1=((struct _node *)_new_node_p_s__parser_e__node_code_p_vo(_cc,21,((void *)_lc_5)));
break;
}
case 47:
{
_condition_9=_get_expression_p_s__parser_in(_cc,0);
if(((void*)_condition_9==(void*)0))
{
return ((struct _node *)0);
}
_block_10=_get_block_p_s__parser(_cc);
if(((void*)_block_10==(void*)0))
{
return ((struct _node *)0);
}
_e_1=((struct _node *)_new_node_p_s__parser_e__node_code_p_vo_p_vo(_cc,22,((void *)_condition_9),((void *)_block_10)));
break;
}
case 55:
{
_block_11=_get_block_p_s__parser(_cc);
if(((void*)_block_11==(void*)0))
{
return ((struct _node *)0);
}
_e_1=((struct _node *)_new_node_p_s__parser_e__node_code_p_vo(_cc,23,((void *)_block_11)));
break;
}
case 58:
{
_value_12=_get_expression_p_s__parser_in(_cc,0);
if(((void*)_value_12==(void*)0))
{
return ((struct _node *)0);
}
if((!_expected_p_s__parser_e__token(_cc,64)))
{
return ((struct _node *)0);
}
_selectors_13=_new_collection_p_s__parser(_cc);
_no_else_block_14=1;
while(1)
{
_token_3=_get_token_p_s__token_stream_p_s__lexeme(((struct _token_stream *)_cc),(&_lexeme_2));
if(((_token_3==59)&&_no_else_block_14))
{
_cl_15=_new_collection_p_s__parser(_cc);
while(1)
{
_ce_16=_get_expression_p_s__parser_in(_cc,0);
if(((void*)_ce_16==(void*)0))
{
return ((struct _node *)0);
}
_append_p_s__collection_p_s__element(_cl_15,((struct _element *)_ce_16));
if((_peek_token_p_s__token_stream(((struct _token_stream *)_cc))!=66))
{
break;
}
_token_3=_get_token_p_s__token_stream_p_s__lexeme(((struct _token_stream *)_cc),(&_lexeme_2));
}
_block_17=_get_mini_block_p_s__parser(_cc);
if(((void*)_block_17==(void*)0))
{
return ((struct _node *)0);
}
_append_p_s__collection_p_s__element(_selectors_13,((struct _element *)_new_node_p_s__parser_p_vo_p_vo(_cc,((void *)_cl_15),((void *)_block_17))));
}
else if(((_token_3==46)&&_no_else_block_14))
{
_block_18=_get_mini_block_p_s__parser(_cc);
if(((void*)_block_18==(void*)0))
{
return ((struct _node *)0);
}
_append_p_s__collection_p_s__element(_selectors_13,((struct _element *)_new_node_p_s__parser_p_vo_p_vo(_cc,((void *)0),((void *)_block_18))));
}
else if((_token_3==48))
{
break;
}
else
{
return ((struct _node *)_syntax_error_p_s__parser_e__token_p_s__lexeme(_cc,_token_3,(&_lexeme_2)));
}
}
_e_1=((struct _node *)_new_node_p_s__parser_e__node_code_p_vo_p_vo(_cc,24,((void *)_value_12),((void *)_selectors_13)));
break;
}
case 49:
{
_e_1=((struct _node *)_get_variable_definition_p_s__parser_bo(_cc,0));
if(((void*)_e_1==(void*)0))
{
return ((struct _node *)0);
}
break;
}
case 50:
{
_e_1=_get_equate_definition_p_s__parser_bo(_cc,0);
if(((void*)_e_1==(void*)0))
{
return ((struct _node *)0);
}
break;
}
case 56:
{
_e_1=((struct _node *)_get_typedef_definition_p_s__parser(_cc));
if(((void*)_e_1==(void*)0))
{
return ((struct _node *)0);
}
break;
}
case 72:
{
_e_1=_get_expression_p_s__parser_in(_cc,0);
if((((void*)_e_1!=(void*)0)&&(!_expected_p_s__parser_e__token(_cc,73))))
{
return ((struct _node *)0);
}
break;
}
case 3:
{
_e_1=((struct _node *)_new_long_p_s__parser_e__node_code_dw(_cc,15,((unsigned int )((&_lexeme_2)->_long_value))));
break;
}
case 4:
{
_string_19=((&_lexeme_2)->_string_value);
_token_3=_peek_token_p_s__token_stream(((struct _token_stream *)_cc));
if((_token_3==4))
{
_sb_21=(_initialize_p_s__string_buffer((&_t_20)),(&_t_20));
_append_p_s__string_buffer_p_a_ch(_sb_21,_string_19);
while((_token_3==4))
{
_get_token_p_s__token_stream_p_s__lexeme(((struct _token_stream *)_cc),(&_lexeme_2));
_append_p_s__string_buffer_p_a_ch(_sb_21,((&_lexeme_2)->_string_value));
_token_3=_peek_token_p_s__token_stream(((struct _token_stream *)_cc));
}
_string_19=_new_string_p_s__memory_bundle_p_a_ch((((struct _token_stream *)_cc)->_bundle),_as_string_p_s__string_buffer(_sb_21));
_release_p_s__string_buffer(_sb_21);
}
_e_1=((struct _node *)_new_string_p_s__parser_e__node_code_p_a_ch(_cc,11,_string_19));
break;
}
case 5:
{
_string_22=((&_lexeme_2)->_string_value);
_token_3=_peek_token_p_s__token_stream(((struct _token_stream *)_cc));
if((_token_3==5))
{
_sb_24=(_initialize_p_s__string_buffer((&_t_23)),(&_t_23));
_append_p_s__string_buffer_p_a_ch(_sb_24,_string_22);
while((_token_3==5))
{
_get_token_p_s__token_stream_p_s__lexeme(((struct _token_stream *)_cc),(&_lexeme_2));
_append_p_s__string_buffer_p_a_ch(_sb_24,((&_lexeme_2)->_string_value));
_token_3=_peek_token_p_s__token_stream(((struct _token_stream *)_cc));
}
_string_22=_new_string_p_s__memory_bundle_p_a_ch((((struct _token_stream *)_cc)->_bundle),_as_string_p_s__string_buffer(_sb_24));
_release_p_s__string_buffer(_sb_24);
}
_e_1=((struct _node *)_new_string_p_s__parser_e__node_code_p_a_ch(_cc,12,_string_22));
break;
}
case 6:
{
_e_1=((struct _node *)_new_long_p_s__parser_e__node_code_dw(_cc,13,((unsigned int )((&_lexeme_2)->_long_value))));
break;
}
case 7:
{
_e_1=((struct _node *)_new_long_p_s__parser_e__node_code_dw(_cc,14,((unsigned int )((&_lexeme_2)->_long_value))));
break;
}
case 17:
{
_name_25=_get_id_p_s__parser(_cc);
if(((void*)_name_25==(void*)0))
{
return ((struct _node *)0);
}
_arguments_26=_get_parameters_p_s__parser_bo(_cc,1);
if(((void*)_arguments_26==(void*)0))
{
return ((struct _node *)0);
}
_e_1=((struct _node *)_new_node_p_s__parser_e__node_code_p_vo_p_vo(_cc,25,((void *)_name_25),((void *)_arguments_26)));
break;
}
case 57:
{
_type_27=_get_type_p_s__parser(_cc);
if(((void*)_type_27==(void*)0))
{
return ((struct _node *)0);
}
_e_1=((struct _node *)_new_node_p_s__parser_e__node_code_p_vo(_cc,26,((void *)_type_27)));
break;
}
case 2:
{
_name_28=((&_lexeme_2)->_string_value);
_arguments_29=_get_list_of_arguments_p_s__parser(_cc);
if(((void*)_arguments_29==(void*)0))
{
return ((struct _node *)0);
}
if(_is_equal_p_a_ch_p_a_ch(_name_28,T536))
{
_e_1=((struct _node *)_new_node_p_s__parser_e__node_code_p_vo_p_vo(_cc,29,((void *)_name_28),((void *)_arguments_29)));
}
else
{
_e_1=((struct _node *)_new_node_p_s__parser_e__node_code_p_vo_p_vo(_cc,27,((void *)_name_28),((void *)_arguments_29)));
}
break;
}
case 62:
{
_e_1=((struct _node *)_get_initializer_p_s__parser(_cc));
if(((void*)_e_1==(void*)0))
{
return ((struct _node *)0);
}
break;
}
case 8:
case 9:
case 18:
case 19:
case 20:
{
_name_31=_prefix_params_e__token_p_a_in(_token_3,((int *)_p_30));
_arg_32=_get_expression_p_s__parser_in(_cc,(_p_30[0]));
if(((void*)_arg_32==(void*)0))
{
return ((struct _node *)0);
}
_arguments_33=_new_collection_p_s__parser(_cc);
_append_p_s__collection_p_s__element(_arguments_33,((struct _element *)_arg_32));
_e_1=((struct _node *)_new_node_p_s__parser_e__node_code_p_vo_p_vo(_cc,27,((void *)_name_31),((void *)_arguments_33)));
break;
}
default:
{
return ((struct _node *)_syntax_error_p_s__parser_e__token_p_s__lexeme(_cc,_token_3,(&_lexeme_2)));
break;
}
}
while(1)
{
_op_34=_peek_token_p_s__token_stream(((struct _token_stream *)_cc));
switch(_op_34)
{
case 23:
case 24:
case 25:
case 26:
case 27:
case 28:
case 29:
case 30:
case 31:
case 32:
case 34:
case 8:
case 9:
case 10:
case 11:
case 12:
case 13:
case 14:
case 37:
case 38:
case 39:
case 40:
case 41:
case 42:
case 15:
case 16:
case 17:
case 21:
case 22:
{
_name_36=_operator_params_e__token_p_s__priorities(_op_34,(&_priorities_35));
if((_current_priority>=((&_priorities_35)->_left)))
{
return _e_1;
}
_get_token_p_s__token_stream(((struct _token_stream *)_cc));
_e2_37=_get_expression_p_s__parser_in(_cc,((&_priorities_35)->_right));
if(((void*)_e2_37==(void*)0))
{
return ((struct _node *)0);
}
_arguments_38=_new_collection_p_s__parser(_cc);
_append_p_s__collection_p_s__element(_arguments_38,((struct _element *)_e_1));
_append_p_s__collection_p_s__element(_arguments_38,((struct _element *)_e2_37));
_e_1=((struct _node *)_new_node_p_s__parser_e__node_code_p_vo_p_vo(_cc,27,((void *)_name_36),((void *)_arguments_38)));
break;
}
case 20:
case 19:
case 35:
case 36:
{
_name_40=_postfix_params_e__token_p_a_in(_op_34,((int *)_left_priority_39));
if((_current_priority>=(_left_priority_39[0])))
{
return _e_1;
}
_get_token_p_s__token_stream(((struct _token_stream *)_cc));
_arguments_41=_new_collection_p_s__parser(_cc);
_append_p_s__collection_p_s__element(_arguments_41,((struct _element *)_e_1));
_e_1=((struct _node *)_new_node_p_s__parser_e__node_code_p_vo_p_vo(_cc,27,((void *)_name_40),((void *)_arguments_41)));
break;
}
case 67:
{
if((_current_priority>=28))
{
return _e_1;
}
_get_token_p_s__token_stream(((struct _token_stream *)_cc));
_type_42=_get_type_p_s__parser(_cc);
if(((void*)_type_42==(void*)0))
{
return ((struct _node *)0);
}
_e_1=((struct _node *)_new_node_p_s__parser_e__node_code_p_vo_p_vo(_cc,30,((void *)_e_1),((void *)_type_42)));
break;
}
case 76:
{
_name_44=_postfix_params_e__token_p_a_in(_op_34,((int *)_left_priority_43));
if((_current_priority>=(_left_priority_43[0])))
{
return _e_1;
}
_get_token_p_s__token_stream(((struct _token_stream *)_cc));
if((_peek_token_p_s__token_stream(((struct _token_stream *)_cc))!=77))
{
_arguments_45=_get_list_of_expressions_p_s__parser(_cc);
if(((void*)_arguments_45==(void*)0))
{
return ((struct _node *)0);
}
}
else
{
_arguments_45=_new_collection_p_s__parser(_cc);
}
if((!_expected_p_s__parser_e__token(_cc,77)))
{
return ((struct _node *)0);
}
_add_first_p_s__collection_p_s__element(_arguments_45,((struct _element *)_e_1));
_e_1=((struct _node *)_new_node_p_s__parser_e__node_code_p_vo_p_vo(_cc,27,((void *)_name_44),((void *)_arguments_45)));
break;
}
case 74:
{
_left_priority_46=32;
if((_current_priority>=_left_priority_46))
{
return _e_1;
}
_get_token_p_s__token_stream(((struct _token_stream *)_cc));
if((_peek_token_p_s__token_stream(((struct _token_stream *)_cc))!=75))
{
_arguments_47=_get_list_of_expressions_p_s__parser(_cc);
if(((void*)_arguments_47==(void*)0))
{
return ((struct _node *)0);
}
}
else
{
_arguments_47=_new_collection_p_s__parser(_cc);
}
if((!_expected_p_s__parser_e__token(_cc,75)))
{
return ((struct _node *)0);
}
_e_1=((struct _node *)_new_node_p_s__parser_e__node_code_p_vo_p_vo(_cc,31,((void *)_e_1),((void *)_arguments_47)));
break;
}
case 78:
{
if((_current_priority>=2))
{
return _e_1;
}
_get_token_p_s__token_stream(((struct _token_stream *)_cc));
_e2_48=_get_expression_p_s__parser_in(_cc,0);
if(((void*)_e2_48==(void*)0))
{
return ((struct _node *)0);
}
if((!_expected_p_s__parser_e__token(_cc,66)))
{
return ((struct _node *)0);
}
_e3_49=_get_expression_p_s__parser_in(_cc,0);
if(((void*)_e3_49==(void*)0))
{
return ((struct _node *)0);
}
_e_1=((struct _node *)_new_node_p_s__parser_e__node_code_p_vo_p_vo_p_vo(_cc,32,((void *)_e_1),((void *)_e2_48),((void *)_e3_49)));
break;
}
case 65:
{
_left_priority_50=32;
if((_current_priority>=_left_priority_50))
{
return _e_1;
}
_get_token_p_s__token_stream(((struct _token_stream *)_cc));
_name_51=_get_id_p_s__parser(_cc);
if(((void*)_name_51==(void*)0))
{
return ((struct _node *)0);
}
_arguments_52=_get_list_of_arguments_p_s__parser(_cc);
if(((void*)_arguments_52==(void*)0))
{
return ((struct _node *)0);
}
_add_first_p_s__collection_p_s__element(_arguments_52,((struct _element *)_e_1));
_e_1=((struct _node *)_new_node_p_s__parser_e__node_code_p_vo_p_vo(_cc,27,((void *)_name_51),((void *)_arguments_52)));
break;
}
case 70:
{
_get_token_p_s__token_stream(((struct _token_stream *)_cc));
_parameters_53=_new_collection_p_s__parser(_cc);
if((_peek_token_p_s__token_stream(((struct _token_stream *)_cc))==2))
{
while(1)
{
_arg_name_54=_get_id_p_s__parser(_cc);
if(((void*)_arg_name_54==(void*)0))
{
return ((struct _node *)0);
}
_append_p_s__collection_p_s__element(_parameters_53,((struct _element *)_new_string_p_s__parser_e__node_code_p_a_ch(_cc,0,_arg_name_54)));
if((_peek_token_p_s__token_stream(((struct _token_stream *)_cc))!=66))
{
break;
}
_get_token_p_s__token_stream(((struct _token_stream *)_cc));
}
}
_block_55=_get_block_p_s__parser(_cc);
if(((void*)_block_55==(void*)0))
{
return ((struct _node *)0);
}
_e_1=((struct _node *)_new_node_p_s__parser_e__node_code_p_vo_p_vo_p_vo(_cc,28,((void *)_e_1),((void *)_parameters_53),((void *)_block_55)));
break;
}
default:
{
return _e_1;
break;
}
}
}
return _e_1;
}

static void *_get_end_of_line_p_s__parser(struct _parser *_cc)
{
union _lexeme _lexeme_1;
int _token_2;
_token_2=_get_token_p_s__token_stream_p_s__lexeme(((struct _token_stream *)_cc),(&_lexeme_1));
if(_unexpected_p_s__parser_e__token_p_s__lexeme_e__token(_cc,_token_2,(&_lexeme_1),64))
{
return ((void *)0);
}
return ((void *)1);
}

static struct _collection *_get_enum_content_p_s__parser(struct _parser *_cc)
{
struct _collection *_items_1;
union _lexeme _lexeme_2;
int _token_3;
char *_name_4;
struct _node *_value_5;
_items_1=_new_collection_p_s__parser(_cc);
while(1)
{
_token_3=_get_token_p_s__token_stream_p_s__lexeme(((struct _token_stream *)_cc),(&_lexeme_2));
if((_token_3==2))
{
_name_4=((&_lexeme_2)->_string_value);
_value_5=((struct _node *)0);
_token_3=_peek_token_p_s__token_stream(((struct _token_stream *)_cc));
if((_token_3==34))
{
_get_token_p_s__token_stream(((struct _token_stream *)_cc));
_value_5=_get_expression_p_s__parser_in(_cc,0);
}
_append_p_s__collection_p_s__element(_items_1,((struct _element *)_new_node_p_s__parser_p_vo_p_vo(_cc,((void *)_name_4),((void *)_value_5))));
if(((void*)_get_end_of_line_p_s__parser(_cc)==(void*)0))
{
return ((struct _collection *)0);
}
}
else if((_token_3==48))
{
break;
}
else
{
return ((struct _collection *)_syntax_error_p_s__parser_e__token_p_s__lexeme(_cc,_token_3,(&_lexeme_2)));
}
}
return _items_1;
}

static struct _node *_get_type_p_s__parser(struct _parser *_cc)
{
union _lexeme _lexeme_1;
int _token_2;
struct _node *_type_3;
int _rparen_4;
int _is_union_5;
int _line_6;
struct _collection *_content_7;
struct _node *_target_type_8;
struct _node *_array_size_9;
int _is_pointer_10;
struct _node *_array_type_11;
struct _node *_type_12;
struct _collection *_types_13;
int _ok_14;
struct _node *_argument_type_15;
struct _node *_returned_type_16;
_token_2=_get_token_p_s__token_stream_p_s__lexeme(((struct _token_stream *)_cc),(&_lexeme_1));
switch(_token_2)
{
case 72:
{
_type_3=_get_type_p_s__parser(_cc);
_rparen_4=_expected_p_s__parser_e__token(_cc,73);
if((((void*)_type_3!=(void*)0)&&(!_rparen_4)))
{
return ((struct _node *)0);
}
return _type_3;
break;
}
case 2:
{
return ((struct _node *)_new_string_p_s__parser_e__node_code_p_a_ch(_cc,10,((&_lexeme_1)->_string_value)));
break;
}
case 51:
case 52:
{
_is_union_5=(_token_2==52);
return ((struct _node *)_get_struct_definition_p_s__parser_bo_bo_bo(_cc,_is_union_5,1,0));
break;
}
case 53:
{
_line_6=(((struct _token_stream *)_cc)->_line);
if((!_expected_p_s__parser_e__token(_cc,64)))
{
return ((struct _node *)0);
}
_content_7=_get_enum_content_p_s__parser(_cc);
if(((void*)_content_7==(void*)0))
{
return ((struct _node *)0);
}
return ((struct _node *)_new_node_p_s__parser_in_e__node_code_p_vo_p_vo(_cc,_line_6,1,((void *)T569),((void *)_content_7)));
break;
}
case 78:
{
_target_type_8=_get_type_p_s__parser(_cc);
if(((void*)_target_type_8==(void*)0))
{
return ((struct _node *)0);
}
return ((struct _node *)_new_node_p_s__parser_e__node_code_p_vo(_cc,16,((void *)_target_type_8)));
break;
}
case 76:
{
_array_size_9=((struct _node *)0);
_token_2=_peek_token_p_s__token_stream(((struct _token_stream *)_cc));
if((_token_2==10))
{
_is_pointer_10=0;
_get_token_p_s__token_stream(((struct _token_stream *)_cc));
_token_2=_peek_token_p_s__token_stream(((struct _token_stream *)_cc));
}
else if((_token_2!=77))
{
_array_size_9=_get_expression_p_s__parser_in(_cc,0);
if(((void*)_array_size_9==(void*)0))
{
return ((struct _node *)0);
}
_token_2=_peek_token_p_s__token_stream(((struct _token_stream *)_cc));
_is_pointer_10=0;
}
else
{
_is_pointer_10=1;
}
if((_token_2!=77))
{
return ((struct _node *)_syntax_error_p_s__parser_e__token_p_s__lexeme(_cc,_token_2,(&_lexeme_1)));
}
_get_token_p_s__token_stream(((struct _token_stream *)_cc));
_array_type_11=_get_type_p_s__parser(_cc);
if(((void*)_array_type_11==(void*)0))
{
return ((struct _node *)0);
}
_type_12=((struct _node *)_new_node_p_s__parser_e__node_code_p_vo_p_vo(_cc,17,((void *)_array_size_9),((void *)_array_type_11)));
if(_is_pointer_10)
{
_type_12=((struct _node *)_new_node_p_s__parser_e__node_code_p_vo(_cc,16,((void *)_type_12)));
}
return _type_12;
break;
}
case 74:
{
_types_13=_new_collection_p_s__parser(_cc);
_token_2=_peek_token_p_s__token_stream(((struct _token_stream *)_cc));
_ok_14=1;
if((_token_2==75))
{
_get_token_p_s__token_stream_p_s__lexeme(((struct _token_stream *)_cc),(&_lexeme_1));
}
else
{
while(1)
{
_argument_type_15=_get_type_p_s__parser(_cc);
if(((void*)_argument_type_15==(void*)0))
{
_ok_14=0;
}
else
{
_append_p_s__collection_p_s__element(_types_13,((struct _element *)_argument_type_15));
}
_token_2=_get_token_p_s__token_stream_p_s__lexeme(((struct _token_stream *)_cc),(&_lexeme_1));
if((_token_2!=66))
{
break;
}
}
}
if((_ok_14&&_unexpected_p_s__parser_e__token_p_s__lexeme_e__token(_cc,_token_2,(&_lexeme_1),75)))
{
return ((struct _node *)0);
}
_returned_type_16=_get_type_p_s__parser(_cc);
if(((!_ok_14)||((void*)_returned_type_16==(void*)0)))
{
return ((struct _node *)0);
}
return ((struct _node *)_new_node_p_s__parser_e__node_code_p_vo_p_vo(_cc,18,((void *)_types_13),((void *)_returned_type_16)));
break;
}
default:
{
return ((struct _node *)_syntax_error_p_s__parser_e__token_p_s__lexeme(_cc,_token_2,(&_lexeme_1)));
break;
}
}
}

static struct _attribute_node *_new_attribute_p_s__parser_in_e__node_code_p_a_ch_p_s__node_e__expose_mode(struct _parser *_cc, int _line, int _code, char *_name, struct _node *_type, int _mode)
{
struct _attribute_node *_n_1;
_n_1=((struct _attribute_node *)_allocate_p_s__memory_bundle_dw((_cc->_bundle),((unsigned int)24)));
(((struct _node *)_n_1)->_code)=_code;
(((struct _node *)_n_1)->_line)=_line;
(_n_1->_name)=_name;
(_n_1->_type)=_type;
(_n_1->_mode)=_mode;
return _n_1;
}

static struct _structure_node *_new_structure_p_s__parser_in_bo_p_s__collection_p_a_ch_p_s__node_p_s__collection(struct _parser *_cc, int _line, int _is_union, struct _collection *_directives, char *_name, struct _node *_parent, struct _collection *_attributes)
{
struct _structure_node *_n_1;
_n_1=((struct _structure_node *)_new_node_p_s__parser_e__node_code_dw(_cc,3,((unsigned int)32)));
(((struct _node *)_n_1)->_line)=_line;
(_n_1->_is_union)=_is_union;
(_n_1->_directives)=((struct _collection_of_directives *)_directives);
(_n_1->_name)=_name;
(_n_1->_parent)=_parent;
(_n_1->_attributes)=_attributes;
return _n_1;
}

static struct _structure_node *_get_struct_definition_p_s__parser_bo_bo_bo(struct _parser *_cc, int _is_union, int _anonymous, int _is_import)
{
struct _collection *_directives_1;
char *_name_2;
int _line_3;
struct _node *_parent_4;
int _token_5;
struct _collection *_attributes_6;
int _global_mode_7;
union _lexeme _lexeme_8;
int _local_mode_9;
char *_attribute_name_10;
int _attribute_line_11;
struct _node *_attribute_type_12;
struct _attribute_node *_attribute_13;
_directives_1=_get_set_of_directives_p_s__parser(_cc);
if(((void*)_directives_1==(void*)0))
{
return ((struct _structure_node *)0);
}
if((!_anonymous))
{
_name_2=_get_id_p_s__parser(_cc);
if(((void*)_name_2==(void*)0))
{
return ((struct _structure_node *)0);
}
}
else
{
_name_2=_empty_string171;
}
_line_3=(((struct _token_stream *)_cc)->_line);
_parent_4=((struct _node *)0);
_token_5=_peek_token_p_s__token_stream(((struct _token_stream *)_cc));
if((_token_5==67))
{
_get_token_p_s__token_stream(((struct _token_stream *)_cc));
_parent_4=_get_type_p_s__parser(_cc);
if(((void*)_parent_4==(void*)0))
{
return ((struct _structure_node *)0);
}
}
_attributes_6=((struct _collection *)0);
if((!_is_import))
{
if((!_expected_p_s__parser_e__token(_cc,64)))
{
return ((struct _structure_node *)0);
}
_attributes_6=_new_collection_p_s__parser(_cc);
_global_mode_7=0;
while(1)
{
_token_5=_get_token_p_s__token_stream_p_s__lexeme(((struct _token_stream *)_cc),(&_lexeme_8));
if((_token_5==48))
{
break;
}
if((_token_5==60))
{
_local_mode_9=1;
_token_5=_get_token_p_s__token_stream_p_s__lexeme(((struct _token_stream *)_cc),(&_lexeme_8));
if((_token_5==64))
{
_global_mode_7=_local_mode_9;
_token_5=_get_token_p_s__token_stream_p_s__lexeme(((struct _token_stream *)_cc),(&_lexeme_8));
}
}
else if((_token_5==61))
{
_local_mode_9=2;
_token_5=_get_token_p_s__token_stream_p_s__lexeme(((struct _token_stream *)_cc),(&_lexeme_8));
if((_token_5==64))
{
_global_mode_7=_local_mode_9;
_token_5=_get_token_p_s__token_stream_p_s__lexeme(((struct _token_stream *)_cc),(&_lexeme_8));
}
}
else
{
_local_mode_9=_global_mode_7;
}
if(_unexpected_p_s__parser_e__token_p_s__lexeme_e__token(_cc,_token_5,(&_lexeme_8),2))
{
return ((struct _structure_node *)0);
}
_attribute_name_10=((&_lexeme_8)->_string_value);
_attribute_line_11=(((struct _token_stream *)_cc)->_line);
if((!_expected_p_s__parser_e__token(_cc,67)))
{
return ((struct _structure_node *)0);
}
_attribute_type_12=_get_type_p_s__parser(_cc);
if(((void*)_attribute_type_12==(void*)0))
{
return ((struct _structure_node *)0);
}
if(((void*)_get_end_of_line_p_s__parser(_cc)==(void*)0))
{
return ((struct _structure_node *)0);
}
_attribute_13=_new_attribute_p_s__parser_in_e__node_code_p_a_ch_p_s__node_e__expose_mode(_cc,_attribute_line_11,0,_attribute_name_10,_attribute_type_12,_local_mode_9);
_append_p_s__collection_p_s__element(_attributes_6,((struct _element *)_attribute_13));
}
}
return _new_structure_p_s__parser_in_bo_p_s__collection_p_a_ch_p_s__node_p_s__collection(_cc,_line_3,_is_union,_directives_1,_name_2,_parent_4,_attributes_6);
}

static struct _binary_node *_get_enum_definition_p_s__parser_bo(struct _parser *_cc, int _is_import)
{
char *_name_1;
int _line_2;
struct _collection *_content_3;
_name_1=_get_id_p_s__parser(_cc);
if(((void*)_name_1==(void*)0))
{
return ((struct _binary_node *)0);
}
_line_2=(((struct _token_stream *)_cc)->_line);
if(_is_import)
{
_content_3=((struct _collection *)0);
}
else if(_expected_p_s__parser_e__token(_cc,64))
{
_content_3=_get_enum_content_p_s__parser(_cc);
if(((void*)_content_3==(void*)0))
{
return ((struct _binary_node *)0);
}
}
else
{
return ((struct _binary_node *)0);
}
return _new_node_p_s__parser_in_e__node_code_p_vo_p_vo(_cc,_line_2,1,((void *)_name_1),((void *)_content_3));
}

static struct _function_node *_new_function_p_s__parser_in_p_s__collection_p_a_ch_p_s__collection_p_s__node_p_s__collection(struct _parser *_cc, int _line, struct _collection *_directives, char *_name, struct _collection *_arguments, struct _node *_returned_type, struct _collection *_body)
{
struct _function_node *_n_1;
_n_1=((struct _function_node *)_new_node_p_s__parser_e__node_code_dw(_cc,4,((unsigned int)32)));
(((struct _node *)_n_1)->_line)=_line;
(_n_1->_directives)=((struct _collection_of_directives *)_directives);
(_n_1->_name)=_name;
(_n_1->_arguments)=_arguments;
(_n_1->_returned_type)=_returned_type;
(_n_1->_body)=_body;
return _n_1;
}

static struct _function_node *_get_function_definition_p_s__parser_bo(struct _parser *_cc, int _is_import)
{
struct _collection *_directives_1;
char *_name_2;
int _line_3;
struct _collection *_arguments_4;
int _token_5;
struct _node *_returned_type_6;
struct _collection *_body_7;
_directives_1=_get_set_of_directives_p_s__parser(_cc);
if(((void*)_directives_1==(void*)0))
{
return ((struct _function_node *)0);
}
_name_2=_get_id_p_s__parser(_cc);
if(((void*)_name_2==(void*)0))
{
return ((struct _function_node *)0);
}
_line_3=(((struct _token_stream *)_cc)->_line);
_arguments_4=_get_parameters_p_s__parser_bo(_cc,_is_import);
if(((void*)_arguments_4==(void*)0))
{
return ((struct _function_node *)0);
}
_token_5=_peek_token_p_s__token_stream(((struct _token_stream *)_cc));
_returned_type_6=((struct _node *)0);
if((_token_5==67))
{
_get_token_p_s__token_stream(((struct _token_stream *)_cc));
_returned_type_6=_get_type_p_s__parser(_cc);
if(((void*)_returned_type_6==(void*)0))
{
return ((struct _function_node *)0);
}
}
_body_7=((struct _collection *)0);
if((!_is_import))
{
_body_7=_get_block_p_s__parser(_cc);
if(((void*)_body_7==(void*)0))
{
return ((struct _function_node *)0);
}
}
return _new_function_p_s__parser_in_p_s__collection_p_a_ch_p_s__collection_p_s__node_p_s__collection(_cc,_line_3,_directives_1,_name_2,_arguments_4,_returned_type_6,_body_7);
}

static struct _section_node *_new_section_p_s__parser_e__expose_mode_p_s__collection_p_s__collection(struct _parser *_cc, int _mode, struct _collection *_content, struct _collection *_directives)
{
struct _section_node *_n_1;
_n_1=((struct _section_node *)_new_node_p_s__parser_e__node_code_dw(_cc,8,((unsigned int)24)));
(_n_1->_mode)=_mode;
(_n_1->_content)=_content;
(_n_1->_directives)=((struct _collection_of_directives *)_directives);
return _n_1;
}

static struct _node *_get_section_p_s__parser_e__expose_mode(struct _parser *_cc, int _mode)
{
struct _collection *_directives_1;
struct _collection *_content_2;
int _token_3;
struct _node *_e_4;
struct _node *_e_5;
_directives_1=_get_set_of_directives_p_s__parser(_cc);
if(((void*)_directives_1==(void*)0))
{
return ((struct _node *)0);
}
_content_2=_new_collection_p_s__parser(_cc);
_token_3=_peek_token_p_s__token_stream(((struct _token_stream *)_cc));
if((_token_3!=64))
{
_e_4=_get_program_element_p_s__parser_bo(_cc,0);
if(((void*)_e_4==(void*)0))
{
return ((struct _node *)0);
}
_append_p_s__collection_p_s__element(_content_2,((struct _element *)_e_4));
}
else
{
_get_token_p_s__token_stream(((struct _token_stream *)_cc));
while(1)
{
_e_5=_get_program_element_p_s__parser_bo(_cc,1);
if(((void*)_e_5==(void*)0))
{
break;
}
_append_p_s__collection_p_s__element(_content_2,((struct _element *)_e_5));
if((!_expected_p_s__parser_e__token(_cc,64)))
{
break;
}
}
}
return ((struct _node *)_new_section_p_s__parser_e__expose_mode_p_s__collection_p_s__collection(_cc,_mode,_content_2,_directives_1));
}

static struct _node *_get_import_p_s__parser(struct _parser *_cc)
{
union _lexeme _lexeme_1;
int _token_2;
_token_2=_get_token_p_s__token_stream_p_s__lexeme(((struct _token_stream *)_cc),(&_lexeme_1));
switch(_token_2)
{
case 4:
case 5:
{
return ((struct _node *)_new_string_p_s__parser_e__node_code_p_a_ch(_cc,9,((&_lexeme_1)->_string_value)));
break;
}
case 51:
{
return ((struct _node *)_get_struct_definition_p_s__parser_bo_bo_bo(_cc,0,0,1));
break;
}
case 52:
{
return ((struct _node *)_get_struct_definition_p_s__parser_bo_bo_bo(_cc,1,0,1));
break;
}
case 53:
{
return ((struct _node *)_get_enum_definition_p_s__parser_bo(_cc,1));
break;
}
case 54:
{
return ((struct _node *)_get_function_definition_p_s__parser_bo(_cc,1));
break;
}
case 49:
{
return ((struct _node *)_get_variable_definition_p_s__parser_bo(_cc,1));
break;
}
default:
{
return ((struct _node *)_syntax_error_p_s__parser_e__token_p_s__lexeme(_cc,_token_2,(&_lexeme_1)));
break;
}
}
}

static struct _node *_get_program_element_p_s__parser_bo(struct _parser *_cc, int _accepts_end)
{
union _lexeme _lexeme_1;
int _token_2;
_token_2=_get_token_p_s__token_stream_p_s__lexeme(((struct _token_stream *)_cc),(&_lexeme_1));
switch(_token_2)
{
case 51:
{
return ((struct _node *)_get_struct_definition_p_s__parser_bo_bo_bo(_cc,0,0,0));
break;
}
case 52:
{
return ((struct _node *)_get_struct_definition_p_s__parser_bo_bo_bo(_cc,1,0,0));
break;
}
case 53:
{
return ((struct _node *)_get_enum_definition_p_s__parser_bo(_cc,0));
break;
}
case 56:
{
return ((struct _node *)_get_typedef_definition_p_s__parser(_cc));
break;
}
case 54:
{
return ((struct _node *)_get_function_definition_p_s__parser_bo(_cc,0));
break;
}
case 49:
{
return ((struct _node *)_get_variable_definition_p_s__parser_bo(_cc,0));
break;
}
case 50:
{
return _get_equate_definition_p_s__parser_bo(_cc,1);
break;
}
case 60:
{
return _get_section_p_s__parser_e__expose_mode(_cc,1);
break;
}
case 61:
{
return _get_section_p_s__parser_e__expose_mode(_cc,2);
break;
}
case 63:
{
return _get_import_p_s__parser(_cc);
break;
}
case 1:
{
return ((struct _node *)0);
break;
}
case 48:
{
if(_accepts_end)
{
return ((struct _node *)0);
}
else
{
return ((struct _node *)_syntax_error_p_s__parser_e__token_p_s__lexeme(_cc,_token_2,(&_lexeme_1)));
}
break;
}
default:
{
return ((struct _node *)_syntax_error_p_s__parser_e__token_p_s__lexeme(_cc,_token_2,(&_lexeme_1)));
break;
}
}
}

static struct _collection *_run_p_s__memory_bundle_p_s__memory_bundle_p_a_ch_p_a_ch_p_s__dictionary_bo(struct _memory_bundle *_string_bundle, struct _memory_bundle *_tree_bundle, char *_filename, char *_source_code, struct _dictionary *_defines, int _unicode)
{
struct _parser _cc_1;
struct _collection *_c_2;
struct _node *_e_3;
union _lexeme _lexeme_4;
int _token_5;
((&_cc_1)->_bundle)=_tree_bundle;
((&_cc_1)->_number_of_errors)=0;
((&_cc_1)->_filename)=_filename;
_initialize_p_s__token_stream_p_s__memory_bundle_p_a_ch_p_s__dictionary_bo(((struct _token_stream *)(&_cc_1)),_string_bundle,_source_code,_defines,_unicode);
_c_2=_new_collection_p_s__parser((&_cc_1));
while(1)
{
_e_3=_get_program_element_p_s__parser_bo((&_cc_1),0);
if(((void*)_e_3==(void*)0))
{
break;
}
_append_p_s__collection_p_s__element(_c_2,((struct _element *)_e_3));
_token_5=_get_token_p_s__token_stream_p_s__lexeme(((struct _token_stream *)(&_cc_1)),(&_lexeme_4));
if(_unexpected_p_s__parser_e__token_p_s__lexeme_e__token((&_cc_1),_token_5,(&_lexeme_4),64))
{
break;
}
}
if((((&_cc_1)->_number_of_errors)==0))
{
return _c_2;
}
else
{
return ((struct _collection *)0);
}
}

static void _release_p_s__file_buffer(struct _file_buffer *_buffer)
{
if(((void*)(_buffer->_base)!=(void*)0))
{
_free_memory_p_vo(((void *)(_buffer->_base)));
}
}

static char *_last_occurrence_p_a_ch_ch(char *_s, char _c)
{
char *_q_1;
_q_1=((_s+_size_p_a_ch(_s))-1);
while(((void*)((unsigned char *)_q_1)>=(void*)((unsigned char *)_s)))
{
if(((_q_1[0])==_c))
{
return _q_1;
}
_q_1-=1;
}
return ((char *)0);
}

static char *_file_path_p_s__memory_bundle_p_a_ch(struct _memory_bundle *_bundle, char *_filename)
{
char *_occurence_1;
_occurence_1=_last_occurrence_p_a_ch_ch(_filename,path_separator());
if(((void*)_occurence_1==(void*)0))
{
return _empty_string171;
}
return _new_string_p_s__memory_bundle_p_a_ch_dw(_bundle,_filename,((unsigned int )(_occurence_1-_filename)));
}

static void _initialize_p_s__collection_of_program_elements_p_s__memory_bundle(struct _collection_of_program_elements *_c, struct _memory_bundle *_bundle)
{
_initialize_p_s__collection((&(_c->_collection)));
(_c->_bundle)=_bundle;
}

static struct _set_of_directives *_new_set_of_directives_p_s__memory_bundle_p_s__set_of_directives(struct _memory_bundle *_bundle, struct _set_of_directives *_parent)
{
struct _set_of_directives *_s_1;
_s_1=((struct _set_of_directives *)_allocate_p_s__memory_bundle_dw(_bundle,((unsigned int)16)));
(_s_1->_bundle)=_bundle;
(_s_1->_parent)=_parent;
_initialize_p_s__collection((&(_s_1->_directives)));
return _s_1;
}

static int _ends_with_p_a_ch_ch(char *_s, char _c)
{
int _len_1;
_len_1=_size_p_a_ch(_s);
return ((_len_1>0)&&((_s[(_len_1-1)])==_c));
}

static int _file_exists_p_a_ch(char *_filename)
{
return (file_exists(_filename)!=0);
}

static char *_get_fullname_p_s__compiler_p_a_ch(struct _compiler *_cc, char *_module_name)
{
char *_filename_1;
struct _string_buffer _t_2;
struct _string_buffer *_fullname_3;
struct _include_directory *_p_4;
_filename_1=_module_name;
_fullname_3=(_initialize_p_s__string_buffer((&_t_2)),(&_t_2));
_p_4=(_cc->_path);
while(((void*)_p_4!=(void*)0))
{
(_fullname_3->_used)=0;
_append_p_s__string_buffer_p_a_ch(_fullname_3,((char *)(((struct _list_item *)_p_4)->_value)));
if((!_ends_with_p_a_ch_ch(((char *)(((struct _list_item *)_p_4)->_value)),'/')))
{
_append_p_s__string_buffer_ch(_fullname_3,'/');
}
_append_p_s__string_buffer_p_a_ch(_fullname_3,_filename_1);
_append_p_s__string_buffer_p_a_ch(_fullname_3,T625);
_to_OS_name_p_a_ch(_as_string_p_s__string_buffer(_fullname_3));
if(_file_exists_p_a_ch(_as_string_p_s__string_buffer(_fullname_3)))
{
_filename_1=_new_string_p_s__memory_bundle_p_a_ch((_cc->_bundle),_as_string_p_s__string_buffer(_fullname_3));
break;
}
_p_4=((struct _include_directory *)(((struct _element *)_p_4)->_next_sibling));
}
_release_p_s__string_buffer(_fullname_3);
return _filename_1;
}

static void _error_p_s__compiler_p_s__node_p_a_ch_p_vo(struct _compiler *_cc, struct _node *_source, char *_fmt, void *_arg1)
{
void *(_args_1)[1];
(_args_1[0])=_arg1;
_error_p_s__compiler_p_s__module_in_p_a_ch_p_a_p_vo(_cc,(_cc->_module),(_source->_line),_fmt,((void **)_args_1));
}

static void _error_p_s__compiler_p_s__node_p_a_ch(struct _compiler *_cc, struct _node *_source, char *_fmt)
{
_error_p_s__compiler_p_s__module_in_p_a_ch_p_a_p_vo(_cc,(_cc->_module),(_source->_line),_fmt,((void **)0));
}

static void _reserve_p_s__vector(struct _vector *_v)
{
unsigned int _new_allocated_1;
void **_new_array_2;
_new_allocated_1=((((((struct _priv_vector *)_v)->_allocated)*5)/4)+1);
_new_array_2=((void **)_allocate_memory_dw((_new_allocated_1*((unsigned int)4))));
_copy_p_vo_p_vo_dw(((void *)_new_array_2),((void *)(((struct _priv_vector *)_v)->_array)),((((struct _priv_vector *)_v)->_allocated)*((unsigned int)4)));
_free_memory_p_vo(((void *)(((struct _priv_vector *)_v)->_array)));
(((struct _priv_vector *)_v)->_allocated)=_new_allocated_1;
(((struct _priv_vector *)_v)->_array)=_new_array_2;
}

static void _append_p_s__vector_p_vo(struct _vector *_v, void *_value)
{
unsigned int _index_1;
_index_1=(_v->_size);
if(((((struct _priv_vector *)_v)->_allocated)==_index_1))
{
_reserve_p_s__vector(_v);
}
((((struct _priv_vector *)_v)->_array)[_index_1])=_value;
(++(_v->_size));
}

static struct _definition_item *_first_definition_p_s__definition_dictionary_p_a_ch_p_s__signature(struct _definition_dictionary *_d, char *_name, struct _signature *_signature)
{
struct _definition_head _t_1;
struct _definition_head *_head_2;
_definition_head_p_s__definition_item_p_a_ch_p_s__signature_p_s__definition_head(((struct _definition_item *)0),_name,_signature,(&_t_1));
_head_2=((struct _definition_head *)_value_p_s__dictionary_p_vo(((struct _dictionary *)_d),((void *)(&_t_1))));
if(((void*)_head_2!=(void*)0))
{
return (_head_2->_first);
}
else
{
return ((struct _definition_item *)0);
}
}

static int _equals_p_s__type_p_s__type(struct _type *_t1, struct _type *_t2)
{
if(((void*)_t1==(void*)_t2))
{
return 1;
}
if(((_t1->_id)!=(_t2->_id)))
{
return 0;
}
switch((_t1->_id))
{
case 15:
{
return _equals_p_s__type_p_s__type((((struct _pointer_type *)_t1)->_target_type),(((struct _pointer_type *)_t2)->_target_type));
break;
}
case 16:
case 17:
{
return 0;
break;
}
case 18:
{
return (_equals_p_s__type_p_s__type((((struct _array_type *)_t1)->_array_type),(((struct _array_type *)_t2)->_array_type))&&((((struct _array_type *)_t1)->_array_size)==(((struct _array_type *)_t2)->_array_size)));
break;
}
case 19:
{
return (_equals_p_s__type_p_s__type((((struct _function_type *)_t1)->_returned_type),(((struct _function_type *)_t2)->_returned_type))&&_equals_p_s__signature_p_s__signature((((struct _function_type *)_t1)->_signature),(((struct _function_type *)_t2)->_signature)));
break;
}
default:
{
return 0;
break;
}
}
}

static int _equals_p_s__signature_p_s__signature(struct _signature *_s1, struct _signature *_s2)
{
int _n_1;
int _i_2;
_n_1=(_s1->_number_of_types);
if((_n_1!=(_s2->_number_of_types)))
{
return 0;
}
_i_2=0;
while((_i_2<_n_1))
{
if((!_equals_p_s__type_p_s__type(_type_p_s__signature_in(_s1,_i_2),_type_p_s__signature_in(_s2,_i_2))))
{
return 0;
}
(++_i_2);
}
return 1;
}

static struct _definition *_find_definition_p_s__definition_dictionary_p_a_ch_p_s__signature(struct _definition_dictionary *_d, char *_name, struct _signature *_signature)
{
struct _definition_item *_item_1;
_item_1=_first_definition_p_s__definition_dictionary_p_a_ch_p_s__signature(_d,_name,_signature);
while(((void*)_item_1!=(void*)0))
{
if((_is_equal_p_a_ch_p_a_ch(((_item_1->_value)->_name),_name)&&_equals_p_s__signature_p_s__signature(((_item_1->_value)->_signature),_signature)))
{
return (_item_1->_value);
}
_item_1=(_item_1->_next);
}
return ((struct _definition *)0);
}

static struct _definition *_find_definition_p_s__compiler_p_a_ch_p_s__signature(struct _compiler *_cc, char *_name, struct _signature *_signature)
{
struct _scope *_scope_1;
struct _definition *_definition_2;
_scope_1=(_cc->_current_scope);
while(((void*)_scope_1!=(void*)0))
{
_definition_2=_find_definition_p_s__definition_dictionary_p_a_ch_p_s__signature((&(_scope_1->_definitions)),_name,_signature);
if(((void*)_definition_2!=(void*)0))
{
return _definition_2;
}
_scope_1=(_scope_1->_next);
}
return ((struct _definition *)0);
}

static void _error_p_s__compiler_p_s__module_in_p_a_ch(struct _compiler *_cc, struct _module *_module, int _line, char *_fmt)
{
_error_p_s__compiler_p_s__module_in_p_a_ch_p_a_p_vo(_cc,_module,_line,_fmt,((void **)0));
}

static void _error_already_defined_p_s__compiler_p_a_ch_in_p_s__module_in(struct _compiler *_cc, char *_name, int _line, struct _module *_first_module, int _first_line)
{
_error_p_s__compiler_in_p_a_ch_p_vo(_cc,_line,_error_already_defined656,((void *)_name));
_error_p_s__compiler_p_s__module_in_p_a_ch(_cc,_first_module,_first_line,_error_first_definition658);
}

static void _error_already_defined_p_s__compiler_p_a_ch_in_p_s__definition(struct _compiler *_cc, char *_name, int _line, struct _definition *_definition)
{
struct _module *_first_module_1;
int _first_line_2;
_first_module_1=((struct _module *)(_definition->_module));
_first_line_2=(_definition->_line);
_error_already_defined_p_s__compiler_p_a_ch_in_p_s__module_in(_cc,_name,_line,_first_module_1,_first_line_2);
}

static int _add_global_definition_p_s__compiler_p_s__definition_bo(struct _compiler *_cc, struct _definition *_definition, int _expose)
{
char *_name_1;
struct _signature *_signature_2;
struct _definition *_previous_3;
_name_1=(_definition->_name);
_signature_2=(_definition->_signature);
_previous_3=_find_definition_p_s__compiler_p_a_ch_p_s__signature(_cc,_name_1,_signature_2);
if(((void*)_previous_3!=(void*)0))
{
_error_already_defined_p_s__compiler_p_a_ch_in_p_s__definition(_cc,_name_1,(_definition->_line),_previous_3);
return 0;
}
_add_definition_p_s__definition_dictionary_p_s__definition((&((&(_cc->_global_scope))->_definitions)),_definition);
if(_expose)
{
_append_p_s__list_p_vo((&((_cc->_module)->_exposed_definitions)),((void *)_definition));
}
return 1;
}

static struct _named_type *_find_type_p_s__compiler_p_a_ch(struct _compiler *_cc, char *_name)
{
struct _scope *_scope_1;
struct _named_type *_type_2;
_scope_1=(_cc->_current_scope);
while(((void*)_scope_1!=(void*)0))
{
_type_2=((struct _named_type *)_value_p_s__dictionary_p_vo(((struct _dictionary *)(&(_scope_1->_types))),((void *)_name)));
if(((void*)_type_2!=(void*)0))
{
return _type_2;
}
_scope_1=(_scope_1->_next);
}
return ((struct _named_type *)0);
}

static void _error_already_defined_p_s__compiler_p_a_ch_in_p_s__named_type(struct _compiler *_cc, char *_name, int _line, struct _named_type *_type)
{
struct _module *_first_module_1;
int _first_line_2;
_first_module_1=((struct _module *)(_type->_module));
_first_line_2=(_type->_line);
_error_already_defined_p_s__compiler_p_a_ch_in_p_s__module_in(_cc,_name,_line,_first_module_1,_first_line_2);
}

static int _add_global_type_p_s__compiler_p_s__named_type_bo(struct _compiler *_cc, struct _named_type *_type, int _expose)
{
char *_name_1;
struct _named_type *_previous_2;
_name_1=(_type->_name);
_previous_2=_find_type_p_s__compiler_p_a_ch(_cc,_name_1);
if(((void*)_previous_2!=(void*)0))
{
_error_already_defined_p_s__compiler_p_a_ch_in_p_s__named_type(_cc,_name_1,(_type->_line),_previous_2);
return 0;
}
_add_p_s__dictionary_p_vo_p_vo(((struct _dictionary *)(&((&(_cc->_global_scope))->_types))),((void *)_name_1),((void *)_type));
if(_expose)
{
_append_p_s__list_p_vo((&((_cc->_module)->_exposed_types)),((void *)_type));
}
return 1;
}

static void _add_import_p_s__compiler_p_s__module_bo(struct _compiler *_cc, struct _module *_module, int _expose)
{
unsigned int _i_1;
unsigned int _size_2;
struct _list_item *_e_3;
void *_v_4;
struct _list_item *_e_5;
void *_v_6;
struct _list_item *_e_7;
void *_v_8;
_i_1=((unsigned int)0);
_size_2=((&(_cc->_imports))->_size);
while((_i_1<_size_2))
{
if(((void*)((((struct _priv_vector *)(&(_cc->_imports)))->_array)[_i_1])==(void*)_module))
{
return;
}
(++_i_1);
}
_append_p_s__vector_p_vo((&(_cc->_imports)),((void *)_module));
if(_expose)
{
_append_p_s__list_p_vo((&((_cc->_module)->_exposed_imports)),((void *)_module));
}
_e_3=((&(_module->_exposed_imports))->_first);
while(((void*)_e_3!=(void*)0))
{
_v_4=(_e_3->_value);
_add_import_p_s__compiler_p_s__module_bo(_cc,((struct _module *)_v_4),_expose);
_e_3=((struct _list_item *)(((struct _element *)_e_3)->_next_sibling));
}
_e_5=((&(_module->_exposed_definitions))->_first);
while(((void*)_e_5!=(void*)0))
{
_v_6=(_e_5->_value);
_add_global_definition_p_s__compiler_p_s__definition_bo(_cc,((struct _definition *)_v_6),0);
_e_5=((struct _list_item *)(((struct _element *)_e_5)->_next_sibling));
}
_e_7=((&(_module->_exposed_types))->_first);
while(((void*)_e_7!=(void*)0))
{
_v_8=(_e_7->_value);
_add_global_type_p_s__compiler_p_s__named_type_bo(_cc,((struct _named_type *)_v_8),0);
_e_7=((struct _list_item *)(((struct _element *)_e_7)->_next_sibling));
}
}

static void _add_p_s__compiler_p_s__expression(struct _compiler *_cc, struct _expression *_e)
{
_append_p_s__collection_p_s__element(((struct _collection *)(&((_cc->_module)->_elements))),((struct _element *)_e));
}

static struct _expression *_new_expression_p_s__memory_bundle_dw_p_s__type_e__expression_code(struct _memory_bundle *_bundle, unsigned int _size, struct _type *_type, int _opcode)
{
struct _expression *_e_1;
_e_1=((struct _expression *)_allocate_p_s__memory_bundle_dw(_bundle,_size));
(_e_1->_type)=_type;
(_e_1->_opcode)=_opcode;
(_e_1->_lvalue)=0;
return _e_1;
}

static struct _module_expression *_new_module_expression_p_s__memory_bundle_p_s__type_e__expression_code_p_s__module(struct _memory_bundle *_bundle, struct _type *_type, int _opcode, struct _module *_value)
{
struct _module_expression *_e_1;
_e_1=((struct _module_expression *)_new_expression_p_s__memory_bundle_dw_p_s__type_e__expression_code(_bundle,((unsigned int)24),_type,_opcode));
(_e_1->_value)=_value;
return _e_1;
}

static struct _directive *_new_directive_p_s__memory_bundle_p_a_ch_p_a_ch(struct _memory_bundle *_bundle, char *_name, char *_value)
{
struct _directive *_d_1;
_d_1=((struct _directive *)_allocate_p_s__memory_bundle_dw(_bundle,((unsigned int)12)));
(_d_1->_name)=_name;
(_d_1->_value)=_value;
return _d_1;
}

static void _add_p_s__set_of_directives_p_a_ch_p_a_ch(struct _set_of_directives *_s, char *_name, char *_value)
{
struct _directive *_d_1;
_d_1=_new_directive_p_s__memory_bundle_p_a_ch_p_a_ch((_s->_bundle),_name,_value);
_append_p_s__collection_p_s__element((&(_s->_directives)),((struct _element *)_d_1));
}

static struct _set_of_directives *_get_directives_p_s__compiler_p_s__set_of_directives_p_s__collection_of_directives(struct _compiler *_cc, struct _set_of_directives *_directives, struct _collection_of_directives *_extension)
{
struct _set_of_directives *_d_1;
struct _element *_e_2;
if(((void*)(((struct _collection *)_extension)->_first)==(void*)0))
{
return _directives;
}
_d_1=_new_set_of_directives_p_s__memory_bundle_p_s__set_of_directives((_cc->_bundle),_directives);
_e_2=(((struct _collection *)_extension)->_first);
while(((void*)_e_2!=(void*)0))
{
_add_p_s__set_of_directives_p_a_ch_p_a_ch(_d_1,((char *)(((struct _binary_node *)((struct _directive_node *)_e_2))->_value_1)),((char *)(((struct _binary_node *)((struct _directive_node *)_e_2))->_value_2)));
_e_2=(_e_2->_next_sibling);
}
return _d_1;
}

static void _set_expose_p_s__type_bo(struct _type *_t, int _expose)
{
if(_expose)
{
(_t->_flags)|=64;
}
else
{
(_t->_flags)&=-65;
}
}

static struct _redirect_type *_new_redirect_type_p_s__memory_bundle_bo_p_vo_p_s__redirect_node(struct _memory_bundle *_bundle, int _expose, void *_module, struct _redirect_node *_source)
{
struct _redirect_type *_t_1;
_t_1=((struct _redirect_type *)_new_type_p_s__memory_bundle_dw_e__type_id_in_dw_dw(_bundle,((unsigned int)52),20,0,((unsigned int )-1),((unsigned int )1)));
_set_expose_p_s__type_bo(((struct _type *)_t_1),_expose);
(((struct _named_type *)_t_1)->_name)=((char *)(((struct _binary_node *)_source)->_value_1));
(((struct _named_type *)_t_1)->_source)=((struct _node *)_source);
(((struct _named_type *)_t_1)->_module)=_module;
(((struct _named_type *)_t_1)->_line)=(((struct _node *)_source)->_line);
(_t_1->_real_type)=((struct _type *)0);
return _t_1;
}

static void _add_p_s__collection_of_program_elements_p_s__node_p_vo(struct _collection_of_program_elements *_c, struct _node *_node, void *_value)
{
struct _program_element *_e_1;
_e_1=((struct _program_element *)_allocate_p_s__memory_bundle_dw((_c->_bundle),((unsigned int)20)));
(_e_1->_node)=_node;
(_e_1->_value)=_value;
_append_p_s__collection_p_s__element((&(_c->_collection)),((struct _element *)_e_1));
}

static char *_new_string_p_s__memory_bundle_dw(struct _memory_bundle *_bundle, unsigned int _len)
{
return ((char *)_allocate_p_s__memory_bundle_dw(_bundle,(_len+1)));
}

static char *_append_p_a_ch_ch(char *_s, char _c)
{
(_s[0])=_c;
return (_s+1);
}

static char *_append_p_a_ch_p_a_ch(char *_s, char *_str)
{
int _size_1;
_size_1=_size_p_a_ch(_str);
_copy_p_vo_p_vo_dw(((void *)_s),((void *)_str),((unsigned int )_size_1));
return (_s+_size_1);
}

static struct _structure_type *_new_structure_type_p_s__memory_bundle_bo_p_s__set_of_directives_p_vo_p_s__structure_node(struct _memory_bundle *_bundle, int _expose, struct _set_of_directives *_directives, void *_module, struct _structure_node *_source)
{
char *_name_1;
char *_local_name_2;
struct _structure_type *_t_3;
_name_1=(_source->_name);
if(((_name_1[0])!='\x00'))
{
_local_name_2=_new_string_p_s__memory_bundle_dw(_bundle,((unsigned int )(6+_size_p_a_ch(_name_1))));
_append_p_a_ch_ch(_append_p_a_ch_p_a_ch(_append_p_a_ch_p_a_ch(_local_name_2,T685),_name_1),'\x00');
}
else
{
_local_name_2=_empty_string171;
}
_t_3=((struct _structure_type *)_new_type_p_s__memory_bundle_dw_e__type_id_in_dw_dw(_bundle,((unsigned int)72),((_source->_is_union)?17:16),2,((unsigned int )-1),((unsigned int )1)));
_set_expose_p_s__type_bo(((struct _type *)_t_3),_expose);
(((struct _named_type *)_t_3)->_name)=_local_name_2;
(((struct _named_type *)_t_3)->_source)=((struct _node *)_source);
(((struct _named_type *)_t_3)->_module)=_module;
(((struct _named_type *)_t_3)->_line)=(((struct _node *)_source)->_line);
(_t_3->_short_name)=_name_1;
(_t_3->_defined)=0;
(_t_3->_extern_name)=_empty_string171;
(_t_3->_parent_type)=((struct _type *)0);
(_t_3->_signature)=_new_signature_p_s__memory_bundle_p_s__type(_bundle,_pointer_p_s__type(((struct _type *)_t_3)));
(_t_3->_directives)=_directives;
return _t_3;
}

static struct _redirect_type *_new_redirect_type_p_s__memory_bundle_bo_p_a_ch_p_s__type_p_vo_in(struct _memory_bundle *_bundle, int _expose, char *_name, struct _type *_type, void *_module, int _line)
{
struct _redirect_type *_t_1;
_t_1=((struct _redirect_type *)_new_type_p_s__memory_bundle_dw_e__type_id_in_dw_dw(_bundle,((unsigned int)52),20,1073741824,((unsigned int )-1),((unsigned int )1)));
_set_expose_p_s__type_bo(((struct _type *)_t_1),_expose);
(((struct _named_type *)_t_1)->_name)=_name;
(((struct _named_type *)_t_1)->_source)=((struct _node *)0);
(((struct _named_type *)_t_1)->_module)=_module;
(((struct _named_type *)_t_1)->_line)=_line;
(_t_1->_real_type)=_type;
return _t_1;
}

static struct _enumeration_type *_new_enumeration_type_p_s__memory_bundle_bo_p_vo_p_s__enumeration_node(struct _memory_bundle *_bundle, int _expose, void *_module, struct _enumeration_node *_source)
{
struct _enumeration_type *_t_1;
_t_1=((struct _enumeration_type *)_new_type_p_s__memory_bundle_dw_e__type_id_in_dw_dw(_bundle,((unsigned int)48),14,40,((unsigned int)4),((unsigned int)4)));
_set_expose_p_s__type_bo(((struct _type *)_t_1),_expose);
(((struct _named_type *)_t_1)->_name)=((char *)(((struct _binary_node *)_source)->_value_1));
(((struct _named_type *)_t_1)->_source)=((struct _node *)_source);
(((struct _named_type *)_t_1)->_module)=_module;
(((struct _named_type *)_t_1)->_line)=(((struct _node *)_source)->_line);
(_t_1->_defined)=0;
return _t_1;
}

static void _add_p_s__collection_of_program_elements_p_s__node_p_vo_bo_p_s__set_of_directives(struct _collection_of_program_elements *_c, struct _node *_node, void *_value, int _expose, struct _set_of_directives *_directives)
{
struct _program_element *_e_1;
_e_1=((struct _program_element *)_allocate_p_s__memory_bundle_dw((_c->_bundle),((unsigned int)20)));
(_e_1->_node)=_node;
(_e_1->_value)=_value;
(_e_1->_expose)=_expose;
(_e_1->_directives)=_directives;
_append_p_s__collection_p_s__element((&(_c->_collection)),((struct _element *)_e_1));
}

static void _declare_types_p_s__compiler_p_s__collection_of_program_elements_p_s__collection_bo_p_s__set_of_directives(struct _compiler *_cc, struct _collection_of_program_elements *_list, struct _collection *_tree, int _expose, struct _set_of_directives *_base_directives)
{
struct _element *_e_1;
char *_filename_2;
struct _module *_module_3;
int _mode_4;
struct _collection *_content_5;
struct _collection_of_directives *_source_directives_6;
int _section_expose_7;
struct _set_of_directives *_section_directives_8;
struct _redirect_type *_redirect_9;
struct _set_of_directives *_directives_10;
struct _structure_type *_structure_11;
char *_name_12;
struct _redirect_type *_pointer_13;
struct _named_type *_t_14;
struct _enumeration_type *_enumeration_15;
_e_1=(_tree->_first);
while(((void*)_e_1!=(void*)0))
{
if(((_cc->_number_of_errors)>=100))
{
return;
}
switch((((struct _node *)_e_1)->_code))
{
case 9:
{
_filename_2=_get_fullname_p_s__compiler_p_a_ch(_cc,(((struct _string_node *)((struct _node *)_e_1))->_value));
_module_3=_load_module_p_s__compiler_session_p_a_ch_p_s__include_directory((_cc->_session),_filename_2,(_cc->_path));
if(((void*)_module_3==(void*)0))
{
_error_p_s__compiler_p_s__node_p_a_ch_p_vo(_cc,((struct _node *)((struct _string_node *)((struct _node *)_e_1))),_error_cant_open_file630,((void *)_filename_2));
return;
}
else if((_module_3->_loading))
{
_error_p_s__compiler_p_s__node_p_a_ch(_cc,((struct _node *)((struct _string_node *)((struct _node *)_e_1))),_error_circular_dependency632);
return;
}
else
{
_add_import_p_s__compiler_p_s__module_bo(_cc,_module_3,_expose);
_add_p_s__compiler_p_s__expression(_cc,((struct _expression *)_new_module_expression_p_s__memory_bundle_p_s__type_e__expression_code_p_s__module((_cc->_bundle),((struct _type *)_void_type114),8,_module_3)));
}
break;
}
case 8:
{
_mode_4=(((struct _section_node *)((struct _node *)_e_1))->_mode);
_content_5=(((struct _section_node *)((struct _node *)_e_1))->_content);
_source_directives_6=(((struct _section_node *)((struct _node *)_e_1))->_directives);
_section_expose_7=(_mode_4==1);
_section_directives_8=_get_directives_p_s__compiler_p_s__set_of_directives_p_s__collection_of_directives(_cc,_base_directives,_source_directives_6);
_declare_types_p_s__compiler_p_s__collection_of_program_elements_p_s__collection_bo_p_s__set_of_directives(_cc,_list,_content_5,_section_expose_7,_section_directives_8);
break;
}
case 2:
{
_redirect_9=_new_redirect_type_p_s__memory_bundle_bo_p_vo_p_s__redirect_node((_cc->_bundle),_expose,((void *)(_cc->_module)),((struct _redirect_node *)((struct _node *)_e_1)));
_add_global_type_p_s__compiler_p_s__named_type_bo(_cc,((struct _named_type *)_redirect_9),_expose);
_add_p_s__collection_of_program_elements_p_s__node_p_vo(_list,((struct _node *)((struct _redirect_node *)((struct _node *)_e_1))),((void *)_redirect_9));
break;
}
case 3:
{
_directives_10=_get_directives_p_s__compiler_p_s__set_of_directives_p_s__collection_of_directives(_cc,_base_directives,(((struct _structure_node *)((struct _node *)_e_1))->_directives));
_structure_11=_new_structure_type_p_s__memory_bundle_bo_p_s__set_of_directives_p_vo_p_s__structure_node((_cc->_bundle),_expose,_directives_10,((void *)(_cc->_module)),((struct _structure_node *)((struct _node *)_e_1)));
_name_12=(((struct _structure_node *)((struct _node *)_e_1))->_name);
_pointer_13=_new_redirect_type_p_s__memory_bundle_bo_p_a_ch_p_s__type_p_vo_in((_cc->_bundle),_expose,_name_12,_pointer_p_s__type(((struct _type *)_structure_11)),((void *)(_cc->_module)),(((struct _node *)((struct _structure_node *)((struct _node *)_e_1)))->_line));
_t_14=_find_type_p_s__compiler_p_a_ch(_cc,(((struct _named_type *)_structure_11)->_name));
if((((void*)_t_14!=(void*)0)&&(((((struct _type *)_t_14)->_flags)&2)!=0)))
{
_error_already_defined_p_s__compiler_p_a_ch_in_p_s__named_type(_cc,_name_12,(((struct _node *)((struct _structure_node *)((struct _node *)_e_1)))->_line),_t_14);
}
else
{
_add_global_type_p_s__compiler_p_s__named_type_bo(_cc,((struct _named_type *)_pointer_13),_expose);
_add_global_type_p_s__compiler_p_s__named_type_bo(_cc,((struct _named_type *)_structure_11),_expose);
_add_p_s__collection_of_program_elements_p_s__node_p_vo(_list,((struct _node *)((struct _structure_node *)((struct _node *)_e_1))),((void *)_structure_11));
}
break;
}
case 1:
{
_enumeration_15=_new_enumeration_type_p_s__memory_bundle_bo_p_vo_p_s__enumeration_node((_cc->_bundle),_expose,((void *)(_cc->_module)),((struct _enumeration_node *)((struct _node *)_e_1)));
_add_global_type_p_s__compiler_p_s__named_type_bo(_cc,((struct _named_type *)_enumeration_15),_expose);
_add_p_s__collection_of_program_elements_p_s__node_p_vo(_list,((struct _node *)((struct _enumeration_node *)((struct _node *)_e_1))),((void *)_enumeration_15));
break;
}
case 5:
case 4:
case 6:
case 7:
{
_add_p_s__collection_of_program_elements_p_s__node_p_vo_bo_p_s__set_of_directives(_list,((struct _node *)_e_1),((void *)0),_expose,_base_directives);
break;
}
}
_e_1=(_e_1->_next_sibling);
}
}

static void _declare_types_p_s__compiler_p_s__collection_of_program_elements_p_s__collection(struct _compiler *_cc, struct _collection_of_program_elements *_list, struct _collection *_tree)
{
int _expose_1;
struct _set_of_directives *_directives_2;
_expose_1=0;
_directives_2=_new_set_of_directives_p_s__memory_bundle_p_s__set_of_directives((_cc->_bundle),((struct _set_of_directives *)0));
_declare_types_p_s__compiler_p_s__collection_of_program_elements_p_s__collection_bo_p_s__set_of_directives(_cc,_list,_tree,_expose_1,_directives_2);
}

static void _initialize_p_s__definition_p_a_ch_p_s__signature_p_s__type_e__definition_code_bo_bo_p_vo_p_s__node(struct _definition *_d, char *_name, struct _signature *_signature, struct _type *_type, int _opcode, int _lvalue, int _expose, void *_module, struct _node *_source)
{
(_d->_opcode)=_opcode;
(_d->_name)=_name;
(_d->_signature)=_signature;
(_d->_type)=_type;
(_d->_lvalue)=_lvalue;
(_d->_expose)=_expose;
(_d->_used)=0;
(_d->_module)=_module;
(_d->_line)=(_source->_line);
(_d->_source)=_source;
}

static struct _variable *_new_variable_p_s__memory_bundle_p_a_ch_bo_p_s__set_of_directives_p_vo_p_s__node(struct _memory_bundle *_bundle, char *_name, int _expose, struct _set_of_directives *_directives, void *_module, struct _node *_source)
{
struct _variable *_v_1;
_v_1=((struct _variable *)_allocate_p_s__memory_bundle_dw(_bundle,((unsigned int)84)));
_initialize_p_s__definition_p_a_ch_p_s__signature_p_s__type_e__definition_code_bo_bo_p_vo_p_s__node(((struct _definition *)_v_1),_name,(&_empty_sig107),((struct _type *)0),1,0,_expose,_module,_source);
(_v_1->_directives)=_directives;
(_v_1->_initial_value)=((struct _expression *)0);
(_v_1->_proxy)=((struct _expression *)0);
(_v_1->_reference)=((struct _expression *)0);
(_v_1->_extern_name)=_empty_string171;
(_v_1->_defined)=0;
(_v_1->_storage_type)=((struct _type *)0);
(_v_1->_redirect)=((struct _variable *)0);
(_v_1->_function)=((struct _function *)0);
return _v_1;
}

static int _add_local_definition_p_s__compiler_p_s__definition(struct _compiler *_cc, struct _definition *_definition)
{
char *_name_1;
struct _signature *_signature_2;
struct _definition *_previous_3;
_name_1=(_definition->_name);
_signature_2=(_definition->_signature);
_previous_3=_find_definition_p_s__compiler_p_a_ch_p_s__signature(_cc,_name_1,_signature_2);
if(((void*)_previous_3!=(void*)0))
{
_error_already_defined_p_s__compiler_p_a_ch_in_p_s__definition(_cc,_name_1,(_definition->_line),_previous_3);
return 0;
}
_add_definition_p_s__definition_dictionary_p_s__definition((&((_cc->_current_scope)->_definitions)),_definition);
return 1;
}

static int _add_definition_p_s__compiler_p_s__definition_bo_bo(struct _compiler *_cc, struct _definition *_definition, int _expose, int _local)
{
if(_local)
{
return _add_local_definition_p_s__compiler_p_s__definition(_cc,_definition);
}
else
{
return _add_global_definition_p_s__compiler_p_s__definition_bo(_cc,_definition,_expose);
}
}

static struct _variable *_declare_variable_p_s__compiler_p_s__variable_node_bo_p_s__set_of_directives_bo(struct _compiler *_cc, struct _variable_node *_source, int _expose, struct _set_of_directives *_base_directives, int _local)
{
struct _set_of_directives *_directives_1;
struct _variable *_variable_2;
_directives_1=_get_directives_p_s__compiler_p_s__set_of_directives_p_s__collection_of_directives(_cc,_base_directives,(_source->_directives));
_variable_2=_new_variable_p_s__memory_bundle_p_a_ch_bo_p_s__set_of_directives_p_vo_p_s__node((_cc->_bundle),(_source->_name),_expose,_directives_1,((void *)(_cc->_module)),((struct _node *)_source));
_add_definition_p_s__compiler_p_s__definition_bo_bo(_cc,((struct _definition *)_variable_2),_expose,_local);
return _variable_2;
}

static struct _variable_expression *_new_variable_expression_p_s__memory_bundle_p_s__type_e__expression_code_p_s__variable(struct _memory_bundle *_bundle, struct _type *_type, int _opcode, struct _variable *_value)
{
struct _variable_expression *_e_1;
_e_1=((struct _variable_expression *)_allocate_p_s__memory_bundle_dw(_bundle,((unsigned int)24)));
(((struct _expression *)_e_1)->_type)=_type;
(((struct _expression *)_e_1)->_opcode)=_opcode;
(((struct _expression *)_e_1)->_lvalue)=0;
(_e_1->_value)=_value;
return _e_1;
}

static struct _collection_of_arguments *_new_collection_of_arguments_p_s__compiler(struct _compiler *_cc)
{
struct _collection_of_arguments *_c_1;
_c_1=((struct _collection_of_arguments *)_allocate_p_s__memory_bundle_dw((_cc->_bundle),((unsigned int)8)));
_initialize_p_s__collection(((struct _collection *)_c_1));
return _c_1;
}

static void _set_resolving_p_s__type(struct _type *_t)
{
(_t->_flags)|=2147483648U;
}

static void _set_resolved_p_s__type(struct _type *_t)
{
(_t->_flags)&=2147483647;
(_t->_flags)|=1073741824;
}

static struct _type *_define_redirect_p_s__compiler_p_s__redirect_type(struct _compiler *_cc, struct _redirect_type *_type)
{
int _previous_exposition_1;
struct _redirect_node *_source_2;
if((!(((((struct _type *)_type)->_flags)&1073741824)!=0)))
{
if((((((struct _type *)_type)->_flags)&2147483648U)!=0))
{
_error_p_s__compiler_in_p_a_ch_p_vo(_cc,(((struct _named_type *)_type)->_line),_error_circular_typedefs720,((void *)_type));
return ((struct _type *)0);
}
_set_resolving_p_s__type(((struct _type *)_type));
_previous_exposition_1=(_cc->_current_exposition);
(_cc->_current_exposition)=(((((struct _type *)_type)->_flags)&64)!=0);
_source_2=((struct _redirect_node *)(((struct _named_type *)_type)->_source));
(_type->_real_type)=_get_type_p_s__compiler_p_s__node(_cc,((struct _node *)(((struct _binary_node *)_source_2)->_value_2)));
(_cc->_current_exposition)=_previous_exposition_1;
_set_resolved_p_s__type(((struct _type *)_type));
}
return (_type->_real_type);
}

static unsigned int _size_p_s__type(struct _type *_t)
{
unsigned int _cell_size_1;
int _cells_2;
if(((_t->_id)==18))
{
_cell_size_1=_size_p_s__type((((struct _array_type *)_t)->_array_type));
if((_cell_size_1==-1))
{
return _cell_size_1;
}
_cells_2=(((struct _array_type *)_t)->_array_size);
return (_cells_2*_cell_size_1);
}
else
{
return (_t->_default_size);
}
}

static int _match_p_s__signature_p_s__signature(struct _signature *_s1, struct _signature *_s2)
{
int _n1_1;
int _n2_2;
int _i_3;
_n1_1=(_s1->_number_of_types);
_n2_2=(_s2->_number_of_types);
if((_n1_1!=_n2_2))
{
return 0;
}
_i_3=0;
while((_i_3<_n1_1))
{
if((!_match_p_s__type_p_s__type(_type_p_s__signature_in(_s1,_i_3),_type_p_s__signature_in(_s2,_i_3))))
{
return 0;
}
(++_i_3);
}
return 1;
}

static int _match_p_s__type_p_s__type(struct _type *_t1, struct _type *_t2)
{
int _idt2_1;
int _idt1_2;
struct _structure_type *_t_3;
struct _type *_at1_4;
struct _type *_at2_5;
_idt2_1=(_t2->_id);
if((_idt2_1==1))
{
return 1;
}
_idt1_2=(_t1->_id);
switch(_idt1_2)
{
case 2:
{
return ((_idt2_1==_idt1_2)||(_idt2_1==15));
break;
}
case 14:
{
return ((((void*)_t1==(void*)_t2)||(_idt2_1==12))||(_idt2_1==8));
break;
}
case 15:
{
return ((_idt2_1==_idt1_2)&&_match_p_s__type_p_s__type((((struct _pointer_type *)_t1)->_target_type),(((struct _pointer_type *)_t2)->_target_type)));
break;
}
case 16:
case 17:
{
if((_idt2_1!=_idt1_2))
{
return 0;
}
_t_3=((struct _structure_type *)_t1);
while(((void*)_t_3!=(void*)0))
{
if(_equals_p_s__type_p_s__type(((struct _type *)_t_3),_t2))
{
return 1;
}
_t_3=((struct _structure_type *)(_t_3->_parent_type));
}
return 0;
break;
}
case 18:
{
if((_idt2_1!=_idt1_2))
{
return 0;
}
_at1_4=(((struct _array_type *)_t1)->_array_type);
_at2_5=(((struct _array_type *)_t2)->_array_type);
if((_equals_p_s__type_p_s__type(_at1_4,_at2_5)||((_at2_5->_id)==1)))
{
return 1;
}
return (((_match_p_s__type_p_s__type(_at1_4,_at2_5)&&(!(_size_p_s__type(_at1_4)==-1)))&&(!(_size_p_s__type(_at2_5)==-1)))&&(_size_p_s__type(_at1_4)==_size_p_s__type(_at2_5)));
break;
}
case 19:
{
return (((_idt2_1==_idt1_2)&&_match_p_s__type_p_s__type((((struct _function_type *)_t1)->_returned_type),(((struct _function_type *)_t2)->_returned_type)))&&_match_p_s__signature_p_s__signature((((struct _function_type *)_t1)->_signature),(((struct _function_type *)_t2)->_signature)));
break;
}
case 10:
{
return ((_idt2_1==_idt1_2)||(_idt2_1==6));
break;
}
case 11:
{
return ((_idt2_1==_idt1_2)||(_idt2_1==7));
break;
}
case 12:
{
return ((_idt2_1==_idt1_2)||(_idt2_1==8));
break;
}
case 13:
{
return ((_idt2_1==_idt1_2)||(_idt2_1==9));
break;
}
default:
{
return (_idt1_2==_idt2_1);
break;
}
}
}

static void _error_p_s__compiler_p_s__node_p_a_ch_p_vo_p_vo(struct _compiler *_cc, struct _node *_source, char *_fmt, void *_arg1, void *_arg2)
{
void *(_args_1)[2];
(_args_1[0])=_arg1;
(_args_1[1])=_arg2;
_error_p_s__compiler_p_s__module_in_p_a_ch_p_a_p_vo(_cc,(_cc->_module),(_source->_line),_fmt,((void **)_args_1));
}

static struct _signature *_common_p_s__signature_p_s__signature(struct _signature *_s1, struct _signature *_s2)
{
int _n1_1;
int _n2_2;
int _i_3;
struct _memory_bundle *_bundle_4;
struct _signature *_s_5;
if(_match_p_s__signature_p_s__signature(_s1,_s2))
{
return _s2;
}
if(_match_p_s__signature_p_s__signature(_s2,_s1))
{
return _s1;
}
_n1_1=(_s1->_number_of_types);
_n2_2=(_s2->_number_of_types);
if((_n1_1!=_n2_2))
{
return ((struct _signature *)0);
}
_i_3=0;
while((_i_3<_n1_1))
{
if(((void*)_common_p_s__type_p_s__type(_type_p_s__signature_in(_s1,_i_3),_type_p_s__signature_in(_s2,_i_3))==(void*)0))
{
return ((struct _signature *)0);
}
(++_i_3);
}
if((_n1_1==0))
{
return (&_empty_sig107);
}
_bundle_4=(_type_p_s__signature_in(_s1,0)->_bundle);
_s_5=_new_signature_p_s__memory_bundle_in(_bundle_4,_n1_1);
_i_3=0;
while((_i_3<_n1_1))
{
_set_type_p_s__signature_in_p_s__type(_s_5,_i_3,_common_p_s__type_p_s__type(_type_p_s__signature_in(_s1,_i_3),_type_p_s__signature_in(_s2,_i_3)));
(++_i_3);
}
return _s_5;
}

static int _hash_p_s__signature(struct _signature *_s)
{
struct _type **_p_1;
int _i_2;
int _sum_3;
_p_1=((struct _type **)(((unsigned char *)_s)+((unsigned int)4)));
_i_2=(_s->_number_of_types);
_sum_3=54321;
while((_i_2>0))
{
_sum_3=((_sum_3<<1)+((_p_1[(--_i_2)])->_hash_code));
}
return _sum_3;
}

static struct _function_type *_new_function_type_p_s__memory_bundle_p_s__signature_p_s__type(struct _memory_bundle *_bundle, struct _signature *_signature, struct _type *_returned_type)
{
struct _function_type *_t_1;
_t_1=((struct _function_type *)_new_type_p_s__memory_bundle_dw_e__type_id_in_dw_dw(_bundle,((unsigned int)36),19,0,((unsigned int )-1),((unsigned int )1)));
(((struct _type *)_t_1)->_hash_code)=((_hash_p_s__signature(_signature)<<1)+(_returned_type->_hash_code));
(_t_1->_signature)=_signature;
(_t_1->_returned_type)=_returned_type;
return _t_1;
}

static struct _type *_common_p_s__type_p_s__type(struct _type *_t1, struct _type *_t2)
{
int _idt1_1;
int _idt2_2;
struct _type *_t_3;
struct _structure_type *_t_4;
struct _type *_at1_5;
struct _type *_at2_6;
struct _type *_t_7;
struct _type *_returned_type_8;
struct _signature *_signature_9;
_idt1_1=(_t1->_id);
_idt2_2=(_t2->_id);
if((_idt1_1==1))
{
if((_idt2_2==1))
{
return _t2;
}
else
{
return ((struct _type *)0);
}
}
if((_idt2_2==1))
{
if((_idt1_1==1))
{
return _t1;
}
else
{
return ((struct _type *)0);
}
}
if(_match_p_s__type_p_s__type(_t1,_t2))
{
return _t2;
}
if(_match_p_s__type_p_s__type(_t2,_t1))
{
return _t1;
}
switch(_idt1_1)
{
case 15:
{
if((_idt2_2==2))
{
return _t1;
}
if((_idt2_2!=_idt1_1))
{
return ((struct _type *)0);
}
_t_3=_common_p_s__type_p_s__type((((struct _pointer_type *)_t1)->_target_type),(((struct _pointer_type *)_t2)->_target_type));
if(((void*)_t_3!=(void*)0))
{
_t_3=_pointer_p_s__type(_t_3);
}
return _t_3;
break;
}
case 16:
case 17:
{
if((_idt2_2!=_idt1_1))
{
return ((struct _type *)0);
}
_t_4=((struct _structure_type *)_t2);
while(((void*)_t_4!=(void*)0))
{
if(_match_p_s__type_p_s__type(_t1,((struct _type *)_t_4)))
{
return ((struct _type *)_t_4);
}
_t_4=((struct _structure_type *)(_t_4->_parent_type));
}
return ((struct _type *)0);
break;
}
case 18:
{
if((_idt2_2!=_idt1_1))
{
return ((struct _type *)0);
}
_at1_5=(((struct _array_type *)_t1)->_array_type);
_at2_6=(((struct _array_type *)_t2)->_array_type);
if((_equals_p_s__type_p_s__type(_at1_5,_at2_6)||((_at2_6->_id)==1)))
{
return _t1;
}
_t_7=_common_p_s__type_p_s__type(_at1_5,_at2_6);
if(((((((void*)_t_7!=(void*)0)&&(!(_size_p_s__type(_at1_5)==-1)))&&(!(_size_p_s__type(_at2_6)==-1)))&&(_size_p_s__type(_at1_5)==_size_p_s__type(_at2_6)))&&(_size_p_s__type(_t_7)==_size_p_s__type(_at2_6))))
{
return ((struct _type *)_new_array_type_p_s__memory_bundle_in_p_s__type((_t1->_bundle),0,_t_7));
}
return ((struct _type *)0);
break;
}
case 19:
{
if((_idt2_2!=_idt1_1))
{
return ((struct _type *)0);
}
_returned_type_8=_common_p_s__type_p_s__type((((struct _function_type *)_t1)->_returned_type),(((struct _function_type *)_t2)->_returned_type));
if(((void*)_returned_type_8==(void*)0))
{
return ((struct _type *)0);
}
_signature_9=_common_p_s__signature_p_s__signature((((struct _function_type *)_t1)->_signature),(((struct _function_type *)_t2)->_signature));
if(((void*)_signature_9==(void*)0))
{
return ((struct _type *)0);
}
return ((struct _type *)_new_function_type_p_s__memory_bundle_p_s__signature_p_s__type((_t1->_bundle),_signature_9,_returned_type_8));
break;
}
case 2:
case 14:
case 10:
case 11:
case 12:
case 13:
{
if(_match_p_s__type_p_s__type(_t1,_t2))
{
return _t2;
}
else
{
return ((struct _type *)0);
}
break;
}
case 6:
case 7:
case 8:
case 9:
{
if(_match_p_s__type_p_s__type(_t2,_t1))
{
return _t1;
}
else
{
return ((struct _type *)0);
}
break;
}
}
return ((struct _type *)0);
}

static void _check_return_p_s__compiler_p_s__node_p_s__expression(struct _compiler *_cc, struct _node *_source, struct _expression *_value)
{
struct _type *_expected_type_1;
struct _type *_returned_type_2;
struct _type *_t_3;
if((_cc->_current_inside_equate))
{
return;
}
if(((void*)(_cc->_current_function)==(void*)0))
{
_error_p_s__compiler_p_s__node_p_a_ch(_cc,_source,_error_bad_return731);
return;
}
_expected_type_1=(((struct _definition *)(_cc->_current_function))->_type);
if(((void*)_value!=(void*)0))
{
_returned_type_2=(_value->_type);
if(((void*)_expected_type_1!=(void*)0))
{
if(((_expected_type_1->_id)==1))
{
_error_p_s__compiler_p_s__node_p_a_ch(_cc,_source,_error_return_value735);
return;
}
if((!_match_p_s__type_p_s__type(_returned_type_2,_expected_type_1)))
{
_error_p_s__compiler_p_s__node_p_a_ch_p_vo_p_vo(_cc,_source,_error_bad_return_type748,((void *)_returned_type_2),((void *)_expected_type_1));
return;
}
}
}
else
{
_returned_type_2=((struct _type *)_void_type114);
if((((void*)_expected_type_1!=(void*)0)&&(!((_expected_type_1->_id)==1))))
{
_error_p_s__compiler_p_s__node_p_a_ch(_cc,_source,_error_must_return_value750);
return;
}
}
if(((void*)_expected_type_1==(void*)0))
{
_t_3=(_cc->_current_found_type);
if(((void*)_t_3!=(void*)0))
{
_t_3=_common_p_s__type_p_s__type(_t_3,_returned_type_2);
}
else
{
_t_3=_returned_type_2;
}
if(((void*)_t_3==(void*)0))
{
_error_p_s__compiler_p_s__node_p_a_ch(_cc,_source,_error_incompatible_ret771);
}
else
{
(_cc->_current_found_type)=_t_3;
}
}
}

static struct _expression_expression *_new_expression_expression_p_s__memory_bundle_p_s__type_e__expression_code_p_s__expression(struct _memory_bundle *_bundle, struct _type *_type, int _opcode, struct _expression *_value)
{
struct _expression_expression *_e_1;
_e_1=((struct _expression_expression *)_allocate_p_s__memory_bundle_dw(_bundle,((unsigned int)24)));
(((struct _expression *)_e_1)->_type)=_type;
(((struct _expression *)_e_1)->_opcode)=_opcode;
(((struct _expression *)_e_1)->_lvalue)=0;
(_e_1->_value)=_value;
return _e_1;
}

static struct _expression *_get_condition_p_s__compiler_p_s__node(struct _compiler *_cc, struct _node *_source)
{
struct _expression *_condition_1;
_condition_1=_get_expression_p_s__compiler_p_s__node(_cc,_source);
if(((void*)_condition_1==(void*)0))
{
return ((struct _expression *)0);
}
if((!(((_condition_1->_type)->_id)==3)))
{
_error_p_s__compiler_p_s__node_p_a_ch(_cc,_source,_error_invalid_condition782);
return ((struct _expression *)0);
}
return _condition_1;
}

static struct _scope *_new_scope_p_s__scope(struct _scope *_n)
{
struct _scope *_scope_1;
_scope_1=((struct _scope *)_allocate_memory_dw(((unsigned int)44)));
_initialize_p_s__scope_p_s__scope(_scope_1,_n);
return _scope_1;
}

static void _open_scope_p_s__compiler_p_s__scope(struct _compiler *_cc, struct _scope *_parent_scope)
{
(_cc->_current_scope)=_new_scope_p_s__scope(_parent_scope);
}

static struct _collection_of_expressions *_new_collection_of_expressions_p_s__compiler(struct _compiler *_cc)
{
struct _collection_of_expressions *_c_1;
_c_1=((struct _collection_of_expressions *)_allocate_p_s__memory_bundle_dw((_cc->_bundle),((unsigned int)8)));
_initialize_p_s__collection(((struct _collection *)_c_1));
return _c_1;
}

static struct _collection_of_expressions *_get_list_of_expressions_p_s__compiler_p_s__collection(struct _compiler *_cc, struct _collection *_source)
{
int _ok_1;
struct _collection_of_expressions *_expressions_2;
struct _element *_e_3;
struct _expression *_e_4;
_ok_1=1;
_expressions_2=_new_collection_of_expressions_p_s__compiler(_cc);
_e_3=(_source->_first);
while(((void*)_e_3!=(void*)0))
{
_e_4=_get_expression_p_s__compiler_p_s__node(_cc,((struct _node *)_e_3));
if(((void*)_e_4==(void*)0))
{
_ok_1=0;
}
else
{
_append_p_s__collection_p_s__element(((struct _collection *)_expressions_2),((struct _element *)_e_4));
}
_e_3=(_e_3->_next_sibling);
}
return (_ok_1?_expressions_2:((struct _collection_of_expressions *)0));
}

static void _release_p_s__definition_head(struct _definition_head *_head)
{
struct _definition_item *_item_1;
struct _definition_item *_next_2;
_item_1=(_head->_first);
while(((void*)_item_1!=(void*)0))
{
_next_2=(_item_1->_next);
_free_memory_p_vo(((void *)_item_1));
_item_1=_next_2;
}
}

static void _release_p_s__dictionary(struct _dictionary *_d)
{
_free_memory_p_vo(((void *)(_d->_associations)));
}

static void _release_p_s__definition_dictionary(struct _definition_dictionary *_d)
{
unsigned int _i_1;
unsigned int _size_2;
void *_v_3;
_i_1=((unsigned int)0);
_size_2=(((struct _dictionary *)_d)->_allocated);
while((_i_1<_size_2))
{
if(((void*)((&((((struct _dictionary *)_d)->_associations)[_i_1]))->_key)!=(void*)0))
{
_v_3=((&((((struct _dictionary *)_d)->_associations)[_i_1]))->_value);
_release_p_s__definition_head(((struct _definition_head *)_v_3));
_free_memory_p_vo(_v_3);
}
(++_i_1);
}
_release_p_s__dictionary(((struct _dictionary *)_d));
}

static void _release_p_s__scope(struct _scope *_s)
{
_release_p_s__definition_dictionary((&(_s->_definitions)));
_release_p_s__dictionary(((struct _dictionary *)(&(_s->_types))));
if(((void*)(_s->_next)!=(void*)0))
{
_delete_p_s__scope((_s->_next));
}
}

static void _delete_p_s__scope(struct _scope *_s)
{
_release_p_s__scope(_s);
_free_memory_p_vo(((void *)_s));
}

static void _close_scope_p_s__compiler(struct _compiler *_cc)
{
struct _scope *_scope_1;
_scope_1=(_cc->_current_scope);
(_cc->_current_scope)=(_scope_1->_next);
(_scope_1->_next)=((struct _scope *)0);
_delete_p_s__scope(_scope_1);
}

static struct _collection_of_expressions *_get_block_p_s__compiler_p_s__collection(struct _compiler *_cc, struct _collection *_source)
{
struct _collection_of_expressions *_block_1;
_open_scope_p_s__compiler_p_s__scope(_cc,(_cc->_current_scope));
_block_1=_get_list_of_expressions_p_s__compiler_p_s__collection(_cc,_source);
_close_scope_p_s__compiler(_cc);
return _block_1;
}

static struct _condition_item *_new_condition_p_s__memory_bundle_p_s__expression_p_s__collection(struct _memory_bundle *_bundle, struct _expression *_condition, struct _collection *_block)
{
struct _condition_item *_c_1;
_c_1=((struct _condition_item *)_allocate_p_s__memory_bundle_dw(_bundle,((unsigned int)12)));
(_c_1->_condition)=_condition;
(_c_1->_block)=((struct _expression *)(_block->_first));
return _c_1;
}

static struct _condition_expression *_new_condition_expression_p_s__memory_bundle_p_s__condition_item(struct _memory_bundle *_bundle, struct _condition_item *_value)
{
struct _condition_expression *_e_1;
_e_1=((struct _condition_expression *)_new_expression_p_s__memory_bundle_dw_p_s__type_e__expression_code(_bundle,((unsigned int)24),((struct _type *)_void_type114),15));
(_e_1->_value)=_value;
return _e_1;
}

static struct _loop_expression *_new_loop_expression_p_s__memory_bundle_p_s__expression_p_s__collection_of_expressions(struct _memory_bundle *_bundle, struct _expression *_condition, struct _collection_of_expressions *_block)
{
struct _loop_expression *_e_1;
_e_1=((struct _loop_expression *)_new_expression_p_s__memory_bundle_dw_p_s__type_e__expression_code(_bundle,((unsigned int)28),((struct _type *)_void_type114),12));
(_e_1->_condition)=_condition;
(_e_1->_block)=((struct _expression *)(((struct _collection *)_block)->_first));
return _e_1;
}

static struct _rept_expression *_new_repeat_expression_p_s__memory_bundle_p_s__collection_of_expressions(struct _memory_bundle *_bundle, struct _collection_of_expressions *_block)
{
struct _rept_expression *_e_1;
_e_1=((struct _rept_expression *)_new_expression_p_s__memory_bundle_dw_p_s__type_e__expression_code(_bundle,((unsigned int)24),((struct _type *)_void_type114),13));
(_e_1->_block)=((struct _expression *)(((struct _collection *)_block)->_first));
return _e_1;
}

static int _get_constant_integer_p_s__compiler_p_s__node_p_s__expression(struct _compiler *_cc, struct _node *_source, struct _expression *_e)
{
if((!((((_e->_type)->_flags)&8)!=0)))
{
_error_p_s__compiler_p_s__node_p_a_ch(_cc,_source,_error_not_integer819);
return 0;
}
if((!((_e->_opcode)==1)))
{
_error_p_s__compiler_p_s__node_p_a_ch(_cc,_source,_error_not_constant821);
return 0;
}
return (((struct _long_expression *)_e)->_value);
}

static char _get_constant_char_p_s__compiler_p_s__node_p_s__expression(struct _compiler *_cc, struct _node *_source, struct _expression *_e)
{
if((!(((_e->_type)->_id)==4)))
{
_error_p_s__compiler_p_s__node_p_a_ch(_cc,_source,_error_not_char823);
return '\x00';
}
if((!((_e->_opcode)==1)))
{
_error_p_s__compiler_p_s__node_p_a_ch(_cc,_source,_error_not_constant821);
return '\x00';
}
return ((char )(((struct _long_expression *)_e)->_value));
}

static short _get_constant_wchar_p_s__compiler_p_s__node_p_s__expression(struct _compiler *_cc, struct _node *_source, struct _expression *_e)
{
if((!(((_e->_type)->_id)==5)))
{
_error_p_s__compiler_p_s__node_p_a_ch(_cc,_source,_error_not_wchar825);
return ((short)0);
}
if((!((_e->_opcode)==1)))
{
_error_p_s__compiler_p_s__node_p_a_ch(_cc,_source,_error_not_constant821);
return ((short)0);
}
return ((short )(((struct _long_expression *)_e)->_value));
}

static struct _constant *_new_constant_p_s__memory_bundle_in(struct _memory_bundle *_bundle, int _value)
{
struct _constant *_c_1;
_c_1=((struct _constant *)_allocate_p_s__memory_bundle_dw(_bundle,((unsigned int)8)));
(_c_1->_value)=_value;
return _c_1;
}

static struct _aswitch_item *_new_switch_item_p_s__memory_bundle_p_s__constant_p_s__collection(struct _memory_bundle *_bundle, struct _constant *_constants, struct _collection *_block)
{
struct _aswitch_item *_c_1;
_c_1=((struct _aswitch_item *)_allocate_p_s__memory_bundle_dw(_bundle,((unsigned int)12)));
(_c_1->_constants)=_constants;
(_c_1->_block)=((struct _expression *)(_block->_first));
return _c_1;
}

static struct _aswitch_item *_new_switch_item_p_s__memory_bundle_p_s__collection_p_s__collection(struct _memory_bundle *_bundle, struct _collection *_constants, struct _collection *_block)
{
return _new_switch_item_p_s__memory_bundle_p_s__constant_p_s__collection(_bundle,((struct _constant *)(_constants->_first)),_block);
}

static struct _aswitch_item *_new_switch_item_p_s__memory_bundle_p_s__collection(struct _memory_bundle *_bundle, struct _collection *_block)
{
struct _aswitch_item *_c_1;
_c_1=((struct _aswitch_item *)_allocate_p_s__memory_bundle_dw(_bundle,((unsigned int)12)));
(_c_1->_constants)=((struct _constant *)0);
(_c_1->_block)=((struct _expression *)(_block->_first));
return _c_1;
}

static struct _aswitch_expression *_new_switch_expression_p_s__memory_bundle_p_s__expression_p_s__collection(struct _memory_bundle *_bundle, struct _expression *_expression, struct _collection *_rules)
{
struct _aswitch_expression *_e_1;
_e_1=((struct _aswitch_expression *)_new_expression_p_s__memory_bundle_dw_p_s__type_e__expression_code(_bundle,((unsigned int)28),((struct _type *)_void_type114),14));
(_e_1->_expression)=_expression;
(_e_1->_rules)=((struct _aswitch_item *)(_rules->_first));
return _e_1;
}

static int _is_reference_p_s__expression(struct _expression *_e)
{
return ((((_e->_opcode)==3)&&(((((struct _definition_expression *)_e)->_definition)->_opcode)==1))&&((void*)(((struct _variable *)(((struct _definition_expression *)_e)->_definition))->_reference)!=(void*)0));
}

static struct _type *_effective_type_p_s__type(struct _type *_t)
{
if(((((_t->_id)==16)||((_t->_id)==17))||((_t->_id)==18)))
{
return _pointer_p_s__type(_t);
}
return _t;
}

static int _check_size_p_s__compiler_p_s__node_p_s__type(struct _compiler *_cc, struct _node *_source, struct _type *_type)
{
if((_size_p_s__type(_type)==-1))
{
_error_p_s__compiler_p_s__node_p_a_ch_p_vo(_cc,_source,_error_type_undefined845,((void *)_type));
return 0;
}
return 1;
}

static int _do_define_variable_p_s__compiler_p_s__variable(struct _compiler *_cc, struct _variable *_variable)
{
struct _variable_node *_source_1;
struct _expression *_initial_value_2;
struct _expression *_reference_3;
struct _expression *_proxy_4;
struct _type *_type_5;
struct _expression *_seq_6;
_source_1=((struct _variable_node *)(((struct _definition *)_variable)->_source));
_initial_value_2=((struct _expression *)0);
_reference_3=((struct _expression *)0);
_proxy_4=((struct _expression *)0);
if(((void*)(_source_1->_type)!=(void*)0))
{
_type_5=_get_type_p_s__compiler_p_s__node(_cc,(_source_1->_type));
if(((void*)_type_5==(void*)0))
{
return 0;
}
}
else if(((void*)(_source_1->_initial_value)!=(void*)0))
{
_initial_value_2=_get_expression_p_s__compiler_p_s__node(_cc,(_source_1->_initial_value));
if(((void*)_initial_value_2==(void*)0))
{
return 0;
}
_type_5=(_initial_value_2->_type);
}
else
{
_reference_3=_get_expression_p_s__compiler_p_s__node(_cc,(_source_1->_reference));
if(((void*)_reference_3==(void*)0))
{
return 0;
}
if(((_reference_3->_opcode)==7))
{
_seq_6=(((struct _sequence_expression *)_reference_3)->_sequence);
_proxy_4=_seq_6;
_reference_3=((struct _expression *)(((struct _element *)_proxy_4)->_next_sibling));
_type_5=(_reference_3->_type);
}
else if((((((_reference_3->_opcode)==5)||((_reference_3->_opcode)==2))||((_reference_3->_opcode)==4))||_is_reference_p_s__expression(_reference_3)))
{
_type_5=(_reference_3->_type);
}
else
{
_error_p_s__compiler_p_s__node_p_a_ch(_cc,(_source_1->_reference),_error_must_be_initializer843);
return 0;
}
}
(((struct _definition *)_variable)->_type)=_effective_type_p_s__type(_type_5);
(_variable->_storage_type)=_type_5;
(((struct _definition *)_variable)->_lvalue)=(((_type_5->_flags)&32)!=0);
if((!(_source_1->_is_import)))
{
_check_size_p_s__compiler_p_s__node_p_s__type(_cc,((struct _node *)_source_1),_type_5);
(_variable->_initial_value)=_initial_value_2;
(_variable->_proxy)=_proxy_4;
(_variable->_reference)=_reference_3;
(_variable->_defined)=1;
}
return 1;
}

static int _define_variable_p_s__compiler_p_s__variable(struct _compiler *_cc, struct _variable *_variable)
{
int _status_1;
int _ok_2;
_status_1=(((struct _definition *)_variable)->_resolve_status);
if((_status_1==2))
{
return 1;
}
else if((_status_1==3))
{
return 0;
}
else if((_status_1==1))
{
_error_p_s__compiler_in_p_a_ch_p_vo(_cc,(((struct _definition *)_variable)->_line),_error_circular_dependency_variable835,((void *)(((struct _definition *)_variable)->_name)));
return 0;
}
(((struct _definition *)_variable)->_resolve_status)=1;
_ok_2=_do_define_variable_p_s__compiler_p_s__variable(_cc,_variable);
(((struct _definition *)_variable)->_resolve_status)=(_ok_2?2:3);
return _ok_2;
}

static struct _operator *_find_operator_p_a_ch(char *_name)
{
struct _operator *_p_1;
_p_1=_operators33;
while(((void*)((&(_p_1[0]))->_op)!=(void*)0))
{
if(_is_equal_p_a_ch_p_a_ch(((&(_p_1[0]))->_op),_name))
{
return (&(_p_1[0]));
}
(++_p_1);
}
return ((struct _operator *)0);
}

static int _size_p_s__collection(struct _collection *_c)
{
int _size_1;
struct _element *_e_2;
_size_1=0;
_e_2=(_c->_first);
while(((void*)_e_2!=(void*)0))
{
_e_2=(_e_2->_next_sibling);
(++_size_1);
}
return _size_1;
}

static int _check_arguments_p_s__compiler_p_s__node_p_a_ch_p_s__collection_of_arguments(struct _compiler *_cc, struct _node *_source, char *_name, struct _collection_of_arguments *_arguments)
{
struct _operator *_p_1;
int _size_2;
if(((_name[0])=='@'))
{
_p_1=_find_operator_p_a_ch(_name);
if(((void*)_p_1!=(void*)0))
{
_size_2=_size_p_s__collection(((struct _collection *)_arguments));
if((_size_2<(_p_1->_min_arg)))
{
_error_p_s__compiler_p_s__node_p_a_ch_p_vo(_cc,_source,_error_too_few_args859,((void *)_name));
return 0;
}
if((((_p_1->_max_arg)!=0)&&(_size_2>(_p_1->_max_arg))))
{
_error_p_s__compiler_p_s__node_p_a_ch_p_vo(_cc,_source,_error_too_many_args861,((void *)_name));
return 0;
}
}
else
{
_error_p_s__compiler_p_s__node_p_a_ch_p_vo(_cc,_source,_error_unknown_operator863,((void *)_name));
return 0;
}
}
return 1;
}

static struct _equate *_new_equate_p_s__memory_bundle_p_a_ch_p_s__signature_p_s__collection_of_arguments_bo_p_vo_p_s__equate_node(struct _memory_bundle *_bundle, char *_name, struct _signature *_signature, struct _collection_of_arguments *_arguments, int _expose, void *_module, struct _equate_node *_source)
{
struct _equate *_equate_1;
_equate_1=((struct _equate *)_allocate_p_s__memory_bundle_dw(_bundle,((unsigned int)56)));
_initialize_p_s__definition_p_a_ch_p_s__signature_p_s__type_e__definition_code_bo_bo_p_vo_p_s__node(((struct _definition *)_equate_1),_name,_signature,((struct _type *)0),11,0,_expose,_module,((struct _node *)_source));
(_equate_1->_value)=((struct _expression *)0);
(_equate_1->_arguments)=((struct _argument *)(((struct _collection *)_arguments)->_first));
return _equate_1;
}

static struct _signature *_new_signature_from_p_s__compiler_p_s__collection_of_arguments(struct _compiler *_cc, struct _collection_of_arguments *_arguments)
{
int _size_1;
struct _signature *_signature_2;
int _i_3;
struct _element *_e_4;
_size_1=_size_p_s__collection(((struct _collection *)_arguments));
_signature_2=_new_signature_p_s__memory_bundle_in((_cc->_bundle),_size_1);
_i_3=0;
_e_4=(((struct _collection *)_arguments)->_first);
while(((void*)_e_4!=(void*)0))
{
_set_type_p_s__signature_in_p_s__type(_signature_2,_i_3,(((struct _definition *)((struct _argument *)_e_4))->_type));
(++_i_3);
_e_4=(_e_4->_next_sibling);
}
return _signature_2;
}

static struct _argument *_new_argument_p_s__memory_bundle_p_a_ch_p_s__type_bo_p_vo_p_s__node(struct _memory_bundle *_bundle, char *_name, struct _type *_type, int _is_return, void *_module, struct _node *_source)
{
struct _argument *_a_1;
_a_1=((struct _argument *)_allocate_p_s__memory_bundle_dw(_bundle,((unsigned int)56)));
_initialize_p_s__definition_p_a_ch_p_s__signature_p_s__type_e__definition_code_bo_bo_p_vo_p_s__node(((struct _definition *)_a_1),_name,(&_empty_sig107),_type,0,1,0,_module,_source);
(_a_1->_is_return)=_is_return;
(_a_1->_substitute)=((struct _expression *)0);
return _a_1;
}

static struct _extension *_new_extension_p_s__memory_bundle_p_s__signature_p_s__type_p_s__definition_p_s__collection_of_arguments(struct _memory_bundle *_bundle, struct _signature *_signature, struct _type *_returned_type, struct _definition *_definition, struct _collection_of_arguments *_arguments)
{
struct _extension *_f_1;
_f_1=((struct _extension *)_allocate_p_s__memory_bundle_dw(_bundle,((unsigned int)56)));
_initialize_p_s__definition_p_a_ch_p_s__signature_p_s__type_e__definition_code_bo_bo_p_vo_p_s__node(((struct _definition *)_f_1),(_definition->_name),_signature,_returned_type,15,0,(_definition->_expose),(_definition->_module),(_definition->_source));
(_f_1->_definition)=_definition;
(_f_1->_arguments)=((struct _argument *)(((struct _collection *)_arguments)->_first));
return _f_1;
}

static void _declare_extension_p_s__compiler_p_s__definition_p_s__collection_of_arguments_bo(struct _compiler *_cc, struct _definition *_source_definition, struct _collection_of_arguments *_source_arguments, int _local)
{
struct _collection_of_arguments *_arguments_1;
struct _element *_e_2;
struct _argument *_argument_3;
struct _type *_returned_type_4;
struct _extension *_extension_5;
_arguments_1=_new_collection_of_arguments_p_s__compiler(_cc);
_e_2=(((struct _collection *)_source_arguments)->_first);
while(((void*)_e_2!=(void*)0))
{
if((!(((struct _argument *)_e_2)->_is_return)))
{
_argument_3=_new_argument_p_s__memory_bundle_p_a_ch_p_s__type_bo_p_vo_p_s__node((_cc->_bundle),(((struct _definition *)((struct _argument *)_e_2))->_name),(((struct _definition *)((struct _argument *)_e_2))->_type),(((struct _argument *)_e_2)->_is_return),((void *)(_cc->_module)),(((struct _definition *)((struct _argument *)_e_2))->_source));
_append_p_s__collection_p_s__element(((struct _collection *)_arguments_1),((struct _element *)_argument_3));
}
else
{
_returned_type_4=(((struct _definition *)((struct _argument *)_e_2))->_type);
}
_e_2=(_e_2->_next_sibling);
}
_extension_5=_new_extension_p_s__memory_bundle_p_s__signature_p_s__type_p_s__definition_p_s__collection_of_arguments((_cc->_bundle),_new_signature_from_p_s__compiler_p_s__collection_of_arguments(_cc,_arguments_1),_returned_type_4,_source_definition,_source_arguments);
_add_definition_p_s__compiler_p_s__definition_bo_bo(_cc,((struct _definition *)_extension_5),(((struct _definition *)_extension_5)->_expose),_local);
}

static struct _equate *_declare_equate_p_s__compiler_p_s__equate_node_bo_bo(struct _compiler *_cc, struct _equate_node *_source, int _expose, int _local)
{
struct _collection *_source_arguments_1;
int (_returned_arguments_2)[1];
struct _collection_of_arguments *_arguments_3;
char *_name_4;
struct _equate *_equate_5;
_source_arguments_1=(_source->_arguments);
_arguments_3=_get_arguments_p_s__compiler_p_s__collection_p_a_in(_cc,_source_arguments_1,((int *)_returned_arguments_2));
if(((void*)_arguments_3==(void*)0))
{
return ((struct _equate *)0);
}
if(((_returned_arguments_2[0])>1))
{
_error_p_s__compiler_p_s__node_p_a_ch(_cc,((struct _node *)_source),_error_too_many_returns851);
return ((struct _equate *)0);
}
_name_4=(_source->_name);
if((!_check_arguments_p_s__compiler_p_s__node_p_a_ch_p_s__collection_of_arguments(_cc,((struct _node *)_source),_name_4,_arguments_3)))
{
return ((struct _equate *)0);
}
_equate_5=_new_equate_p_s__memory_bundle_p_a_ch_p_s__signature_p_s__collection_of_arguments_bo_p_vo_p_s__equate_node((_cc->_bundle),_name_4,_new_signature_from_p_s__compiler_p_s__collection_of_arguments(_cc,_arguments_3),_arguments_3,_expose,((void *)(_cc->_module)),_source);
_add_definition_p_s__compiler_p_s__definition_bo_bo(_cc,((struct _definition *)_equate_5),_expose,_local);
if(((_returned_arguments_2[0])==1))
{
_declare_extension_p_s__compiler_p_s__definition_p_s__collection_of_arguments_bo(_cc,((struct _definition *)_equate_5),_arguments_3,0);
}
return _equate_5;
}

static struct _expression *_new_ignore_expression_p_s__memory_bundle(struct _memory_bundle *_bundle)
{
struct _expression *_e_1;
_e_1=((struct _expression *)_allocate_p_s__memory_bundle_dw(_bundle,((unsigned int)20)));
(_e_1->_type)=((struct _type *)_void_type114);
(_e_1->_opcode)=19;
(_e_1->_lvalue)=0;
return _e_1;
}

static struct _iterator *_new_iterator_p_s__memory_bundle_p_a_ch_p_s__signature_p_s__collection_of_arguments_bo_p_vo_p_s__iterator_node(struct _memory_bundle *_bundle, char *_name, struct _signature *_signature, struct _collection_of_arguments *_arguments, int _expose, void *_module, struct _iterator_node *_source)
{
struct _iterator *_f_1;
_f_1=((struct _iterator *)_allocate_p_s__memory_bundle_dw(_bundle,((unsigned int)68)));
_initialize_p_s__definition_p_a_ch_p_s__signature_p_s__type_e__definition_code_bo_bo_p_vo_p_s__node(((struct _definition *)_f_1),_name,_signature,((struct _type *)0),4,0,_expose,_module,((struct _node *)_source));
(_f_1->_instructions)=((struct _expression *)0);
(_f_1->_arguments)=((struct _argument *)(((struct _collection *)_arguments)->_first));
(_f_1->_yield_signature)=((struct _signature *)0);
(_f_1->_block)=((struct _expression *)0);
(_f_1->_parameters)=((struct _collection_of_arguments *)0);
return _f_1;
}

static struct _iterator *_declare_iterator_p_s__compiler_p_s__iterator_node_bo_bo(struct _compiler *_cc, struct _iterator_node *_source, int _expose, int _local)
{
struct _collection *_source_arguments_1;
int (_returned_arguments_2)[1];
struct _collection_of_arguments *_arguments_3;
char *_name_4;
struct _iterator *_iterator_5;
_source_arguments_1=(_source->_arguments);
_arguments_3=_get_arguments_p_s__compiler_p_s__collection_p_a_in(_cc,_source_arguments_1,((int *)_returned_arguments_2));
if(((void*)_arguments_3==(void*)0))
{
return ((struct _iterator *)0);
}
if(((_returned_arguments_2[0])!=0))
{
_error_p_s__compiler_p_s__node_p_a_ch(_cc,((struct _node *)_source),_error_no_return_arg_in_iterator883);
return ((struct _iterator *)0);
}
_name_4=(_source->_name);
if((!_check_arguments_p_s__compiler_p_s__node_p_a_ch_p_s__collection_of_arguments(_cc,((struct _node *)_source),_name_4,_arguments_3)))
{
return ((struct _iterator *)0);
}
_iterator_5=_new_iterator_p_s__memory_bundle_p_a_ch_p_s__signature_p_s__collection_of_arguments_bo_p_vo_p_s__iterator_node((_cc->_bundle),_name_4,_new_signature_from_p_s__compiler_p_s__collection_of_arguments(_cc,_arguments_3),_arguments_3,_expose,((void *)(_cc->_module)),_source);
_add_definition_p_s__compiler_p_s__definition_bo_bo(_cc,((struct _definition *)_iterator_5),_expose,_local);
return _iterator_5;
}

static int _add_local_type_p_s__compiler_p_s__named_type(struct _compiler *_cc, struct _named_type *_type)
{
char *_name_1;
struct _named_type *_previous_2;
_name_1=(_type->_name);
_previous_2=_find_type_p_s__compiler_p_a_ch(_cc,_name_1);
if(((void*)_previous_2!=(void*)0))
{
_error_already_defined_p_s__compiler_p_a_ch_in_p_s__named_type(_cc,_name_1,(_type->_line),_previous_2);
return 0;
}
_add_p_s__dictionary_p_vo_p_vo(((struct _dictionary *)(&((_cc->_current_scope)->_types))),((void *)_name_1),((void *)_type));
return 1;
}

static struct _long_expression *_new_litteral_long_expression_p_s__memory_bundle_p_s__type_dw(struct _memory_bundle *_bundle, struct _type *_type, unsigned int _value)
{
struct _long_expression *_e_1;
_e_1=((struct _long_expression *)_allocate_p_s__memory_bundle_dw(_bundle,((unsigned int)24)));
(((struct _expression *)_e_1)->_type)=_type;
(((struct _expression *)_e_1)->_opcode)=1;
(((struct _expression *)_e_1)->_lvalue)=0;
(_e_1->_value)=((int )_value);
return _e_1;
}

static struct _string_expression *_new_string_expression_p_s__memory_bundle_p_s__type_e__expression_code_p_a_ch(struct _memory_bundle *_bundle, struct _type *_type, int _opcode, char *_value)
{
struct _string_expression *_e_1;
_e_1=((struct _string_expression *)_new_expression_p_s__memory_bundle_dw_p_s__type_e__expression_code(_bundle,((unsigned int)24),_type,_opcode));
(_e_1->_value)=_value;
return _e_1;
}

static struct _string_expression *_new_litteral_string_expression_p_s__memory_bundle_p_s__type_p_a_ch(struct _memory_bundle *_bundle, struct _type *_type, char *_value)
{
return _new_string_expression_p_s__memory_bundle_p_s__type_e__expression_code_p_a_ch(_bundle,_type,2,_value);
}

static int _match_p_s__collection_of_expressions_p_s__definition(struct _collection_of_expressions *_arguments, struct _definition *_definition)
{
struct _signature *_s_1;
int _i_2;
struct _element *_e_3;
struct _type *_t1_4;
struct _type *_t2_5;
_s_1=(_definition->_signature);
_i_2=0;
_e_3=(((struct _collection *)_arguments)->_first);
while(((void*)_e_3!=(void*)0))
{
_t1_4=(((struct _expression *)_e_3)->_type);
_t2_5=_type_p_s__signature_in(_s_1,_i_2);
if((!_match_p_s__type_p_s__type(_t1_4,_t2_5)))
{
return 0;
}
(++_i_2);
_e_3=(_e_3->_next_sibling);
}
return 1;
}

static int _match_p_s__definition_p_s__definition(struct _definition *_d1, struct _definition *_d2)
{
return _match_p_s__signature_p_s__signature((_d1->_signature),(_d2->_signature));
}

static struct _definition *_find_definition_p_s__compiler_p_s__node_p_a_ch_p_s__collection_of_expressions(struct _compiler *_cc, struct _node *_source, char *_name, struct _collection_of_expressions *_arguments)
{
struct _signature _signature_1;
struct _definition *_best_2;
struct _scope *_scope_3;
struct _definition_item *_item_4;
struct _definition *_definition_5;
((&_signature_1)->_number_of_types)=_size_p_s__collection(((struct _collection *)_arguments));
_best_2=((struct _definition *)0);
_scope_3=(_cc->_current_scope);
while(((void*)_scope_3!=(void*)0))
{
_item_4=_first_definition_p_s__definition_dictionary_p_a_ch_p_s__signature((&(_scope_3->_definitions)),_name,(&_signature_1));
while(((void*)_item_4!=(void*)0))
{
_definition_5=(_item_4->_value);
if(_match_p_s__collection_of_expressions_p_s__definition(_arguments,_definition_5))
{
if((((void*)_best_2==(void*)0)||_match_p_s__definition_p_s__definition(_definition_5,_best_2)))
{
_best_2=_definition_5;
}
else if((!_match_p_s__definition_p_s__definition(_best_2,_definition_5)))
{
_error_p_s__compiler_p_s__node_p_a_ch_p_vo_p_vo(_cc,_source,_error_ambigous907,((void *)_name),((void *)_arguments));
}
}
_item_4=(_item_4->_next);
}
_scope_3=(_scope_3->_next);
}
return _best_2;
}

static struct _definition_expression *_new_definition_expression_p_s__memory_bundle_p_s__definition_p_s__collection_of_expressions(struct _memory_bundle *_bundle, struct _definition *_definition, struct _collection_of_expressions *_arguments)
{
struct _definition_expression *_e_1;
_e_1=((struct _definition_expression *)_allocate_p_s__memory_bundle_dw(_bundle,((unsigned int)28)));
(((struct _expression *)_e_1)->_type)=(_definition->_type);
(((struct _expression *)_e_1)->_opcode)=3;
(((struct _expression *)_e_1)->_lvalue)=(_definition->_lvalue);
(_e_1->_definition)=_definition;
(_e_1->_arguments)=((struct _expression *)(((struct _collection *)_arguments)->_first));
return _e_1;
}

static struct _type *_check_array_p_s__compiler_p_s__node_p_s__type(struct _compiler *_cc, struct _node *_source, struct _type *_type)
{
struct _type *_array_1;
struct _type *_cell_type_2;
if(((_type->_id)==2))
{
_error_p_s__compiler_p_s__node_p_a_ch_p_vo(_cc,_source,_error_unknown_size926,((void *)_type));
return ((struct _type *)0);
}
_array_1=(((struct _pointer_type *)_type)->_target_type);
_cell_type_2=(((struct _array_type *)_array_1)->_array_type);
if((_size_p_s__type(_cell_type_2)==-1))
{
_error_p_s__compiler_p_s__node_p_a_ch_p_vo(_cc,_source,_error_unknown_size926,((void *)_cell_type_2));
return ((struct _type *)0);
}
return _cell_type_2;
}

static int _add_local_definitions_p_s__compiler_p_s__argument(struct _compiler *_cc, struct _argument *_arguments)
{
int _ok_1;
struct _argument *_a_2;
_ok_1=1;
_a_2=_arguments;
while(((void*)_a_2!=(void*)0))
{
_ok_1&=_add_local_definition_p_s__compiler_p_s__definition(_cc,((struct _definition *)_a_2));
_a_2=((struct _argument *)(((struct _element *)_a_2)->_next_sibling));
}
return _ok_1;
}

static struct _collection_of_expressions *_get_sequence_of_instructions_p_s__compiler_p_s__collection(struct _compiler *_cc, struct _collection *_source)
{
struct _collection_of_expressions *_previous_block_1;
struct _collection_of_expressions *_block_2;
int _ok_3;
struct _element *_e_4;
struct _expression *_e_5;
_previous_block_1=(_cc->_current_block);
_block_2=_new_collection_of_expressions_p_s__compiler(_cc);
(_cc->_current_block)=_block_2;
_ok_3=1;
_e_4=(_source->_first);
while(((void*)_e_4!=(void*)0))
{
_e_5=_get_expression_p_s__compiler_p_s__node(_cc,((struct _node *)_e_4));
if(((void*)_e_5!=(void*)0))
{
_append_p_s__collection_p_s__element(((struct _collection *)_block_2),((struct _element *)_e_5));
}
else
{
_ok_3=0;
}
_e_4=(_e_4->_next_sibling);
}
(_cc->_current_block)=_previous_block_1;
return (_ok_3?_block_2:((struct _collection_of_expressions *)0));
}

static int _do_define_iterator_p_s__compiler_p_s__iterator(struct _compiler *_cc, struct _iterator *_iterator)
{
struct _iterator_node *_source_1;
struct _collection *_source_body_2;
struct _collection_of_expressions *_body_3;
_source_1=((struct _iterator_node *)(((struct _definition *)_iterator)->_source));
_source_body_2=(_source_1->_body);
_add_local_definitions_p_s__compiler_p_s__argument(_cc,(_iterator->_arguments));
_body_3=_get_sequence_of_instructions_p_s__compiler_p_s__collection(_cc,_source_body_2);
if(((void*)_body_3==(void*)0))
{
return 0;
}
(_iterator->_instructions)=((struct _expression *)(((struct _collection *)_body_3)->_first));
return 1;
}

static int _define_iterator_p_s__compiler_p_s__iterator(struct _compiler *_cc, struct _iterator *_iterator)
{
int _status_1;
int _previous_exposition_2;
struct _iterator *_previous_iterator_3;
struct _function *_previous_function_4;
struct _type *_previous_found_type_5;
struct _scope *_previous_scope_6;
int _previous_inside_equate_7;
int _ok_8;
_status_1=(((struct _definition *)_iterator)->_resolve_status);
if((_status_1==2))
{
return 1;
}
else if((_status_1==3))
{
return 0;
}
else if((_status_1==1))
{
_error_p_s__compiler_in_p_a_ch_p_vo(_cc,(((struct _definition *)_iterator)->_line),_error_circular_dependency_equate938,((void *)(((struct _definition *)_iterator)->_name)));
return 0;
}
(((struct _definition *)_iterator)->_resolve_status)=1;
_previous_exposition_2=(_cc->_current_exposition);
(_cc->_current_exposition)=(((struct _definition *)_iterator)->_expose);
_previous_iterator_3=(_cc->_current_iterator);
(_cc->_current_iterator)=_iterator;
_previous_function_4=(_cc->_current_function);
_previous_found_type_5=(_cc->_current_found_type);
_previous_scope_6=(_cc->_current_scope);
_previous_inside_equate_7=(_cc->_current_inside_equate);
(_cc->_current_function)=((struct _function *)0);
(_cc->_current_found_type)=((struct _type *)0);
(_cc->_current_inside_equate)=1;
_open_scope_p_s__compiler_p_s__scope(_cc,((((struct _iterator_node *)(((struct _definition *)_iterator)->_source))->_global)?(&(_cc->_global_scope)):(_cc->_current_scope)));
_ok_8=_do_define_iterator_p_s__compiler_p_s__iterator(_cc,_iterator);
_close_scope_p_s__compiler(_cc);
(_cc->_current_function)=_previous_function_4;
(_cc->_current_found_type)=_previous_found_type_5;
(_cc->_current_scope)=_previous_scope_6;
(_cc->_current_inside_equate)=_previous_inside_equate_7;
(_cc->_current_iterator)=_previous_iterator_3;
(_cc->_current_exposition)=_previous_exposition_2;
(((struct _definition *)_iterator)->_resolve_status)=(_ok_8?2:3);
return _ok_8;
}

static struct _collection_of_arguments *_new_collection_of_arguments_p_s__memory_bundle(struct _memory_bundle *_bundle)
{
struct _collection_of_arguments *_a_1;
_a_1=((struct _collection_of_arguments *)_allocate_p_s__memory_bundle_dw(_bundle,((unsigned int)8)));
_initialize_p_s__collection(((struct _collection *)_a_1));
return _a_1;
}

static void _set_values_of_arguments_p_s__argument_p_s__expression(struct _argument *_arguments, struct _expression *_values)
{
struct _expression *_e_1;
struct _argument *_a_2;
_e_1=_values;
_a_2=_arguments;
while(((void*)_e_1!=(void*)0))
{
(_a_2->_substitute)=_e_1;
_a_2=((struct _argument *)(((struct _element *)_a_2)->_next_sibling));
_e_1=((struct _expression *)(((struct _element *)_e_1)->_next_sibling));
}
}

static void _collection_of_expressions_p_s__collection_of_expressions(struct _collection_of_expressions *_c)
{
_initialize_p_s__collection(((struct _collection *)_c));
}

static struct _function_expression *_new_function_expression_p_s__memory_bundle_p_s__type_e__expression_code_p_s__function(struct _memory_bundle *_bundle, struct _type *_type, int _opcode, struct _function *_value)
{
struct _function_expression *_e_1;
_e_1=((struct _function_expression *)_new_expression_p_s__memory_bundle_dw_p_s__type_e__expression_code(_bundle,((unsigned int)24),_type,_opcode));
(_e_1->_value)=_value;
return _e_1;
}

static struct _type *_reference_type_p_s__function(struct _function *_f)
{
if(((void*)(_f->_ref_type)==(void*)0))
{
(_f->_ref_type)=_pointer_p_s__type(((struct _type *)_new_function_type_p_s__memory_bundle_p_s__signature_p_s__type((_f->_bundle),(((struct _definition *)_f)->_signature),(((struct _definition *)_f)->_type))));
}
return (_f->_ref_type);
}

static void _check_assign_p_s__compiler_p_s__node_p_s__expression_p_s__expression(struct _compiler *_cc, struct _node *_source, struct _expression *_dst, struct _expression *_src)
{
struct _type *_left_type_1;
struct _type *_right_type_2;
_left_type_1=(_dst->_type);
_right_type_2=(_src->_type);
if((!_match_p_s__type_p_s__type(_right_type_2,_left_type_1)))
{
_error_p_s__compiler_p_s__node_p_a_ch_p_vo_p_vo(_cc,_source,_error_cant_assign983,((void *)_right_type_2),((void *)_left_type_1));
}
}

static void _check_lvalue_p_s__compiler_p_s__node_p_s__expression(struct _compiler *_cc, struct _node *_source, struct _expression *_e)
{
if(((!(_e->_lvalue))&&(!(_cc->_current_inside_equate))))
{
_error_p_s__compiler_p_s__node_p_a_ch(_cc,_source,_error_must_be_lvalue985);
}
}

static struct _long_expression *_new_bool_expression_p_s__memory_bundle_bo(struct _memory_bundle *_bundle, int _value)
{
return _new_litteral_long_expression_p_s__memory_bundle_p_s__type_dw(_bundle,((struct _type *)_bool_type123),((unsigned int )((int )_value)));
}

static struct _long_expression *_new_nil_expression_p_s__memory_bundle(struct _memory_bundle *_bundle)
{
return _new_litteral_long_expression_p_s__memory_bundle_p_s__type_dw(_bundle,((struct _type *)_nil_type108),((unsigned int )0));
}

static struct _long_expression *_new_true_expression_p_s__memory_bundle(struct _memory_bundle *_bundle)
{
return _new_litteral_long_expression_p_s__memory_bundle_p_s__type_dw(_bundle,((struct _type *)_bool_type123),((unsigned int )1));
}

static struct _long_expression *_new_false_expression_p_s__memory_bundle(struct _memory_bundle *_bundle)
{
return _new_litteral_long_expression_p_s__memory_bundle_p_s__type_dw(_bundle,((struct _type *)_bool_type123),((unsigned int )0));
}

static struct _definition_expression *_new_definition_expression_p_s__memory_bundle_p_s__definition_p_s__collection_of_expressions_p_s__type_bo(struct _memory_bundle *_bundle, struct _definition *_definition, struct _collection_of_expressions *_arguments, struct _type *_type, int _lvalue)
{
struct _definition_expression *_e_1;
_e_1=((struct _definition_expression *)_allocate_p_s__memory_bundle_dw(_bundle,((unsigned int)28)));
(((struct _expression *)_e_1)->_type)=_type;
(((struct _expression *)_e_1)->_opcode)=3;
(((struct _expression *)_e_1)->_lvalue)=_lvalue;
(_e_1->_definition)=_definition;
(_e_1->_arguments)=((struct _expression *)(((struct _collection *)_arguments)->_first));
return _e_1;
}

static struct _expression *_build_definition_expression_p_s__compiler_p_s__node_p_s__definition_p_s__collection_of_expressions_p_s__type_bo(struct _compiler *_cc, struct _node *_source, struct _definition *_definition, struct _collection_of_expressions *_arguments, struct _type *_type, int _lvalue)
{
struct _expression *_arg1_1;
struct _expression *_arg2_2;
struct _type *_cell_type_3;
int _v_4;
unsigned int _s_5;
struct _type *_t1_6;
struct _type *_t2_7;
int _v1_8;
int _v2_9;
int _signed_10;
int _opcode_11;
int _v_12;
struct _type *_t_13;
int _v_14;
struct _type *_t_15;
int _v_16;
struct _type *_t_17;
int _v_18;
struct _type *_t_19;
int _v_20;
struct _type *_t_21;
int _v_22;
struct _type *_t_23;
unsigned int _v_24;
struct _type *_t_25;
int _i1_26;
int _i2_27;
unsigned int _v_28;
struct _type *_t_29;
int _i1_30;
int _i2_31;
unsigned int _v_32;
int _v_33;
int _i1_34;
int _i2_35;
unsigned int _v_36;
struct _type *_t_37;
int _v_38;
struct _type *_t_39;
int _v_40;
struct _type *_t_41;
int _v_42;
struct _type *_t_43;
int _v_44;
struct _type *_t_45;
int _v_46;
_arg1_1=((struct _expression *)(((struct _collection *)_arguments)->_first));
_arg2_2=((struct _expression *)(((struct _collection *)_arguments)->_last));
switch((_definition->_opcode))
{
case 6:
{
if((((_cc->_current_loop_level)==0)&&(!(_cc->_current_inside_equate))))
{
_error_p_s__compiler_p_s__node_p_a_ch(_cc,_source,_error_invalid_break977);
}
break;
}
case 7:
{
if((((_cc->_current_loop_level)==0)&&(!(_cc->_current_inside_equate))))
{
_error_p_s__compiler_p_s__node_p_a_ch(_cc,_source,_error_invalid_continue979);
}
break;
}
case 66:
{
_check_assign_p_s__compiler_p_s__node_p_s__expression_p_s__expression(_cc,_source,_arg1_1,_arg2_2);
_check_lvalue_p_s__compiler_p_s__node_p_s__expression(_cc,_source,_arg1_1);
break;
}
case 69:
case 70:
{
_cell_type_3=_check_array_p_s__compiler_p_s__node_p_s__type(_cc,_source,_type);
if(((void*)_cell_type_3==(void*)0))
{
return ((struct _expression *)0);
}
if((((_arg1_1->_opcode)==1)&&((_arg2_2->_opcode)==1)))
{
_v_4=(((struct _long_expression *)_arg1_1)->_value);
_s_5=(_size_p_s__type(_cell_type_3)*(((struct _long_expression *)_arg2_2)->_value));
if(((_definition->_opcode)==69))
{
_v_4+=_s_5;
}
else
{
_v_4-=_s_5;
}
return ((struct _expression *)_new_litteral_long_expression_p_s__memory_bundle_p_s__type_dw((_cc->_bundle),_type,((unsigned int )_v_4)));
}
break;
}
case 72:
case 73:
case 74:
case 75:
{
_check_lvalue_p_s__compiler_p_s__node_p_s__expression(_cc,_source,_arg1_1);
if(((void*)_check_array_p_s__compiler_p_s__node_p_s__type(_cc,_source,_type)==(void*)0))
{
return ((struct _expression *)0);
}
break;
}
case 49:
{
_check_assign_p_s__compiler_p_s__node_p_s__expression_p_s__expression(_cc,_source,_arg1_1,_arg2_2);
_check_lvalue_p_s__compiler_p_s__node_p_s__expression(_cc,_source,_arg1_1);
break;
}
case 50:
case 51:
case 52:
case 53:
case 54:
case 55:
case 56:
case 57:
case 58:
case 59:
case 61:
case 60:
case 62:
case 63:
case 64:
case 65:
case 76:
case 77:
{
_check_lvalue_p_s__compiler_p_s__node_p_s__expression(_cc,_source,_arg1_1);
break;
}
case 16:
case 17:
case 18:
case 19:
case 20:
case 21:
case 41:
case 42:
{
_t1_6=(_arg1_1->_type);
_t2_7=(_arg2_2->_type);
if(((((((_t1_6->_flags)&16)!=0)!=(((_t2_7->_flags)&16)!=0))&&(!(((((_arg1_1->_type)->_flags)&8)!=0)&&((_arg1_1->_opcode)==1))))&&(!(((((_arg2_2->_type)->_flags)&8)!=0)&&((_arg2_2->_opcode)==1)))))
{
_error_p_s__compiler_p_s__node_p_a_ch_p_vo_p_vo(_cc,_source,_error_signed_mismatch992,((void *)_t1_6),((void *)_t2_7));
}
if((((_arg1_1->_opcode)==1)&&((_arg2_2->_opcode)==1)))
{
_v1_8=(((struct _long_expression *)_arg1_1)->_value);
_v2_9=(((struct _long_expression *)_arg2_2)->_value);
_signed_10=(((_t1_6->_flags)&16)!=0);
_opcode_11=(_definition->_opcode);
if(((_opcode_11==16)||(_opcode_11==41)))
{
_v_12=(_v1_8==_v2_9);
}
else if(((_opcode_11==17)||(_opcode_11==42)))
{
_v_12=(_v1_8!=_v2_9);
}
else if(_signed_10)
{
if((_opcode_11==18))
{
_v_12=(_v1_8<=_v2_9);
}
else if((_opcode_11==19))
{
_v_12=(_v1_8<_v2_9);
}
else if((_opcode_11==20))
{
_v_12=(_v1_8>=_v2_9);
}
else if((_opcode_11==21))
{
_v_12=(_v1_8>_v2_9);
}
}
else
{
if((_opcode_11==18))
{
_v_12=(((unsigned int )_v1_8)<=((unsigned int )_v2_9));
}
else if((_opcode_11==19))
{
_v_12=(((unsigned int )_v1_8)<((unsigned int )_v2_9));
}
else if((_opcode_11==20))
{
_v_12=(((unsigned int )_v1_8)>=((unsigned int )_v2_9));
}
else if((_opcode_11==21))
{
_v_12=(((unsigned int )_v1_8)>((unsigned int )_v2_9));
}
}
return ((struct _expression *)_new_bool_expression_p_s__memory_bundle_bo((_cc->_bundle),_v_12));
}
break;
}
case 12:
{
return ((struct _expression *)_new_nil_expression_p_s__memory_bundle((_cc->_bundle)));
break;
}
case 13:
{
return ((struct _expression *)_new_true_expression_p_s__memory_bundle((_cc->_bundle)));
break;
}
case 14:
{
return ((struct _expression *)_new_false_expression_p_s__memory_bundle((_cc->_bundle)));
break;
}
case 5:
{
_t_13=(_definition->_type);
_v_14=(((struct _enumeration_item *)_definition)->_value);
return ((struct _expression *)_new_litteral_long_expression_p_s__memory_bundle_p_s__type_dw((_cc->_bundle),_t_13,((unsigned int )_v_14)));
break;
}
case 39:
{
if(((_arg1_1->_opcode)==1))
{
if(((((struct _long_expression *)_arg1_1)->_value)==1))
{
return _expand_expression_p_s__compiler_p_s__node_p_s__expression(_cc,_source,_arg2_2);
}
else
{
return ((struct _expression *)_new_false_expression_p_s__memory_bundle((_cc->_bundle)));
}
}
else if((((_arg2_2->_opcode)==1)&&((((struct _long_expression *)_arg2_2)->_value)==1)))
{
return _expand_expression_p_s__compiler_p_s__node_p_s__expression(_cc,_source,_arg1_1);
}
break;
}
case 40:
{
if(((_arg1_1->_opcode)==1))
{
if(((((struct _long_expression *)_arg1_1)->_value)==1))
{
return ((struct _expression *)_new_true_expression_p_s__memory_bundle((_cc->_bundle)));
}
else
{
return _expand_expression_p_s__compiler_p_s__node_p_s__expression(_cc,_source,_arg2_2);
}
}
else if((((_arg2_2->_opcode)==1)&&((((struct _long_expression *)_arg2_2)->_value)==0)))
{
return _expand_expression_p_s__compiler_p_s__node_p_s__expression(_cc,_source,_arg1_1);
}
break;
}
case 26:
{
if((((_arg1_1->_opcode)==1)&&((_arg2_2->_opcode)==1)))
{
_t_15=_common_p_s__type_p_s__type((_arg1_1->_type),(_arg2_2->_type));
_v_16=((((struct _long_expression *)_arg1_1)->_value)+(((struct _long_expression *)_arg2_2)->_value));
return ((struct _expression *)_new_litteral_long_expression_p_s__memory_bundle_p_s__type_dw((_cc->_bundle),_t_15,((unsigned int )_v_16)));
}
break;
}
case 27:
{
if((((_arg1_1->_opcode)==1)&&((_arg2_2->_opcode)==1)))
{
_t_17=_common_p_s__type_p_s__type((_arg1_1->_type),(_arg2_2->_type));
_v_18=((((struct _long_expression *)_arg1_1)->_value)-(((struct _long_expression *)_arg2_2)->_value));
return ((struct _expression *)_new_litteral_long_expression_p_s__memory_bundle_p_s__type_dw((_cc->_bundle),_t_17,((unsigned int )_v_18)));
}
break;
}
case 33:
{
if(((_arg1_1->_opcode)==1))
{
_t_19=(_arg1_1->_type);
_v_20=(((struct _long_expression *)_arg1_1)->_value);
return ((struct _expression *)_new_litteral_long_expression_p_s__memory_bundle_p_s__type_dw((_cc->_bundle),_t_19,((unsigned int )_v_20)));
}
break;
}
case 34:
{
if(((_arg1_1->_opcode)==1))
{
_t_21=(_arg1_1->_type);
_v_22=(-(((struct _long_expression *)_arg1_1)->_value));
return ((struct _expression *)_new_litteral_long_expression_p_s__memory_bundle_p_s__type_dw((_cc->_bundle),_t_21,((unsigned int )_v_22)));
}
break;
}
case 28:
{
if((((_arg1_1->_opcode)==1)&&((_arg2_2->_opcode)==1)))
{
_t_23=_common_p_s__type_p_s__type((_arg1_1->_type),(_arg2_2->_type));
if((((_t_23->_flags)&16)!=0))
{
_v_24=((((struct _long_expression *)_arg1_1)->_value)*(((struct _long_expression *)_arg2_2)->_value));
}
else
{
_v_24=(((unsigned int )(((struct _long_expression *)_arg1_1)->_value))*((unsigned int )(((struct _long_expression *)_arg2_2)->_value)));
}
return ((struct _expression *)_new_litteral_long_expression_p_s__memory_bundle_p_s__type_dw((_cc->_bundle),_t_23,_v_24));
}
break;
}
case 29:
{
if((((_arg1_1->_opcode)==1)&&((_arg2_2->_opcode)==1)))
{
_t_25=_common_p_s__type_p_s__type((_arg1_1->_type),(_arg2_2->_type));
_i1_26=(((struct _long_expression *)_arg1_1)->_value);
_i2_27=(((struct _long_expression *)_arg2_2)->_value);
if((_i2_27==0))
{
_error_p_s__compiler_p_s__node_p_a_ch(_cc,_source,_error_divide_by_zero1014);
return ((struct _expression *)0);
}
if((((_t_25->_flags)&16)!=0))
{
_v_28=(_i1_26/_i2_27);
}
else
{
_v_28=(((unsigned int )_i1_26)/((unsigned int )_i2_27));
}
return ((struct _expression *)_new_litteral_long_expression_p_s__memory_bundle_p_s__type_dw((_cc->_bundle),_t_25,_v_28));
}
break;
}
case 30:
{
if((((_arg1_1->_opcode)==1)&&((_arg2_2->_opcode)==1)))
{
_t_29=_common_p_s__type_p_s__type((_arg1_1->_type),(_arg2_2->_type));
_i1_30=(((struct _long_expression *)_arg1_1)->_value);
_i2_31=(((struct _long_expression *)_arg2_2)->_value);
if((_i2_31==0))
{
_error_p_s__compiler_p_s__node_p_a_ch(_cc,_source,_error_divide_by_zero1014);
return ((struct _expression *)0);
}
if((((_t_29->_flags)&16)!=0))
{
_v_32=(_i1_30%_i2_31);
}
else
{
_v_32=(((unsigned int )_i1_30)%((unsigned int )_i2_31));
}
return ((struct _expression *)_new_litteral_long_expression_p_s__memory_bundle_p_s__type_dw((_cc->_bundle),_t_29,_v_32));
}
break;
}
case 31:
{
if((((_arg1_1->_opcode)==1)&&((_arg2_2->_opcode)==1)))
{
_v_33=((((struct _long_expression *)_arg1_1)->_value)<<(((struct _long_expression *)_arg2_2)->_value));
return ((struct _expression *)_new_litteral_long_expression_p_s__memory_bundle_p_s__type_dw((_cc->_bundle),(_arg1_1->_type),((unsigned int )_v_33)));
}
break;
}
case 32:
{
if((((_arg1_1->_opcode)==1)&&((_arg2_2->_opcode)==1)))
{
_i1_34=(((struct _long_expression *)_arg1_1)->_value);
_i2_35=(((struct _long_expression *)_arg2_2)->_value);
if(((((_arg1_1->_type)->_flags)&16)!=0))
{
_v_36=(_i1_34>>_i2_35);
}
else
{
_v_36=(((unsigned int )_i1_34)>>((unsigned int )_i2_35));
}
return ((struct _expression *)_new_litteral_long_expression_p_s__memory_bundle_p_s__type_dw((_cc->_bundle),(_arg1_1->_type),_v_36));
}
break;
}
case 22:
case 35:
{
if((((_arg1_1->_opcode)==1)&&((_arg2_2->_opcode)==1)))
{
_t_37=_common_p_s__type_p_s__type((_arg1_1->_type),(_arg2_2->_type));
_v_38=((((struct _long_expression *)_arg1_1)->_value)&(((struct _long_expression *)_arg2_2)->_value));
return ((struct _expression *)_new_litteral_long_expression_p_s__memory_bundle_p_s__type_dw((_cc->_bundle),_t_37,((unsigned int )_v_38)));
}
break;
}
case 23:
case 36:
{
if((((_arg1_1->_opcode)==1)&&((_arg2_2->_opcode)==1)))
{
_t_39=_common_p_s__type_p_s__type((_arg1_1->_type),(_arg2_2->_type));
_v_40=((((struct _long_expression *)_arg1_1)->_value)|(((struct _long_expression *)_arg2_2)->_value));
return ((struct _expression *)_new_litteral_long_expression_p_s__memory_bundle_p_s__type_dw((_cc->_bundle),_t_39,((unsigned int )_v_40)));
}
break;
}
case 24:
case 37:
{
if((((_arg1_1->_opcode)==1)&&((_arg2_2->_opcode)==1)))
{
_t_41=_common_p_s__type_p_s__type((_arg1_1->_type),(_arg2_2->_type));
_v_42=((((struct _long_expression *)_arg1_1)->_value)^(((struct _long_expression *)_arg2_2)->_value));
return ((struct _expression *)_new_litteral_long_expression_p_s__memory_bundle_p_s__type_dw((_cc->_bundle),_t_41,((unsigned int )_v_42)));
}
break;
}
case 25:
{
if(((_arg1_1->_opcode)==1))
{
_t_43=(_arg1_1->_type);
_v_44=(~(((struct _long_expression *)_arg1_1)->_value));
return ((struct _expression *)_new_litteral_long_expression_p_s__memory_bundle_p_s__type_dw((_cc->_bundle),_t_43,((unsigned int )_v_44)));
}
break;
}
case 38:
{
if(((_arg1_1->_opcode)==1))
{
_t_45=(_arg1_1->_type);
_v_46=(1-(((struct _long_expression *)_arg1_1)->_value));
return ((struct _expression *)_new_litteral_long_expression_p_s__memory_bundle_p_s__type_dw((_cc->_bundle),_t_45,((unsigned int )_v_46)));
}
break;
}
}
return ((struct _expression *)_new_definition_expression_p_s__memory_bundle_p_s__definition_p_s__collection_of_expressions_p_s__type_bo((_cc->_bundle),_definition,_arguments,_type,_lvalue));
}

static int _is_variable_p_s__expression(struct _expression *_e)
{
return (((_e->_opcode)==3)&&(((((struct _definition_expression *)_e)->_definition)->_opcode)==1));
}

static int _is_constant_p_s__expression(struct _expression *_expression)
{
struct _expression *_e_1;
struct _variable *_v_2;
struct _type *_t_3;
_e_1=_expression;
if(_is_reference_p_s__expression(_e_1))
{
_e_1=(((struct _variable *)(((struct _definition_expression *)_e_1)->_definition))->_reference);
}
if(_is_variable_p_s__expression(_e_1))
{
_v_2=((struct _variable *)(((struct _definition_expression *)_e_1)->_definition));
_t_3=(_v_2->_storage_type);
if((((_t_3->_id)==16)|((_t_3->_id)==18)))
{
return 1;
}
}
return (((((_e_1->_opcode)==1)||((_e_1->_opcode)==2))||((_e_1->_opcode)==4))||((_e_1->_opcode)==5));
}

static struct _expression *_check_initializer_p_s__compiler_p_s__node_p_s__expression_p_s__type(struct _compiler *_cc, struct _node *_source, struct _expression *_expression, struct _type *_type)
{
struct _expression *_e_1;
struct _element *_e_2;
struct _type *_cell_type_3;
int _cell_size_4;
int _s_5;
_e_1=_expression;
if(((_type->_id)==16))
{
if(((void*)((struct _structure_type *)(((struct _structure_type *)_type)->_parent_type))!=(void*)0))
{
_e_1=_check_initializer_p_s__compiler_p_s__node_p_s__expression_p_s__type(_cc,_source,_e_1,((struct _type *)((struct _structure_type *)(((struct _structure_type *)_type)->_parent_type))));
}
_e_2=((struct _element *)(((struct _structure_type *)_type)->_attributes));
while(((void*)_e_2!=(void*)0))
{
if(((void*)_e_1==(void*)0))
{
_error_p_s__compiler_p_s__node_p_a_ch(_cc,_source,_error_too_few_arguments1039);
return ((struct _expression *)0);
}
_e_1=_check_initializer_p_s__compiler_p_s__node_p_s__expression_p_s__type(_cc,_source,_e_1,(((struct _attribute *)_e_2)->_storage_type));
_e_2=(_e_2->_next_sibling);
}
}
else if(((_type->_id)==17))
{
if(((void*)(((struct _structure_type *)_type)->_attributes)!=(void*)0))
{
_e_1=_check_initializer_p_s__compiler_p_s__node_p_s__expression_p_s__type(_cc,_source,_e_1,((((struct _structure_type *)_type)->_attributes)->_storage_type));
}
}
else if(((_type->_id)==18))
{
_cell_type_3=(((struct _array_type *)_type)->_array_type);
_cell_size_4=0;
while(((void*)_e_1!=(void*)0))
{
_e_1=_check_initializer_p_s__compiler_p_s__node_p_s__expression_p_s__type(_cc,_source,_e_1,_cell_type_3);
(++_cell_size_4);
}
_s_5=(((struct _array_type *)_type)->_array_size);
if(((_s_5!=0)&(_s_5!=_cell_size_4)))
{
_error_p_s__compiler_p_s__node_p_a_ch(_cc,_source,_error_incompatible_array_size1044);
return ((struct _expression *)0);
}
}
else
{
if((!_match_p_s__type_p_s__type((_e_1->_type),_type)))
{
_error_p_s__compiler_p_s__node_p_a_ch_p_vo_p_vo(_cc,_source,_error_incompatible_types1046,((void *)(_e_1->_type)),((void *)_type));
return ((struct _expression *)0);
}
if(((!_is_constant_p_s__expression(_e_1))&&(!(_cc->_current_inside_equate))))
{
_error_p_s__compiler_p_s__node_p_a_ch(_cc,_source,_error_not_constant821);
return ((struct _expression *)0);
}
_e_1=((struct _expression *)(((struct _element *)_e_1)->_next_sibling));
}
return _e_1;
}

static struct _collection_expression *_new_collection_expression_p_s__memory_bundle_p_s__type_e__expression_code_p_s__collection_of_expressions(struct _memory_bundle *_bundle, struct _type *_type, int _opcode, struct _collection_of_expressions *_value)
{
struct _collection_expression *_e_1;
_e_1=((struct _collection_expression *)_allocate_p_s__memory_bundle_dw(_bundle,((unsigned int)24)));
(((struct _expression *)_e_1)->_type)=_type;
(((struct _expression *)_e_1)->_opcode)=_opcode;
(((struct _expression *)_e_1)->_lvalue)=0;
(_e_1->_value)=((struct _expression *)(((struct _collection *)_value)->_first));
return _e_1;
}

static struct _variable *_new_variable_p_s__memory_bundle_p_s__variable_bo(struct _memory_bundle *_bundle, struct _variable *_old, int _expose)
{
struct _variable *_v_1;
_v_1=((struct _variable *)_allocate_p_s__memory_bundle_dw(_bundle,((unsigned int)84)));
_initialize_p_s__definition_p_a_ch_p_s__signature_p_s__type_e__definition_code_bo_bo_p_vo_p_s__node(((struct _definition *)_v_1),(((struct _definition *)_old)->_name),(&_empty_sig107),(((struct _definition *)_old)->_type),1,(((struct _definition *)_old)->_lvalue),_expose,(((struct _definition *)_old)->_module),(((struct _definition *)_old)->_source));
(_v_1->_directives)=(_old->_directives);
(_v_1->_initial_value)=((struct _expression *)0);
(_v_1->_proxy)=((struct _expression *)0);
(_v_1->_reference)=((struct _expression *)0);
(_v_1->_extern_name)=_empty_string171;
(_v_1->_defined)=0;
(_v_1->_storage_type)=(_old->_storage_type);
(_v_1->_redirect)=((struct _variable *)0);
(_v_1->_function)=((struct _function *)0);
return _v_1;
}

static struct _variable *_expand_variable_p_s__compiler_p_s__node_p_s__variable(struct _compiler *_cc, struct _node *_source, struct _variable *_old_variable)
{
struct _variable *_new_variable_1;
struct _expression *_initial_value_2;
struct _expression *_p_3;
struct _expression *_r_4;
_new_variable_1=_new_variable_p_s__memory_bundle_p_s__variable_bo((_cc->_bundle),_old_variable,0);
_initial_value_2=(_old_variable->_initial_value);
if(((void*)_initial_value_2!=(void*)0))
{
_initial_value_2=_expand_expression_p_s__compiler_p_s__node_p_s__expression(_cc,_source,_initial_value_2);
}
(_new_variable_1->_initial_value)=_initial_value_2;
(_new_variable_1->_defined)=(_old_variable->_defined);
_p_3=(_old_variable->_proxy);
if(((void*)_p_3!=(void*)0))
{
_p_3=_expand_expression_p_s__compiler_p_s__node_p_s__expression(_cc,_source,_p_3);
}
(_new_variable_1->_proxy)=_p_3;
_r_4=(_old_variable->_reference);
if(((void*)_r_4!=(void*)0))
{
_r_4=_expand_expression_p_s__compiler_p_s__node_p_s__expression(_cc,_source,_r_4);
}
(_new_variable_1->_reference)=_r_4;
(_old_variable->_redirect)=_new_variable_1;
return _new_variable_1;
}

static struct _condition_operator_expression *_new_condition_operator_expression_p_s__memory_bundle_p_s__type_p_s__expression_p_s__expression_p_s__expression(struct _memory_bundle *_bundle, struct _type *_type, struct _expression *_condition, struct _expression *_then_expression, struct _expression *_otherwise_expression)
{
struct _condition_operator_expression *_e_1;
_e_1=((struct _condition_operator_expression *)_new_expression_p_s__memory_bundle_dw_p_s__type_e__expression_code(_bundle,((unsigned int)32),_type,16));
(_e_1->_condition)=_condition;
(_e_1->_then_expression)=_then_expression;
(_e_1->_otherwise_expression)=_otherwise_expression;
return _e_1;
}

static struct _yield_expression *_new_yield_expression_p_s__memory_bundle_p_s__collection_of_expressions_p_s__iterator(struct _memory_bundle *_bundle, struct _collection_of_expressions *_arguments, struct _iterator *_iterator)
{
struct _yield_expression *_e_1;
_e_1=((struct _yield_expression *)_new_expression_p_s__memory_bundle_dw_p_s__type_e__expression_code(_bundle,((unsigned int)28),((struct _type *)_void_type114),20));
(_e_1->_arguments)=((struct _expression *)(((struct _collection *)_arguments)->_first));
(_e_1->_iterator)=_iterator;
return _e_1;
}

static struct _sequence_expression *_new_sequence_expression_p_s__memory_bundle_p_s__collection_of_expressions(struct _memory_bundle *_bundle, struct _collection_of_expressions *_sequence)
{
struct _sequence_expression *_e_1;
_e_1=((struct _sequence_expression *)_new_expression_p_s__memory_bundle_dw_p_s__type_e__expression_code(_bundle,((unsigned int)24),(((struct _expression *)(((struct _collection *)_sequence)->_last))->_type),7));
(_e_1->_sequence)=((struct _expression *)(((struct _collection *)_sequence)->_first));
return _e_1;
}

static struct _expression *_expand_expression_p_s__compiler_p_s__node_p_s__expression(struct _compiler *_cc, struct _node *_source, struct _expression *_s)
{
struct _type *_t_1;
int _v_2;
struct _type *_t_3;
char *_v_4;
struct _definition *_d_5;
struct _collection_of_expressions _ce_6;
struct _collection_of_expressions *_a_7;
struct _collection_of_expressions *_a_8;
struct _variable *_old_variable_9;
struct _variable *_new_variable_10;
struct _expression *_value_11;
struct _expression *_c_12;
struct _collection_of_expressions *_b_13;
struct _collection_of_expressions *_b_14;
struct _expression *_exp_15;
struct _collection _rules_16;
struct _aswitch_item *_i_17;
struct _constant *_c_18;
struct _collection_of_expressions *_b_19;
struct _collection _rules_20;
struct _condition_item *_c_21;
struct _expression *_c_22;
struct _collection_of_expressions *_b_23;
struct _expression *_c_24;
struct _expression *_t_25;
struct _expression *_o_26;
struct _expression *_v_27;
struct _expression_expression *_exp_28;
struct _collection_of_expressions *_a_29;
struct _iterator *_iterator_30;
struct _collection_of_expressions *_b_31;
struct _collection_of_expressions *_a_32;
struct _collection_of_expressions *_b_33;
struct _collection_of_expressions *_b_34;
switch((_s->_opcode))
{
case 1:
{
_t_1=(_s->_type);
_v_2=(((struct _long_expression *)_s)->_value);
return ((struct _expression *)_new_litteral_long_expression_p_s__memory_bundle_p_s__type_dw((_cc->_bundle),_t_1,((unsigned int )_v_2)));
break;
}
case 2:
{
_t_3=(_s->_type);
_v_4=(((struct _string_expression *)_s)->_value);
return ((struct _expression *)_new_litteral_string_expression_p_s__memory_bundle_p_s__type_p_a_ch((_cc->_bundle),_t_3,_v_4));
break;
}
case 4:
{
return ((struct _expression *)_new_function_expression_p_s__memory_bundle_p_s__type_e__expression_code_p_s__function((_cc->_bundle),_reference_type_p_s__function((((struct _function_expression *)_s)->_value)),4,(((struct _function_expression *)_s)->_value)));
break;
}
case 3:
{
_d_5=(((struct _definition_expression *)_s)->_definition);
if((((_d_5->_opcode)==0)&&((void*)(((struct _argument *)_d_5)->_substitute)!=(void*)0)))
{
return _expand_expression_p_s__compiler_p_s__node_p_s__expression(_cc,_source,(((struct _argument *)_d_5)->_substitute));
}
else if((((_d_5->_opcode)==1)&&((void*)(((struct _variable *)_d_5)->_redirect)!=(void*)0)))
{
_d_5=((struct _definition *)(((struct _variable *)_d_5)->_redirect));
}
_a_7=_expand_expressions_p_s__compiler_p_s__node_p_s__expression_p_s__collection_of_expressions(_cc,_source,(((struct _definition_expression *)_s)->_arguments),(&_ce_6));
return _build_definition_expression_p_s__compiler_p_s__node_p_s__definition_p_s__collection_of_expressions_p_s__type_bo(_cc,_source,_d_5,_a_7,(((struct _expression *)((struct _definition_expression *)_s))->_type),(((struct _expression *)((struct _definition_expression *)_s))->_lvalue));
break;
}
case 5:
{
_a_8=_expand_expressions_p_s__compiler_p_s__node_p_s__expression_p_s__collection_of_expressions(_cc,_source,(((struct _collection_expression *)_s)->_value),(&_ce_6));
_check_initializer_p_s__compiler_p_s__node_p_s__expression_p_s__type(_cc,_source,((struct _expression *)(((struct _collection *)_a_8)->_first)),(((struct _pointer_type *)(((struct _expression *)((struct _collection_expression *)_s))->_type))->_target_type));
return ((struct _expression *)_new_collection_expression_p_s__memory_bundle_p_s__type_e__expression_code_p_s__collection_of_expressions((_cc->_bundle),(((struct _expression *)((struct _collection_expression *)_s))->_type),5,_a_8));
break;
}
case 9:
{
_old_variable_9=(((struct _variable_expression *)_s)->_value);
_new_variable_10=_expand_variable_p_s__compiler_p_s__node_p_s__variable(_cc,_source,_old_variable_9);
return ((struct _expression *)_new_variable_expression_p_s__memory_bundle_p_s__type_e__expression_code_p_s__variable((_cc->_bundle),((struct _type *)_void_type114),9,_new_variable_10));
break;
}
case 11:
{
_value_11=(((struct _expression_expression *)_s)->_value);
if(((void*)_value_11!=(void*)0))
{
_value_11=_expand_expression_p_s__compiler_p_s__node_p_s__expression(_cc,_source,_value_11);
}
_check_return_p_s__compiler_p_s__node_p_s__expression(_cc,_source,_value_11);
return ((struct _expression *)_new_expression_expression_p_s__memory_bundle_p_s__type_e__expression_code_p_s__expression((_cc->_bundle),((struct _type *)_void_type114),11,_value_11));
break;
}
case 12:
{
_c_12=_expand_expression_p_s__compiler_p_s__node_p_s__expression(_cc,_source,(((struct _loop_expression *)_s)->_condition));
(++(_cc->_current_loop_level));
_b_13=_expand_expressions_p_s__compiler_p_s__node_p_s__expression_p_s__collection_of_expressions(_cc,_source,(((struct _loop_expression *)_s)->_block),(&_ce_6));
(--(_cc->_current_loop_level));
return ((struct _expression *)_new_loop_expression_p_s__memory_bundle_p_s__expression_p_s__collection_of_expressions((_cc->_bundle),_c_12,_b_13));
break;
}
case 13:
{
(++(_cc->_current_loop_level));
_b_14=_expand_expressions_p_s__compiler_p_s__node_p_s__expression_p_s__collection_of_expressions(_cc,_source,(((struct _rept_expression *)_s)->_block),(&_ce_6));
(--(_cc->_current_loop_level));
return ((struct _expression *)_new_repeat_expression_p_s__memory_bundle_p_s__collection_of_expressions((_cc->_bundle),_b_14));
break;
}
case 14:
{
_exp_15=_expand_expression_p_s__compiler_p_s__node_p_s__expression(_cc,_source,(((struct _aswitch_expression *)_s)->_expression));
_initialize_p_s__collection((&_rules_16));
_i_17=(((struct _aswitch_expression *)_s)->_rules);
while(((void*)_i_17!=(void*)0))
{
_c_18=(_i_17->_constants);
_b_19=_expand_expressions_p_s__compiler_p_s__node_p_s__expression_p_s__collection_of_expressions(_cc,_source,(_i_17->_block),(&_ce_6));
_append_p_s__collection_p_s__element((&_rules_16),((struct _element *)_new_switch_item_p_s__memory_bundle_p_s__constant_p_s__collection((_cc->_bundle),_c_18,((struct _collection *)_b_19))));
_i_17=((struct _aswitch_item *)(((struct _element *)_i_17)->_next_sibling));
}
return ((struct _expression *)_new_switch_expression_p_s__memory_bundle_p_s__expression_p_s__collection((_cc->_bundle),_exp_15,(&_rules_16)));
break;
}
case 15:
{
_initialize_p_s__collection((&_rules_20));
_c_21=(((struct _condition_expression *)_s)->_value);
while(((void*)_c_21!=(void*)0))
{
_c_22=(_c_21->_condition);
if(((void*)_c_22!=(void*)0))
{
_c_22=_expand_expression_p_s__compiler_p_s__node_p_s__expression(_cc,_source,_c_22);
}
_b_23=_expand_expressions_p_s__compiler_p_s__node_p_s__expression_p_s__collection_of_expressions(_cc,_source,(_c_21->_block),(&_ce_6));
_append_p_s__collection_p_s__element((&_rules_20),((struct _element *)_new_condition_p_s__memory_bundle_p_s__expression_p_s__collection((_cc->_bundle),_c_22,((struct _collection *)_b_23))));
_c_21=((struct _condition_item *)(((struct _element *)_c_21)->_next_sibling));
}
return ((struct _expression *)_new_condition_expression_p_s__memory_bundle_p_s__condition_item((_cc->_bundle),((struct _condition_item *)((&_rules_20)->_first))));
break;
}
case 16:
{
_c_24=_expand_expression_p_s__compiler_p_s__node_p_s__expression(_cc,_source,(((struct _condition_operator_expression *)_s)->_condition));
_t_25=_expand_expression_p_s__compiler_p_s__node_p_s__expression(_cc,_source,(((struct _condition_operator_expression *)_s)->_then_expression));
_o_26=_expand_expression_p_s__compiler_p_s__node_p_s__expression(_cc,_source,(((struct _condition_operator_expression *)_s)->_otherwise_expression));
return ((struct _expression *)_new_condition_operator_expression_p_s__memory_bundle_p_s__type_p_s__expression_p_s__expression_p_s__expression((_cc->_bundle),(((struct _expression *)((struct _condition_operator_expression *)_s))->_type),_c_24,_t_25,_o_26));
break;
}
case 17:
{
_v_27=_expand_expression_p_s__compiler_p_s__node_p_s__expression(_cc,_source,(((struct _expression_expression *)_s)->_value));
_exp_28=_new_expression_expression_p_s__memory_bundle_p_s__type_e__expression_code_p_s__expression((_cc->_bundle),(((struct _expression *)((struct _expression_expression *)_s))->_type),17,_v_27);
(((struct _expression *)_exp_28)->_lvalue)=(((struct _expression *)((struct _expression_expression *)_s))->_lvalue);
return ((struct _expression *)_exp_28);
break;
}
case 18:
{
_a_29=_expand_expressions_p_s__compiler_p_s__node_p_s__expression_p_s__collection_of_expressions(_cc,_source,(((struct _expression_expression *)_s)->_value),(&_ce_6));
return ((struct _expression *)_new_expression_expression_p_s__memory_bundle_p_s__type_e__expression_code_p_s__expression((_cc->_bundle),(((struct _expression *)((struct _expression_expression *)_s))->_type),18,((struct _expression *)(((struct _collection *)_a_29)->_first))));
break;
}
case 20:
{
_iterator_30=(((struct _yield_expression *)_s)->_iterator);
if(((void*)(_iterator_30->_parameters)!=(void*)0))
{
_set_values_of_arguments_p_s__argument_p_s__expression(((struct _argument *)(((struct _collection *)(_iterator_30->_parameters))->_first)),(((struct _yield_expression *)_s)->_arguments));
_b_31=_expand_expressions_p_s__compiler_p_s__node_p_s__expression_p_s__collection_of_expressions(_cc,_source,(_iterator_30->_block),(&_ce_6));
return ((struct _expression *)_new_collection_expression_p_s__memory_bundle_p_s__type_e__expression_code_p_s__collection_of_expressions((_cc->_bundle),(((struct _expression *)((struct _yield_expression *)_s))->_type),6,_b_31));
}
else
{
_a_32=_expand_expressions_p_s__compiler_p_s__node_p_s__expression_p_s__collection_of_expressions(_cc,_source,(((struct _yield_expression *)_s)->_arguments),(&_ce_6));
return ((struct _expression *)_new_yield_expression_p_s__memory_bundle_p_s__collection_of_expressions_p_s__iterator((_cc->_bundle),_a_32,(((struct _yield_expression *)_s)->_iterator)));
}
break;
}
case 6:
{
_b_33=_expand_expressions_p_s__compiler_p_s__node_p_s__expression_p_s__collection_of_expressions(_cc,_source,(((struct _collection_expression *)_s)->_value),(&_ce_6));
return ((struct _expression *)_new_collection_expression_p_s__memory_bundle_p_s__type_e__expression_code_p_s__collection_of_expressions((_cc->_bundle),(((struct _expression *)((struct _collection_expression *)_s))->_type),6,_b_33));
break;
}
case 7:
{
_b_34=_expand_expressions_p_s__compiler_p_s__node_p_s__expression_p_s__collection_of_expressions(_cc,_source,(((struct _sequence_expression *)_s)->_sequence),(&_ce_6));
return ((struct _expression *)_new_sequence_expression_p_s__memory_bundle_p_s__collection_of_expressions((_cc->_bundle),_b_34));
break;
}
case 19:
{
return _new_ignore_expression_p_s__memory_bundle((_cc->_bundle));
break;
}
default:
{
return ((struct _expression *)0);
break;
}
}
}

static struct _collection_of_expressions *_expand_expressions_p_s__compiler_p_s__node_p_s__expression_p_s__collection_of_expressions(struct _compiler *_cc, struct _node *_source, struct _expression *_s, struct _collection_of_expressions *_block)
{
struct _expression *_e_1;
_initialize_p_s__collection(((struct _collection *)_block));
_e_1=_s;
while(((void*)_e_1!=(void*)0))
{
_append_p_s__collection_p_s__element(((struct _collection *)_block),((struct _element *)_expand_expression_p_s__compiler_p_s__node_p_s__expression(_cc,_source,_e_1)));
_e_1=((struct _expression *)(((struct _element *)_e_1)->_next_sibling));
}
return _block;
}

static struct _collection_expression *_build_iterator_call_p_s__compiler_p_s__node_p_a_ch_p_s__collection_of_expressions_p_s__iterator_p_s__collection_of_arguments_p_s__collection_of_expressions(struct _compiler *_cc, struct _node *_source, char *_name, struct _collection_of_expressions *_arguments, struct _iterator *_iterator, struct _collection_of_arguments *_parameters, struct _collection_of_expressions *_block)
{
struct _collection_of_expressions _t_1;
struct _collection_of_expressions *_c_2;
(_iterator->_block)=((struct _expression *)(((struct _collection *)_block)->_first));
(_iterator->_parameters)=_parameters;
_set_values_of_arguments_p_s__argument_p_s__expression((_iterator->_arguments),((struct _expression *)(((struct _collection *)_arguments)->_first)));
_c_2=(_collection_of_expressions_p_s__collection_of_expressions((&_t_1)),(&_t_1));
_c_2=_expand_expressions_p_s__compiler_p_s__node_p_s__expression_p_s__collection_of_expressions(_cc,_source,(_iterator->_instructions),_c_2);
return _new_collection_expression_p_s__memory_bundle_p_s__type_e__expression_code_p_s__collection_of_expressions((_cc->_bundle),((struct _type *)_void_type114),6,_c_2);
}

static int _do_define_equate_p_s__compiler_p_s__equate(struct _compiler *_cc, struct _equate *_equate)
{
struct _equate_node *_source_1;
struct _node *_source_value_2;
struct _expression *_value_3;
_source_1=((struct _equate_node *)(((struct _definition *)_equate)->_source));
_source_value_2=(_source_1->_value);
_add_local_definitions_p_s__compiler_p_s__argument(_cc,(_equate->_arguments));
_value_3=_get_expression_p_s__compiler_p_s__node(_cc,_source_value_2);
if(((void*)_value_3==(void*)0))
{
return 0;
}
(_equate->_value)=_value_3;
(((struct _definition *)_equate)->_type)=(_value_3->_type);
(((struct _definition *)_equate)->_lvalue)=(_value_3->_lvalue);
return 1;
}

static int _define_equate_p_s__compiler_p_s__equate(struct _compiler *_cc, struct _equate *_equate)
{
int _status_1;
int _previous_exposition_2;
struct _function *_previous_function_3;
struct _type *_previous_found_type_4;
struct _scope *_previous_scope_5;
int _previous_inside_equate_6;
int _ok_7;
_status_1=(((struct _definition *)_equate)->_resolve_status);
if((_status_1==2))
{
return 1;
}
else if((_status_1==3))
{
return 0;
}
else if((_status_1==1))
{
_error_p_s__compiler_in_p_a_ch_p_vo(_cc,(((struct _definition *)_equate)->_line),_error_circular_dependency_equate938,((void *)(((struct _definition *)_equate)->_name)));
return 0;
}
(((struct _definition *)_equate)->_resolve_status)=1;
_previous_exposition_2=(_cc->_current_exposition);
(_cc->_current_exposition)=(((struct _definition *)_equate)->_expose);
_previous_function_3=(_cc->_current_function);
_previous_found_type_4=(_cc->_current_found_type);
_previous_scope_5=(_cc->_current_scope);
_previous_inside_equate_6=(_cc->_current_inside_equate);
(_cc->_current_function)=((struct _function *)0);
(_cc->_current_found_type)=((struct _type *)0);
(_cc->_current_inside_equate)=1;
_open_scope_p_s__compiler_p_s__scope(_cc,((((struct _equate_node *)(((struct _definition *)_equate)->_source))->_global)?(&(_cc->_global_scope)):(_cc->_current_scope)));
_ok_7=_do_define_equate_p_s__compiler_p_s__equate(_cc,_equate);
_close_scope_p_s__compiler(_cc);
(_cc->_current_function)=_previous_function_3;
(_cc->_current_found_type)=_previous_found_type_4;
(_cc->_current_scope)=_previous_scope_5;
(_cc->_current_inside_equate)=_previous_inside_equate_6;
(_cc->_current_exposition)=_previous_exposition_2;
(((struct _definition *)_equate)->_resolve_status)=(_ok_7?2:3);
return _ok_7;
}

static void _define_p_s__function_p_s__collection_of_expressions(struct _function *_f, struct _collection_of_expressions *_instructions)
{
(_f->_instructions)=((struct _expression *)(((struct _collection *)_instructions)->_first));
(_f->_defined)=1;
}

static int _always_return_p_s__expression(struct _expression *_block)
{
struct _expression *_e_1;
int _b_2;
int _result_3;
int _has_else_block_4;
struct _aswitch_item *_i_5;
int _result_6;
int _has_else_block_7;
struct _condition_item *_i_8;
_e_1=_block;
while(((void*)_e_1!=(void*)0))
{
_b_2=0;
switch((_e_1->_opcode))
{
case 11:
{
_b_2=1;
break;
}
case 12:
{
_b_2=_always_return_p_s__expression((((struct _loop_expression *)_e_1)->_block));
break;
}
case 13:
{
_b_2=_always_return_p_s__expression((((struct _rept_expression *)_e_1)->_block));
break;
}
case 14:
{
_result_3=1;
_has_else_block_4=0;
_i_5=(((struct _aswitch_expression *)_e_1)->_rules);
while(((void*)_i_5!=(void*)0))
{
if(((void*)(_i_5->_constants)!=(void*)0))
{
_has_else_block_4=1;
}
_result_3&=_always_return_p_s__expression((_i_5->_block));
_i_5=((struct _aswitch_item *)(((struct _element *)_i_5)->_next_sibling));
}
_b_2=(_result_3&_has_else_block_4);
break;
}
case 15:
{
_result_6=1;
_has_else_block_7=0;
_i_8=(((struct _condition_expression *)_e_1)->_value);
while(((void*)_i_8!=(void*)0))
{
if(((void*)(_i_8->_condition)==(void*)0))
{
_has_else_block_7=1;
}
_result_6&=_always_return_p_s__expression((_i_8->_block));
_i_8=((struct _condition_item *)(((struct _element *)_i_8)->_next_sibling));
}
_b_2=(_result_6&_has_else_block_7);
break;
}
case 6:
{
_b_2=_always_return_p_s__expression((((struct _collection_expression *)_e_1)->_value));
break;
}
}
if(_b_2)
{
return 1;
}
_e_1=((struct _expression *)(((struct _element *)_e_1)->_next_sibling));
}
return 0;
}

static int _do_define_function_p_s__compiler_p_s__function(struct _compiler *_cc, struct _function *_function)
{
struct _function_node *_source_1;
struct _collection *_source_body_2;
struct _collection_of_expressions *_body_3;
struct _type *_found_type_4;
_add_local_definitions_p_s__compiler_p_s__argument(_cc,(_function->_arguments));
_source_1=((struct _function_node *)(((struct _definition *)_function)->_source));
_source_body_2=(_source_1->_body);
_body_3=_get_sequence_of_instructions_p_s__compiler_p_s__collection(_cc,_source_body_2);
if(((void*)_body_3==(void*)0))
{
return 0;
}
_define_p_s__function_p_s__collection_of_expressions(_function,_body_3);
if(((void*)(((struct _definition *)_function)->_type)==(void*)0))
{
_found_type_4=(_cc->_current_found_type);
(((struct _definition *)_function)->_type)=(((void*)_found_type_4==(void*)0)?((struct _type *)_void_type114):_found_type_4);
}
if((!(((((struct _definition *)_function)->_type)->_id)==1)))
{
if((!_always_return_p_s__expression((_function->_instructions))))
{
_error_p_s__compiler_p_s__node_p_a_ch_p_vo(_cc,((struct _node *)_source_1),_error_missing_return1118,((void *)(((struct _definition *)_function)->_name)));
return 0;
}
}
return 1;
}

static int _define_function_p_s__compiler_p_s__function(struct _compiler *_cc, struct _function *_function)
{
int _status_1;
int _previous_exposition_2;
struct _set_of_directives *_previous_directives_3;
struct _function *_previous_function_4;
struct _type *_previous_found_type_5;
struct _scope *_previous_scope_6;
int _previous_inside_equate_7;
int _ok_8;
_status_1=(((struct _definition *)_function)->_resolve_status);
if((_status_1==2))
{
return 1;
}
else if((_status_1==3))
{
return 0;
}
else if((_status_1==1))
{
_error_p_s__compiler_in_p_a_ch_p_vo(_cc,(((struct _definition *)_function)->_line),_error_circular_dependency_function1097,((void *)(((struct _definition *)_function)->_name)));
return 0;
}
(((struct _definition *)_function)->_resolve_status)=1;
_previous_exposition_2=(_cc->_current_exposition);
(_cc->_current_exposition)=(((struct _definition *)_function)->_expose);
_previous_directives_3=(_cc->_current_directives);
(_cc->_current_directives)=(_function->_directives);
_previous_function_4=(_cc->_current_function);
_previous_found_type_5=(_cc->_current_found_type);
_previous_scope_6=(_cc->_current_scope);
_previous_inside_equate_7=(_cc->_current_inside_equate);
(_cc->_current_function)=_function;
(_cc->_current_found_type)=((struct _type *)0);
(_cc->_current_inside_equate)=0;
_open_scope_p_s__compiler_p_s__scope(_cc,(&(_cc->_global_scope)));
_ok_8=_do_define_function_p_s__compiler_p_s__function(_cc,_function);
_close_scope_p_s__compiler(_cc);
(_cc->_current_function)=_previous_function_4;
(_cc->_current_found_type)=_previous_found_type_5;
(_cc->_current_scope)=_previous_scope_6;
(_cc->_current_inside_equate)=_previous_inside_equate_7;
(_cc->_current_directives)=_previous_directives_3;
(_cc->_current_exposition)=_previous_exposition_2;
(((struct _definition *)_function)->_resolve_status)=(_ok_8?2:3);
return _ok_8;
}

static struct _variable *_new_variable_p_s__memory_bundle_p_a_ch_p_s__type_p_s__set_of_directives_p_vo_p_s__node(struct _memory_bundle *_bundle, char *_name, struct _type *_type, struct _set_of_directives *_directives, void *_module, struct _node *_source)
{
struct _variable *_v_1;
_v_1=((struct _variable *)_allocate_p_s__memory_bundle_dw(_bundle,((unsigned int)84)));
_initialize_p_s__definition_p_a_ch_p_s__signature_p_s__type_e__definition_code_bo_bo_p_vo_p_s__node(((struct _definition *)_v_1),_name,(&_empty_sig107),_effective_type_p_s__type(_type),1,(((_type->_flags)&32)!=0),0,_module,_source);
(_v_1->_directives)=_directives;
(_v_1->_initial_value)=((struct _expression *)0);
(_v_1->_proxy)=((struct _expression *)0);
(_v_1->_reference)=((struct _expression *)0);
(_v_1->_extern_name)=_empty_string171;
(_v_1->_defined)=1;
(_v_1->_storage_type)=_type;
(_v_1->_redirect)=((struct _variable *)0);
(_v_1->_function)=((struct _function *)0);
return _v_1;
}

static struct _expression *_build_expression_call_p_s__compiler_p_s__node_p_s__definition_p_s__collection_of_expressions(struct _compiler *_cc, struct _node *_source, struct _definition *_definition, struct _collection_of_expressions *_arguments)
{
char *_name_1;
struct _type *_type_2;
int _lvalue_3;
struct _expression *_arg1_4;
struct _pointer_type *_pointer_5;
struct _type *_target_6;
struct _structure_type *_t_7;
struct _type *_parent_8;
struct _expression *_e_9;
struct _type *_cell_type_10;
struct _expression *_arg1_11;
struct _expression *_arg2_12;
struct _type *_t1_13;
struct _type *_t2_14;
struct _collection_of_expressions *_block_15;
struct _collection_of_arguments *_parameters_16;
struct _argument *_a_17;
struct _variable *_v_18;
struct _variable_expression *_variable_definition_19;
struct _collection_of_expressions *_block_20;
struct _definition_expression *_v_e_21;
struct _collection_of_expressions _t_22;
struct _expression *_a_23;
struct _expression *_next_24;
struct _argument *_b_25;
struct _expression *_sub_expression_26;
struct _collection_of_expressions _t_27;
_name_1=(_definition->_name);
_type_2=(_definition->_type);
_lvalue_3=(_definition->_lvalue);
switch((_definition->_opcode))
{
case 10:
{
_arg1_4=((struct _expression *)(((struct _collection *)_arguments)->_first));
_pointer_5=((struct _pointer_type *)(_arg1_4->_type));
_target_6=(_pointer_5->_target_type);
if((!((_target_6->_id)==16)))
{
_error_p_s__compiler_p_s__node_p_a_ch_p_vo(_cc,_source,_error_super_no_struct918,((void *)_target_6));
return ((struct _expression *)0);
}
else
{
_t_7=((struct _structure_type *)_target_6);
if(((void*)((struct _structure_type *)(_t_7->_parent_type))==(void*)0))
{
_error_p_s__compiler_p_s__node_p_a_ch_p_vo(_cc,_source,_error_super_no_parent921,((void *)_t_7));
return ((struct _expression *)0);
}
_parent_8=_pointer_p_s__type(((struct _type *)((struct _structure_type *)(_t_7->_parent_type))));
_e_9=((struct _expression *)_new_expression_expression_p_s__memory_bundle_p_s__type_e__expression_code_p_s__expression((_cc->_bundle),_parent_8,17,_arg1_4));
(_e_9->_lvalue)=(_arg1_4->_lvalue);
return _e_9;
}
break;
}
case 67:
{
_type_2=(((struct _pointer_type *)(((struct _expression *)(((struct _collection *)_arguments)->_first))->_type))->_target_type);
_e_9=((struct _expression *)_new_definition_expression_p_s__memory_bundle_p_s__definition_p_s__collection_of_expressions((_cc->_bundle),_definition,_arguments));
(_e_9->_lvalue)=(((_type_2->_flags)&32)!=0);
(_e_9->_type)=_effective_type_p_s__type(_type_2);
return _e_9;
break;
}
case 68:
{
_type_2=(((struct _expression *)(((struct _collection *)_arguments)->_first))->_type);
_cell_type_10=_check_array_p_s__compiler_p_s__node_p_s__type(_cc,_source,_type_2);
if(((void*)_cell_type_10==(void*)0))
{
return ((struct _expression *)0);
}
_e_9=((struct _expression *)_new_definition_expression_p_s__memory_bundle_p_s__definition_p_s__collection_of_expressions((_cc->_bundle),_definition,_arguments));
(_e_9->_lvalue)=(((_cell_type_10->_flags)&32)!=0);
(_e_9->_type)=_effective_type_p_s__type(_cell_type_10);
return _e_9;
break;
}
case 69:
case 70:
case 72:
case 73:
case 74:
case 75:
{
_type_2=(((struct _expression *)(((struct _collection *)_arguments)->_first))->_type);
break;
}
case 71:
{
_arg1_11=((struct _expression *)(((struct _collection *)_arguments)->_first));
_arg2_12=((struct _expression *)(((struct _collection *)_arguments)->_last));
if((((_arg1_11->_type)->_id)==2))
{
_error_p_s__compiler_p_s__node_p_a_ch_p_vo(_cc,_source,_error_unknown_size926,((void *)(_arg1_11->_type)));
return ((struct _expression *)0);
}
_t1_13=(_arg1_11->_type);
_t2_14=(_arg2_12->_type);
if((!_equals_p_s__type_p_s__type(_t2_14,_t1_13)))
{
_error_p_s__compiler_p_s__node_p_a_ch_p_vo_p_vo(_cc,_source,_error_cant_diff935,((void *)_t2_14),((void *)_t1_13));
}
break;
}
case 4:
{
if((!_define_iterator_p_s__compiler_p_s__iterator(_cc,((struct _iterator *)_definition))))
{
return ((struct _expression *)0);
}
if(((void*)(((struct _iterator *)_definition)->_yield_signature)!=(void*)0))
{
_error_p_s__compiler_p_s__node_p_a_ch(_cc,_source,_error_missing_block957);
return ((struct _expression *)0);
}
_block_15=_new_collection_of_expressions_p_s__compiler(_cc);
_parameters_16=_new_collection_of_arguments_p_s__memory_bundle((_cc->_bundle));
return ((struct _expression *)_build_iterator_call_p_s__compiler_p_s__node_p_a_ch_p_s__collection_of_expressions_p_s__iterator_p_s__collection_of_arguments_p_s__collection_of_expressions(_cc,_source,(_definition->_name),_arguments,((struct _iterator *)_definition),_parameters_16,_block_15));
break;
}
case 11:
{
if((!_define_equate_p_s__compiler_p_s__equate(_cc,((struct _equate *)_definition))))
{
return ((struct _expression *)0);
}
_set_values_of_arguments_p_s__argument_p_s__expression((((struct _equate *)_definition)->_arguments),((struct _expression *)(((struct _collection *)_arguments)->_first)));
return _expand_expression_p_s__compiler_p_s__node_p_s__expression(_cc,_source,(((struct _equate *)_definition)->_value));
break;
}
case 3:
{
if(((void*)_type_2==(void*)0))
{
if((!_define_function_p_s__compiler_p_s__function(_cc,((struct _function *)_definition))))
{
return ((struct _expression *)0);
}
_type_2=(((struct _definition *)((struct _function *)_definition))->_type);
}
break;
}
case 1:
{
if((!_define_variable_p_s__compiler_p_s__variable(_cc,((struct _variable *)_definition))))
{
return ((struct _expression *)0);
}
_type_2=(_definition->_type);
_lvalue_3=(_definition->_lvalue);
break;
}
case 15:
{
_type_2=((struct _type *)0);
_a_17=(((struct _extension *)_definition)->_arguments);
while(((void*)_a_17!=(void*)0))
{
if((_a_17->_is_return))
{
_type_2=(((struct _pointer_type *)(((struct _definition *)_a_17)->_type))->_target_type);
}
_a_17=((struct _argument *)(((struct _element *)_a_17)->_next_sibling));
}
_v_18=_new_variable_p_s__memory_bundle_p_a_ch_p_s__type_p_s__set_of_directives_p_vo_p_s__node((_cc->_bundle),T1121,_type_2,(_cc->_current_directives),((void *)(_cc->_module)),_source);
_variable_definition_19=_new_variable_expression_p_s__memory_bundle_p_s__type_e__expression_code_p_s__variable((_cc->_bundle),((struct _type *)_void_type114),9,_v_18);
_block_20=(_cc->_current_block);
if(((void*)_block_20==(void*)0))
{
_add_p_s__compiler_p_s__expression(_cc,((struct _expression *)_variable_definition_19));
}
else
{
_append_p_s__collection_p_s__element(((struct _collection *)_block_20),((struct _element *)_variable_definition_19));
}
_v_e_21=_new_definition_expression_p_s__memory_bundle_p_s__definition_p_s__collection_of_expressions((_cc->_bundle),((struct _definition *)_v_18),((struct _collection_of_expressions *)(&_empty_collection351)));
_collection_of_expressions_p_s__collection_of_expressions((&_t_22));
_a_23=((struct _expression *)(((struct _collection *)_arguments)->_first));
_next_24=((struct _expression *)0);
if(((void*)_a_23!=(void*)0))
{
_next_24=((struct _expression *)(((struct _element *)_a_23)->_next_sibling));
}
_b_25=(((struct _extension *)_definition)->_arguments);
while(((void*)_b_25!=(void*)0))
{
if((_b_25->_is_return))
{
_append_p_s__collection_p_s__element(((struct _collection *)(&_t_22)),((struct _element *)_v_e_21));
}
else
{
_append_p_s__collection_p_s__element(((struct _collection *)(&_t_22)),((struct _element *)_a_23));
_a_23=_next_24;
if(((void*)_a_23!=(void*)0))
{
_next_24=((struct _expression *)(((struct _element *)_a_23)->_next_sibling));
}
}
_b_25=((struct _argument *)(((struct _element *)_b_25)->_next_sibling));
}
_sub_expression_26=_build_expression_call_p_s__compiler_p_s__node_p_s__definition_p_s__collection_of_expressions(_cc,_source,(((struct _extension *)_definition)->_definition),(&_t_22));
if(((void*)_sub_expression_26==(void*)0))
{
return ((struct _expression *)0);
}
_collection_of_expressions_p_s__collection_of_expressions((&_t_27));
_append_p_s__collection_p_s__element(((struct _collection *)(&_t_27)),((struct _element *)_sub_expression_26));
_append_p_s__collection_p_s__element(((struct _collection *)(&_t_27)),((struct _element *)_new_definition_expression_p_s__memory_bundle_p_s__definition_p_s__collection_of_expressions((_cc->_bundle),((struct _definition *)_v_18),((struct _collection_of_expressions *)(&_empty_collection351)))));
return ((struct _expression *)_new_sequence_expression_p_s__memory_bundle_p_s__collection_of_expressions((_cc->_bundle),(&_t_27)));
break;
}
}
return _build_definition_expression_p_s__compiler_p_s__node_p_s__definition_p_s__collection_of_expressions_p_s__type_bo(_cc,_source,_definition,_arguments,_type_2,_lvalue_3);
}

static struct _expression *_build_expression_call_p_s__compiler_p_s__node_p_a_ch_p_s__collection_of_expressions(struct _compiler *_cc, struct _node *_source, char *_name, struct _collection_of_expressions *_arguments)
{
struct _definition *_definition_1;
_definition_1=_find_definition_p_s__compiler_p_s__node_p_a_ch_p_s__collection_of_expressions(_cc,_source,_name,_arguments);
if(((void*)_definition_1==(void*)0))
{
_error_p_s__compiler_p_s__node_p_a_ch_p_vo_p_vo(_cc,_source,_error_def_not_found910,((void *)_name),((void *)_arguments));
return ((struct _expression *)0);
}
return _build_expression_call_p_s__compiler_p_s__node_p_s__definition_p_s__collection_of_expressions(_cc,_source,_definition_1,_arguments);
}

static struct _signature *_new_signature_from_p_s__compiler_p_s__collection_of_expressions(struct _compiler *_cc, struct _collection_of_expressions *_arguments)
{
int _size_1;
struct _signature *_signature_2;
int _i_3;
struct _element *_e_4;
_size_1=_size_p_s__collection(((struct _collection *)_arguments));
_signature_2=_new_signature_p_s__memory_bundle_in((_cc->_bundle),_size_1);
_i_3=0;
_e_4=(((struct _collection *)_arguments)->_first);
while(((void*)_e_4!=(void*)0))
{
_set_type_p_s__signature_in_p_s__type(_signature_2,_i_3,(((struct _expression *)_e_4)->_type));
(++_i_3);
_e_4=(_e_4->_next_sibling);
}
return _signature_2;
}

static struct _type *_check_initializer_type_p_s__compiler_p_s__node_p_s__type(struct _compiler *_cc, struct _node *_source, struct _type *_type)
{
int _ok_1;
struct _type *_tt_2;
_ok_1=1;
if(((_type->_id)==15))
{
_tt_2=(((struct _pointer_type *)_type)->_target_type);
_ok_1=((((_tt_2->_id)==16)|((_tt_2->_id)==17))|((_tt_2->_id)==18));
}
else
{
_ok_1=0;
}
if((!_ok_1))
{
_error_p_s__compiler_p_s__node_p_a_ch(_cc,_source,_error_invalid_initializer_type1188);
return ((struct _type *)0);
}
return _tt_2;
}

static struct _collection_expression *_get_initializer_p_s__compiler_p_s__initializer_node(struct _compiler *_cc, struct _initializer_node *_source)
{
struct _node *_source_type_1;
struct _type *_type_2;
struct _type *_tt_3;
struct _collection *_source_arguments_4;
struct _collection_of_expressions *_arguments_5;
struct _expression *_e_6;
_source_type_1=((struct _node *)(((struct _binary_node *)_source)->_value_1));
_type_2=_get_type_p_s__compiler_p_s__node(_cc,_source_type_1);
if(((void*)_type_2==(void*)0))
{
return ((struct _collection_expression *)0);
}
_tt_3=_check_initializer_type_p_s__compiler_p_s__node_p_s__type(_cc,_source_type_1,_type_2);
if(((void*)_tt_3==(void*)0))
{
return ((struct _collection_expression *)0);
}
_source_arguments_4=((struct _collection *)(((struct _binary_node *)_source)->_value_2));
_arguments_5=_get_list_of_expressions_p_s__compiler_p_s__collection(_cc,_source_arguments_4);
if(((void*)_arguments_5==(void*)0))
{
return ((struct _collection_expression *)0);
}
_e_6=_check_initializer_p_s__compiler_p_s__node_p_s__expression_p_s__type(_cc,((struct _node *)_source),((struct _expression *)(((struct _collection *)_arguments_5)->_first)),_tt_3);
if(((void*)_e_6!=(void*)0))
{
_error_p_s__compiler_p_s__node_p_a_ch(_cc,((struct _node *)_source),_error_too_many_arguments1194);
return ((struct _collection_expression *)0);
}
return _new_collection_expression_p_s__memory_bundle_p_s__type_e__expression_code_p_s__collection_of_expressions((_cc->_bundle),_type_2,5,_arguments_5);
}

static void _append_p_s__element_p_s__element(struct _element *_e1, struct _element *_e2)
{
(_e1->_next_sibling)=_e2;
}

static struct _expression *_get_expression_p_s__compiler_p_s__node(struct _compiler *_cc, struct _node *_node)
{
struct _node *_source_expression_1;
struct _expression *_value_2;
struct _collection_of_rule_nodes *_rules_3;
struct _collection _iil_4;
int _ok_5;
struct _element *_e_6;
struct _node *_condition_7;
struct _collection *_block_8;
struct _expression *_condition_9;
struct _collection_of_expressions *_action_10;
struct _expression *_condition_11;
struct _collection_of_expressions *_block_12;
struct _collection_of_expressions *_block_13;
struct _expression *_expression_14;
struct _type *_type_15;
struct _collection _sil_16;
struct _element *_e_17;
struct _collection_of_values *_values_18;
struct _collection *_block_19;
struct _collection_of_expressions *_block_20;
struct _collection _cl_21;
struct _element *_e_22;
struct _expression *_ce_23;
struct _type *_constant_type_24;
int _value_25;
struct _variable *_variable_26;
struct _equate *_equate_27;
struct _iterator *_iterator_28;
struct _redirect_type *_redirect_29;
char *_name_30;
struct _collection *_source_arguments_31;
struct _collection_of_expressions *_arguments_32;
struct _node *_source_expression_33;
char *_name_34;
struct _collection *_source_arguments_35;
struct _collection *_source_parameters_36;
struct _collection *_source_block_37;
struct _collection_of_expressions *_arguments_38;
struct _definition *_definition_39;
struct _signature *_signature_40;
struct _collection_of_arguments *_parameters_41;
int _size_42;
int _i_43;
struct _element *_e_44;
char *_arg_name_45;
int _in_equate_46;
struct _collection_of_expressions *_block_47;
char *_name_48;
struct _collection *_source_arguments_49;
struct _collection_of_expressions *_arguments_50;
struct _signature *_signature_51;
struct _signature *_current_signature_52;
struct _collection *_source_arguments_53;
int (_returned_arguments_54)[1];
struct _collection_of_arguments *_arguments_55;
char *_name_56;
struct _signature *_signature_57;
struct _definition *_definition_58;
struct _type *_type_59;
struct _node *_source_type_60;
struct _node *_source_expression_61;
struct _expression *_e_62;
struct _type *_type_63;
int _v_64;
int _lvalue_65;
struct _type *_t_66;
struct _expression *_reference_67;
struct _collection_of_expressions *_arguments_68;
struct _type *_type_69;
struct _function_type *_function_type_70;
struct _signature *_signature_71;
struct _type *_returned_type_72;
int _i_73;
struct _expression *_e_74;
struct _expression *_e1_75;
struct _expression *_e2_76;
struct _expression *_e3_77;
struct _type *_type_78;
switch((_node->_code))
{
case 20:
{
_source_expression_1=((struct _node *)(((struct _unary_node *)((struct _returned_node *)_node))->_value_1));
if(((void*)_source_expression_1!=(void*)0))
{
_value_2=_get_expression_p_s__compiler_p_s__node(_cc,_source_expression_1);
if(((void*)_value_2==(void*)0))
{
return ((struct _expression *)0);
}
}
else
{
_value_2=((struct _expression *)0);
}
_check_return_p_s__compiler_p_s__node_p_s__expression(_cc,((struct _node *)((struct _returned_node *)_node)),_value_2);
return ((struct _expression *)_new_expression_expression_p_s__memory_bundle_p_s__type_e__expression_code_p_s__expression((_cc->_bundle),((struct _type *)_void_type114),11,_value_2));
break;
}
case 21:
{
_rules_3=((struct _collection_of_rule_nodes *)(((struct _unary_node *)((struct _condition_node *)_node))->_value_1));
_initialize_p_s__collection((&_iil_4));
_ok_5=1;
_e_6=(((struct _collection *)_rules_3)->_first);
while(((void*)_e_6!=(void*)0))
{
_condition_7=((struct _node *)(((struct _binary_node *)((struct _rule_node *)_e_6))->_value_1));
_block_8=((struct _collection *)(((struct _binary_node *)((struct _rule_node *)_e_6))->_value_2));
_condition_9=((struct _expression *)0);
if(((void*)_condition_7!=(void*)0))
{
_condition_9=_get_condition_p_s__compiler_p_s__node(_cc,_condition_7);
_ok_5&=((void*)_condition_9!=(void*)0);
}
_action_10=_get_block_p_s__compiler_p_s__collection(_cc,_block_8);
_ok_5&=((void*)_action_10!=(void*)0);
if(_ok_5)
{
_append_p_s__collection_p_s__element((&_iil_4),((struct _element *)_new_condition_p_s__memory_bundle_p_s__expression_p_s__collection((_cc->_bundle),_condition_9,((struct _collection *)_action_10))));
}
_e_6=(_e_6->_next_sibling);
}
return ((struct _expression *)(_ok_5?_new_condition_expression_p_s__memory_bundle_p_s__condition_item((_cc->_bundle),((struct _condition_item *)((&_iil_4)->_first))):((struct _condition_expression *)0)));
break;
}
case 22:
{
_condition_11=_get_condition_p_s__compiler_p_s__node(_cc,((struct _node *)(((struct _binary_node *)((struct _rule_node *)_node))->_value_1)));
(++(_cc->_current_loop_level));
_block_12=_get_block_p_s__compiler_p_s__collection(_cc,((struct _collection *)(((struct _binary_node *)((struct _rule_node *)_node))->_value_2)));
(--(_cc->_current_loop_level));
return ((struct _expression *)((((void*)_condition_11!=(void*)0)&&((void*)_block_12!=(void*)0))?_new_loop_expression_p_s__memory_bundle_p_s__expression_p_s__collection_of_expressions((_cc->_bundle),_condition_11,_block_12):((struct _loop_expression *)0)));
break;
}
case 23:
{
(++(_cc->_current_loop_level));
_block_13=_get_block_p_s__compiler_p_s__collection(_cc,((struct _collection *)(((struct _unary_node *)((struct _loop_node *)_node))->_value_1)));
(--(_cc->_current_loop_level));
return ((struct _expression *)(((void*)_block_13!=(void*)0)?_new_repeat_expression_p_s__memory_bundle_p_s__collection_of_expressions((_cc->_bundle),_block_13):((struct _rept_expression *)0)));
break;
}
case 24:
{
_expression_14=_get_expression_p_s__compiler_p_s__node(_cc,((struct _node *)(((struct _binary_node *)((struct _selectors_node *)_node))->_value_1)));
if(((void*)_expression_14==(void*)0))
{
return ((struct _expression *)0);
}
_type_15=(_expression_14->_type);
_initialize_p_s__collection((&_sil_16));
_e_17=(((struct _collection *)((struct _collection_of_selectors *)(((struct _binary_node *)((struct _selectors_node *)_node))->_value_2)))->_first);
while(((void*)_e_17!=(void*)0))
{
_values_18=((struct _collection_of_values *)(((struct _binary_node *)((struct _selector_node *)_e_17))->_value_1));
_block_19=((struct _collection *)(((struct _binary_node *)((struct _selector_node *)_e_17))->_value_2));
_block_20=_get_block_p_s__compiler_p_s__collection(_cc,_block_19);
if(((void*)_values_18!=(void*)0))
{
_initialize_p_s__collection((&_cl_21));
_e_22=(((struct _collection *)_values_18)->_first);
while(((void*)_e_22!=(void*)0))
{
_ce_23=_get_expression_p_s__compiler_p_s__node(_cc,((struct _node *)_e_22));
if(((void*)_ce_23==(void*)0))
{
return ((struct _expression *)0);
}
_constant_type_24=(_ce_23->_type);
if((!_match_p_s__type_p_s__type(_constant_type_24,_type_15)))
{
_error_p_s__compiler_p_s__node_p_a_ch_p_vo_p_vo(_cc,((struct _node *)_e_22),_error_case_type816,((void *)_constant_type_24),((void *)_type_15));
}
else
{
_value_25=0;
if((((_constant_type_24->_flags)&8)!=0))
{
_value_25=_get_constant_integer_p_s__compiler_p_s__node_p_s__expression(_cc,((struct _node *)_e_22),_ce_23);
}
else if(((_constant_type_24->_id)==4))
{
_value_25=((int )((unsigned char )_get_constant_char_p_s__compiler_p_s__node_p_s__expression(_cc,((struct _node *)_e_22),_ce_23)));
}
else if(((_constant_type_24->_id)==5))
{
_value_25=((int )((unsigned short )_get_constant_wchar_p_s__compiler_p_s__node_p_s__expression(_cc,((struct _node *)_e_22),_ce_23)));
}
else
{
_error_p_s__compiler_p_s__node_p_a_ch(_cc,((struct _node *)_e_22),_error_case_not_constant827);
}
_append_p_s__collection_p_s__element((&_cl_21),((struct _element *)_new_constant_p_s__memory_bundle_in((_cc->_bundle),_value_25)));
}
_e_22=(_e_22->_next_sibling);
}
if(((void*)_block_20!=(void*)0))
{
_append_p_s__collection_p_s__element((&_sil_16),((struct _element *)_new_switch_item_p_s__memory_bundle_p_s__collection_p_s__collection((_cc->_bundle),(&_cl_21),((struct _collection *)_block_20))));
}
}
else
{
if(((void*)_block_20!=(void*)0))
{
_append_p_s__collection_p_s__element((&_sil_16),((struct _element *)_new_switch_item_p_s__memory_bundle_p_s__collection((_cc->_bundle),((struct _collection *)_block_20))));
}
}
_e_17=(_e_17->_next_sibling);
}
return ((struct _expression *)_new_switch_expression_p_s__memory_bundle_p_s__expression_p_s__collection((_cc->_bundle),_expression_14,(&_sil_16)));
break;
}
case 5:
{
_variable_26=_declare_variable_p_s__compiler_p_s__variable_node_bo_p_s__set_of_directives_bo(_cc,((struct _variable_node *)_node),0,(_cc->_current_directives),1);
if(((void*)_variable_26==(void*)0))
{
return ((struct _expression *)0);
}
if((!_define_variable_p_s__compiler_p_s__variable(_cc,_variable_26)))
{
return ((struct _expression *)0);
}
return ((struct _expression *)_new_variable_expression_p_s__memory_bundle_p_s__type_e__expression_code_p_s__variable((_cc->_bundle),((struct _type *)_void_type114),9,_variable_26));
break;
}
case 6:
{
_equate_27=_declare_equate_p_s__compiler_p_s__equate_node_bo_bo(_cc,((struct _equate_node *)_node),0,1);
if(((void*)_equate_27==(void*)0))
{
return ((struct _expression *)0);
}
return _new_ignore_expression_p_s__memory_bundle((_cc->_bundle));
break;
}
case 7:
{
_iterator_28=_declare_iterator_p_s__compiler_p_s__iterator_node_bo_bo(_cc,((struct _iterator_node *)_node),0,1);
if(((void*)_iterator_28==(void*)0))
{
return ((struct _expression *)0);
}
return _new_ignore_expression_p_s__memory_bundle((_cc->_bundle));
break;
}
case 2:
{
_redirect_29=_new_redirect_type_p_s__memory_bundle_bo_p_vo_p_s__redirect_node((_cc->_bundle),0,((void *)(_cc->_module)),((struct _redirect_node *)_node));
_add_local_type_p_s__compiler_p_s__named_type(_cc,((struct _named_type *)_redirect_29));
return _new_ignore_expression_p_s__memory_bundle((_cc->_bundle));
break;
}
case 15:
{
return ((struct _expression *)_new_litteral_long_expression_p_s__memory_bundle_p_s__type_dw((_cc->_bundle),((struct _type *)_int_type144),(((struct _long_node *)_node)->_value)));
break;
}
case 11:
{
return ((struct _expression *)_new_litteral_string_expression_p_s__memory_bundle_p_s__type_p_a_ch((_cc->_bundle),_string_type152,(((struct _string_node *)_node)->_value)));
break;
}
case 12:
{
return ((struct _expression *)_new_litteral_string_expression_p_s__memory_bundle_p_s__type_p_a_ch((_cc->_bundle),_wstring_type156,(((struct _string_node *)_node)->_value)));
break;
}
case 13:
{
return ((struct _expression *)_new_litteral_long_expression_p_s__memory_bundle_p_s__type_dw((_cc->_bundle),((struct _type *)_char_type117),(((struct _long_node *)_node)->_value)));
break;
}
case 14:
{
return ((struct _expression *)_new_litteral_long_expression_p_s__memory_bundle_p_s__type_dw((_cc->_bundle),((struct _type *)_wchar_type120),(((struct _long_node *)_node)->_value)));
break;
}
case 27:
{
_name_30=((char *)(((struct _binary_node *)((struct _definition_node *)_node))->_value_1));
_source_arguments_31=((struct _collection *)(((struct _binary_node *)((struct _definition_node *)_node))->_value_2));
_arguments_32=_get_list_of_expressions_p_s__compiler_p_s__collection(_cc,_source_arguments_31);
if(((void*)_arguments_32==(void*)0))
{
return ((struct _expression *)0);
}
return _build_expression_call_p_s__compiler_p_s__node_p_a_ch_p_s__collection_of_expressions(_cc,((struct _node *)((struct _definition_node *)_node)),_name_30,_arguments_32);
break;
}
case 28:
{
_source_expression_33=((struct _node *)(((struct _ternary_node *)((struct _iterator_call_node *)_node))->_value_1));
if(((_source_expression_33->_code)!=27))
{
return ((struct _expression *)0);
}
_name_34=((char *)(((struct _binary_node *)((struct _definition_node *)_source_expression_33))->_value_1));
_source_arguments_35=((struct _collection *)(((struct _binary_node *)((struct _definition_node *)_source_expression_33))->_value_2));
_source_parameters_36=((struct _collection *)(((struct _ternary_node *)((struct _iterator_call_node *)_node))->_value_2));
_source_block_37=((struct _collection *)(((struct _ternary_node *)((struct _iterator_call_node *)_node))->_value_3));
_arguments_38=_get_list_of_expressions_p_s__compiler_p_s__collection(_cc,_source_arguments_35);
if(((void*)_arguments_38==(void*)0))
{
return ((struct _expression *)0);
}
_definition_39=_find_definition_p_s__compiler_p_s__node_p_a_ch_p_s__collection_of_expressions(_cc,_source_expression_33,_name_34,_arguments_38);
if(((void*)_definition_39==(void*)0))
{
_error_p_s__compiler_p_s__node_p_a_ch_p_vo_p_vo(_cc,((struct _node *)((struct _iterator_call_node *)_node)),_error_def_not_found910,((void *)_name_34),((void *)_arguments_38));
return ((struct _expression *)0);
}
if(((_definition_39->_opcode)!=4))
{
_error_p_s__compiler_p_s__node_p_a_ch_p_vo(_cc,((struct _node *)((struct _iterator_call_node *)_node)),_error_not_iterator1142,((void *)_name_34));
return ((struct _expression *)0);
}
if((!_define_iterator_p_s__compiler_p_s__iterator(_cc,((struct _iterator *)_definition_39))))
{
return ((struct _expression *)0);
}
_signature_40=(((struct _iterator *)_definition_39)->_yield_signature);
if(((void*)_signature_40==(void*)0))
{
_error_p_s__compiler_p_s__node_p_a_ch_p_vo(_cc,((struct _node *)((struct _iterator_call_node *)_node)),_error_unexpected_block1145,((void *)_name_34));
return ((struct _expression *)0);
}
_parameters_41=_new_collection_of_arguments_p_s__compiler(_cc);
_size_42=(_signature_40->_number_of_types);
if((_size_42>0))
{
_i_43=0;
_e_44=(_source_parameters_36->_first);
while(((void*)_e_44!=(void*)0))
{
_arg_name_45=(((struct _string_node *)_e_44)->_value);
if((_i_43==_size_42))
{
_error_p_s__compiler_p_s__node_p_a_ch(_cc,((struct _node *)((struct _iterator_call_node *)_node)),_error_too_many_vars1152);
return ((struct _expression *)0);
}
else if((_i_43<_size_42))
{
_append_p_s__collection_p_s__element(((struct _collection *)_parameters_41),((struct _element *)_new_argument_p_s__memory_bundle_p_a_ch_p_s__type_bo_p_vo_p_s__node((_cc->_bundle),_arg_name_45,_type_p_s__signature_in(_signature_40,_i_43),0,((void *)(_cc->_module)),((struct _node *)((struct _string_node *)_e_44)))));
}
(++_i_43);
_e_44=(_e_44->_next_sibling);
}
if((_i_43<_size_42))
{
_error_p_s__compiler_p_s__node_p_a_ch(_cc,((struct _node *)((struct _iterator_call_node *)_node)),_error_too_few_vars1154);
return ((struct _expression *)0);
}
}
_open_scope_p_s__compiler_p_s__scope(_cc,(_cc->_current_scope));
_in_equate_46=(_cc->_current_inside_equate);
(_cc->_current_inside_equate)=1;
_add_local_definitions_p_s__compiler_p_s__argument(_cc,((struct _argument *)(((struct _collection *)_parameters_41)->_first)));
_block_47=_get_block_p_s__compiler_p_s__collection(_cc,_source_block_37);
(_cc->_current_inside_equate)=_in_equate_46;
_close_scope_p_s__compiler(_cc);
if(((void*)_block_47==(void*)0))
{
return ((struct _expression *)0);
}
return ((struct _expression *)_build_iterator_call_p_s__compiler_p_s__node_p_a_ch_p_s__collection_of_expressions_p_s__iterator_p_s__collection_of_arguments_p_s__collection_of_expressions(_cc,((struct _node *)((struct _iterator_call_node *)_node)),_name_34,_arguments_38,((struct _iterator *)_definition_39),_parameters_41,_block_47));
break;
}
case 29:
{
_name_48=((char *)(((struct _binary_node *)((struct _definition_node *)_node))->_value_1));
_source_arguments_49=((struct _collection *)(((struct _binary_node *)((struct _definition_node *)_node))->_value_2));
_arguments_50=_get_list_of_expressions_p_s__compiler_p_s__collection(_cc,_source_arguments_49);
if(((void*)_arguments_50==(void*)0))
{
return ((struct _expression *)0);
}
if(((void*)(_cc->_current_iterator)==(void*)0))
{
_error_p_s__compiler_p_s__node_p_a_ch(_cc,((struct _node *)((struct _definition_node *)_node)),_error_invalid_yield1161);
return ((struct _expression *)0);
}
_signature_51=_new_signature_from_p_s__compiler_p_s__collection_of_expressions(_cc,_arguments_50);
_current_signature_52=((_cc->_current_iterator)->_yield_signature);
if(((void*)_current_signature_52!=(void*)0))
{
_signature_51=_common_p_s__signature_p_s__signature(_current_signature_52,_signature_51);
if(((void*)_signature_51==(void*)0))
{
_error_p_s__compiler_p_s__node_p_a_ch(_cc,((struct _node *)((struct _definition_node *)_node)),_error_yield_signature1169);
return ((struct _expression *)0);
}
}
((_cc->_current_iterator)->_yield_signature)=_signature_51;
return ((struct _expression *)_new_yield_expression_p_s__memory_bundle_p_s__collection_of_expressions_p_s__iterator((_cc->_bundle),_arguments_50,(_cc->_current_iterator)));
break;
}
case 25:
{
_source_arguments_53=((struct _collection *)(((struct _binary_node *)((struct _function_reference_node *)_node))->_value_2));
_arguments_55=_get_arguments_p_s__compiler_p_s__collection_p_a_in(_cc,_source_arguments_53,((int *)_returned_arguments_54));
if(((void*)_arguments_55==(void*)0))
{
return ((struct _expression *)0);
}
if(((_returned_arguments_54[0])!=0))
{
_error_p_s__compiler_p_s__node_p_a_ch(_cc,((struct _node *)((struct _function_reference_node *)_node)),_error_no_return_arg_in_references1174);
return ((struct _expression *)0);
}
_name_56=((char *)(((struct _binary_node *)((struct _function_reference_node *)_node))->_value_1));
if((!_check_arguments_p_s__compiler_p_s__node_p_a_ch_p_s__collection_of_arguments(_cc,((struct _node *)((struct _function_reference_node *)_node)),_name_56,_arguments_55)))
{
return ((struct _expression *)0);
}
_signature_57=_new_signature_from_p_s__compiler_p_s__collection_of_arguments(_cc,_arguments_55);
_definition_58=_find_definition_p_s__compiler_p_a_ch_p_s__signature(_cc,_name_56,_signature_57);
if(((void*)_definition_58==(void*)0))
{
_error_p_s__compiler_p_s__node_p_a_ch_p_vo(_cc,((struct _node *)((struct _function_reference_node *)_node)),_error_function_not_found1179,((void *)_name_56));
return ((struct _expression *)0);
}
if(((_definition_58->_opcode)!=3))
{
_error_p_s__compiler_p_s__node_p_a_ch_p_vo(_cc,((struct _node *)((struct _function_reference_node *)_node)),_error_no_function1181,((void *)_name_56));
return ((struct _expression *)0);
}
if(((void*)(((struct _definition *)((struct _function *)_definition_58))->_type)==(void*)0))
{
if((!_define_function_p_s__compiler_p_s__function(_cc,((struct _function *)_definition_58))))
{
return ((struct _expression *)0);
}
}
return ((struct _expression *)_new_function_expression_p_s__memory_bundle_p_s__type_e__expression_code_p_s__function((_cc->_bundle),_reference_type_p_s__function(((struct _function *)_definition_58)),4,((struct _function *)_definition_58)));
break;
}
case 26:
{
_type_59=_get_type_p_s__compiler_p_s__node(_cc,((struct _node *)(((struct _unary_node *)((struct _size_node *)_node))->_value_1)));
if(((void*)_type_59==(void*)0))
{
return ((struct _expression *)0);
}
_check_size_p_s__compiler_p_s__node_p_s__type(_cc,((struct _node *)((struct _size_node *)_node)),_type_59);
return ((struct _expression *)_new_litteral_long_expression_p_s__memory_bundle_p_s__type_dw((_cc->_bundle),((struct _type *)_dword_type132),_size_p_s__type(_type_59)));
break;
}
case 19:
{
return ((struct _expression *)_get_initializer_p_s__compiler_p_s__initializer_node(_cc,((struct _initializer_node *)_node)));
break;
}
case 30:
{
_source_type_60=((struct _node *)(((struct _binary_node *)((struct _cast_node *)_node))->_value_2));
_source_expression_61=((struct _node *)(((struct _binary_node *)((struct _cast_node *)_node))->_value_1));
_e_62=_get_expression_p_s__compiler_p_s__node(_cc,_source_expression_61);
_type_63=_get_registerable_type_p_s__compiler_p_s__node(_cc,_source_type_60);
if((((void*)_e_62==(void*)0)||((void*)_type_63==(void*)0)))
{
return ((struct _expression *)0);
}
if((((_e_62->_type)->_id)==1))
{
_error_p_s__compiler_p_s__node_p_a_ch(_cc,((struct _node *)((struct _cast_node *)_node)),_error_cast_void1200);
return ((struct _expression *)0);
}
else if(((_e_62->_opcode)==1))
{
_v_64=(((struct _long_expression *)_e_62)->_value);
switch((_type_63->_id))
{
case 4:
{
_v_64=((int )((char )_v_64));
break;
}
case 5:
{
_v_64=((int )((short )_v_64));
break;
}
case 10:
{
_v_64=((int )((char )_v_64));
break;
}
case 6:
{
_v_64=(_v_64&255);
break;
}
case 11:
{
_v_64=((int )((short )_v_64));
break;
}
case 7:
{
_v_64=(_v_64&65535);
break;
}
case 3:
{
if((_v_64!=0))
{
_v_64=1;
}
break;
}
}
return ((struct _expression *)_new_litteral_long_expression_p_s__memory_bundle_p_s__type_dw((_cc->_bundle),_type_63,((unsigned int )_v_64)));
}
else
{
_lvalue_65=(_e_62->_lvalue);
_e_62=((struct _expression *)_new_expression_expression_p_s__memory_bundle_p_s__type_e__expression_code_p_s__expression((_cc->_bundle),_type_63,17,_e_62));
if(_lvalue_65)
{
_t_66=(_e_62->_type);
if(((!((_t_66->_id)==15))||(!((((((struct _pointer_type *)_t_66)->_target_type)->_flags)&2)!=0))))
{
_lvalue_65=0;
}
}
(_e_62->_lvalue)=_lvalue_65;
return _e_62;
}
break;
}
case 31:
{
_reference_67=_get_expression_p_s__compiler_p_s__node(_cc,((struct _node *)(((struct _binary_node *)((struct _invoke_node *)_node))->_value_1)));
_arguments_68=_get_list_of_expressions_p_s__compiler_p_s__collection(_cc,((struct _collection *)(((struct _binary_node *)((struct _invoke_node *)_node))->_value_2)));
if((((void*)_reference_67==(void*)0)||((void*)_arguments_68==(void*)0)))
{
return ((struct _expression *)0);
}
_type_69=(_reference_67->_type);
if(((!((_type_69->_id)==15))&&(!(((((struct _pointer_type *)_type_69)->_target_type)->_id)==19))))
{
_error_p_s__compiler_p_s__node_p_a_ch(_cc,((struct _node *)((struct _invoke_node *)_node)),_error_1st_not_function1208);
return ((struct _expression *)0);
}
_function_type_70=((struct _function_type *)(((struct _pointer_type *)_type_69)->_target_type));
_signature_71=(_function_type_70->_signature);
_returned_type_72=(_function_type_70->_returned_type);
if(((_signature_71->_number_of_types)!=_size_p_s__collection(((struct _collection *)_arguments_68))))
{
_error_p_s__compiler_p_s__node_p_a_ch(_cc,((struct _node *)((struct _invoke_node *)_node)),_error_bad_arg_number1213);
return ((struct _expression *)0);
}
_i_73=0;
_e_74=((struct _expression *)(((struct _collection *)_arguments_68)->_first));
while(((void*)_e_74!=(void*)0))
{
if((!_match_p_s__type_p_s__type((_e_74->_type),_type_p_s__signature_in(_signature_71,_i_73))))
{
_error_p_s__compiler_p_s__node_p_a_ch_p_vo_p_vo(_cc,((struct _node *)((struct _invoke_node *)_node)),_error_bad_arg_type1217,((void *)(_e_74->_type)),((void *)_type_p_s__signature_in(_signature_71,_i_73)));
}
(++_i_73);
_e_74=((struct _expression *)(((struct _element *)_e_74)->_next_sibling));
}
_append_p_s__element_p_s__element(((struct _element *)_reference_67),((struct _element *)((struct _expression *)(((struct _collection *)_arguments_68)->_first))));
return ((struct _expression *)_new_expression_expression_p_s__memory_bundle_p_s__type_e__expression_code_p_s__expression((_cc->_bundle),_returned_type_72,18,_reference_67));
break;
}
case 32:
{
_e1_75=_get_condition_p_s__compiler_p_s__node(_cc,((struct _node *)(((struct _ternary_node *)((struct _cond_node *)_node))->_value_1)));
_e2_76=_get_expression_p_s__compiler_p_s__node(_cc,((struct _node *)(((struct _ternary_node *)((struct _cond_node *)_node))->_value_2)));
_e3_77=_get_expression_p_s__compiler_p_s__node(_cc,((struct _node *)(((struct _ternary_node *)((struct _cond_node *)_node))->_value_3)));
if(((((void*)_e1_75==(void*)0)||((void*)_e2_76==(void*)0))||((void*)_e3_77==(void*)0)))
{
return ((struct _expression *)0);
}
_type_78=_common_p_s__type_p_s__type((_e2_76->_type),(_e3_77->_type));
if(((void*)_type_78==(void*)0))
{
_error_p_s__compiler_p_s__node_p_a_ch_p_vo_p_vo(_cc,((struct _node *)((struct _cond_node *)_node)),_error_incompatible_types1046,((void *)(_e2_76->_type)),((void *)(_e3_77->_type)));
return ((struct _expression *)0);
}
return ((struct _expression *)_new_condition_operator_expression_p_s__memory_bundle_p_s__type_p_s__expression_p_s__expression_p_s__expression((_cc->_bundle),_type_78,_e1_75,_e2_76,_e3_77));
break;
}
default:
{
return ((struct _expression *)0);
break;
}
}
}

static int _get_constant_integer_p_s__compiler_p_s__node(struct _compiler *_cc, struct _node *_source)
{
struct _expression *_e_1;
_e_1=_get_expression_p_s__compiler_p_s__node(_cc,_source);
if(((void*)_e_1==(void*)0))
{
return 0;
}
return _get_constant_integer_p_s__compiler_p_s__node_p_s__expression(_cc,_source,_e_1);
}

static void _collection_of_arguments_p_s__collection_of_arguments(struct _collection_of_arguments *_c)
{
_initialize_p_s__collection(((struct _collection *)_c));
}

static struct _type *_get_registerable_type_or_void_p_s__compiler_p_s__node(struct _compiler *_cc, struct _node *_source)
{
return _get_registerable_type_p_s__compiler_p_s__node(_cc,_source);
}

static struct _enumeration_item *_new_enumeration_item_p_s__memory_bundle_p_a_ch_p_s__type_in_bo_p_vo_p_s__node(struct _memory_bundle *_bundle, char *_name, struct _type *_type, int _value, int _expose, void *_module, struct _node *_source)
{
struct _enumeration_item *_item_1;
_item_1=((struct _enumeration_item *)_allocate_p_s__memory_bundle_dw(_bundle,((unsigned int)52)));
_initialize_p_s__definition_p_a_ch_p_s__signature_p_s__type_e__definition_code_bo_bo_p_vo_p_s__node(((struct _definition *)_item_1),_name,(&_empty_sig107),_type,5,0,_expose,_module,_source);
(_item_1->_value)=_value;
return _item_1;
}

static void _define_enumeration_p_s__compiler_p_s__enumeration_type(struct _compiler *_cc, struct _enumeration_type *_enumeration)
{
int _expose_1;
int _previous_exposition_2;
struct _enumeration_node *_source_3;
struct _collection_of_enumeration_item_nodes *_content_4;
int _item_value_5;
struct _element *_e_6;
char *_item_name_7;
struct _node *_source_value_8;
struct _enumeration_item *_enumeration_item_9;
if((((((struct _type *)_enumeration)->_flags)&1073741824)!=0))
{
return;
}
if((((((struct _type *)_enumeration)->_flags)&2147483648U)!=0))
{
_error_p_s__compiler_in_p_a_ch_p_vo(_cc,(((struct _named_type *)_enumeration)->_line),_error_circular_dependency_type1236,((void *)_enumeration));
return;
}
_set_resolving_p_s__type(((struct _type *)_enumeration));
_expose_1=(((((struct _type *)_enumeration)->_flags)&64)!=0);
_previous_exposition_2=(_cc->_current_exposition);
(_cc->_current_exposition)=_expose_1;
_source_3=((struct _enumeration_node *)(((struct _named_type *)_enumeration)->_source));
_content_4=((struct _collection_of_enumeration_item_nodes *)(((struct _binary_node *)_source_3)->_value_2));
if(((void*)_content_4!=(void*)0))
{
_item_value_5=0;
_e_6=(((struct _collection *)((struct _collection_of_enumeration_item_nodes *)(((struct _binary_node *)_source_3)->_value_2)))->_first);
while(((void*)_e_6!=(void*)0))
{
_item_name_7=((char *)(((struct _binary_node *)((struct _enumeration_item_node *)_e_6))->_value_1));
_source_value_8=((struct _node *)(((struct _binary_node *)((struct _enumeration_item_node *)_e_6))->_value_2));
if(((void*)_source_value_8!=(void*)0))
{
_item_value_5=_get_constant_integer_p_s__compiler_p_s__node(_cc,_source_value_8);
}
_enumeration_item_9=_new_enumeration_item_p_s__memory_bundle_p_a_ch_p_s__type_in_bo_p_vo_p_s__node((_cc->_bundle),_item_name_7,((struct _type *)_enumeration),_item_value_5,_expose_1,((void *)(_cc->_module)),((struct _node *)((struct _enumeration_item_node *)_e_6)));
_add_global_definition_p_s__compiler_p_s__definition_bo(_cc,((struct _definition *)_enumeration_item_9),_expose_1);
(++_item_value_5);
_e_6=(_e_6->_next_sibling);
}
}
(_enumeration->_defined)=1;
(_cc->_current_exposition)=_previous_exposition_2;
_set_resolved_p_s__type(((struct _type *)_enumeration));
}

static struct _structure_type *_get_structure_p_s__compiler_p_s__node_bo(struct _compiler *_cc, struct _node *_source, int _is_union)
{
struct _type *_type_1;
_type_1=_get_type_p_s__compiler_p_s__node(_cc,_source);
if(((void*)_type_1==(void*)0))
{
return ((struct _structure_type *)0);
}
if(_is_union)
{
if((!((_type_1->_id)==17)))
{
_error_p_s__compiler_p_s__node_p_a_ch_p_vo(_cc,_source,_error_not_an_union1256,((void *)_type_1));
return ((struct _structure_type *)0);
}
}
else
{
if((!((_type_1->_id)==16)))
{
_error_p_s__compiler_p_s__node_p_a_ch_p_vo(_cc,_source,_error_not_a_struct1258,((void *)_type_1));
return ((struct _structure_type *)0);
}
}
return ((struct _structure_type *)_type_1);
}

static struct _attribute *_new_attribute_p_s__memory_bundle_p_a_ch_p_s__type_bo_p_s__structure_type_p_vo_p_s__attribute_node(struct _memory_bundle *_bundle, char *_name, struct _type *_type, int _expose, struct _structure_type *_structure, void *_module, struct _attribute_node *_source)
{
struct _attribute *_a_1;
_a_1=((struct _attribute *)_allocate_p_s__memory_bundle_dw(_bundle,((unsigned int)56)));
_initialize_p_s__definition_p_a_ch_p_s__signature_p_s__type_e__definition_code_bo_bo_p_vo_p_s__node(((struct _definition *)_a_1),_name,(_structure->_signature),_effective_type_p_s__type(_type),2,(((_type->_flags)&32)!=0),_expose,_module,((struct _node *)_source));
(_a_1->_storage_type)=_type;
(_a_1->_structure)=_structure;
return _a_1;
}

static void _define_p_s__structure_type_p_s__structure_type_p_s__collection(struct _structure_type *_t, struct _structure_type *_parent, struct _collection *_attributes)
{
struct _attribute *_attr_1;
unsigned int _sum_2;
unsigned int _align_max_3;
unsigned int _align_4;
struct _type *_st_5;
unsigned int _align_6;
unsigned int _align_7;
struct _type *_st_8;
unsigned int _align_9;
_attr_1=((struct _attribute *)(_attributes->_first));
(_t->_defined)=1;
(_t->_parent_type)=((struct _type *)_parent);
(_t->_attributes)=_attr_1;
_sum_2=((unsigned int)0);
_align_max_3=((unsigned int)1);
if(((((struct _type *)_t)->_id)==16))
{
if(((void*)_parent!=(void*)0))
{
_align_4=_align_p_s__type(((struct _type *)_parent));
if((_align_4>_align_max_3))
{
_align_max_3=_align_4;
}
(--_align_4);
_sum_2+=_align_4;
_sum_2&=(~_align_4);
_sum_2+=_size_p_s__type(((struct _type *)_parent));
}
while(((void*)_attr_1!=(void*)0))
{
_st_5=(_attr_1->_storage_type);
_align_6=_align_p_s__type(_st_5);
if((_align_6>_align_max_3))
{
_align_max_3=_align_6;
}
(--_align_6);
_sum_2+=_align_6;
_sum_2&=(~_align_6);
_sum_2+=_size_p_s__type(_st_5);
_attr_1=((struct _attribute *)(((struct _element *)_attr_1)->_next_sibling));
}
}
else
{
if(((void*)_parent!=(void*)0))
{
_align_7=_align_p_s__type(((struct _type *)_parent));
if((_align_7>_align_max_3))
{
_align_max_3=_align_7;
}
_sum_2=_max_dw_dw(_sum_2,_size_p_s__type(((struct _type *)_parent)));
}
while(((void*)_attr_1!=(void*)0))
{
_st_8=(_attr_1->_storage_type);
_align_9=_align_p_s__type(_st_8);
if((_align_9>_align_max_3))
{
_align_max_3=_align_9;
}
_sum_2=_max_dw_dw(_sum_2,_size_p_s__type(_st_8));
_attr_1=((struct _attribute *)(((struct _element *)_attr_1)->_next_sibling));
}
}
(((struct _type *)_t)->_default_align)=_align_max_3;
(--_align_max_3);
_sum_2+=_align_max_3;
_sum_2&=(~_align_max_3);
(((struct _type *)_t)->_default_size)=_sum_2;
}

static void _define_p_s__structure_type_p_s__structure_type(struct _structure_type *_t, struct _structure_type *_parent)
{
(_t->_parent_type)=((struct _type *)_parent);
}

static void _define_structure_p_s__compiler_p_s__structure_type(struct _compiler *_cc, struct _structure_type *_structure)
{
int _previous_exposition_1;
struct _set_of_directives *_previous_directives_2;
struct _structure_node *_source_3;
int _is_union_4;
struct _structure_type *_parent_5;
struct _node *_source_of_parent_6;
struct _collection *_source_attributes_7;
struct _collection _attributes_8;
struct _element *_e_9;
int _expose_10;
char *_attribute_name_11;
struct _node *_source_type_12;
struct _type *_attribute_type_13;
struct _attribute *_attribute_14;
if((((((struct _type *)_structure)->_flags)&1073741824)!=0))
{
return;
}
if((((((struct _type *)_structure)->_flags)&2147483648U)!=0))
{
_error_p_s__compiler_in_p_a_ch_p_vo(_cc,(((struct _named_type *)_structure)->_line),_error_circular_dependency_type1236,((void *)_structure));
return;
}
_set_resolving_p_s__type(((struct _type *)_structure));
_previous_exposition_1=(_cc->_current_exposition);
(_cc->_current_exposition)=(((((struct _type *)_structure)->_flags)&64)!=0);
_previous_directives_2=(_cc->_current_directives);
(_cc->_current_directives)=(_structure->_directives);
_source_3=((struct _structure_node *)(((struct _named_type *)_structure)->_source));
_is_union_4=(_source_3->_is_union);
_parent_5=((struct _structure_type *)0);
_source_of_parent_6=(_source_3->_parent);
if(((void*)_source_of_parent_6!=(void*)0))
{
_parent_5=_get_structure_p_s__compiler_p_s__node_bo(_cc,_source_of_parent_6,_is_union_4);
}
_source_attributes_7=(_source_3->_attributes);
if(((void*)_source_attributes_7!=(void*)0))
{
if((((void*)_parent_5!=(void*)0)&&(!(_parent_5->_defined))))
{
_error_p_s__compiler_p_s__node_p_a_ch_p_vo(_cc,_source_of_parent_6,_error_parent_undefined1261,((void *)_structure));
_parent_5=((struct _structure_type *)0);
}
_initialize_p_s__collection((&_attributes_8));
_e_9=(_source_attributes_7->_first);
while(((void*)_e_9!=(void*)0))
{
switch((((struct _attribute_node *)_e_9)->_mode))
{
case 0:
{
_expose_10=(((((struct _type *)_structure)->_flags)&64)!=0);
break;
}
case 1:
{
_expose_10=1;
break;
}
case 2:
{
_expose_10=0;
break;
}
}
_attribute_name_11=(((struct _attribute_node *)_e_9)->_name);
_source_type_12=(((struct _attribute_node *)_e_9)->_type);
_attribute_type_13=_get_type_p_s__compiler_p_s__node(_cc,_source_type_12);
if(((void*)_attribute_type_13!=(void*)0))
{
_check_size_p_s__compiler_p_s__node_p_s__type(_cc,_source_type_12,_attribute_type_13);
_attribute_14=_new_attribute_p_s__memory_bundle_p_a_ch_p_s__type_bo_p_s__structure_type_p_vo_p_s__attribute_node((_cc->_bundle),_attribute_name_11,_attribute_type_13,_expose_10,_structure,((void *)(_cc->_module)),((struct _attribute_node *)_e_9));
_append_p_s__collection_p_s__element((&_attributes_8),((struct _element *)_attribute_14));
_add_global_definition_p_s__compiler_p_s__definition_bo(_cc,((struct _definition *)_attribute_14),_expose_10);
}
_e_9=(_e_9->_next_sibling);
}
_define_p_s__structure_type_p_s__structure_type_p_s__collection(_structure,_parent_5,(&_attributes_8));
}
else
{
_define_p_s__structure_type_p_s__structure_type(_structure,_parent_5);
}
(_cc->_current_directives)=_previous_directives_2;
(_cc->_current_exposition)=_previous_exposition_1;
_set_resolved_p_s__type(((struct _type *)_structure));
}

static struct _type *_get_type_p_s__compiler_p_s__node(struct _compiler *_cc, struct _node *_node)
{
char *_id_1;
struct _type *_type_2;
struct _node *_target_node_3;
struct _type *_target_type_4;
struct _node *_source_array_size_5;
struct _node *_source_array_type_6;
int _array_size_7;
struct _type *_array_type_8;
struct _collection *_source_types_9;
struct _node *_source_returned_type_10;
int _ok_11;
struct _collection_of_arguments _t_12;
struct _collection_of_arguments *_arguments_13;
struct _element *_e_14;
struct _type *_argument_type_15;
struct _argument *_argument_16;
struct _type *_returned_type_17;
struct _signature *_signature_18;
struct _enumeration_type *_enumeration_19;
struct _structure_type *_structure_20;
switch((_node->_code))
{
case 10:
{
_id_1=(((struct _string_node *)_node)->_value);
_type_2=((struct _type *)_find_type_p_s__compiler_p_a_ch(_cc,_id_1));
if(((void*)_type_2==(void*)0))
{
_error_p_s__compiler_p_s__node_p_a_ch_p_vo(_cc,((struct _node *)((struct _string_node *)_node)),_error_undefined_type718,((void *)_id_1));
}
else if(((_type_2->_id)==20))
{
_type_2=_define_redirect_p_s__compiler_p_s__redirect_type(_cc,((struct _redirect_type *)_type_2));
if(((void*)_type_2==(void*)0))
{
_error_p_s__compiler_p_s__node_p_a_ch_p_vo(_cc,((struct _node *)((struct _string_node *)_node)),_error_undefined_type718,((void *)_id_1));
}
}
break;
}
case 16:
{
_target_node_3=((struct _node *)(((struct _unary_node *)((struct _pointer_node *)_node))->_value_1));
_target_type_4=_get_type_p_s__compiler_p_s__node(_cc,_target_node_3);
if(((void*)_target_type_4==(void*)0))
{
return ((struct _type *)0);
}
_type_2=_pointer_p_s__type(_target_type_4);
break;
}
case 17:
{
_source_array_size_5=((struct _node *)(((struct _binary_node *)((struct _array_node *)_node))->_value_1));
_source_array_type_6=((struct _node *)(((struct _binary_node *)((struct _array_node *)_node))->_value_2));
if(((void*)_source_array_size_5!=(void*)0))
{
_array_size_7=_get_constant_integer_p_s__compiler_p_s__node(_cc,_source_array_size_5);
}
else
{
_array_size_7=0;
}
_array_type_8=_get_type_p_s__compiler_p_s__node(_cc,_source_array_type_6);
if(((void*)_array_type_8==(void*)0))
{
return ((struct _type *)0);
}
_type_2=((struct _type *)_new_array_type_p_s__memory_bundle_in_p_s__type((_cc->_bundle),_array_size_7,_array_type_8));
break;
}
case 18:
{
_source_types_9=((struct _collection *)(((struct _binary_node *)((struct _function_type_node *)_node))->_value_1));
_source_returned_type_10=((struct _node *)(((struct _binary_node *)((struct _function_type_node *)_node))->_value_2));
_ok_11=1;
_arguments_13=(_collection_of_arguments_p_s__collection_of_arguments((&_t_12)),(&_t_12));
_e_14=(_source_types_9->_first);
while(((void*)_e_14!=(void*)0))
{
_argument_type_15=_get_registerable_type_p_s__compiler_p_s__node(_cc,((struct _node *)_e_14));
if(((void*)_argument_type_15==(void*)0))
{
_ok_11=0;
}
else
{
_argument_16=_new_argument_p_s__memory_bundle_p_a_ch_p_s__type_bo_p_vo_p_s__node((_cc->_bundle),_empty_string171,_argument_type_15,0,((void *)(_cc->_module)),((struct _node *)_e_14));
_append_p_s__collection_p_s__element(((struct _collection *)_arguments_13),((struct _element *)_argument_16));
}
_e_14=(_e_14->_next_sibling);
}
_returned_type_17=_get_registerable_type_or_void_p_s__compiler_p_s__node(_cc,_source_returned_type_10);
if(((!_ok_11)||((void*)_returned_type_17==(void*)0)))
{
return ((struct _type *)0);
}
_signature_18=_new_signature_from_p_s__compiler_p_s__collection_of_arguments(_cc,_arguments_13);
_type_2=((struct _type *)_new_function_type_p_s__memory_bundle_p_s__signature_p_s__type((_cc->_bundle),_signature_18,_returned_type_17));
_type_2=_pointer_p_s__type(_type_2);
break;
}
case 1:
{
_enumeration_19=_new_enumeration_type_p_s__memory_bundle_bo_p_vo_p_s__enumeration_node((_cc->_bundle),(_cc->_current_exposition),((void *)(_cc->_module)),((struct _enumeration_node *)_node));
_define_enumeration_p_s__compiler_p_s__enumeration_type(_cc,_enumeration_19);
return ((struct _type *)_enumeration_19);
break;
}
case 3:
{
_structure_20=_new_structure_type_p_s__memory_bundle_bo_p_s__set_of_directives_p_vo_p_s__structure_node((_cc->_bundle),(_cc->_current_exposition),(_cc->_current_directives),((void *)(_cc->_module)),((struct _structure_node *)_node));
_define_structure_p_s__compiler_p_s__structure_type(_cc,_structure_20);
return ((struct _type *)_structure_20);
break;
}
}
return _type_2;
}

static struct _type *_get_registerable_type_p_s__compiler_p_s__node(struct _compiler *_cc, struct _node *_source)
{
return _get_type_p_s__compiler_p_s__node(_cc,_source);
}

static struct _collection_of_arguments *_get_arguments_p_s__compiler_p_s__collection_p_a_in(struct _compiler *_cc, struct _collection *_source, int *_returned_arguments)
{
int _ok_1;
struct _collection_of_arguments *_arguments_2;
struct _element *_e_3;
char *_argument_name_4;
struct _node *_source_type_5;
int _is_return_6;
struct _type *_argument_type_7;
struct _argument *_argument_8;
(_returned_arguments[0])=0;
_ok_1=1;
_arguments_2=_new_collection_of_arguments_p_s__compiler(_cc);
_e_3=(_source->_first);
while(((void*)_e_3!=(void*)0))
{
_argument_name_4=((char *)(((struct _ternary_node *)((struct _argument_node *)_e_3))->_value_1));
_source_type_5=((struct _node *)(((struct _ternary_node *)((struct _argument_node *)_e_3))->_value_2));
_is_return_6=((void*)(((struct _ternary_node *)((struct _argument_node *)_e_3))->_value_3)!=(void*)0);
_argument_type_7=_get_registerable_type_p_s__compiler_p_s__node(_cc,_source_type_5);
if(((void*)_argument_type_7!=(void*)0))
{
_argument_8=_new_argument_p_s__memory_bundle_p_a_ch_p_s__type_bo_p_vo_p_s__node((_cc->_bundle),_argument_name_4,_argument_type_7,_is_return_6,((void *)(_cc->_module)),((struct _node *)((struct _argument_node *)_e_3)));
_append_p_s__collection_p_s__element(((struct _collection *)_arguments_2),((struct _element *)_argument_8));
if(_is_return_6)
{
if(((!((_argument_type_7->_id)==15))||(!((((((struct _pointer_type *)_argument_type_7)->_target_type)->_flags)&6)!=0))))
{
_error_p_s__compiler_p_s__node_p_a_ch_p_vo(_cc,_source_type_5,_error_must_be_a_pointer_to_struct_or_array1282,((void *)_argument_name_4));
return ((struct _collection_of_arguments *)0);
}
(_returned_arguments[0])+=1;
}
}
else
{
_ok_1=0;
}
_e_3=(_e_3->_next_sibling);
}
return (_ok_1?_arguments_2:((struct _collection_of_arguments *)0));
}

static struct _function *_new_function_p_s__memory_bundle_p_s__signature_p_s__collection_of_arguments_p_s__type_bo_p_s__set_of_directives_p_vo_p_s__function_node(struct _memory_bundle *_bundle, struct _signature *_signature, struct _collection_of_arguments *_arguments, struct _type *_returned_type, int _expose, struct _set_of_directives *_directives, void *_module, struct _function_node *_source)
{
struct _function *_f_1;
_f_1=((struct _function *)_allocate_p_s__memory_bundle_dw(_bundle,((unsigned int)92)));
_initialize_p_s__definition_p_a_ch_p_s__signature_p_s__type_e__definition_code_bo_bo_p_vo_p_s__node(((struct _definition *)_f_1),(_source->_name),_signature,_returned_type,3,0,_expose,_module,((struct _node *)_source));
(_f_1->_bundle)=_bundle;
(_f_1->_ref_type)=((struct _type *)0);
(_f_1->_arguments)=((struct _argument *)(((struct _collection *)_arguments)->_first));
(_f_1->_defined)=0;
(_f_1->_instructions)=((struct _expression *)0);
(_f_1->_directives)=_directives;
(_f_1->_extern_name)=_empty_string171;
if(((void*)(_source->_body)==(void*)0))
{
(((struct _definition *)_f_1)->_resolve_status)=2;
}
return _f_1;
}

static struct _function *_declare_function_p_s__compiler_p_s__function_node_bo_p_s__set_of_directives(struct _compiler *_cc, struct _function_node *_source, int _expose, struct _set_of_directives *_base_directives)
{
struct _collection *_source_arguments_1;
int (_returned_arguments_2)[1];
struct _collection_of_arguments *_arguments_3;
char *_name_4;
struct _node *_source_type_5;
struct _type *_returned_type_6;
struct _set_of_directives *_directives_7;
struct _function *_function_8;
_source_arguments_1=(_source->_arguments);
_arguments_3=_get_arguments_p_s__compiler_p_s__collection_p_a_in(_cc,_source_arguments_1,((int *)_returned_arguments_2));
if(((void*)_arguments_3==(void*)0))
{
return ((struct _function *)0);
}
if(((_returned_arguments_2[0])>1))
{
_error_p_s__compiler_p_s__node_p_a_ch(_cc,((struct _node *)_source),_error_too_many_returns851);
return ((struct _function *)0);
}
_name_4=(_source->_name);
if((!_check_arguments_p_s__compiler_p_s__node_p_a_ch_p_s__collection_of_arguments(_cc,((struct _node *)_source),_name_4,_arguments_3)))
{
return ((struct _function *)0);
}
_source_type_5=(_source->_returned_type);
_returned_type_6=((struct _type *)0);
if(((void*)_source_type_5!=(void*)0))
{
_returned_type_6=_get_registerable_type_p_s__compiler_p_s__node(_cc,_source_type_5);
if(((void*)_returned_type_6==(void*)0))
{
return ((struct _function *)0);
}
}
else if(((void*)(_source->_body)==(void*)0))
{
_returned_type_6=((struct _type *)_void_type114);
}
_directives_7=_get_directives_p_s__compiler_p_s__set_of_directives_p_s__collection_of_directives(_cc,_base_directives,(_source->_directives));
_function_8=_new_function_p_s__memory_bundle_p_s__signature_p_s__collection_of_arguments_p_s__type_bo_p_s__set_of_directives_p_vo_p_s__function_node((_cc->_bundle),_new_signature_from_p_s__compiler_p_s__collection_of_arguments(_cc,_arguments_3),_arguments_3,_returned_type_6,_expose,_directives_7,((void *)(_cc->_module)),_source);
_add_global_definition_p_s__compiler_p_s__definition_bo(_cc,((struct _definition *)_function_8),_expose);
if(((_returned_arguments_2[0])==1))
{
_declare_extension_p_s__compiler_p_s__definition_p_s__collection_of_arguments_bo(_cc,((struct _definition *)_function_8),_arguments_3,0);
}
return _function_8;
}

static void _declare_definitions_p_s__compiler_p_s__collection_of_program_elements(struct _compiler *_cc, struct _collection_of_program_elements *_list)
{
struct _element *_e_1;
struct _node *_node_2;
int _expose_3;
struct _set_of_directives *_directives_4;
struct _variable *_variable_5;
struct _function *_function_6;
struct _equate *_equate_7;
struct _iterator *_iterator_8;
_e_1=((&(_list->_collection))->_first);
while(((void*)_e_1!=(void*)0))
{
if(((_cc->_number_of_errors)>=100))
{
return;
}
_node_2=(((struct _program_element *)_e_1)->_node);
_expose_3=(((struct _program_element *)_e_1)->_expose);
_directives_4=(((struct _program_element *)_e_1)->_directives);
switch((_node_2->_code))
{
case 5:
{
_variable_5=_declare_variable_p_s__compiler_p_s__variable_node_bo_p_s__set_of_directives_bo(_cc,((struct _variable_node *)_node_2),_expose_3,_directives_4,0);
if(((void*)_variable_5!=(void*)0))
{
(((struct _program_element *)_e_1)->_value)=((void *)_variable_5);
_add_p_s__compiler_p_s__expression(_cc,((struct _expression *)_new_variable_expression_p_s__memory_bundle_p_s__type_e__expression_code_p_s__variable((_cc->_bundle),((struct _type *)_void_type114),9,_variable_5)));
}
break;
}
case 4:
{
_function_6=_declare_function_p_s__compiler_p_s__function_node_bo_p_s__set_of_directives(_cc,((struct _function_node *)_node_2),_expose_3,_directives_4);
if(((void*)_function_6!=(void*)0))
{
(((struct _program_element *)_e_1)->_value)=((void *)_function_6);
_add_p_s__compiler_p_s__expression(_cc,((struct _expression *)_new_function_expression_p_s__memory_bundle_p_s__type_e__expression_code_p_s__function((_cc->_bundle),((struct _type *)_void_type114),10,_function_6)));
}
break;
}
case 6:
{
_equate_7=_declare_equate_p_s__compiler_p_s__equate_node_bo_bo(_cc,((struct _equate_node *)_node_2),_expose_3,0);
if(((void*)_equate_7!=(void*)0))
{
(((struct _program_element *)_e_1)->_value)=((void *)_equate_7);
}
break;
}
case 7:
{
_iterator_8=_declare_iterator_p_s__compiler_p_s__iterator_node_bo_bo(_cc,((struct _iterator_node *)_node_2),_expose_3,0);
if(((void*)_iterator_8!=(void*)0))
{
(((struct _program_element *)_e_1)->_value)=((void *)_iterator_8);
}
break;
}
}
_e_1=(_e_1->_next_sibling);
}
}

static void _define_types_p_s__compiler_p_s__collection_of_program_elements(struct _compiler *_cc, struct _collection_of_program_elements *_list)
{
struct _element *_e_1;
struct _node *_node_2;
void *_value_3;
_e_1=((&(_list->_collection))->_first);
while(((void*)_e_1!=(void*)0))
{
if(((_cc->_number_of_errors)>=100))
{
return;
}
_node_2=(((struct _program_element *)_e_1)->_node);
_value_3=(((struct _program_element *)_e_1)->_value);
if(((void*)_value_3!=(void*)0))
{
switch((_node_2->_code))
{
case 2:
{
_define_redirect_p_s__compiler_p_s__redirect_type(_cc,((struct _redirect_type *)_value_3));
break;
}
case 3:
{
_define_structure_p_s__compiler_p_s__structure_type(_cc,((struct _structure_type *)_value_3));
break;
}
case 1:
{
_define_enumeration_p_s__compiler_p_s__enumeration_type(_cc,((struct _enumeration_type *)_value_3));
break;
}
}
}
_e_1=(_e_1->_next_sibling);
}
}

static void _define_definitions_p_s__compiler_p_s__collection_of_program_elements(struct _compiler *_cc, struct _collection_of_program_elements *_list)
{
struct _element *_e_1;
struct _node *_node_2;
void *_value_3;
_e_1=((&(_list->_collection))->_first);
while(((void*)_e_1!=(void*)0))
{
if(((_cc->_number_of_errors)>=100))
{
return;
}
_node_2=(((struct _program_element *)_e_1)->_node);
_value_3=(((struct _program_element *)_e_1)->_value);
if(((void*)_value_3!=(void*)0))
{
switch((_node_2->_code))
{
case 5:
{
_define_variable_p_s__compiler_p_s__variable(_cc,((struct _variable *)_value_3));
break;
}
case 4:
{
_define_function_p_s__compiler_p_s__function(_cc,((struct _function *)_value_3));
break;
}
case 6:
{
_define_equate_p_s__compiler_p_s__equate(_cc,((struct _equate *)_value_3));
break;
}
case 7:
{
_define_iterator_p_s__compiler_p_s__iterator(_cc,((struct _iterator *)_value_3));
break;
}
}
}
_e_1=(_e_1->_next_sibling);
}
}

static int _start_p_s__compiler_p_s__include_directory(struct _compiler *_cc, struct _include_directory *_path)
{
struct _module *_module_1;
struct _memory_bundle *_bundle_2;
char *_filename_3;
struct _file_buffer _fb_4;
int _code_5;
struct _collection *_tree_6;
struct _include_directory _include_7;
struct _collection_of_program_elements _list_8;
_module_1=(_cc->_module);
_bundle_2=(_cc->_bundle);
_filename_3=(_module_1->_name);
_code_5=_initialize_p_s__file_buffer_p_a_ch_bo((&_fb_4),_filename_3,1);
if((_code_5!=0))
{
_error_p_s__compiler_in_p_a_ch_p_vo(_cc,0,_error_cannot_open_file306,((void *)_filename_3));
}
else
{
_tree_6=_run_p_s__memory_bundle_p_s__memory_bundle_p_a_ch_p_a_ch_p_s__dictionary_bo(_bundle_2,_bundle_2,_filename_3,((char *)((&_fb_4)->_base)),((_cc->_session)->_defines),((_cc->_session)->_unicode));
_release_p_s__file_buffer((&_fb_4));
if(((void*)_tree_6!=(void*)0))
{
(((struct _element *)(&_include_7))->_next_sibling)=((struct _element *)_path);
(((struct _list_item *)(&_include_7))->_value)=((void *)_file_path_p_s__memory_bundle_p_a_ch((_cc->_bundle),_filename_3));
(_cc->_path)=(&_include_7);
_initialize_p_s__collection_of_program_elements_p_s__memory_bundle((&_list_8),_bundle_2);
if(((_cc->_number_of_errors)<100))
{
_declare_types_p_s__compiler_p_s__collection_of_program_elements_p_s__collection(_cc,(&_list_8),_tree_6);
}
if(((_cc->_number_of_errors)<100))
{
_declare_definitions_p_s__compiler_p_s__collection_of_program_elements(_cc,(&_list_8));
}
if(((_cc->_number_of_errors)<100))
{
_define_types_p_s__compiler_p_s__collection_of_program_elements(_cc,(&_list_8));
}
if(((_cc->_number_of_errors)<100))
{
_define_definitions_p_s__compiler_p_s__collection_of_program_elements(_cc,(&_list_8));
}
}
}
return ((_cc->_number_of_errors)==0);
}

static void _release_p_s__vector(struct _vector *_v)
{
_free_memory_p_vo(((void *)(((struct _priv_vector *)_v)->_array)));
}

static void _release_p_s__compiler(struct _compiler *_cc)
{
_release_p_s__scope((_cc->_current_scope));
_release_p_s__vector((&(_cc->_imports)));
}

static struct _module *_load_module_p_s__compiler_session_p_a_ch_p_s__include_directory(struct _compiler_session *_self, char *_filename, struct _include_directory *_path)
{
struct _list_item *_e_1;
void *_v_2;
struct _module *_module_3;
struct _compiler _cc_4;
int _ok_5;
_e_1=((&(_self->_modules))->_first);
while(((void*)_e_1!=(void*)0))
{
_v_2=(_e_1->_value);
if(_is_equal_p_a_ch_p_a_ch((((struct _module *)_v_2)->_name),_filename))
{
return ((struct _module *)_v_2);
}
_e_1=((struct _list_item *)(((struct _element *)_e_1)->_next_sibling));
}
_module_3=_new_module_p_s__memory_bundle_p_a_ch((_self->_bundle),_filename);
_append_p_s__list_p_vo((&(_self->_modules)),((void *)_module_3));
(_module_3->_loading)=1;
_initialize_p_s__compiler_p_s__compiler_session_p_s__memory_bundle_p_s__module((&_cc_4),_self,(_self->_bundle),_module_3);
_ok_5=_start_p_s__compiler_p_s__include_directory((&_cc_4),_path);
_release_p_s__compiler((&_cc_4));
if((!_ok_5))
{
return ((struct _module *)0);
}
(_module_3->_loading)=0;
return _module_3;
}

static struct _module *_start_p_s__compiler_session_p_s__memory_bundle_p_a_ch_p_s__list_p_s__dictionary_bo(struct _compiler_session *_self, struct _memory_bundle *_bundle, char *_filename, struct _list *_path, struct _dictionary *_defines, int _unicode)
{
(_self->_bundle)=_bundle;
_initialize_p_s__list_p_s__memory_bundle((&(_self->_modules)),_bundle);
(_self->_unicode)=_unicode;
(_self->_defines)=_defines;
return _load_module_p_s__compiler_session_p_a_ch_p_s__include_directory(_self,_filename,((struct _include_directory *)(_path->_first)));
}

static void _initialize_p_s__litteral_string_dictionary(struct _litteral_string_dictionary *_d)
{
_initialize_p_s__dictionary_p_s__dictionary_class(((struct _dictionary *)_d),(&_litteral_string_dictionary_class164));
}

static void _initialize_p_s__c_dump_p_s__memory_bundle(struct _c_dump *_d, struct _memory_bundle *_bundle)
{
(_d->_bundle)=_bundle;
_initialize_p_s__string_buffer((&(_d->_tmp)));
_initialize_p_s__litteral_string_dictionary((&(_d->_strings)));
_initialize_p_s__litteral_string_dictionary((&(_d->_wstrings)));
_initialize_p_s__list_p_s__memory_bundle((&(_d->_initializers)),_bundle);
_initialize_p_s__list_p_s__memory_bundle((&(_d->_structures)),_bundle);
_initialize_p_s__list_p_s__memory_bundle((&(_d->_functions)),_bundle);
_initialize_p_s__list_p_s__memory_bundle((&(_d->_variables)),_bundle);
}

static char *_value_p_s__set_of_directives_p_a_ch(struct _set_of_directives *_s, char *_name)
{
struct _set_of_directives *_c_1;
struct _element *_e_2;
_c_1=_s;
while(((void*)_c_1!=(void*)0))
{
_e_2=((&(_c_1->_directives))->_first);
while(((void*)_e_2!=(void*)0))
{
if(_is_equal_p_a_ch_p_a_ch((((struct _directive *)_e_2)->_name),_name))
{
return (((struct _directive *)_e_2)->_value);
}
_e_2=(_e_2->_next_sibling);
}
_c_1=(_c_1->_parent);
}
return ((char *)0);
}

static int _naming_convention_p_s__set_of_directives(struct _set_of_directives *_s)
{
char *_value_1;
int _nc_2;
_value_1=_value_p_s__set_of_directives_p_a_ch(_s,T1313);
if((((void*)_value_1!=(void*)0)&&_is_equal_p_a_ch_p_a_ch(_value_1,T1315)))
{
_nc_2=1;
}
else
{
_nc_2=0;
}
return _nc_2;
}

static void _append_identifier_p_s__string_buffer_p_a_ch(struct _string_buffer *_sb, char *_s)
{
char *_p_1;
char _c_2;
_p_1=_s;
if(((_p_1[0])=='\x00'))
{
return;
}
_append_p_s__string_buffer_ch(_sb,'_');
while(1)
{
_c_2=((_p_1++)[0]);
if((_c_2=='\x00'))
{
break;
}
if(((_c_2==' ')||(_c_2=='@')))
{
_append_p_s__string_buffer_ch(_sb,'_');
}
else
{
_append_p_s__string_buffer_ch(_sb,_c_2);
}
}
}

static void _scan_type_p_s__c_dump_p_s__type(struct _c_dump *_d, struct _type *_type)
{
struct _element *_e_1;
struct _type *_attr_type_2;
struct _element *_e_3;
struct _type *_attr_type_4;
int _n_5;
int _i_6;
if((((_type->_flags)&128)!=0))
{
return;
}
(_type->_flags)|=128;
switch((_type->_id))
{
case 15:
{
_scan_type_p_s__c_dump_p_s__type(_d,(((struct _pointer_type *)_type)->_target_type));
break;
}
case 16:
case 17:
{
if((_naming_convention_p_s__set_of_directives((((struct _structure_type *)_type)->_directives))==1))
{
(((struct _structure_type *)_type)->_extern_name)=(((struct _structure_type *)_type)->_short_name);
}
else
{
((&(_d->_tmp))->_used)=0;
if((((((struct _named_type *)((struct _structure_type *)_type))->_name)[0])=='\x00'))
{
_append_p_s__string_buffer_in(_append_p_s__string_buffer_p_a_ch((&(_d->_tmp)),T1317),(_d->_count));
(++(_d->_count));
}
else
{
_append_identifier_p_s__string_buffer_p_a_ch((&(_d->_tmp)),(((struct _structure_type *)_type)->_short_name));
}
(((struct _structure_type *)_type)->_extern_name)=_new_string_p_s__memory_bundle_p_a_ch((_d->_bundle),_as_string_p_s__string_buffer((&(_d->_tmp))));
}
if((!(((struct _structure_type *)_type)->_defined)))
{
_append_p_s__list_p_vo((&(_d->_structures)),((void *)((struct _structure_type *)_type)));
return;
}
if(((void*)((struct _structure_type *)(((struct _structure_type *)_type)->_parent_type))!=(void*)0))
{
_scan_type_p_s__c_dump_p_s__type(_d,((struct _type *)((struct _structure_type *)(((struct _structure_type *)_type)->_parent_type))));
}
_e_1=((struct _element *)(((struct _structure_type *)_type)->_attributes));
while(((void*)_e_1!=(void*)0))
{
_attr_type_2=(((struct _attribute *)_e_1)->_storage_type);
if(((_attr_type_2->_id)!=15))
{
_scan_type_p_s__c_dump_p_s__type(_d,_attr_type_2);
}
_e_1=(_e_1->_next_sibling);
}
_append_p_s__list_p_vo((&(_d->_structures)),((void *)((struct _structure_type *)_type)));
_e_3=((struct _element *)(((struct _structure_type *)_type)->_attributes));
while(((void*)_e_3!=(void*)0))
{
_attr_type_4=(((struct _attribute *)_e_3)->_storage_type);
if(((_attr_type_4->_id)==15))
{
_scan_type_p_s__c_dump_p_s__type(_d,(((struct _pointer_type *)_attr_type_4)->_target_type));
}
_e_3=(_e_3->_next_sibling);
}
break;
}
case 18:
{
_scan_type_p_s__c_dump_p_s__type(_d,(((struct _array_type *)_type)->_array_type));
break;
}
case 19:
{
_scan_type_p_s__c_dump_p_s__type(_d,(((struct _function_type *)_type)->_returned_type));
_n_5=((((struct _function_type *)_type)->_signature)->_number_of_types);
_i_6=0;
while((_i_6<_n_5))
{
_scan_type_p_s__c_dump_p_s__type(_d,_type_p_s__signature_in((((struct _function_type *)_type)->_signature),_i_6));
(++_i_6);
}
break;
}
}
}

static void _flush_identifier_p_s__string_buffer_p_s__type(struct _string_buffer *_sb, struct _type *_type)
{
int _n_1;
int _i_2;
switch((_type->_id))
{
case 3:
{
_append_p_s__string_buffer_p_a_ch(_sb,T1334);
break;
}
case 6:
{
_append_p_s__string_buffer_p_a_ch(_sb,T1335);
break;
}
case 7:
{
_append_p_s__string_buffer_p_a_ch(_sb,T1336);
break;
}
case 8:
{
_append_p_s__string_buffer_p_a_ch(_sb,T1337);
break;
}
case 9:
{
_append_p_s__string_buffer_p_a_ch(_sb,T1338);
break;
}
case 10:
{
_append_p_s__string_buffer_p_a_ch(_sb,T1339);
break;
}
case 11:
{
_append_p_s__string_buffer_p_a_ch(_sb,T1340);
break;
}
case 12:
{
_append_p_s__string_buffer_p_a_ch(_sb,T1341);
break;
}
case 13:
{
_append_p_s__string_buffer_p_a_ch(_sb,T1342);
break;
}
case 4:
{
_append_p_s__string_buffer_p_a_ch(_sb,T1343);
break;
}
case 5:
{
_append_p_s__string_buffer_p_a_ch(_sb,T1344);
break;
}
case 1:
{
_append_p_s__string_buffer_p_a_ch(_sb,T1345);
break;
}
case 14:
{
_append_p_s__string_buffer_p_a_ch(_sb,T1346);
_append_identifier_p_s__string_buffer_p_a_ch(_sb,(((struct _named_type *)((struct _enumeration_type *)_type))->_name));
break;
}
case 16:
case 17:
{
_append_p_s__string_buffer_p_a_ch(_sb,T1347);
_append_p_s__string_buffer_p_a_ch(_sb,(((struct _structure_type *)_type)->_extern_name));
break;
}
case 15:
{
_append_p_s__string_buffer_p_a_ch(_sb,T1348);
_flush_identifier_p_s__string_buffer_p_s__type(_sb,(((struct _pointer_type *)_type)->_target_type));
break;
}
case 18:
{
_append_p_s__string_buffer_p_a_ch(_sb,T1349);
_flush_identifier_p_s__string_buffer_p_s__type(_sb,(((struct _array_type *)_type)->_array_type));
break;
}
case 19:
{
_append_p_s__string_buffer_p_a_ch(_sb,T1350);
_n_1=((((struct _function_type *)_type)->_signature)->_number_of_types);
_i_2=0;
while((_i_2<_n_1))
{
_flush_identifier_p_s__string_buffer_p_s__type(_sb,_type_p_s__signature_in((((struct _function_type *)_type)->_signature),_i_2));
(++_i_2);
}
_flush_identifier_p_s__string_buffer_p_s__type(_sb,(((struct _function_type *)_type)->_returned_type));
break;
}
}
}

static void _scan_definition_p_s__c_dump_p_s__definition(struct _c_dump *_d, struct _definition *_definition)
{
struct _expression *_r_1;
int _global_2;
struct _variable *_variable_3;
struct _argument *_a_4;
struct _function *_previous_5;
char *_name_6;
struct _argument *_a_7;
if((_definition->_used))
{
return;
}
(_definition->_used)=1;
switch((_definition->_opcode))
{
case 1:
{
_scan_type_p_s__c_dump_p_s__type(_d,(((struct _definition *)((struct _variable *)_definition))->_type));
if(((void*)(((struct _variable *)_definition)->_initial_value)!=(void*)0))
{
_scan_expression_p_s__c_dump_p_s__expression(_d,(((struct _variable *)_definition)->_initial_value));
}
if(((void*)(((struct _variable *)_definition)->_proxy)!=(void*)0))
{
_scan_expression_p_s__c_dump_p_s__expression(_d,(((struct _variable *)_definition)->_proxy));
}
_r_1=(((struct _variable *)_definition)->_reference);
if(((void*)_r_1!=(void*)0))
{
_scan_expression_p_s__c_dump_p_s__expression(_d,_r_1);
}
_global_2=((void*)(((struct _variable *)_definition)->_function)==(void*)0);
if((_global_2&(_naming_convention_p_s__set_of_directives((((struct _variable *)_definition)->_directives))==1)))
{
(((struct _variable *)_definition)->_extern_name)=(((struct _definition *)((struct _variable *)_definition))->_name);
}
else
{
((&(_d->_tmp))->_used)=0;
_append_identifier_p_s__string_buffer_p_a_ch((&(_d->_tmp)),(((struct _definition *)((struct _variable *)_definition))->_name));
_append_p_s__string_buffer_in((&(_d->_tmp)),(_d->_count));
(++(_d->_count));
(((struct _variable *)_definition)->_extern_name)=_new_string_p_s__memory_bundle_p_a_ch((_d->_bundle),_as_string_p_s__string_buffer((&(_d->_tmp))));
}
if(((void*)_r_1==(void*)0))
{
if(_global_2)
{
_append_p_s__list_p_vo((&(_d->_variables)),((void *)((struct _variable *)_definition)));
}
else
{
_append_p_s__list_p_vo((&((((struct _variable *)_definition)->_function)->_variables)),((void *)((struct _variable *)_definition)));
}
}
else
{
if((((_r_1->_opcode)==5)||((_r_1->_opcode)==2)))
{
(((struct _global_constant *)(_r_1->_user_data))->_expose)=(((struct _definition *)((struct _variable *)_definition))->_expose);
(((struct _global_constant *)(_r_1->_user_data))->_name)=(((struct _variable *)_definition)->_extern_name);
}
else if((((_r_1->_opcode)==4)||_is_reference_p_s__expression(_r_1)))
{
}
else
{
_variable_3=(((struct _variable_expression *)_r_1)->_value);
(((struct _definition *)_variable_3)->_expose)=(((struct _definition *)((struct _variable *)_definition))->_expose);
(_variable_3->_extern_name)=(((struct _variable *)_definition)->_extern_name);
}
}
break;
}
case 3:
{
_scan_type_p_s__c_dump_p_s__type(_d,(((struct _definition *)((struct _function *)_definition))->_type));
_a_4=(((struct _function *)_definition)->_arguments);
while(((void*)_a_4!=(void*)0))
{
_scan_type_p_s__c_dump_p_s__type(_d,(((struct _definition *)_a_4)->_type));
_a_4=((struct _argument *)(((struct _element *)_a_4)->_next_sibling));
}
if((((struct _function *)_definition)->_defined))
{
_initialize_p_s__list_p_s__memory_bundle((&(((struct _function *)_definition)->_variables)),(_d->_bundle));
_previous_5=(_d->_current_function);
(_d->_current_function)=((struct _function *)_definition);
_scan_list_of_expressions_p_s__c_dump_p_s__expression(_d,(((struct _function *)_definition)->_instructions));
(_d->_current_function)=_previous_5;
}
if((_naming_convention_p_s__set_of_directives((((struct _function *)_definition)->_directives))==1))
{
(((struct _function *)_definition)->_extern_name)=(((struct _definition *)((struct _function *)_definition))->_name);
}
else
{
((&(_d->_tmp))->_used)=0;
_name_6=(((struct _definition *)((struct _function *)_definition))->_name);
if((!((((_name_6[0])>='a')&&((_name_6[0])<='z'))||(((_name_6[0])>='A')&&((_name_6[0])<='Z')))))
{
_append_p_s__string_buffer_p_a_ch((&(_d->_tmp)),T1332);
}
_append_identifier_p_s__string_buffer_p_a_ch((&(_d->_tmp)),_name_6);
_a_7=(((struct _function *)_definition)->_arguments);
while(((void*)_a_7!=(void*)0))
{
_append_p_s__string_buffer_ch((&(_d->_tmp)),'_');
_flush_identifier_p_s__string_buffer_p_s__type((&(_d->_tmp)),(((struct _definition *)_a_7)->_type));
_a_7=((struct _argument *)(((struct _element *)_a_7)->_next_sibling));
}
(((struct _function *)_definition)->_extern_name)=_new_string_p_s__memory_bundle_p_a_ch((_d->_bundle),_as_string_p_s__string_buffer((&(_d->_tmp))));
}
_append_p_s__list_p_vo((&(_d->_functions)),((void *)((struct _function *)_definition)));
break;
}
}
}

static void _scan_sub_module_p_s__c_dump_p_s__module(struct _c_dump *_d, struct _module *_m)
{
struct _element *_e_1;
struct _variable *_v_2;
struct _function *_f_3;
char *_entry_point_4;
if((_m->_scanned))
{
return;
}
(_m->_scanned)=1;
_e_1=(((struct _collection *)(&(_m->_elements)))->_first);
while(((void*)_e_1!=(void*)0))
{
switch((((struct _expression *)_e_1)->_opcode))
{
case 8:
{
_scan_sub_module_p_s__c_dump_p_s__module(_d,(((struct _module_expression *)((struct _expression *)_e_1))->_value));
break;
}
case 9:
{
_v_2=(((struct _variable_expression *)((struct _expression *)_e_1))->_value);
(((struct _definition *)_v_2)->_expose)=0;
break;
}
case 10:
{
_f_3=(((struct _function_expression *)((struct _expression *)_e_1))->_value);
_entry_point_4=_value_p_s__set_of_directives_p_a_ch((_f_3->_directives),T1311);
if((((struct _definition *)_f_3)->_expose))
{
if((((void*)_entry_point_4!=(void*)0)&&_is_equal_p_a_ch_p_a_ch(_entry_point_4,T258)))
{
_scan_definition_p_s__c_dump_p_s__definition(_d,((struct _definition *)_f_3));
}
else
{
(((struct _definition *)_f_3)->_expose)=0;
}
}
break;
}
}
_e_1=(_e_1->_next_sibling);
}
}

static struct _global_constant *_new_global_constant_p_s__memory_bundle_bo_p_a_ch(struct _memory_bundle *_bundle, int _expose, char *_name)
{
struct _global_constant *_g_1;
_g_1=((struct _global_constant *)_allocate_p_s__memory_bundle_dw(_bundle,((unsigned int)8)));
(_g_1->_expose)=_expose;
(_g_1->_name)=_name;
return _g_1;
}

static char *_get_name_p_s__c_dump(struct _c_dump *_d)
{
((&(_d->_tmp))->_used)=0;
_append_p_s__string_buffer_in(_append_p_s__string_buffer_p_a_ch((&(_d->_tmp)),T1357),(_d->_count));
(++(_d->_count));
return _new_string_p_s__memory_bundle_p_a_ch((_d->_bundle),_as_string_p_s__string_buffer((&(_d->_tmp))));
}

static void _scan_litteral_string_p_s__c_dump_p_s__string_expression(struct _c_dump *_d, struct _string_expression *_e)
{
struct _global_constant *_c_1;
struct _global_constant *_c_2;
if(((void*)(((struct _expression *)_e)->_type)==(void*)_wstring_type156))
{
_c_1=((struct _global_constant *)_value_p_s__dictionary_p_vo(((struct _dictionary *)(&(_d->_wstrings))),((void *)(_e->_value))));
if(((void*)_c_1==(void*)0))
{
_c_1=_new_global_constant_p_s__memory_bundle_bo_p_a_ch((_d->_bundle),0,_get_name_p_s__c_dump(_d));
_add_p_s__dictionary_p_vo_p_vo(((struct _dictionary *)(&(_d->_wstrings))),((void *)(_e->_value)),((void *)_c_1));
}
(((struct _expression *)_e)->_user_data)=((void *)_c_1);
}
else
{
_c_2=((struct _global_constant *)_value_p_s__dictionary_p_vo(((struct _dictionary *)(&(_d->_strings))),((void *)(_e->_value))));
if(((void*)_c_2==(void*)0))
{
_c_2=_new_global_constant_p_s__memory_bundle_bo_p_a_ch((_d->_bundle),0,_get_name_p_s__c_dump(_d));
_add_p_s__dictionary_p_vo_p_vo(((struct _dictionary *)(&(_d->_strings))),((void *)(_e->_value)),((void *)_c_2));
}
(((struct _expression *)_e)->_user_data)=((void *)_c_2);
}
}

static void _scan_initializer_p_s__c_dump_p_s__collection_expression(struct _c_dump *_d, struct _collection_expression *_initializer)
{
struct _expression *_e_1;
struct _global_constant *_c_2;
_e_1=(_initializer->_value);
while(((void*)_e_1!=(void*)0))
{
_scan_expression_p_s__c_dump_p_s__expression(_d,_e_1);
_e_1=((struct _expression *)(((struct _element *)_e_1)->_next_sibling));
}
_append_p_s__list_p_vo((&(_d->_initializers)),((void *)_initializer));
_c_2=_new_global_constant_p_s__memory_bundle_bo_p_a_ch((_d->_bundle),0,_get_name_p_s__c_dump(_d));
(((struct _expression *)_initializer)->_user_data)=((void *)_c_2);
}

static void _scan_expression_p_s__c_dump_p_s__expression(struct _c_dump *_d, struct _expression *_e)
{
struct _variable *_v_1;
struct _function *_f_2;
struct _expression *_returned_expression_3;
struct _aswitch_item *_i_4;
struct _condition_item *_c_5;
switch((_e->_opcode))
{
case 8:
{
_scan_sub_module_p_s__c_dump_p_s__module(_d,(((struct _module_expression *)_e)->_value));
break;
}
case 17:
{
_scan_type_p_s__c_dump_p_s__type(_d,(_e->_type));
_scan_expression_p_s__c_dump_p_s__expression(_d,(((struct _expression_expression *)_e)->_value));
break;
}
case 9:
{
_v_1=(((struct _variable_expression *)_e)->_value);
(_v_1->_function)=(_d->_current_function);
if((((((struct _definition *)_v_1)->_expose)||((void*)(_v_1->_initial_value)!=(void*)0))||((void*)(_v_1->_proxy)!=(void*)0)))
{
_scan_definition_p_s__c_dump_p_s__definition(_d,((struct _definition *)_v_1));
}
break;
}
case 10:
{
_f_2=(((struct _function_expression *)_e)->_value);
if((((struct _definition *)_f_2)->_expose))
{
_scan_definition_p_s__c_dump_p_s__definition(_d,((struct _definition *)_f_2));
}
break;
}
case 2:
{
_scan_litteral_string_p_s__c_dump_p_s__string_expression(_d,((struct _string_expression *)_e));
break;
}
case 5:
{
_scan_initializer_p_s__c_dump_p_s__collection_expression(_d,((struct _collection_expression *)_e));
break;
}
case 3:
{
_scan_definition_p_s__c_dump_p_s__definition(_d,(((struct _definition_expression *)_e)->_definition));
_scan_list_of_expressions_p_s__c_dump_p_s__expression(_d,(((struct _definition_expression *)_e)->_arguments));
break;
}
case 4:
{
_scan_definition_p_s__c_dump_p_s__definition(_d,((struct _definition *)(((struct _function_expression *)_e)->_value)));
break;
}
case 11:
{
_returned_expression_3=(((struct _expression_expression *)_e)->_value);
if(((void*)_returned_expression_3!=(void*)0))
{
_scan_expression_p_s__c_dump_p_s__expression(_d,_returned_expression_3);
}
break;
}
case 12:
{
_scan_expression_p_s__c_dump_p_s__expression(_d,(((struct _loop_expression *)_e)->_condition));
_scan_list_of_expressions_p_s__c_dump_p_s__expression(_d,(((struct _loop_expression *)_e)->_block));
break;
}
case 13:
{
_scan_list_of_expressions_p_s__c_dump_p_s__expression(_d,(((struct _rept_expression *)_e)->_block));
break;
}
case 14:
{
_i_4=(((struct _aswitch_expression *)_e)->_rules);
while(((void*)_i_4!=(void*)0))
{
_scan_list_of_expressions_p_s__c_dump_p_s__expression(_d,(_i_4->_block));
_i_4=((struct _aswitch_item *)(((struct _element *)_i_4)->_next_sibling));
}
break;
}
case 15:
{
_c_5=(((struct _condition_expression *)_e)->_value);
while(((void*)_c_5!=(void*)0))
{
if(((void*)(_c_5->_condition)!=(void*)0))
{
_scan_expression_p_s__c_dump_p_s__expression(_d,(_c_5->_condition));
}
_scan_list_of_expressions_p_s__c_dump_p_s__expression(_d,(_c_5->_block));
_c_5=((struct _condition_item *)(((struct _element *)_c_5)->_next_sibling));
}
break;
}
case 16:
{
_scan_expression_p_s__c_dump_p_s__expression(_d,(((struct _condition_operator_expression *)_e)->_condition));
_scan_expression_p_s__c_dump_p_s__expression(_d,(((struct _condition_operator_expression *)_e)->_then_expression));
_scan_expression_p_s__c_dump_p_s__expression(_d,(((struct _condition_operator_expression *)_e)->_otherwise_expression));
break;
}
case 18:
{
_scan_list_of_expressions_p_s__c_dump_p_s__expression(_d,(((struct _expression_expression *)_e)->_value));
break;
}
case 6:
{
_scan_list_of_expressions_p_s__c_dump_p_s__expression(_d,(((struct _collection_expression *)_e)->_value));
break;
}
case 7:
{
_scan_list_of_expressions_p_s__c_dump_p_s__expression(_d,(((struct _sequence_expression *)_e)->_sequence));
break;
}
}
}

static void _scan_list_of_expressions_p_s__c_dump_p_s__expression(struct _c_dump *_d, struct _expression *_expressions)
{
struct _expression *_e_1;
_e_1=_expressions;
while(((void*)_e_1!=(void*)0))
{
_scan_expression_p_s__c_dump_p_s__expression(_d,_e_1);
_e_1=((struct _expression *)(((struct _element *)_e_1)->_next_sibling));
}
}

static struct _string_buffer *_append_p_s__string_label_p_s__string_buffer(struct _string_label *_self, struct _string_buffer *_stream)
{
_append_p_s__string_buffer_p_a_ch(_stream,(_self->_id));
return _stream;
}

static void _string_label_p_a_ch_p_s__string_label(char *_s, struct _string_label *_self)
{
(((struct _label *)_self)->_append)=((struct _string_buffer *(*)(struct _label *,struct _string_buffer *))_append_p_s__string_label_p_s__string_buffer);
(_self->_id)=_s;
}

static struct _string_buffer *_append_p_s__pointer_label_p_s__string_buffer(struct _pointer_label *_self, struct _string_buffer *_stream)
{
((_self->_inner)->_append)((_self->_inner),_append_p_s__string_buffer_ch(_stream,'*'));
return _stream;
}

static void _pointer_label_p_s__label_p_s__pointer_label(struct _label *_inner, struct _pointer_label *_self)
{
(((struct _label *)_self)->_append)=((struct _string_buffer *(*)(struct _label *,struct _string_buffer *))_append_p_s__pointer_label_p_s__string_buffer);
(_self->_inner)=_inner;
}

static struct _string_buffer *_append_p_s__array_label_p_s__string_buffer(struct _array_label *_self, struct _string_buffer *_stream)
{
_append_p_s__string_buffer_ch(((_self->_inner)->_append)((_self->_inner),_append_p_s__string_buffer_ch(_stream,'(')),')');
_append_p_s__string_buffer_ch(_stream,'[');
if(((_self->_size)!=0))
{
_append_p_s__string_buffer_in(_stream,(_self->_size));
}
_append_p_s__string_buffer_ch(_stream,']');
return _stream;
}

static void _array_label_in_p_s__label_p_s__array_label(int _size, struct _label *_inner, struct _array_label *_self)
{
(((struct _label *)_self)->_append)=((struct _string_buffer *(*)(struct _label *,struct _string_buffer *))_append_p_s__array_label_p_s__string_buffer);
(_self->_inner)=_inner;
(_self->_size)=_size;
}

static struct _string_buffer *_append_p_s__function_type_label_p_s__string_buffer(struct _function_type_label *_self, struct _string_buffer *_stream)
{
struct _signature *_signature_1;
int _i_2;
int _n_3;
struct _type *_arg_type_4;
_append_p_s__string_buffer_ch(_stream,'(');
((_self->_inner)->_append)((_self->_inner),_stream);
_append_p_s__string_buffer_p_a_ch(_stream,T1385);
_signature_1=((_self->_function)->_signature);
_i_2=0;
_n_3=(_signature_1->_number_of_types);
while((_i_2<_n_3))
{
_arg_type_4=_type_p_s__signature_in(_signature_1,_i_2);
if((_i_2>0))
{
_append_p_s__string_buffer_ch(_stream,',');
}
_flush_declaration_p_s__string_buffer_p_s__type_p_a_ch(_stream,_arg_type_4,_empty_string171);
(++_i_2);
}
_append_p_s__string_buffer_ch(_stream,')');
return _stream;
}

static void _function_type_label_p_s__function_type_p_s__label_p_s__function_type_label(struct _function_type *_f, struct _label *_inner, struct _function_type_label *_self)
{
(((struct _label *)_self)->_append)=((struct _string_buffer *(*)(struct _label *,struct _string_buffer *))_append_p_s__function_type_label_p_s__string_buffer);
(_self->_function)=_f;
(_self->_inner)=_inner;
}

static void _flush_declaration_p_s__string_buffer_p_s__type_p_s__label(struct _string_buffer *_stream, struct _type *_type, struct _label *_label)
{
struct _pointer_label _t_1;
struct _pointer_label *_ptr_2;
struct _type *_target_3;
struct _array_label _t_4;
struct _array_label *_ary_5;
struct _function_type_label _t_6;
struct _function_type_label *_f_7;
switch((_type->_id))
{
case 3:
{
_append_p_s__string_buffer_p_a_ch(_stream,T145);
break;
}
case 6:
{
_append_p_s__string_buffer_p_a_ch(_stream,T1374);
break;
}
case 7:
{
_append_p_s__string_buffer_p_a_ch(_stream,T1375);
break;
}
case 8:
{
_append_p_s__string_buffer_p_a_ch(_stream,T1376);
break;
}
case 9:
{
_append_p_s__string_buffer_p_a_ch(_stream,T1377);
break;
}
case 10:
{
_append_p_s__string_buffer_p_a_ch(_stream,T224);
break;
}
case 11:
{
_append_p_s__string_buffer_p_a_ch(_stream,T142);
break;
}
case 12:
{
_append_p_s__string_buffer_p_a_ch(_stream,T145);
break;
}
case 13:
{
_append_p_s__string_buffer_p_a_ch(_stream,T1378);
break;
}
case 14:
{
_append_p_s__string_buffer_p_a_ch(_stream,T145);
break;
}
case 4:
{
_append_p_s__string_buffer_p_a_ch(_stream,T224);
break;
}
case 5:
{
_append_p_s__string_buffer_p_a_ch(_stream,T1375);
break;
}
case 1:
{
_append_p_s__string_buffer_p_a_ch(_stream,T115);
break;
}
case 2:
{
_append_p_s__string_buffer_p_a_ch(_stream,T1379);
break;
}
case 16:
{
_append_p_s__string_buffer_p_a_ch(_stream,T294);
_append_p_s__string_buffer_p_a_ch(_stream,(((struct _structure_type *)_type)->_extern_name));
break;
}
case 17:
{
_append_p_s__string_buffer_p_a_ch(_stream,T295);
_append_p_s__string_buffer_p_a_ch(_stream,(((struct _structure_type *)_type)->_extern_name));
break;
}
case 15:
{
_ptr_2=(_pointer_label_p_s__label_p_s__pointer_label(_label,(&_t_1)),(&_t_1));
_target_3=(((struct _pointer_type *)_type)->_target_type);
if(((_target_3->_id)==18))
{
_target_3=(((struct _array_type *)_target_3)->_array_type);
}
_flush_declaration_p_s__string_buffer_p_s__type_p_s__label(_stream,_target_3,((struct _label *)_ptr_2));
return;
break;
}
case 18:
{
_ary_5=(_array_label_in_p_s__label_p_s__array_label((((struct _array_type *)_type)->_array_size),_label,(&_t_4)),(&_t_4));
_flush_declaration_p_s__string_buffer_p_s__type_p_s__label(_stream,(((struct _array_type *)_type)->_array_type),((struct _label *)_ary_5));
return;
break;
}
case 19:
{
_f_7=(_function_type_label_p_s__function_type_p_s__label_p_s__function_type_label(((struct _function_type *)_type),_label,(&_t_6)),(&_t_6));
_flush_declaration_p_s__string_buffer_p_s__type_p_s__label(_stream,(((struct _function_type *)_type)->_returned_type),((struct _label *)_f_7));
return;
break;
}
default:
{
_append_p_s__string_buffer_p_a_ch(_stream,T1392);
break;
}
}
(_label->_append)(_label,_append_p_s__string_buffer_ch(_stream,' '));
}

static void _flush_declaration_p_s__string_buffer_p_s__type_p_a_ch(struct _string_buffer *_stream, struct _type *_type, char *_name)
{
struct _string_label _t_1;
struct _string_label *_label_2;
_label_2=(_string_label_p_a_ch_p_s__string_label(_name,(&_t_1)),(&_t_1));
_flush_declaration_p_s__string_buffer_p_s__type_p_s__label(_stream,_type,((struct _label *)_label_2));
}

static struct _string_buffer *_append_p_s__identifier_label_p_s__string_buffer(struct _identifier_label *_self, struct _string_buffer *_stream)
{
_append_identifier_p_s__string_buffer_p_a_ch(_stream,(_self->_id));
return _stream;
}

static void _identifier_label_p_a_ch_p_s__identifier_label(char *_s, struct _identifier_label *_self)
{
(((struct _label *)_self)->_append)=((struct _string_buffer *(*)(struct _label *,struct _string_buffer *))_append_p_s__identifier_label_p_s__string_buffer);
(_self->_id)=_s;
}

static void _flush_raw_declaration_p_s__string_buffer_p_s__type_p_a_ch(struct _string_buffer *_stream, struct _type *_type, char *_name)
{
struct _identifier_label _t_1;
struct _identifier_label *_label_2;
_label_2=(_identifier_label_p_a_ch_p_s__identifier_label(_name,(&_t_1)),(&_t_1));
_flush_declaration_p_s__string_buffer_p_s__type_p_s__label(_stream,_type,((struct _label *)_label_2));
}

static struct _string_buffer *_append_p_s__function_label_p_s__string_buffer(struct _function_label *_self, struct _string_buffer *_sb)
{
struct _function *_f_1;
char *_cc_2;
int _comma_3;
struct _argument *_a_4;
_f_1=(_self->_function);
_cc_2=_value_p_s__set_of_directives_p_a_ch((_f_1->_directives),T1403);
if(((void*)_cc_2!=(void*)0))
{
_append_p_s__string_buffer_ch(_append_p_s__string_buffer_p_a_ch(_sb,_cc_2),' ');
}
_append_p_s__string_buffer_p_a_ch(_sb,(_f_1->_extern_name));
_append_p_s__string_buffer_ch(_sb,'(');
_comma_3=0;
_a_4=(_f_1->_arguments);
while(((void*)_a_4!=(void*)0))
{
if(_comma_3)
{
_append_p_s__string_buffer_p_a_ch(_sb,T303);
}
else
{
_comma_3=1;
}
_flush_raw_declaration_p_s__string_buffer_p_s__type_p_a_ch(_sb,(((struct _definition *)_a_4)->_type),(((struct _definition *)_a_4)->_name));
_a_4=((struct _argument *)(((struct _element *)_a_4)->_next_sibling));
}
_append_p_s__string_buffer_ch(_sb,')');
return _sb;
}

static void _function_label_p_s__function_p_s__function_label(struct _function *_f, struct _function_label *_self)
{
(((struct _label *)_self)->_append)=((struct _string_buffer *(*)(struct _label *,struct _string_buffer *))_append_p_s__function_label_p_s__string_buffer);
(_self->_function)=_f;
}

static void _flush_function_declaration_p_s__string_buffer_p_s__function(struct _string_buffer *_stream, struct _function *_function)
{
struct _function_label _t_1;
struct _function_label *_label_2;
struct _type *_type_3;
_label_2=(_function_label_p_s__function_p_s__function_label(_function,(&_t_1)),(&_t_1));
_type_3=(((struct _definition *)_function)->_type);
_flush_declaration_p_s__string_buffer_p_s__type_p_s__label(_stream,_type_3,((struct _label *)_label_2));
}

static void _flush_char_p_s__string_buffer_ch(struct _string_buffer *_stream, char _c)
{
char _q_1;
if((((unsigned char )_c)<((unsigned char)32)))
{
switch(_c)
{
case 7:
{
_append_p_s__string_buffer_p_a_ch(_stream,T374);
break;
}
case 8:
{
_append_p_s__string_buffer_p_a_ch(_stream,T375);
break;
}
case 12:
{
_append_p_s__string_buffer_p_a_ch(_stream,T376);
break;
}
case 10:
{
_append_p_s__string_buffer_p_a_ch(_stream,T377);
break;
}
case 13:
{
_append_p_s__string_buffer_p_a_ch(_stream,T378);
break;
}
case 9:
{
_append_p_s__string_buffer_p_a_ch(_stream,T379);
break;
}
case 11:
{
_append_p_s__string_buffer_p_a_ch(_stream,T380);
break;
}
default:
{
_append_p_s__string_buffer_p_a_ch(_stream,T1420);
_q_1=(_c>>4);
if((_q_1>='\n'))
{
_q_1+='W';
}
else
{
_q_1+='0';
}
_append_p_s__string_buffer_ch(_stream,_q_1);
_q_1=(_c&'\x0f');
if((_q_1>='\n'))
{
_q_1+='W';
}
else
{
_q_1+='0';
}
_append_p_s__string_buffer_ch(_stream,_q_1);
break;
}
}
}
else
{
switch(_c)
{
case 34:
{
_append_p_s__string_buffer_p_a_ch(_stream,T1422);
break;
}
case 39:
{
_append_p_s__string_buffer_p_a_ch(_stream,T1423);
break;
}
case 92:
{
_append_p_s__string_buffer_p_a_ch(_stream,T1424);
break;
}
default:
{
_append_p_s__string_buffer_ch(_stream,_c);
break;
}
}
}
}

static void _flush_pointer_comparator_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(struct _c_dump *_d, struct _string_buffer *_stream, char *_op, struct _expression *_arguments)
{
_append_p_s__string_buffer_p_a_ch(_stream,T1462);
_flush_expression_p_s__c_dump_p_s__string_buffer_p_s__expression(_d,_stream,_arguments);
_append_p_s__string_buffer_p_a_ch(_stream,_op);
_append_p_s__string_buffer_p_a_ch(_stream,T1465);
_flush_expression_p_s__c_dump_p_s__string_buffer_p_s__expression(_d,_stream,((struct _expression *)(((struct _element *)_arguments)->_next_sibling)));
_append_p_s__string_buffer_ch(_stream,')');
}

static void _flush_binary_operator_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(struct _c_dump *_d, struct _string_buffer *_stream, char *_op, struct _expression *_arguments)
{
_append_p_s__string_buffer_ch(_stream,'(');
_flush_expression_p_s__c_dump_p_s__string_buffer_p_s__expression(_d,_stream,_arguments);
_append_p_s__string_buffer_p_a_ch(_stream,_op);
_flush_expression_p_s__c_dump_p_s__string_buffer_p_s__expression(_d,_stream,((struct _expression *)(((struct _element *)_arguments)->_next_sibling)));
_append_p_s__string_buffer_ch(_stream,')');
}

static void _flush_prefix_operator_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(struct _c_dump *_d, struct _string_buffer *_stream, char *_op, struct _expression *_e)
{
_append_p_s__string_buffer_ch(_stream,'(');
_append_p_s__string_buffer_p_a_ch(_stream,_op);
_flush_expression_p_s__c_dump_p_s__string_buffer_p_s__expression(_d,_stream,_e);
_append_p_s__string_buffer_ch(_stream,')');
}

static void _flush_assignment_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(struct _c_dump *_d, struct _string_buffer *_stream, char *_op, struct _expression *_arguments)
{
_flush_expression_p_s__c_dump_p_s__string_buffer_p_s__expression(_d,_stream,_arguments);
_append_p_s__string_buffer_p_a_ch(_stream,_op);
_flush_expression_p_s__c_dump_p_s__string_buffer_p_s__expression(_d,_stream,((struct _expression *)(((struct _element *)_arguments)->_next_sibling)));
}

static void _flush_unary_assignment_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(struct _c_dump *_d, struct _string_buffer *_stream, char *_op, struct _expression *_arguments)
{
_flush_expression_p_s__c_dump_p_s__string_buffer_p_s__expression(_d,_stream,_arguments);
_append_p_s__string_buffer_p_a_ch(_stream,_op);
_flush_expression_p_s__c_dump_p_s__string_buffer_p_s__expression(_d,_stream,_arguments);
}

static void _flush_postfix_operator_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(struct _c_dump *_d, struct _string_buffer *_stream, char *_op, struct _expression *_e)
{
_append_p_s__string_buffer_ch(_stream,'(');
_flush_expression_p_s__c_dump_p_s__string_buffer_p_s__expression(_d,_stream,_e);
_append_p_s__string_buffer_p_a_ch(_stream,_op);
_append_p_s__string_buffer_ch(_stream,')');
}

static void _flush_definition_p_s__c_dump_p_s__string_buffer_p_s__definition_p_s__expression(struct _c_dump *_d, struct _string_buffer *_stream, struct _definition *_definition, struct _expression *_arguments)
{
struct _expression *_e_1;
int _ea_2;
struct _argument *_a_3;
struct _expression *_e_4;
struct _expression *_left_5;
struct _expression *_right_6;
struct _type *_type_7;
struct _expression *_pointer_8;
int _ea_9;
struct _expression *_array_10;
struct _expression *_index_11;
struct _type *_array_type_12;
int _ea_13;
switch((_definition->_opcode))
{
case 0:
{
_append_identifier_p_s__string_buffer_p_a_ch(_stream,(_definition->_name));
break;
}
case 1:
{
if(((void*)(((struct _variable *)_definition)->_reference)!=(void*)0))
{
_flush_expression_p_s__c_dump_p_s__string_buffer_p_s__expression(_d,_stream,(((struct _variable *)_definition)->_reference));
}
else if(((((((struct _variable *)_definition)->_storage_type)->_flags)&2)!=0))
{
_append_p_s__string_buffer_p_a_ch(_stream,T1449);
_append_p_s__string_buffer_p_a_ch(_stream,(((struct _variable *)_definition)->_extern_name));
_append_p_s__string_buffer_ch(_stream,')');
}
else
{
_append_p_s__string_buffer_p_a_ch(_stream,(((struct _variable *)_definition)->_extern_name));
}
break;
}
case 2:
{
_e_1=_arguments;
_ea_2=((((((struct _attribute *)_definition)->_storage_type)->_flags)&2)!=0);
if(_ea_2)
{
_append_p_s__string_buffer_p_a_ch(_stream,T1449);
}
_append_p_s__string_buffer_ch(_stream,'(');
_flush_expression_p_s__c_dump_p_s__string_buffer_p_s__expression_p_s__type(_d,_stream,_e_1,_pointer_p_s__type(((struct _type *)(((struct _attribute *)_definition)->_structure))));
_append_p_s__string_buffer_p_a_ch(_stream,T430);
_append_identifier_p_s__string_buffer_p_a_ch(_stream,(((struct _definition *)((struct _attribute *)_definition))->_name));
_append_p_s__string_buffer_ch(_stream,')');
if(_ea_2)
{
_append_p_s__string_buffer_ch(_stream,')');
}
break;
}
case 3:
{
_append_p_s__string_buffer_p_a_ch(_stream,(((struct _function *)_definition)->_extern_name));
_append_p_s__string_buffer_ch(_stream,'(');
_a_3=(((struct _function *)_definition)->_arguments);
_e_4=_arguments;
while(((void*)_e_4!=(void*)0))
{
_flush_expression_p_s__c_dump_p_s__string_buffer_p_s__expression_p_s__type(_d,_stream,_e_4,(((struct _definition *)_a_3)->_type));
_a_3=((struct _argument *)(((struct _element *)_a_3)->_next_sibling));
if(((void*)_a_3!=(void*)0))
{
_append_p_s__string_buffer_ch(_stream,',');
}
_e_4=((struct _expression *)(((struct _element *)_e_4)->_next_sibling));
}
_append_p_s__string_buffer_ch(_stream,')');
break;
}
case 6:
{
_append_p_s__string_buffer_p_a_ch(_stream,T236);
break;
}
case 7:
{
_append_p_s__string_buffer_p_a_ch(_stream,T237);
break;
}
case 66:
{
_left_5=_arguments;
_right_6=((struct _expression *)(((struct _element *)_left_5)->_next_sibling));
while(((_left_5->_opcode)==17))
{
_left_5=(((struct _expression_expression *)_left_5)->_value);
}
_type_7=(_left_5->_type);
_flush_expression_p_s__c_dump_p_s__string_buffer_p_s__expression(_d,_stream,_left_5);
_append_p_s__string_buffer_ch(_stream,'=');
_flush_expression_p_s__c_dump_p_s__string_buffer_p_s__expression_p_s__type(_d,_stream,_right_6,_type_7);
break;
}
case 67:
{
_pointer_8=_arguments;
_ea_9=((((((struct _pointer_type *)(_pointer_8->_type))->_target_type)->_flags)&2)!=0);
if((!_ea_9))
{
_append_p_s__string_buffer_p_a_ch(_stream,T1461);
}
_flush_expression_p_s__c_dump_p_s__string_buffer_p_s__expression(_d,_stream,_pointer_8);
if((!_ea_9))
{
_append_p_s__string_buffer_ch(_stream,')');
}
break;
}
case 8:
{
_append_p_s__string_buffer_p_a_ch(_stream,T1462);
_flush_expression_p_s__c_dump_p_s__string_buffer_p_s__expression(_d,_stream,_arguments);
_append_p_s__string_buffer_p_a_ch(_stream,T1463);
break;
}
case 9:
{
_append_p_s__string_buffer_p_a_ch(_stream,T1462);
_flush_expression_p_s__c_dump_p_s__string_buffer_p_s__expression(_d,_stream,_arguments);
_append_p_s__string_buffer_p_a_ch(_stream,T1464);
break;
}
case 43:
{
_flush_pointer_comparator_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T411,_arguments);
break;
}
case 44:
{
_flush_pointer_comparator_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T1466,_arguments);
break;
}
case 45:
{
_flush_pointer_comparator_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T413,_arguments);
break;
}
case 46:
{
_flush_pointer_comparator_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T414,_arguments);
break;
}
case 47:
{
_flush_pointer_comparator_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T415,_arguments);
break;
}
case 48:
{
_flush_pointer_comparator_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T416,_arguments);
break;
}
case 16:
case 41:
{
_flush_binary_operator_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T411,_arguments);
break;
}
case 17:
case 42:
{
_flush_binary_operator_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T1466,_arguments);
break;
}
case 18:
{
_flush_binary_operator_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T413,_arguments);
break;
}
case 19:
{
_flush_binary_operator_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T414,_arguments);
break;
}
case 20:
{
_flush_binary_operator_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T415,_arguments);
break;
}
case 21:
{
_flush_binary_operator_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T416,_arguments);
break;
}
case 22:
case 35:
{
_flush_binary_operator_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T390,_arguments);
break;
}
case 23:
case 36:
{
_flush_binary_operator_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T391,_arguments);
break;
}
case 39:
{
_flush_binary_operator_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T396,_arguments);
break;
}
case 40:
{
_flush_binary_operator_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T397,_arguments);
break;
}
case 24:
{
_flush_binary_operator_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T392,_arguments);
break;
}
case 37:
{
_flush_binary_operator_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T1466,_arguments);
break;
}
case 25:
{
_flush_prefix_operator_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T393,_arguments);
break;
}
case 38:
{
_flush_prefix_operator_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T420,_arguments);
break;
}
case 26:
{
_flush_binary_operator_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T383,_arguments);
break;
}
case 27:
{
_flush_binary_operator_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T384,_arguments);
break;
}
case 28:
{
_flush_binary_operator_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T385,_arguments);
break;
}
case 29:
{
_flush_binary_operator_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T386,_arguments);
break;
}
case 30:
{
_flush_binary_operator_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T387,_arguments);
break;
}
case 31:
{
_flush_binary_operator_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T388,_arguments);
break;
}
case 32:
{
_flush_binary_operator_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T389,_arguments);
break;
}
case 49:
{
_flush_assignment_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T408,_arguments);
break;
}
case 50:
{
_flush_assignment_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T398,_arguments);
break;
}
case 51:
{
_flush_assignment_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T399,_arguments);
break;
}
case 52:
{
_flush_assignment_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T400,_arguments);
break;
}
case 53:
{
_flush_assignment_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T401,_arguments);
break;
}
case 54:
{
_flush_assignment_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T402,_arguments);
break;
}
case 55:
{
_flush_assignment_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T403,_arguments);
break;
}
case 56:
{
_flush_assignment_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T404,_arguments);
break;
}
case 57:
{
_flush_assignment_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T405,_arguments);
break;
}
case 58:
{
_flush_assignment_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T406,_arguments);
break;
}
case 59:
{
_flush_assignment_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T407,_arguments);
break;
}
case 61:
{
_flush_unary_assignment_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T1467,_arguments);
break;
}
case 60:
{
_flush_unary_assignment_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T1468,_arguments);
break;
}
case 33:
{
_flush_expression_p_s__c_dump_p_s__string_buffer_p_s__expression(_d,_stream,_arguments);
break;
}
case 34:
{
_flush_prefix_operator_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T384,_arguments);
break;
}
case 62:
{
_flush_prefix_operator_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T394,_arguments);
break;
}
case 63:
{
_flush_prefix_operator_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T395,_arguments);
break;
}
case 64:
{
_flush_postfix_operator_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T394,_arguments);
break;
}
case 65:
{
_flush_postfix_operator_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T395,_arguments);
break;
}
case 69:
{
_flush_binary_operator_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T383,_arguments);
break;
}
case 70:
{
_flush_binary_operator_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T384,_arguments);
break;
}
case 72:
{
_flush_prefix_operator_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T394,_arguments);
break;
}
case 73:
{
_flush_prefix_operator_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T395,_arguments);
break;
}
case 74:
{
_flush_postfix_operator_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T394,_arguments);
break;
}
case 75:
{
_flush_postfix_operator_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T395,_arguments);
break;
}
case 76:
{
_flush_assignment_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T398,_arguments);
break;
}
case 77:
{
_flush_assignment_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T399,_arguments);
break;
}
case 71:
{
_flush_binary_operator_p_s__c_dump_p_s__string_buffer_p_a_ch_p_s__expression(_d,_stream,T384,_arguments);
break;
}
case 68:
{
_array_10=_arguments;
_index_11=((struct _expression *)(((struct _element *)_arguments)->_next_sibling));
_array_type_12=(((struct _pointer_type *)(_arguments->_type))->_target_type);
_ea_13=((((((struct _array_type *)_array_type_12)->_array_type)->_flags)&2)!=0);
if(_ea_13)
{
_append_p_s__string_buffer_p_a_ch(_stream,T1449);
}
_append_p_s__string_buffer_ch(_stream,'(');
_flush_expression_p_s__c_dump_p_s__string_buffer_p_s__expression(_d,_stream,_array_10);
_append_p_s__string_buffer_ch(_stream,'[');
if(((void*)_index_11!=(void*)0))
{
_flush_expression_p_s__c_dump_p_s__string_buffer_p_s__expression(_d,_stream,_index_11);
}
else
{
_append_p_s__string_buffer_p_a_ch(_stream,T1473);
}
_append_p_s__string_buffer_p_a_ch(_stream,T1474);
if(_ea_13)
{
_append_p_s__string_buffer_ch(_stream,')');
}
break;
}
default:
{
_append_p_s__string_buffer_p_a_ch(_stream,T1475);
break;
}
}
}

static void _flush_definition_p_s__c_dump_p_s__string_buffer_p_s__expression(struct _c_dump *_d, struct _string_buffer *_stream, struct _expression *_e)
{
struct _definition *_definition_1;
struct _expression *_arguments_2;
_definition_1=(((struct _definition_expression *)_e)->_definition);
_arguments_2=(((struct _definition_expression *)_e)->_arguments);
_flush_definition_p_s__c_dump_p_s__string_buffer_p_s__definition_p_s__expression(_d,_stream,_definition_1,_arguments_2);
}

static void _flush_instruction_p_s__c_dump_p_s__string_buffer_p_s__expression(struct _c_dump *_d, struct _string_buffer *_stream, struct _expression *_e)
{
if(_flush_expression_p_s__c_dump_p_s__string_buffer_p_s__expression(_d,_stream,_e))
{
_append_p_s__string_buffer_p_a_ch(_stream,T1367);
}
}

static void _flush_instructions_p_s__c_dump_p_s__string_buffer_p_s__expression(struct _c_dump *_d, struct _string_buffer *_stream, struct _expression *_block)
{
struct _expression *_e_1;
_e_1=_block;
while(((void*)_e_1!=(void*)0))
{
_flush_instruction_p_s__c_dump_p_s__string_buffer_p_s__expression(_d,_stream,_e_1);
_e_1=((struct _expression *)(((struct _element *)_e_1)->_next_sibling));
}
}

static void _flush_block_p_s__c_dump_p_s__string_buffer_p_s__expression(struct _c_dump *_d, struct _string_buffer *_stream, struct _expression *_block)
{
_append_p_s__string_buffer_p_a_ch(_stream,T1484);
_flush_instructions_p_s__c_dump_p_s__string_buffer_p_s__expression(_d,_stream,_block);
_append_p_s__string_buffer_p_a_ch(_stream,T1486);
}

static int _flush_expression_p_s__c_dump_p_s__string_buffer_p_s__expression(struct _c_dump *_d, struct _string_buffer *_stream, struct _expression *_e)
{
int _value_1;
struct _function *_function_2;
struct _variable *_variable_3;
struct _condition_item *_item_4;
char *_prefix_5;
struct _expression *_condition_6;
struct _expression *_block_7;
struct _expression *_block_8;
struct _expression *_block_9;
struct _expression *_expression_10;
struct _aswitch_item *_item_11;
struct _constant *_c_12;
struct _type *_type_13;
struct _expression *_org_14;
struct _expression *_arguments_15;
struct _expression *_reference_16;
struct _type *_type_17;
struct _function_type *_function_type_18;
struct _signature *_signature_19;
int _i_20;
struct _expression *_arg_21;
int _comma_22;
struct _expression *_a_23;
switch((_e->_opcode))
{
case 19:
case 10:
{
return 0;
break;
}
case 1:
{
switch(((_e->_type)->_id))
{
case 6:
{
_append_p_s__string_buffer_p_a_ch(_stream,T1441);
_append_p_s__string_buffer_in(_stream,(((struct _long_expression *)_e)->_value));
_append_p_s__string_buffer_ch(_stream,')');
break;
}
case 7:
{
_append_p_s__string_buffer_p_a_ch(_stream,T1442);
_append_p_s__string_buffer_in(_stream,(((struct _long_expression *)_e)->_value));
_append_p_s__string_buffer_ch(_stream,')');
break;
}
case 8:
{
_append_p_s__string_buffer_p_a_ch(_stream,T1443);
_append_p_s__string_buffer_in(_stream,(((struct _long_expression *)_e)->_value));
_append_p_s__string_buffer_ch(_stream,')');
break;
}
case 9:
{
_append_p_s__string_buffer_p_a_ch(_stream,T1444);
_append_p_s__string_buffer_in(_stream,(((struct _long_expression *)_e)->_value));
_append_p_s__string_buffer_ch(_stream,')');
break;
}
case 10:
{
_append_p_s__string_buffer_p_a_ch(_stream,T1445);
_append_p_s__string_buffer_in(_stream,(((struct _long_expression *)_e)->_value));
_append_p_s__string_buffer_ch(_stream,')');
break;
}
case 11:
{
_append_p_s__string_buffer_p_a_ch(_stream,T1446);
_append_p_s__string_buffer_in(_stream,(((struct _long_expression *)_e)->_value));
_append_p_s__string_buffer_ch(_stream,')');
break;
}
case 13:
{
_append_p_s__string_buffer_p_a_ch(_stream,T1447);
_append_p_s__string_buffer_in(_stream,(((struct _long_expression *)_e)->_value));
_append_p_s__string_buffer_ch(_stream,')');
break;
}
case 4:
{
_append_p_s__string_buffer_ch(_stream,'\'');
_flush_char_p_s__string_buffer_ch(_stream,((char )(((struct _long_expression *)_e)->_value)));
_append_p_s__string_buffer_ch(_stream,'\'');
break;
}
case 5:
{
_append_p_s__string_buffer_p_a_ch(_stream,T1446);
_append_p_s__string_buffer_in(_stream,(((struct _long_expression *)_e)->_value));
_append_p_s__string_buffer_ch(_stream,')');
break;
}
case 15:
{
_append_p_s__string_buffer_p_a_ch(_stream,T1440);
_flush_declaration_p_s__string_buffer_p_s__type_p_a_ch(_stream,(_e->_type),_empty_string171);
_append_p_s__string_buffer_p_a_ch(_stream,T425);
_append_p_s__string_buffer_in(_stream,(((struct _long_expression *)_e)->_value));
_append_p_s__string_buffer_ch(_stream,')');
break;
}
default:
{
_value_1=(((struct _long_expression *)_e)->_value);
if((_value_1!=2147483648U))
{
_append_p_s__string_buffer_in(_stream,_value_1);
}
else
{
_append_p_s__string_buffer_ch(_append_p_s__string_buffer_dw(_stream,((unsigned int )_value_1)),'U');
}
break;
}
}
return 1;
break;
}
case 2:
{
_append_p_s__string_buffer_p_a_ch(_stream,(((struct _global_constant *)(_e->_user_data))->_name));
return 1;
break;
}
case 5:
{
if(((((((struct _pointer_type *)(_e->_type))->_target_type)->_flags)&2)!=0))
{
_append_p_s__string_buffer_ch(_append_p_s__string_buffer_p_a_ch(_append_p_s__string_buffer_p_a_ch(_stream,T1449),(((struct _global_constant *)(_e->_user_data))->_name)),')');
}
else
{
_append_p_s__string_buffer_p_a_ch(_stream,(((struct _global_constant *)(_e->_user_data))->_name));
}
return 1;
break;
}
case 3:
{
_flush_definition_p_s__c_dump_p_s__string_buffer_p_s__expression(_d,_stream,_e);
return 1;
break;
}
case 4:
{
_function_2=(((struct _function_expression *)_e)->_value);
_append_p_s__string_buffer_p_a_ch(_stream,(_function_2->_extern_name));
return 1;
break;
}
case 9:
{
_variable_3=(((struct _variable_expression *)_e)->_value);
if(((void*)(_variable_3->_initial_value)!=(void*)0))
{
_append_p_s__string_buffer_p_a_ch(_stream,(_variable_3->_extern_name));
_append_p_s__string_buffer_ch(_stream,'=');
_flush_expression_p_s__c_dump_p_s__string_buffer_p_s__expression_p_s__type(_d,_stream,(_variable_3->_initial_value),(((struct _definition *)_variable_3)->_type));
return 1;
}
else if(((void*)(_variable_3->_proxy)!=(void*)0))
{
_flush_expression_p_s__c_dump_p_s__string_buffer_p_s__expression(_d,_stream,(_variable_3->_proxy));
return 1;
}
break;
}
case 11:
{
_append_p_s__string_buffer_p_a_ch(_stream,T31);
if(((void*)(((struct _expression_expression *)_e)->_value)!=(void*)0))
{
_append_p_s__string_buffer_ch(_stream,' ');
_flush_expression_p_s__c_dump_p_s__string_buffer_p_s__expression_p_s__type(_d,_stream,(((struct _expression_expression *)_e)->_value),(((struct _definition *)(_d->_current_function))->_type));
}
return 1;
break;
}
case 15:
{
_item_4=(((struct _condition_expression *)_e)->_value);
_prefix_5=T1479;
while(((void*)_item_4!=(void*)0))
{
if(((void*)(_item_4->_condition)!=(void*)0))
{
_append_p_s__string_buffer_p_a_ch(_stream,_prefix_5);
_flush_expression_p_s__c_dump_p_s__string_buffer_p_s__expression(_d,_stream,(_item_4->_condition));
_append_p_s__string_buffer_p_a_ch(_stream,T1481);
_prefix_5=T1482;
}
else
{
_append_p_s__string_buffer_p_a_ch(_stream,T1483);
}
_flush_block_p_s__c_dump_p_s__string_buffer_p_s__expression(_d,_stream,(_item_4->_block));
_item_4=((struct _condition_item *)(((struct _element *)_item_4)->_next_sibling));
}
break;
}
case 12:
{
_condition_6=(((struct _loop_expression *)_e)->_condition);
_block_7=(((struct _loop_expression *)_e)->_block);
_append_p_s__string_buffer_p_a_ch(_stream,T1489);
_flush_expression_p_s__c_dump_p_s__string_buffer_p_s__expression(_d,_stream,_condition_6);
_append_p_s__string_buffer_p_a_ch(_stream,T1481);
_flush_block_p_s__c_dump_p_s__string_buffer_p_s__expression(_d,_stream,_block_7);
break;
}
case 13:
{
_block_8=(((struct _rept_expression *)_e)->_block);
_append_p_s__string_buffer_p_a_ch(_stream,T1491);
_flush_block_p_s__c_dump_p_s__string_buffer_p_s__expression(_d,_stream,_block_8);
break;
}
case 6:
{
_block_9=(((struct _collection_expression *)_e)->_value);
_flush_instructions_p_s__c_dump_p_s__string_buffer_p_s__expression(_d,_stream,_block_9);
break;
}
case 14:
{
_expression_10=(((struct _aswitch_expression *)_e)->_expression);
_item_11=(((struct _aswitch_expression *)_e)->_rules);
_append_p_s__string_buffer_p_a_ch(_stream,T1495);
_flush_expression_p_s__c_dump_p_s__string_buffer_p_s__expression(_d,_stream,_expression_10);
_append_p_s__string_buffer_p_a_ch(_stream,T1496);
while(((void*)_item_11!=(void*)0))
{
_c_12=(_item_11->_constants);
if(((void*)_c_12!=(void*)0))
{
while(((void*)_c_12!=(void*)0))
{
_append_p_s__string_buffer_p_a_ch(_append_p_s__string_buffer_in(_append_p_s__string_buffer_p_a_ch(_stream,T1498),(_c_12->_value)),T1499);
_c_12=((struct _constant *)(((struct _element *)_c_12)->_next_sibling));
}
}
else
{
_append_p_s__string_buffer_p_a_ch(_stream,T1500);
}
_append_p_s__string_buffer_p_a_ch(_stream,T1484);
_flush_instructions_p_s__c_dump_p_s__string_buffer_p_s__expression(_d,_stream,(_item_11->_block));
_append_p_s__string_buffer_p_a_ch(_stream,T1501);
_item_11=((struct _aswitch_item *)(((struct _element *)_item_11)->_next_sibling));
}
_append_p_s__string_buffer_p_a_ch(_stream,T1486);
break;
}
case 16:
{
_append_p_s__string_buffer_ch(_stream,'(');
_flush_expression_p_s__c_dump_p_s__string_buffer_p_s__expression(_d,_stream,(((struct _condition_operator_expression *)_e)->_condition));
_append_p_s__string_buffer_ch(_stream,'?');
_flush_expression_p_s__c_dump_p_s__string_buffer_p_s__expression_p_s__type(_d,_stream,(((struct _condition_operator_expression *)_e)->_then_expression),(((struct _expression *)((struct _condition_operator_expression *)_e))->_type));
_append_p_s__string_buffer_ch(_stream,':');
_flush_expression_p_s__c_dump_p_s__string_buffer_p_s__expression_p_s__type(_d,_stream,(((struct _condition_operator_expression *)_e)->_otherwise_expression),(((struct _expression *)((struct _condition_operator_expression *)_e))->_type));
_append_p_s__string_buffer_ch(_stream,')');
return 1;
break;
}
case 17:
{
_type_13=(_e->_type);
_org_14=(((struct _expression_expression *)_e)->_value);
_flush_expression_p_s__c_dump_p_s__string_buffer_p_s__expression_p_s__type(_d,_stream,_org_14,_type_13);
return 1;
break;
}
case 18:
{
_arguments_15=(((struct _expression_expression *)_e)->_value);
_reference_16=_arguments_15;
_type_17=(_reference_16->_type);
_function_type_18=((struct _function_type *)(((struct _pointer_type *)_type_17)->_target_type));
_signature_19=(_function_type_18->_signature);
_flush_expression_p_s__c_dump_p_s__string_buffer_p_s__expression(_d,_stream,_reference_16);
_i_20=0;
_arg_21=((struct _expression *)(((struct _element *)_reference_16)->_next_sibling));
_append_p_s__string_buffer_ch(_stream,'(');
_comma_22=0;
while(((void*)_arg_21!=(void*)0))
{
if(_comma_22)
{
_append_p_s__string_buffer_ch(_stream,',');
}
_comma_22=1;
_flush_expression_p_s__c_dump_p_s__string_buffer_p_s__expression_p_s__type(_d,_stream,_arg_21,_type_p_s__signature_in(_signature_19,_i_20));
_arg_21=((struct _expression *)(((struct _element *)_arg_21)->_next_sibling));
(++_i_20);
}
_append_p_s__string_buffer_ch(_stream,')');
return 1;
break;
}
case 7:
{
_append_p_s__string_buffer_ch(_stream,'(');
_a_23=(((struct _sequence_expression *)_e)->_sequence);
while(((void*)_a_23!=(void*)0))
{
_flush_expression_p_s__c_dump_p_s__string_buffer_p_s__expression(_d,_stream,_a_23);
_a_23=((struct _expression *)(((struct _element *)_a_23)->_next_sibling));
if(((void*)_a_23!=(void*)0))
{
_append_p_s__string_buffer_ch(_stream,',');
}
}
_append_p_s__string_buffer_ch(_stream,')');
return 1;
break;
}
default:
{
_append_p_s__string_buffer_p_a_ch(_stream,T1513);
break;
}
}
return 0;
}

static void _flush_expression_p_s__c_dump_p_s__string_buffer_p_s__expression_p_s__type(struct _c_dump *_d, struct _string_buffer *_stream, struct _expression *_e, struct _type *_type)
{
int _cast_1;
_cast_1=(!_equals_p_s__type_p_s__type((_e->_type),_type));
if(_cast_1)
{
_append_p_s__string_buffer_p_a_ch(_stream,T1440);
_flush_declaration_p_s__string_buffer_p_s__type_p_a_ch(_stream,_type,_empty_string171);
_append_p_s__string_buffer_ch(_stream,')');
}
_flush_expression_p_s__c_dump_p_s__string_buffer_p_s__expression(_d,_stream,_e);
if(_cast_1)
{
_append_p_s__string_buffer_ch(_stream,')');
}
}

static struct _expression *_flush_initializer_p_s__c_dump_p_s__string_buffer_p_s__expression_p_s__type(struct _c_dump *_d, struct _string_buffer *_stream, struct _expression *_expression, struct _type *_type)
{
struct _expression *_e_1;
struct _element *_e_2;
struct _type *_cell_type_3;
_e_1=_expression;
if(((_type->_id)==16))
{
if(((void*)((struct _structure_type *)(((struct _structure_type *)_type)->_parent_type))!=(void*)0))
{
_e_1=_flush_initializer_p_s__c_dump_p_s__string_buffer_p_s__expression_p_s__type(_d,_stream,_e_1,((struct _type *)((struct _structure_type *)(((struct _structure_type *)_type)->_parent_type))));
}
_e_2=((struct _element *)(((struct _structure_type *)_type)->_attributes));
while(((void*)_e_2!=(void*)0))
{
_e_1=_flush_initializer_p_s__c_dump_p_s__string_buffer_p_s__expression_p_s__type(_d,_stream,_e_1,(((struct _attribute *)_e_2)->_storage_type));
_e_2=(_e_2->_next_sibling);
}
}
else if(((_type->_id)==17))
{
if(((void*)(((struct _structure_type *)_type)->_attributes)!=(void*)0))
{
_e_1=_flush_initializer_p_s__c_dump_p_s__string_buffer_p_s__expression_p_s__type(_d,_stream,_e_1,((((struct _structure_type *)_type)->_attributes)->_storage_type));
}
}
else if(((_type->_id)==18))
{
_cell_type_3=(((struct _array_type *)_type)->_array_type);
while(((void*)_e_1!=(void*)0))
{
_e_1=_flush_initializer_p_s__c_dump_p_s__string_buffer_p_s__expression_p_s__type(_d,_stream,_e_1,_cell_type_3);
}
}
else
{
_flush_expression_p_s__c_dump_p_s__string_buffer_p_s__expression_p_s__type(_d,_stream,_e_1,_type);
_e_1=((struct _expression *)(((struct _element *)_e_1)->_next_sibling));
if(((void*)_e_1!=(void*)0))
{
_append_p_s__string_buffer_p_a_ch(_stream,T419);
}
}
return _e_1;
}

static void _flush_body_p_s__c_dump_p_s__string_buffer_p_s__function(struct _c_dump *_d, struct _string_buffer *_stream, struct _function *_function)
{
struct _list_item *_e_1;
void *_v_2;
_e_1=((&(_function->_variables))->_first);
while(((void*)_e_1!=(void*)0))
{
_v_2=(_e_1->_value);
((&(_d->_tmp))->_used)=0;
_append_identifier_p_s__string_buffer_p_a_ch((&(_d->_tmp)),(((struct _definition *)((struct _variable *)_v_2))->_name));
_append_p_s__string_buffer_ch((&(_d->_tmp)),'_');
_append_p_s__string_buffer_in((&(_d->_tmp)),((_d->_index)++));
(((struct _variable *)_v_2)->_extern_name)=_new_string_p_s__memory_bundle_p_a_ch((_d->_bundle),_as_string_p_s__string_buffer((&(_d->_tmp))));
_flush_declaration_p_s__string_buffer_p_s__type_p_a_ch(_stream,(((struct _variable *)_v_2)->_storage_type),(((struct _variable *)_v_2)->_extern_name));
_append_p_s__string_buffer_p_a_ch(_stream,T1367);
_e_1=((struct _list_item *)(((struct _element *)_e_1)->_next_sibling));
}
_flush_instructions_p_s__c_dump_p_s__string_buffer_p_s__expression(_d,_stream,(_function->_instructions));
}

static void _flush_module_p_s__c_dump_p_s__string_buffer_p_s__module(struct _c_dump *_d, struct _string_buffer *_stream, struct _module *_module)
{
struct _list_item *_e_1;
void *_v_2;
struct _list_item *_e_3;
void *_v_4;
struct _element *_e_5;
struct _list_item *_e_6;
void *_v_7;
struct _list_item *_e_8;
void *_v_9;
unsigned int _i_10;
unsigned int _size_11;
struct _association *_a_12;
char *_p_13;
char _ch_14;
unsigned int _i_15;
unsigned int _size_16;
struct _association *_a_17;
char *_p_18;
char _ch_19;
struct _list_item *_e_20;
void *_v_21;
struct _global_constant *_c_22;
struct _list_item *_e_23;
void *_v_24;
struct _list_item *_e_25;
void *_v_26;
(_d->_module)=_module;
(_d->_count)=1;
(_d->_current_function)=((struct _function *)0);
_scan_list_of_expressions_p_s__c_dump_p_s__expression(_d,((struct _expression *)(((struct _collection *)(&(_module->_elements)))->_first)));
_append_p_s__string_buffer_p_a_ch(_stream,T1364);
_e_1=((&(_d->_structures))->_first);
while(((void*)_e_1!=(void*)0))
{
_v_2=(_e_1->_value);
if(((((struct _type *)((struct _structure_type *)_v_2))->_id)==17))
{
_append_p_s__string_buffer_p_a_ch(_stream,T295);
}
else
{
_append_p_s__string_buffer_p_a_ch(_stream,T294);
}
_append_p_s__string_buffer_p_a_ch(_stream,(((struct _structure_type *)_v_2)->_extern_name));
_append_p_s__string_buffer_p_a_ch(_stream,T1367);
_e_1=((struct _list_item *)(((struct _element *)_e_1)->_next_sibling));
}
_e_3=((&(_d->_structures))->_first);
while(((void*)_e_3!=(void*)0))
{
_v_4=(_e_3->_value);
if((((struct _structure_type *)_v_4)->_defined))
{
if(((((struct _type *)((struct _structure_type *)_v_4))->_id)==17))
{
_append_p_s__string_buffer_p_a_ch(_stream,T16);
}
else
{
_append_p_s__string_buffer_p_a_ch(_stream,T15);
}
_append_p_s__string_buffer_p_a_ch(_stream,T1370);
_append_p_s__string_buffer_p_a_ch(_stream,(((struct _structure_type *)_v_4)->_extern_name));
_append_p_s__string_buffer_p_a_ch(_stream,T1371);
if(((void*)((struct _structure_type *)(((struct _structure_type *)_v_4)->_parent_type))!=(void*)0))
{
_flush_declaration_p_s__string_buffer_p_s__type_p_a_ch(_stream,((struct _type *)((struct _structure_type *)(((struct _structure_type *)_v_4)->_parent_type))),T1393);
_append_p_s__string_buffer_p_a_ch(_stream,T1367);
}
_e_5=((struct _element *)(((struct _structure_type *)_v_4)->_attributes));
while(((void*)_e_5!=(void*)0))
{
_flush_raw_declaration_p_s__string_buffer_p_s__type_p_a_ch(_stream,(((struct _attribute *)_e_5)->_storage_type),(((struct _definition *)((struct _attribute *)_e_5))->_name));
_append_p_s__string_buffer_p_a_ch(_stream,T1367);
_e_5=(_e_5->_next_sibling);
}
_append_p_s__string_buffer_p_a_ch(_stream,T427);
}
_append_p_s__string_buffer_p_a_ch(_stream,T1367);
_e_3=((struct _list_item *)(((struct _element *)_e_3)->_next_sibling));
}
_append_p_s__string_buffer_p_a_ch(_stream,T1397);
_e_6=((&(_d->_functions))->_first);
while(((void*)_e_6!=(void*)0))
{
_v_7=(_e_6->_value);
if(((!(((struct _definition *)((struct _function *)_v_7))->_expose))&&(((struct _function *)_v_7)->_defined)))
{
_append_p_s__string_buffer_p_a_ch(_stream,T1400);
}
else
{
_append_p_s__string_buffer_p_a_ch(_stream,T1401);
}
_flush_function_declaration_p_s__string_buffer_p_s__function(_stream,((struct _function *)_v_7));
_append_p_s__string_buffer_p_a_ch(_stream,T1367);
_e_6=((struct _list_item *)(((struct _element *)_e_6)->_next_sibling));
}
_append_p_s__string_buffer_p_a_ch(_stream,T1410);
_e_8=((&(_d->_variables))->_first);
while(((void*)_e_8!=(void*)0))
{
_v_9=(_e_8->_value);
if((((struct _variable *)_v_9)->_defined))
{
if((!(((struct _definition *)((struct _variable *)_v_9))->_expose)))
{
_append_p_s__string_buffer_p_a_ch(_stream,T1400);
}
}
else
{
_append_p_s__string_buffer_p_a_ch(_stream,T1401);
}
_flush_declaration_p_s__string_buffer_p_s__type_p_a_ch(_stream,(((struct _variable *)_v_9)->_storage_type),(((struct _variable *)_v_9)->_extern_name));
_append_p_s__string_buffer_p_a_ch(_stream,T1367);
_e_8=((struct _list_item *)(((struct _element *)_e_8)->_next_sibling));
}
_i_10=((unsigned int)0);
_size_11=(((struct _dictionary *)(&(_d->_strings)))->_allocated);
while((_i_10<_size_11))
{
if(((void*)((&((((struct _dictionary *)(&(_d->_strings)))->_associations)[_i_10]))->_key)!=(void*)0))
{
_a_12=(&((((struct _dictionary *)(&(_d->_strings)))->_associations)[_i_10]));
if((!(((struct _global_constant *)(_a_12->_value))->_expose)))
{
_append_p_s__string_buffer_p_a_ch(_stream,T1400);
}
_append_p_s__string_buffer_p_a_ch(_append_p_s__string_buffer_p_a_ch(_append_p_s__string_buffer_p_a_ch(_stream,T1416),(((struct _global_constant *)(_a_12->_value))->_name)),T1417);
_p_13=((char *)(_a_12->_key));
while(1)
{
_ch_14=((_p_13++)[0]);
if((_ch_14=='\x00'))
{
break;
}
_flush_char_p_s__string_buffer_ch(_stream,_ch_14);
}
_append_p_s__string_buffer_p_a_ch(_stream,T1425);
}
(++_i_10);
}
_i_15=((unsigned int)0);
_size_16=(((struct _dictionary *)(&(_d->_wstrings)))->_allocated);
while((_i_15<_size_16))
{
if(((void*)((&((((struct _dictionary *)(&(_d->_wstrings)))->_associations)[_i_15]))->_key)!=(void*)0))
{
_a_17=(&((((struct _dictionary *)(&(_d->_wstrings)))->_associations)[_i_15]));
if((!(((struct _global_constant *)(_a_17->_value))->_expose)))
{
_append_p_s__string_buffer_p_a_ch(_stream,T1400);
}
_append_p_s__string_buffer_p_a_ch(_append_p_s__string_buffer_p_a_ch(_append_p_s__string_buffer_p_a_ch(_stream,T1429),(((struct _global_constant *)(_a_17->_value))->_name)),T1430);
_p_18=((char *)(_a_17->_key));
while(1)
{
_ch_19=((_p_18++)[0]);
if((_ch_19=='\x00'))
{
break;
}
_flush_char_p_s__string_buffer_ch(_stream,_ch_19);
}
_append_p_s__string_buffer_p_a_ch(_stream,T1425);
}
(++_i_15);
}
_e_20=((&(_d->_initializers))->_first);
while(((void*)_e_20!=(void*)0))
{
_v_21=(_e_20->_value);
_c_22=((struct _global_constant *)(((struct _expression *)((struct _collection_expression *)_v_21))->_user_data));
if((!(_c_22->_expose)))
{
_append_p_s__string_buffer_p_a_ch(_stream,T1400);
}
_flush_declaration_p_s__string_buffer_p_s__type_p_a_ch(_stream,(((struct _pointer_type *)(((struct _expression *)((struct _collection_expression *)_v_21))->_type))->_target_type),(_c_22->_name));
_append_p_s__string_buffer_ch(_stream,'=');
_append_p_s__string_buffer_ch(_stream,'{');
_flush_initializer_p_s__c_dump_p_s__string_buffer_p_s__expression_p_s__type(_d,_stream,(((struct _collection_expression *)_v_21)->_value),(((struct _pointer_type *)(((struct _expression *)((struct _collection_expression *)_v_21))->_type))->_target_type));
_append_p_s__string_buffer_p_a_ch(_stream,T1514);
_e_20=((struct _list_item *)(((struct _element *)_e_20)->_next_sibling));
}
_append_p_s__string_buffer_p_a_ch(_stream,T1515);
_e_23=((&(_d->_variables))->_first);
while(((void*)_e_23!=(void*)0))
{
_v_24=(_e_23->_value);
if((((struct _variable *)_v_24)->_defined))
{
if((!(((struct _definition *)((struct _variable *)_v_24))->_expose)))
{
_append_p_s__string_buffer_p_a_ch(_stream,T1400);
}
_flush_declaration_p_s__string_buffer_p_s__type_p_a_ch(_stream,(((struct _variable *)_v_24)->_storage_type),(((struct _variable *)_v_24)->_extern_name));
if(((void*)(((struct _variable *)_v_24)->_initial_value)!=(void*)0))
{
_append_p_s__string_buffer_ch(_stream,'=');
_flush_expression_p_s__c_dump_p_s__string_buffer_p_s__expression(_d,_stream,(((struct _variable *)_v_24)->_initial_value));
}
_append_p_s__string_buffer_p_a_ch(_stream,T1367);
}
_e_23=((struct _list_item *)(((struct _element *)_e_23)->_next_sibling));
}
_append_p_s__string_buffer_p_a_ch(_stream,T1518);
_e_25=((&(_d->_functions))->_first);
while(((void*)_e_25!=(void*)0))
{
_v_26=(_e_25->_value);
(_d->_index)=1;
(_d->_current_function)=((struct _function *)_v_26);
if((((struct _function *)_v_26)->_defined))
{
if((!(((struct _definition *)((struct _function *)_v_26))->_expose)))
{
_append_p_s__string_buffer_p_a_ch(_stream,T1400);
}
_flush_function_declaration_p_s__string_buffer_p_s__function(_stream,((struct _function *)_v_26));
_append_p_s__string_buffer_p_a_ch(_stream,T304);
_append_p_s__string_buffer_p_a_ch(_stream,T1484);
_flush_body_p_s__c_dump_p_s__string_buffer_p_s__function(_d,_stream,((struct _function *)_v_26));
_append_p_s__string_buffer_p_a_ch(_stream,T1523);
}
_e_25=((struct _list_item *)(((struct _element *)_e_25)->_next_sibling));
}
}

static int _write_content_p_a_ch_p_a_ch_dw(char *_filename, char *_base, unsigned int _len)
{
struct _file *_f_1;
unsigned int _res_2;
_f_1=fopen(_filename,T1524);
if(((void*)_f_1==(void*)0))
{
return 1;
}
_res_2=fwrite(((void *)_base),_len,((unsigned int )1),_f_1);
fclose(_f_1);
if((_res_2==0))
{
return 2;
}
return 0;
}

static void _release_p_s__c_dump(struct _c_dump *_d)
{
_release_p_s__dictionary(((struct _dictionary *)(&(_d->_wstrings))));
_release_p_s__dictionary(((struct _dictionary *)(&(_d->_strings))));
_release_p_s__string_buffer((&(_d->_tmp)));
}

static void _release_p_s__memory_bundle(struct _memory_bundle *_bundle)
{
unsigned char *_base_1;
unsigned char *_next_2;
_base_1=(_bundle->_chunk_base);
while(((void*)_base_1!=(void*)0))
{
_next_2=(((struct _bundle_head *)(_base_1-((unsigned int)8)))->_next);
_free_memory_p_vo(((void *)(_base_1-((unsigned int)8))));
_base_1=_next_2;
}
}

static int _start_in_p_a_p_a_ch(int _argc, char **_argv)
{
struct _memory_bundle _t_1;
struct _memory_bundle *_bundle_2;
struct _list _t_3;
struct _list *_include_path_4;
struct _dictionary _defines_5;
char *_filename_6;
char *_out_filename_7;
int _version_8;
int _unicode_9;
int _invalid_define_10;
int _i_11;
char *_arg_12;
char *_equal_13;
char *_name_14;
char *_value_15;
struct _module *_module_16;
struct _compiler_session _session_17;
struct _c_dump _dump_18;
struct _string_buffer _t_19;
struct _string_buffer *_stream_20;
int _err_21;
struct _string_buffer _t_22;
struct _string_buffer *_stats_23;
_bundle_2=(_bundle_dw_p_s__memory_bundle(((unsigned int )65536),(&_t_1)),(&_t_1));
_initialize_lexer();
_initialize_operators();
_initialize_type_p_s__memory_bundle(_bundle_2);
_initialize_definition();
_initialize_c_dump();
_include_path_4=(_initialize_p_s__list_p_s__memory_bundle((&_t_3),_bundle_2),(&_t_3));
_initialize_p_s__dictionary_p_s__dictionary_class((&_defines_5),(&T169));
_filename_6=_empty_string171;
_out_filename_7=T173;
_version_8=0;
_unicode_9=0;
_invalid_define_10=0;
_i_11=1;
while((_i_11<_argc))
{
_arg_12=(_argv[_i_11]);
if(_is_equal_p_a_ch_p_a_ch(_arg_12,T180))
{
_debug1=1;
}
else if(_is_equal_p_a_ch_p_a_ch(_arg_12,T181))
{
_version_8=1;
}
else if(_is_equal_p_a_ch_p_a_ch(_arg_12,T182))
{
_unicode_9=1;
}
else if((_is_equal_p_a_ch_p_a_ch(_arg_12,T183)&&(_i_11<(_argc-1))))
{
_out_filename_7=_new_string_p_s__memory_bundle_p_a_ch(_bundle_2,(_argv[(++_i_11)]));
_to_OS_name_p_a_ch(_out_filename_7);
}
else if((_is_equal_p_a_ch_p_a_ch(_arg_12,T192)&&(_i_11<(_argc-1))))
{
_append_p_s__list_p_vo(_include_path_4,((void *)_new_string_p_s__memory_bundle_p_a_ch(_bundle_2,(_argv[(++_i_11)]))));
}
else if((_is_equal_p_a_ch_p_a_ch(_arg_12,T194)&&(_i_11<(_argc-1))))
{
_arg_12=(_argv[(++_i_11)]);
_equal_13=_first_occurrence_p_a_ch_ch(_arg_12,'=');
if(((void*)_equal_13==(void*)0))
{
_invalid_define_10=1;
}
else
{
_name_14=_new_string_p_s__memory_bundle_p_a_ch_dw(_bundle_2,_arg_12,((unsigned int )(_equal_13-_arg_12)));
_value_15=_new_string_p_s__memory_bundle_p_a_ch(_bundle_2,(_equal_13+1));
_add_p_s__dictionary_p_vo_p_vo((&_defines_5),((void *)_name_14),((void *)_value_15));
}
}
else
{
_filename_6=_new_string_p_s__memory_bundle_p_a_ch(_bundle_2,_arg_12);
}
(++_i_11);
}
_module_16=((struct _module *)0);
if(_invalid_define_10)
{
puts(T216);
}
else if(_version_8)
{
puts(T217);
}
else if(((_filename_6[0])=='\x00'))
{
puts(T218);
_print_usage();
}
else
{
_to_OS_name_p_a_ch(_filename_6);
_module_16=_start_p_s__compiler_session_p_s__memory_bundle_p_a_ch_p_s__list_p_s__dictionary_bo((&_session_17),_bundle_2,_filename_6,_include_path_4,(&_defines_5),_unicode_9);
if(((void*)_module_16!=(void*)0))
{
_initialize_p_s__c_dump_p_s__memory_bundle((&_dump_18),_bundle_2);
_stream_20=(_initialize_p_s__string_buffer((&_t_19)),(&_t_19));
_flush_module_p_s__c_dump_p_s__string_buffer_p_s__module((&_dump_18),_stream_20,_module_16);
_err_21=_write_content_p_a_ch_p_a_ch_dw(_out_filename_7,(_stream_20->_p),(_stream_20->_used));
if((_err_21!=0))
{
fputs_stderr(T1528);
}
_release_p_s__string_buffer(_stream_20);
_release_p_s__c_dump((&_dump_18));
}
}
if(_debug1)
{
_stats_23=(_initialize_p_s__string_buffer((&_t_22)),(&_t_22));
_append_p_s__string_buffer_p_a_ch(_stats_23,T1531);
_append_p_s__string_buffer_ch(_append_p_s__string_buffer_dw(_append_p_s__string_buffer_p_a_ch(_stats_23,T1532),(_bundle_2->_total)),'\n');
puts(_as_string_p_s__string_buffer(_stats_23));
_release_p_s__string_buffer(_stats_23);
}
_release_p_s__dictionary((&_defines_5));
_release_p_s__memory_bundle(_bundle_2);
if(((void*)_module_16!=(void*)0))
{
return 0;
}
else
{
return 1;
}
}

static void _release_allocator()
{
unsigned char *_block_1;
unsigned char *_next_2;
_block_1=_first_block2;
while(((void*)_block_1!=(void*)0))
{
_next_2=(((struct _mem_head *)(_block_1-((unsigned int)12)))->_next);
free(((void *)(_block_1-((unsigned int)12))));
_block_1=_next_2;
}
}

int main(int _argc, char **_argv)
{
int _result_1;
unsigned int _total_2;
unsigned int _max_3;
unsigned int _left_4;
struct _string_buffer _t_5;
struct _string_buffer *_stats_6;
_initialize_allocator();
_result_1=_start_in_p_a_p_a_ch(_argc,_argv);
if(_debug1)
{
_total_2=_total_allocated3;
_max_3=_max_allocated4;
_left_4=_current_allocated7;
_stats_6=(_initialize_p_s__string_buffer((&_t_5)),(&_t_5));
_append_p_s__string_buffer_p_a_ch(_stats_6,T1531);
_append_p_s__string_buffer_ch(_append_p_s__string_buffer_dw(_append_p_s__string_buffer_p_a_ch(_stats_6,T1541),_total_2),'\n');
_append_p_s__string_buffer_ch(_append_p_s__string_buffer_dw(_append_p_s__string_buffer_p_a_ch(_stats_6,T1542),_max_3),'\n');
_append_p_s__string_buffer_ch(_append_p_s__string_buffer_dw(_append_p_s__string_buffer_p_a_ch(_stats_6,T1543),_left_4),'\n');
puts(_as_string_p_s__string_buffer(_stats_6));
_release_p_s__string_buffer(_stats_6);
}
_release_allocator();
return _result_1;
}

