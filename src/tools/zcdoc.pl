#!/usr/bin/env perl

# This is a rudimentary documentation extractor for Zinc programs
use strict;
use warnings;

sub trim($)
{
	my $string = shift;
	$string =~ s/^\s+//;
	$string =~ s/\s+$//;
	return $string;
}

sub ltrim($)
{
	my $string = shift;
	$string =~ s/^\s+//;
	return $string;
}

sub rtrim($)
{
	my $string = shift;
	$string =~ s/\s+$//;
	return $string;
}

sub extract_from_file {
    my $fileName = shift;

    open (BUFFER, $fileName) or die ("cannot read from $fileName");

    my $recordNo = 0;
    my $lastRecordNo = 0;
    my $found = 0;
    my $toPrint = "";
    my $shouldAlwaysPrint = 0;
    my $lastType = "doc";
    my $foundType = "doc";

    while (my $record = <BUFFER>) {
	$found = 0;
	$toPrint = "";

	if ($shouldAlwaysPrint) {
	    $toPrint = "    " . $record;
	    if (trim($record) eq "end") {
		$shouldAlwaysPrint = 0;
	    }
	    $foundType = "code";
	} else {
	    if ($record =~ /^[\t ]*public[ \t\n]+/) {
		$toPrint = "    " . $record;
		if ($toPrint =~ /^[\t ]*public[\t ]+struct/) {
		    $shouldAlwaysPrint = 1;
		}
		if ($toPrint =~ /^[\t ]*public[\t ]+enum/) {
		    $shouldAlwaysPrint = 1;
		}
		$found = 1;
		$foundType = "code";
	    }
	    
	    if ($record =~ /^[\t ]*\/\/\/[\t ]*(.*)/) {
		$toPrint = "$1\n";
		$found = 1;
		$foundType = "doc";
	    }
	}

	$recordNo = $recordNo + 1;
	if ($found && ($recordNo!=($lastRecordNo+1) || $foundType ne $lastType)) {
	    print "\n";
	}
	print $toPrint;

	if ($found ) {
	    $lastRecordNo = $recordNo;
	    $lastType = $foundType;
	}
    }
    close (BUFFER);
}

foreach my $i (0 .. $#ARGV) {
    extract_from_file ($ARGV[$i]);
}
