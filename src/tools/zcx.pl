#!/usr/bin/env perl
use warnings;
use strict;
use Getopt::Long;

sub ExecuteAndLog {
    my $command = shift;

    print $command . "\n";
    if (system ($command)) {
	return 0;
    } else {
	return 1;
    }
}

sub ReportErrorAndQuit {
    my $message = shift;
    print "ERROR $message\n";
    exit;
}

# This script compiles a Zinc program to an exe file using
# the GCC compiler

my %args;
my $OutputName = 'a';
my @PathInclusione;
my $ShouldKeep = 0;

GetOptions (
    'output|o' => \$OutputName, 
    'include|I=s' => \@PathInclusione,
    'keep|k' => \$ShouldKeep,
    ) or die("Error processing options");

my $ZincCommandLine = "zc -o _tmp/temp.c ";
my $CCommandLine = "gcc -o $OutputName _tmp/temp.c ";
my $FoundMainScript = 0;

foreach my $includePath (@PathInclusione) {
    $ZincCommandLine = $ZincCommandLine . " -I " . $includePath . " ";
}

while (my $arg = shift) {
    if ($arg =~ ".*\\.zc") {
	if ($FoundMainScript) {
	    ReportErrorAndQuit ("you must specify only one zinc file");
	} else {
	    $ZincCommandLine = $ZincCommandLine . $arg;
	    $FoundMainScript = 1;
	}
    } else {
	$CCommandLine = $CCommandLine . " " . $arg;
    }
}

if (!$FoundMainScript) {
    ReportErrorAndQuit ("you must specify one zinc file to compile");
}

# Create the temporary directory
# and compile!
mkdir ("_tmp");
if (ExecuteAndLog ($ZincCommandLine)) {
    ExecuteAndLog ($CCommandLine);
    if (!$ShouldKeep) {
	unlink ("_tmp/temp.c");
    }
}

if (!$ShouldKeep) {
    rmdir ("_tmp");
}
