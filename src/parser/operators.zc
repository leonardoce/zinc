//[c]operators - standard operators
//[c]
//[c]Zinc - Compiler for the zinc programming language
//[c]Copyright (C) 2005 Marc Kerbiquet
//[c]
//[c]This program is free software; you can redistribute it and/or modify
//[c]it under the terms of the GNU General Public License as published by
//[c]the Free Software Foundation; either version 2 of the License, or
//[c](at your option) any later version.
//[c]
//[c]This program is distributed in the hope that it will be useful,
//[c]but WITHOUT ANY WARRANTY; without even the implied warranty of
//[c]MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//[c]GNU General Public License for more details.
//[c]
//[c]You should have received a copy of the GNU General Public License
//[c]along with this program; if not, write to the Free Software
//[c]Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//[c]
//[c]
import "base/types"
import "text/string"

//[of]:structure
public struct operator

	// operator name
	op: string

	// minimum of arguments
	min arg: int
	
	// maximum of arguments (0 for unlimited)
	max arg: int

end
//[cf]
//[of]:operators
//[c]
public def op add = "@add"
public def op sub = "@sub"
public def op mul = "@mul"
public def op div = "@div"
public def op mod = "@mod"
public def op shl = "@shl"
public def op shr = "@shr"

public def op eq = "@equal"
public def op ne = "@not equal"
public def op le = "@less or equal"
public def op lt = "@less than" 
public def op ge = "@greater or equal"
public def op gt = "@greater than"

public def op and = "@and"
public def op xor = "@xor"
public def op or = "@or"
public def op andand = "@andand"
public def op oror = "@oror"

public def op assign = "@assign"
public def op assign add = "@assign add"
public def op assign sub = "@assign sub"
public def op assign mul = "@assign mul"
public def op assign div = "@assign div"
public def op assign mod = "@assign mod"
public def op assign shl = "@assign shl"
public def op assign shr = "@assign shr"
public def op assign and = "@assign and"
public def op assign or = "@assign or"
public def op assign xor = "@assign xor"
public def op assign not = "@assign not"
public def op assign neg = "@assign neg"
//[c]
//[c]unary operators
//[c]
public def op not = "@not"
public def op pre inc = "@preinc"
public def op pre dec = "@predec"
public def op post inc = "@postinc"
public def op post dec = "@postdec"
//[c]
//[c]n-ary operator
//[c]
public def op at = "@at"
//[cf]

//[of]:private
//[of]:globals
//[c]
//[c]table for user defined opertors
//[c]
equ op size = 37
def operators: [op size] local operator
//[cf]
//[cf]
//[of]:module initialization
//[of]:initialize operators
//[c]
func add(p: [] local operator, op: string, min: int, max: int)

	p[].op = op
	p[].min arg = min
	p[].max arg = max
	return p+1

end
//[c]
public func initialize operators

	def p = operators
	
	p = add(p, op add        , 1, 2 )
	p = add(p, op sub        , 1, 2 )
	p = add(p, op mul        , 2, 2 )
	p = add(p, op div        , 2, 2 )
	p = add(p, op mod        , 2, 2 )
	p = add(p, op shl        , 2, 2 )
	p = add(p, op shr        , 2, 2 )
	
	p = add(p, op eq         , 2, 2 )
	p = add(p, op ne         , 2, 2 )
	p = add(p, op le         , 2, 2 )
	p = add(p, op lt         , 2, 2 )
	p = add(p, op ge         , 2, 2 )
	p = add(p, op gt         , 2, 2 )
	
	p = add(p, op and        , 2, 2 )
	p = add(p, op xor        , 2, 2 )
	p = add(p, op or         , 2, 2 )
	p = add(p, op andand     , 2, 2 )
	p = add(p, op oror       , 2, 2 )
	
	p = add(p, op assign add , 2, 2 )
	p = add(p, op assign sub , 2, 2 )
	p = add(p, op assign mul , 2, 2 )
	p = add(p, op assign div , 2, 2 )
	p = add(p, op assign mod , 2, 2 )
	p = add(p, op assign shl , 2, 2 )
	p = add(p, op assign shr , 2, 2 )
	p = add(p, op assign and , 2, 2 )
	p = add(p, op assign or  , 2, 2 )
	p = add(p, op assign xor , 2, 2 )
	p = add(p, op assign neg , 1, 1 )
	p = add(p, op assign not , 1, 1 )
	
	p = add(p, op not        , 1, 1 )
	p = add(p, op pre inc    , 1, 1 )
	p = add(p, op pre dec    , 1, 1 )
	p = add(p, op post inc   , 1, 1 )
	p = add(p, op post dec   , 1, 1 )
	
	p = add(p, op at         , 0, 0 )
	
	p = add(p, nil , 0, 0)

end
//[cf]
//[cf]
//[of]:accessing
//[of]:find operator (name)
//[c]Find operator from name
//[c]
public func find operator (name: string)

	def p = operators
	while not nil (op (p[]))
		if is equal (op (p[]), name) 
			return p[]
		end
		++p
	end
	
	return nil

end
//[cf]
//[cf]
