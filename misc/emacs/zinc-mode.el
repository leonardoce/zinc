(require 'generic-x)
(require 'cl)

;; TODO List
;; --
;;
;; * Remember to warn about missing newline at the end of the file when
;; the buffer is being saved.
;; * Correct the strange enter key behaviour

(define-generic-mode 
	'zinc-mode

  ; Comments
  '("//" ("/*" . "*/"))
  
  ; Keywords
  '("def" "equ" "func" "struct" "union" "enum" "typedef" "sizeof" "public" "private"
    "const" "import" "if" "elsif" "while" "switch" "case" "else" "end" "return" "repeat")

  ; Font lock list
  '(
    ("\\(\\$.\\)" 1 font-lock-string-face)
    )

  ; auto mode alist
  '("\\.zc$")

  ; function list
  '(zinc-set-indent-function zinc-set-electric-commands zinc-set-syntax-table)

  "Little mode for Zinc files"
)

(defvar zinc-indent-more-keywords
  '("if" "while" "repeat" "public" "private" "switch"
    "struct" "func" "else" "elsif")
  "If a line starts with one of this keywords the next line
  should be indented more")

(defvar zinc-indent-less-keywords
  '("end" "else" "elsif")
  "If a line starts with one of this keywords it should be indented less than the previous")

(defun zinc-set-indent-function ()
  "Set the indent function"
  (set (make-local-variable 'indent-line-function) 'zinc-indent-line))

(defun zinc-indent-line ()
  "Indent the current line in Zinc mode"
  (interactive)
  (let ((savep (> (current-column) (current-indentation)))
		(indent (condition-case err (max (zinc-calculate-indentation) 0)
			  (error (message "%S" err)))))
	(if savep 
		(save-excursion (indent-line-to indent))
	  (indent-line-to indent))))

(defvar zinc-mode-syntax-table nil
  "Syntax table in use in `zinc-mode' buffers.")

(setq zinc-mode-syntax-table (make-syntax-table))
(modify-syntax-entry ?$ "\\" zinc-mode-syntax-table)
(modify-syntax-entry ?/ "< 124" zinc-mode-syntax-table)
(modify-syntax-entry ?\n ">" zinc-mode-syntax-table)

(defun zinc-set-syntax-table ()
  (set-syntax-table zinc-mode-syntax-table))

(defun zinc-get-previous-line-indentation ()
  "Get the indentation of the previous non-blank line"  
  (save-excursion
    (beginning-of-line)
    (skip-chars-backward " \t\n\r")
    (list (thing-at-point 'line) (current-indentation))))

(defun zinc-how-should-next-line-be-indented (previous-line-contents current-line-contents)
  "Return the relative indentation of the current line based on the previous line"
  (save-excursion
    (beginning-of-line)
    (skip-chars-backward " \t\n\r")
    (flet ((check-this-keyword (keyword line-to-check)
			       (let ((exp (concat "^[ \t\n\r]*" keyword "[ \t\n\r]*")))
				 (string-match exp line-to-check)))
	   (check-keyword-list (keyword-list line-to-check) 
			       (if (not keyword-list) 0
				 (if (check-this-keyword (car keyword-list) line-to-check)
				     1
				   (check-keyword-list (cdr keyword-list) line-to-check)))))
      (let ((less-check (check-keyword-list zinc-indent-less-keywords current-line-contents)))
	(if (not (= less-check 0)) (- 0 less-check)
	  (check-keyword-list zinc-indent-more-keywords previous-line-contents))))))

(defun zinc-calculate-indentation ()
  "Return the number of column to which the current line should be indented."
  (save-excursion 
    (beginning-of-line)
    (if (bobp) 
	0 ; The first line should not be indented
      
      (let* ((previous-line-data (zinc-get-previous-line-indentation))
	     (previous-line-contents (nth 0 previous-line-data))
	     (previous-line-indentation (nth 1 previous-line-data))
	     (current-line-contents (save-excursion (beginning-of-line) (thing-at-point 'line)))
	     (relative-indentation (zinc-how-should-next-line-be-indented previous-line-contents current-line-contents)))
	  
	(+ previous-line-indentation (* tab-width relative-indentation))))))

;; -----------------
;; Electric commands
;; -----------------

(defun electric-zinc-terminate-line ()
  (interactive)
  (if (eolp) (zinc-indent-line))
  (newline))

(defun zinc-mode-map ()
  "Get the Zinc mode map"
  (let ((map (make-sparse-keymap)))
    (define-key map "\r"       'electric-zinc-terminate-line)
    map))

(defun zinc-set-electric-commands ()
  (use-local-map (zinc-mode-map)))

