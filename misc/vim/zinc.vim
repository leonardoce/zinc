" Vim syntax file:
" Language:	Zinc 1.2
" Maintainer: Leonardo Cecchi <leonardoce@interfree.it>
" Some things based on c.vim by Bram Moolenaar and pascal.vim by Mario Eusebio
" Last Change:	8/1/2011

" For version 5.x: Clear all syntax items
" For version 6.x: Quit when a syntax file was already loaded
if version < 600
  syntax clear
elseif exists("b:current_syntax")
  finish
endif

" Exceptions for my "Very Own" (TM) user variables naming style.
" If you don't like this, comment it
syn match  zincUserVariable	"\<[a,b,c,d,l,n,o,u,x][A-Z][A-Za-z0-9_]*\>"
syn match  zincUserVariable	"\<[a-z]\>"

" is case insensitive ( see "exception" above )
syn case ignore

" keywords ( in no particular order )
syn keyword zincFunction def
syn keyword zincStatement equ
syn keyword zincFunction func
syn keyword zincStorageClass struct
syn keyword zincStorageClass union
syn keyword zincStorageClass enum
syn keyword zincStorageClass typedef
syn keyword zincStatement sizeof
syn keyword zincStorageClass public
syn keyword zincStorageClass private
syn keyword zincStorageClass const
syn keyword zincInclude import
syn keyword zincConditional if
syn keyword zincConditional elsif
syn keyword zincRepeat while
syn keyword zincConditional switch
syn keyword zincConditional case
syn keyword zincConditional else
syn keyword zincStatement end
syn keyword zincStatement return
syn keyword zincRepeat repeat

" Operators
syn match   zincOperator	"$\|%\|&\|+\|-\|->\|!"
syn match   zincOperator	"\.AND\.\|\.NOT\.\|\.OR\."
syn match   zincOperator	":=\|<\|<=\|<>\|!=\|#\|=\|==\|>\|>=\|@"
syn match   zincOperator     "*"

" Numbers
syn match   zincNumber	"\<\d\+\(u\=l\=\|lu\|f\)\>"

" String and Character constants
syn region zincString	start=+"+ end=+"+
syn region zincString	start=+'+ end=+'+

" Delimiters
syn match  zincDelimiters	"[()]\|[\[\]]\|[{}]\|[||]"

" Special
syn match zincLineContinuation	"\\"

" This is from Bram Moolenaar:
if exists("c_comment_strings")
  " A comment can contain cString, cCharacter and cNumber.
  " But a "*/" inside a cString in a zincComment DOES end the comment!
  " So we need to use a special type of cString: zincCommentString, which
  " also ends on "*/", and sees a "*" at the start of the line as comment
  " again. Unfortunately this doesn't very well work for // type of comments :-(
  syntax match zincCommentSkip	contained "^\s*\*\($\|\s\+\)"
  syntax region zincCommentString	contained start=+"+ skip=+\\\\\|\\"+ end=+"+ end=+\*/+me=s-1 contains=zincCommentSkip
  syntax region zincComment2String	contained start=+"+ skip=+\\\\\|\\"+ end=+"+ end="$"
  syntax region zincComment		start="/\*" end="\*/" contains=zincCommentString,zincCharacter,zincNumber,zincString
  syntax match  zincComment		"//.*" contains=zincComment2String,zincCharacter,zincNumber
else
  syn region zincComment		start="/\*" end="\*/"
  syn match zincComment		"//.*"
endif
syntax match zincCommentError	"\*/"


" Define the default highlighting.
" For version 5.7 and earlier: only when not done already
" For version 5.8 and later: only when an item doesn't have highlighting yet
if version >= 508 || !exists("did_zinc_syntax_inits")
  if version < 508
    let did_zinc_syntax_inits = 1
    command -nargs=+ HiLink hi link <args>
  else
    command -nargs=+ HiLink hi def link <args>
  endif

  HiLink zincConditional		Conditional
  HiLink zincRepeat			Repeat
  HiLink zincNumber			Number
  HiLink zincInclude		Include
  HiLink zincComment		Comment
  HiLink zincOperator		Operator
  HiLink zincStorageClass		StorageClass
  HiLink zincStatement		Statement
  HiLink zincString			String
  HiLink zincFunction		Function
  HiLink zincLineContinuation	Special
  HiLink zincDelimiters		Delimiter
  HiLink zincUserVariable		Identifier

  delcommand HiLink
endif

let b:current_syntax = "zinc"

" vim: ts=4
